<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace api\controllers;

use Yii;
use yii\rest\Controller;
use yii\base\Exception;
use yii\base\UserException;
use yii\web\HttpException;

/**
 * Site rest controller
 *
 * @author Ali Irani <ali@irani.im>
 */
class SiteController extends Controller
{
    /**
     * Showing error in api app
     * @return array Error name and message
     */
    public function actionError()
    {
        if (($exception = Yii::$app->getErrorHandler()->exception) === null) {
            return '';
        }

        if ($exception instanceof HttpException) {
            $code = $exception->statusCode;
        } else {
            $code = $exception->getCode();
        }
        if ($exception instanceof Exception) {
            $name = $exception->getName();
        } else {
            $name = Yii::t('yii', 'Error');
        }
        if ($code) {
            $name .= " (#$code)";
        }

        if ($exception instanceof UserException) {
            $message = $exception->getMessage();
        } else {
            $message = Yii::t('yii', 'An internal server error occurred.');
        }

        return [
            'name' => $name,
            'error' => $message,
            'code' => $code,
            'status' => $exception->statusCode,
        ];
    }
}
