<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace api\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use yii\web\UnauthorizedHttpException;
use \yii\filters\Cors;
use api\models\Channel;
use api\models\Project;

/**
 * Channel rest controller
 *
 * @author Reza Ebrahimi <reza_2ebt@yahoo.com>
 */
class ChannelController extends ActiveController
{
    const LIMIT_CACHE_DURATION = 60;

    public $modelClass = 'api\models\channel';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['update'], $actions['delete'], $actions['view'], $actions['create']);
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'user' => new Project(),
        ];
        $behaviors['pageCache'] = [
            'class' => 'yii\filters\PageCache',
            'duration' =>  self::LIMIT_CACHE_DURATION,
            'variations' => [
                Yii::$app->request->get('page'),
            ]
        ];
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET'],
            ],
        ];
        return $behaviors;
    }

    /**
     * @return ActiveDataProvider
     */
    public function prepareDataProvider()
    {
        $query = Channel::find()->project(static::findProjectModel()->id)->active();
        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }

    /**
     * Find Channels with access-token
     * @return null|static
     * @throws UnauthorizedHttpException
     */
    public static function findProjectModel()
    {
        $token = Yii::$app->request->getQueryParam('access-token');
        $result = new Project();
        if (($channel = $result->loginByAccessToken($token)) === null) {
            throw new UnauthorizedHttpException;
        }
        return $channel;
    }
}
