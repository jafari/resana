<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace api\controllers;

use Yii;
use api\models\Content;
use api\models\Project;
use yii\filters\auth\QueryParamAuth;
use yii\filters\PageCache;
use yii\helpers\Url;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\web\ServerErrorHttpException;
use yii\web\UnauthorizedHttpException;
use yii\filters\Cors;

/**
 * Content rest controller
 *
 * @author Majid Salimi <salimionnet@gmail.com>
 * @author Morteza Pouretemadi <e.morteza94@yahoo.com>
 */
class ContentController extends ActiveController
{
    const CACHE_DURATION = 60;

    /**
     * @inheritdoc
     */
    public $modelClass = 'api\models\content';
    
    /**
     * @inheritdoc
     */
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    /**
     * @inheritdoc
     */
    public $createScenario = Content::SCENARIO_CREATE;

    /**
     * @inheritdoc
     */
    public $updateScenario = Content::SCENARIO_UPDATE;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'user' => new Project(),
        ];
        $behaviors['PageCache'] = [
            'class' => PageCache::className(),
            'only' => ['index'],
            'duration' => self::CACHE_DURATION,
            'variations' => [
                Yii::$app->request->getQueryParam('sort'),
                Yii::$app->request->getQueryParam('page'),
            ],
        ];
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST'],
            ],
        ];
        return $behaviors;
    }
    
    /**
     * @inheritdoc
     * @return Content
     * @throws UnauthorizedHttpException
     */
    public function findModel($id)
    {
        /**
         * @var Project $project
         * @var Content $model
         */
        $project = $this->findProjectModel();
        if (($model = Content::findOne(['id' => $id, 'project_id' => $project->id])) !== null) {
            return $model;
        } else {
            throw new UnauthorizedHttpException;
        }
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();

        unset($actions['update'], $actions['delete'], $actions['create']);
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        $actions['view']['findModel'] = [$this, 'findModel'];
        
        return $actions;
    }

    /**
     * Add new content
    * @return Content
    * @throws ServerErrorHttpException
    * @throws UnauthorizedHttpException
    * @throws \yii\base\InvalidConfigException
    */
    public function actionCreate()
    {
        /**
         * @var $model Content
         * @var $project Project
         */
        $project = $this->findProjectModel();
        $attributes = Yii::$app->getRequest()->getBodyParams();

        $model = new Content([
            'scenario' => Content::SCENARIO_CREATE,
        ]);
        $model->project_id = $project->id;
        $model->created_by = $project->projectOwner->user_id;
        $model->setAttributes([
            'text' => isset($attributes['text']) ? $attributes['text'] : null,
            'published_at' => isset($attributes['publish_time']) ? $attributes['publish_time'] : null,
            'channelsList' => isset($attributes['channels']) ? explode(', ', $attributes['channels']) : [],
            'groupsList' => isset($attributes['groups']) ? explode(',', $attributes['groups']) : null,
            'files' => isset($_FILES['attachments']) ? $_FILES['attachments'] : null,
        ]);
        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::toRoute(['content/view', 'id' => $id], true));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }
        return $model;
    }

    /**
     * @return ActiveDataProvider
     */
    public function prepareDataProvider()
    {
        $projectId = $this->findProjectModel()->id;
        $query = Content::find()->typePublic()->project($projectId);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'published_at',
                ],
                'defaultOrder' => [
                    'published_at' => SORT_DESC,
                ],
            ],
        ]);
        return $dataProvider;
    }
    
    /**
     * @return null|static
     * @throws UnauthorizedHttpException
     */
    public function findProjectModel()
    {
        $accessToken = Yii::$app->request->getQueryParam('access-token');
        $model = new Project();
        if (($project = $model->loginByAccessToken($accessToken)) !== null) {
            return $project;
        } else {
            throw new UnauthorizedHttpException;
        }
    }
}
