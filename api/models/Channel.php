<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace api\models;

/**
 * Channel fields for channel api
 * @author Reza Ebrahimi <reza_2ebt@yahoo.com>
 */
class Channel extends \common\models\Channel
{
    public function fields()
    {
        return [
            'id' => 'id',
            'type' => 'typeName',
            'type_id' => 'type',
            'name',
            'groups'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::className(), ['channel_id' => 'id']);
    }
}
