<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace api\models;

/**
 * Group model in api app
 * @author Majid Salimi <salimionnet@gmail.com>
 */
class Group extends \common\models\Group
{
    /**
     * @inheritdoc
     */
    public function fields()
    {
        return  [
            'id',
            'name',
            'channel_id',
        ];
    }
}
