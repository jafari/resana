<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace api\models;

use Yii;
use yii\web\Link;
use yii\web\Linkable;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * Class Content
 * @author Morteza Pouretemadi <e.morteza94@yahoo.com>
 */
class Content extends \common\models\Content implements Linkable
{
    /**
     * @property Content $model
     * @return array
     */
    public function fields()
    {
        return [
            'id',
            'text',
            'attachments' => 'files',
            'status' => 'statusName',
            'status_id' => 'status',
            'create_time' => 'created_at',
            'publish_time' => 'published_at',
            'cnt_sent',
            'cnt_failed',
            'channels',
        ];
    }

    /**
     * @return array
     */
    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['content/view', 'id' => $this->id], true),
        ];
    }

    /**
     * @inheritdoc
    */
    public function getChannels()
    {
        return $this->hasMany(Channel::className(), ['id' => 'channel_id'])->viaTable('{{%content_channel}}', ['content_id' => 'id']);
    }
    
    /**
     * Save files
     */
    public function setFiles()
    {
        if ($files = UploadedFile::getInstancesByName('attachments')) {
            foreach ($files as $file) {
                $media = Yii::$app->media->save($file);
                if (isset($media['success']) && $media['success']) {
                    $mediaInfo = $media['data']['media'][0];
                    $this->media_type = static::getMediaTypeId($file->type);
                    $attachments[] = [
                        'url' => $mediaInfo['uriMin'],
                        'name' => $mediaInfo['original_file_name'],
                        'media_type' => $this->media_type,
                    ];
                } elseif (isset($media['message'])) {
                    Yii::error($media['message'], __METHOD__);
                }
                $file->saveAs($this->uploadPath . $file->name);
            }

            if (isset($attachments)) {
                if ($this->attachments) {
                    $attachments = array_merge(json_decode($this->attachments), $attachments);
                }

                $this->attachments = json_encode($attachments);
            }
        }
    }
}
