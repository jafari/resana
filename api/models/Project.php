<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace api\models;

/**
 * Project model in aoi aoo
 *
 * @author Morteza Pouretemadi <e.morteza94@yahoo.com>
 */
class Project extends \common\models\Project
{

    /**
     * returns the project with the given token
     * @param $token
     * @param null $type
     * @return null|static
     */
    public function loginByAccessToken($token, $type = null)
    {
        if ($token === '') {
            return null;
        }
        return static::findOne(['auth_key' => $token]);
    }
}
