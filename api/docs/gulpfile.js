var gulp = require('gulp');
var raml2html = require('gulp-raml2html');
var reloader = require('gulp-reloader');

gulp.task('apidoc', function() {
  return gulp.src('index.raml')
    .pipe(raml2html())
    .pipe(gulp.dest(''));
});

gulp.task('reload', function(){
  reloader.listen()
  return gulp.src('*.html')
    .pipe(reloader('http://localhost/jarchi/api/docs/index.html'));
});

var watcher = gulp.watch('*.raml', ['apidoc', 'reload']);
watcher.on('change', function(event) {
  console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
});