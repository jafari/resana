# Resana #
[![build status](http://gitlab.noghteh.net/ci/projects/3/status.png?ref=develop)](http://gitlab.noghteh.net/ci/projects/3?ref=develop)

## Installation ##
For using `./yii content/worker` as a service on ubuntu 15.04 or upper with systemd you should copy files in ` system ` directory in project root to ` /etc/systemd/system ` and then enable with below lines for first time and then start them: 
```
sudo systemctl enable resana-worker.service
sudo systemctl enable resana-worker-delayed.service
sudo systemctl start resana-worker.service
sudo systemctl start resana-worker-delayed.service
```

and you should after any deployed project `restart` these services