<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace console\controllers;

use Yii;
use common\models\User;
use common\rbac\rules\ProjectAdminRule;
use common\rbac\rules\ProjectOwnerRule;

/**
 * Class RbacController
 * @package console\controllers
 * @author Ali Irani <ali@irani.im>
 */
class RbacController extends \yii\console\Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAllAssignments();
        $auth->removeAllRoles();
        $auth->removeAllPermissions();
        $auth->removeAllRules();

        $projectAdminRule = new ProjectAdminRule;
        $auth->add($projectAdminRule);

        $projectOwnerRule = new ProjectOwnerRule;
        $auth->add($projectOwnerRule);

        $readProject = $auth->createPermission('readProject');
        $auth->add($readProject);

        $updateProject = $auth->createPermission('updateProject');
        $auth->add($updateProject);

        $updateOwnProject = $auth->createPermission('updateOwnProject');
        $updateOwnProject->ruleName = $projectOwnerRule->name;
        $auth->add($updateOwnProject);

        $readMemberProject = $auth->createPermission('readMemberProject');
        $readMemberProject->ruleName = $projectAdminRule->name;
        $auth->add($readMemberProject);

        $superAdmin = $auth->createRole('superAdmin');
        $auth->add($superAdmin);

        $member = $auth->createRole('member');
        $auth->add($member);

        $auth->addChild($superAdmin, $member);
        $auth->addChild($superAdmin, $updateProject);
        $auth->addChild($superAdmin, $readProject);
        $auth->addChild($member, $updateOwnProject);
        $auth->addChild($member, $readMemberProject);
        $auth->addChild($readMemberProject, $readProject);
        $auth->addChild($updateOwnProject, $updateProject);

        $auth->assign($superAdmin, 1);

        $users = User::find()->all();
        foreach ($users as $user) {
            $auth->assign($member, $user->id);
        }
        exit();
    }
}