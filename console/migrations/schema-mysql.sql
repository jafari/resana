/**
 * Database schema of resana
 *
 * @author Sam <sam@noghteh.ir>
 * @link http://www.resana.im/
 * @copyright 2015 Noghteh
 * @license http://www.resana.im/license/
 * @date 1394-01-24
 */

-- DROP DATABASE IF EXISTS `db_resana`;

CREATE DATABASE IF NOT EXISTS `db_resana` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

USE `db_resana`;

/*********************** the order of drop is important **********************/

DROP TABLE IF EXISTS `tbl_message`;
DROP TABLE IF EXISTS `tbl_content_channel`;
DROP TABLE IF EXISTS `tbl_content_group`;
DROP TABLE IF EXISTS `tbl_content_member`;
DROP TABLE IF EXISTS `tbl_content`;
DROP TABLE IF EXISTS `tbl_channel_member`;
DROP TABLE IF EXISTS `tbl_group_member`;
DROP TABLE IF EXISTS `tbl_group`;
DROP TABLE IF EXISTS `tbl_mobile`;
DROP TABLE IF EXISTS `tbl_email`;
DROP TABLE IF EXISTS `tbl_chat`;
DROP TABLE IF EXISTS `tbl_member`;
DROP TABLE IF EXISTS `tbl_command`;
DROP TABLE IF EXISTS `tbl_mail`;
DROP TABLE IF EXISTS `tbl_telegram_channel`;
DROP TABLE IF EXISTS `tbl_bot`;
DROP TABLE IF EXISTS `tbl_twitter`;
DROP TABLE IF EXISTS `tbl_channel`;
DROP TABLE IF EXISTS `tbl_project_user`;
DROP TABLE IF EXISTS `tbl_project`;
DROP TABLE IF EXISTS `tbl_user`;

/*****************************************************************************/
/*
SET FOREIGN_KEY_CHECKS = 0

TRUNCATE `tbl_message`;
TRUNCATE `tbl_content_channel`;
TRUNCATE `tbl_content_group`;
TRUNCATE `tbl_content_member`;
TRUNCATE `tbl_content`;
TRUNCATE `tbl_channel_member`;
TRUNCATE `tbl_group_member`;
TRUNCATE `tbl_group`;
TRUNCATE `tbl_mobile`;
TRUNCATE `tbl_email`;
TRUNCATE `tbl_chat`;
TRUNCATE `tbl_member`;
TRUNCATE `tbl_command`;
TRUNCATE `tbl_mail`;
TRUNCATE `tbl_telegram_channel`;
TRUNCATE `tbl_bot`;
TRUNCATE `tbl_twitter`;
TRUNCATE `tbl_channel`;
TRUNCATE `tbl_project_user`;
TRUNCATE `tbl_project`;
TRUNCATE `tbl_user`;

SET FOREIGN_KEY_CHECKS = 1
*/
/*****************************************************************************/

CREATE TABLE IF NOT EXISTS `tbl_user`(
  `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sid` BIGINT(13) DEFAULT NULL COMMENT 'Shenasa ID',
  `username` VARCHAR(32) DEFAULT NULL,
  `auth_key` VARCHAR(32) NOT NULL DEFAULT '' COMMENT 'Yii2',
  `password_hash` VARCHAR(128) NOT NULL DEFAULT '' COMMENT 'Yii2',
  `password_reset_token` VARCHAR(128) NOT NULL DEFAULT '' COMMENT 'Yii2',
  `type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1 : local user , 2 : shenasa user , 3 : app , 4 : device',
  `status` TINYINT(4) NOT NULL DEFAULT 0,
  `role` TINYINT(4) NOT NULL DEFAULT 0,
  `created_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'create_time',
  `updated_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'update_time',
  `email` VARCHAR(64) NOT NULL,
  `mobile` VARCHAR(16) DEFAULT NULL,
  `name` VARCHAR(128) NOT NULL DEFAULT '',
  `first_name` VARCHAR(128) NOT NULL DEFAULT '',
  `last_name` VARCHAR(128) NOT NULL DEFAULT '',
  `text` VARCHAR(512) NOT NULL DEFAULT '',
  -- `gender` BIT(1) DEFAULT NULL COMMENT 'null : other , 0 : female , 1 : male',
  `gender` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '0 : unknown , 1 : male , 2 : female , 3 : legal person',
  -- `verified` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '0 : unknown , 1 : verified',
  -- `city` VARCHAR(128) NOT NULL DEFAULT '',
  -- `city` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  -- `birthday` DATE DEFAULT NULL,
  -- `image` VARCHAR(128) NOT NULL DEFAULT '' COMMENT 'url of image - avatar',
  `config` VARCHAR(1024) NOT NULL DEFAULT '' COMMENT '(settings) in JSON format',
  -- `website` VARCHAR(128) NOT NULL DEFAULT '' COMMENT 'url',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `sid` (`sid`),
  UNIQUE INDEX `username` (`username`),
  UNIQUE INDEX `email` (`email`),
  UNIQUE INDEX `mobile` (`mobile`)
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `tbl_project` (
  `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) NOT NULL COMMENT 'title',
  `description` VARCHAR(1024) NOT NULL DEFAULT '' COMMENT '',
  `status` TINYINT(4) NOT NULL DEFAULT 0,
  `created_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'create_time',
  `updated_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'update_time',
  `auth_key` VARCHAR(128) NOT NULL DEFAULT '' COMMENT 'API Key',
  -- `cnt_member` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  -- `cnt_content` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  -- `cnt_message` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_unicode_ci COMMENT '';

CREATE TABLE IF NOT EXISTS `tbl_project_user` (
  -- `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  -- `type` TINYINT(4) NOT NULL DEFAULT 0,
  `status` TINYINT(4) NOT NULL DEFAULT 0,
  `created_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'create_time',
  `updated_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'update_time',
  `role` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1 : owner , 2 : administrator , 3 : moderator , 4 : operator',
  `project_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  `user_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  -- PRIMARY KEY (id),
  PRIMARY KEY (`project_id`, `user_id`),
  FOREIGN KEY (`project_id`) REFERENCES `tbl_project` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`) ON DELETE CASCADE
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_unicode_ci COMMENT 'project_permissions | project_accounts';

/***********************************************/

CREATE TABLE IF NOT EXISTS `tbl_channel`(
  `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  -- `name` VARCHAR(128) NOT NULL DEFAULT '' COMMENT 'title',
  -- `type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1 : public , 2 : private',
  `type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1 : telegram , 2 : email , 3 : sms , 4 : gcm , 5 : twitter , 6 : facebook , 7 : afsaran',
  `status` TINYINT(4) NOT NULL DEFAULT 0,
  `created_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'create_time',
  `updated_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'update_time',
  `config` TEXT() NOT NULL DEFAULT '' COMMENT '(settings) in JSON format',
  -- `cnt_member` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  -- `cnt_content` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  -- `cnt_message` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  `project_id` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT '',
  PRIMARY KEY (`id`),
  FOREIGN KEY (`project_id`) REFERENCES `tbl_project` (`id`) ON DELETE CASCADE
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `tbl_twitter`(
  `name` VARCHAR(128) NOT NULL DEFAULT '',
  `username` VARCHAR(128) NOT NULL DEFAULT '',
  `user_id` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Twitter User ID',
  `token` VARCHAR(128) NOT NULL DEFAULT '' COMMENT 'OAuth Token',
  `token_secret` VARCHAR(128) NOT NULL DEFAULT '' COMMENT 'OAuth Token Secret',
  `channel_id` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT '',
  -- `project_id` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT '',
  PRIMARY KEY (`channel_id`),
  FOREIGN KEY (`channel_id`) REFERENCES `tbl_channel` (`id`) ON DELETE CASCADE
  -- FOREIGN KEY (`project_id`) REFERENCES `tbl_project` (`id`) ON DELETE CASCADE
  -- UNIQUE INDEX `username` (`username`)
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_unicode_ci COMMENT 'channel : twitter account';

CREATE TABLE IF NOT EXISTS `tbl_bot`(
  `bot_id` INT(10) NOT NULL COMMENT 'telegram bot user id',
  `name` VARCHAR(256) NOT NULL DEFAULT '',
  `username` VARCHAR(128) NOT NULL DEFAULT '',
  `token` VARCHAR(128) NOT NULL DEFAULT '',
  -- `owner` INT(10) DEFAULT NULL COMMENT 'user id of telegram user that created this bot',
  `owner` VARCHAR(512) NOT NULL DEFAULT '' COMMENT 'username of telegram user that created this bot or other admins',
  `channel_id` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT '',
  -- `project_id` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT '',
  PRIMARY KEY (`channel_id`),
  FOREIGN KEY (`channel_id`) REFERENCES `tbl_channel` (`id`) ON DELETE CASCADE,
  -- FOREIGN KEY (`project_id`) REFERENCES `tbl_project` (`id`) ON DELETE CASCADE,
  UNIQUE INDEX `bot_id` (`bot_id`),
  UNIQUE INDEX `username` (`username`)
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_unicode_ci COMMENT 'channel : telegram robot';

CREATE TABLE IF NOT EXISTS `tbl_telegram_channel`(
  -- `telegram_channel_id` INT(10) NOT NULL COMMENT 'telegram channel id',
  `name` VARCHAR(256) NOT NULL DEFAULT '',
  `username` VARCHAR(128) NOT NULL DEFAULT '',
  `channel_id` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT '',
  `bot_channel_id` MEDIUMINT(8) UNSIGNED DEFAULT NULL COMMENT 'telegram bot that is an admin of this telegram channel',
  -- `project_id` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT '',
  PRIMARY KEY (`channel_id`),
  FOREIGN KEY (`channel_id`) REFERENCES `tbl_channel` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`bot_channel_id`) REFERENCES `tbl_bot` (`channel_id`),
  -- FOREIGN KEY (`project_id`) REFERENCES `tbl_project` (`id`) ON DELETE CASCADE,
  -- UNIQUE INDEX `telegram_channel_id` (`telegram_channel_id`),
  UNIQUE INDEX `username` (`username`)
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_unicode_ci COMMENT 'channel : telegram channel';

CREATE TABLE IF NOT EXISTS `tbl_mail`(
  `name` VARCHAR(128) NOT NULL DEFAULT '' COMMENT 'from name',
  `address` VARCHAR(128) NOT NULL COMMENT 'from email address',
  `host` VARCHAR(64) NOT NULL DEFAULT '',
  `port` VARCHAR(8) NOT NULL DEFAULT '',
  `username` VARCHAR(64) NOT NULL DEFAULT '',
  `password` VARCHAR(64) NOT NULL DEFAULT '',
  `channel_id` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT '',
  -- `project_id` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT '',
  PRIMARY KEY (`channel_id`),
  FOREIGN KEY (`channel_id`) REFERENCES `tbl_channel` (`id`) ON DELETE CASCADE
  -- FOREIGN KEY (`project_id`) REFERENCES `tbl_project` (`id`) ON DELETE CASCADE
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_unicode_ci COMMENT 'channel : email';

/***********************************************/

CREATE TABLE IF NOT EXISTS `tbl_command`(
  `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) NOT NULL,
  `action` VARCHAR(128) NOT NULL DEFAULT '',
  `response` VARCHAR(2048) NOT NULL DEFAULT '' COMMENT 'description | text',
  `type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1 : static , 2 : menu , 3 : action',
  `status` TINYINT(4) NOT NULL DEFAULT 0,
  `created_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'create_time',
  `updated_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'update_time',
  `cnt_message` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'usage count',
  `parent_id` MEDIUMINT(8) UNSIGNED DEFAULT NULL COMMENT '',
  `channel_id` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT '',
  PRIMARY KEY (`id`),
  FOREIGN KEY (`parent_id`) REFERENCES `tbl_command` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`channel_id`) REFERENCES `tbl_channel` (`id`) ON DELETE CASCADE
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8 DEFAULT COLLATE = utf8_general_ci;

/***********************************************/

CREATE TABLE IF NOT EXISTS `tbl_member`(
  `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1 : telegram , 2 : email , 3 : sms , 4 : gcm',
  `status` TINYINT(4) NOT NULL DEFAULT 0,
  `created_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'create_time',
  `updated_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'update_time',
  -- `name` VARCHAR(128) NOT NULL DEFAULT '',
  -- `cnt_content` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  -- `cnt_message` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_unicode_ci COMMENT = 'recipient | audience | end-user';

CREATE TABLE IF NOT EXISTS `tbl_chat`(
  `chat_id` BIGINT(14) NOT NULL COMMENT 'telegram chat_id : user_id or group_chat_id',
  `name` VARCHAR(128) NOT NULL DEFAULT '' COMMENT 'first_name + last_name | title',
  `username` VARCHAR(128) NOT NULL DEFAULT '',
  `type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1 : user , 2 : group , 3 : bot',
  `member_id` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT '',
  PRIMARY KEY (`member_id`),
  FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE,
  UNIQUE INDEX `chat_id` (`chat_id`)
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_unicode_ci COMMENT = 'telegram users and groups';

CREATE TABLE IF NOT EXISTS `tbl_email`(
  `email` VARCHAR(64) NOT NULL COMMENT 'email address',
  `member_id` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT '',
  PRIMARY KEY (`member_id`),
  FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE,
  UNIQUE INDEX `email` (`email`)
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_unicode_ci COMMENT = '';

CREATE TABLE IF NOT EXISTS `tbl_mobile`(
  `mobile` VARCHAR(16) NOT NULL COMMENT 'phone number',
  `member_id` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT '',
  PRIMARY KEY (`member_id`),
  FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE,
  UNIQUE INDEX `mobile` (`mobile`)
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_unicode_ci COMMENT = 'tbl_phone';

/***********************************************/

CREATE TABLE IF NOT EXISTS `tbl_group`(
  `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) NOT NULL DEFAULT '' COMMENT 'title',
  `type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1 : public , 2 : private',
  `status` TINYINT(4) NOT NULL DEFAULT 0,
  `created_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'create_time',
  `updated_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'update_time',
  -- `cnt_member` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  -- `cnt_content` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  -- `project_id` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT '',
  `channel_id` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT '',
  `command_id` MEDIUMINT(8) UNSIGNED DEFAULT NULL COMMENT '',
  PRIMARY KEY (`id`),
  -- FOREIGN KEY (`project_id`) REFERENCES `tbl_project` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`channel_id`) REFERENCES `tbl_channel` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`command_id`) REFERENCES `tbl_command` (`id`) ON DELETE SET NULL
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_unicode_ci COMMENT 'tbl_list';

CREATE TABLE IF NOT EXISTS `tbl_group_member` (
  `status` TINYINT(4) NOT NULL DEFAULT 0,
  `created_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'create_time',
  `updated_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'update_time',
  -- `cnt_content` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  `group_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  `member_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  PRIMARY KEY (`group_id`, `member_id`),
  FOREIGN KEY (`group_id`) REFERENCES `tbl_group` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_unicode_ci COMMENT '';

CREATE TABLE IF NOT EXISTS `tbl_channel_member` (
  `status` TINYINT(4) NOT NULL DEFAULT 0,
  `created_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'create_time',
  `updated_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'update_time',
  -- `cnt_content` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  -- `cnt_message` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  `channel_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  `member_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  PRIMARY KEY (`channel_id`, `member_id`),
  FOREIGN KEY (`channel_id`) REFERENCES `tbl_channel` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_unicode_ci COMMENT '';

/***********************************************/

CREATE TABLE IF NOT EXISTS `tbl_content`(
  `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `text` TEXT COMMENT 'text or caption',
  `attachments` VARCHAR(2048) NOT NULL DEFAULT '' COMMENT 'attachments properties in JSON',
  `media_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1 : text , 2 : photo , 3 : video , 4 : audio , 5 : document , 6 : sticker , 7 : location , 8 : forward',
  `type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1 : public , 2 : private(direct)',
  `status` TINYINT(4) NOT NULL DEFAULT 0,
  `created_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'create_time',
  `updated_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'update_time',
  `published_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'publish_time',
  -- `recipients` VARCHAR(1024) NOT NULL DEFAULT '' COMMENT '?!',
  -- `channels` VARCHAR(1024) NOT NULL DEFAULT '' COMMENT '?!',
  -- `groups` VARCHAR(1024) NOT NULL DEFAULT '' COMMENT '?!',
  `cnt_sent` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  `cnt_failed` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  `created_by` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT 'user_id',
  `project_id` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT '',
  -- `channel_id` MEDIUMINT(8) UNSIGNED DEFAULT NULL COMMENT '',
  -- `group_id` MEDIUMINT(8) UNSIGNED DEFAULT NULL COMMENT '',
  -- `member_id` MEDIUMINT(8) UNSIGNED DEFAULT NULL COMMENT '',
  `f_chat_id` BIGINT(14) NOT NULL DEFAULT 0 COMMENT 'telegram chat id (in forward)',
  `f_message_id` INT(10) NOT NULL DEFAULT 0 COMMENT 'telegram message id (in forward)',
  `message_id` MEDIUMINT(8) UNSIGNED DEFAULT NULL COMMENT 'replied to (for private type)',
  PRIMARY KEY (`id`),
  FOREIGN KEY (`created_by`) REFERENCES `tbl_user` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`project_id`) REFERENCES `tbl_project` (`id`) ON DELETE CASCADE,
  -- FOREIGN KEY (`channel_id`) REFERENCES `tbl_channel` (`id`),
  -- FOREIGN KEY (`group_id`) REFERENCES `tbl_group` (`id`),
  -- FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`),
  FOREIGN KEY (`message_id`) REFERENCES `tbl_message` (`id`) ON DELETE SET NULL
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_general_ci COMMENT 'message that sent to several users and/or groups';

CREATE TABLE IF NOT EXISTS `tbl_content_member` (
  `status` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1 : pending , 2 : sent , 3 : delivered , 4 : failed',
  `sent_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'send_time',
  `content_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  `member_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  -- `channel_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  -- `group_id` MEDIUMINT(8) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`content_id`, `member_id`),
  FOREIGN KEY (`content_id`) REFERENCES `tbl_content` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE
  -- FOREIGN KEY (`channel_id`) REFERENCES `tbl_channel` (`id`) ON DELETE CASCADE
  -- FOREIGN KEY (`group_id`) REFERENCES `tbl_group` (`id`) ON DELETE SET NULL
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_unicode_ci COMMENT 'recipients : members';

CREATE TABLE IF NOT EXISTS `tbl_content_group` (
  `status` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1 : pending , 2 : sent , 3 : delivered , 4 : failed',
  `sent_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'send_time',
  `cnt_sent` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  `cnt_failed` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  `content_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  `group_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  PRIMARY KEY (`content_id`, `group_id`),
  FOREIGN KEY (`content_id`) REFERENCES `tbl_content` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`group_id`) REFERENCES `tbl_group` (`id`) ON DELETE CASCADE
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_unicode_ci COMMENT 'recipients : groups';

CREATE TABLE IF NOT EXISTS `tbl_content_channel` (
  `status` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1 : pending , 2 : sent , 3 : delivered , 4 : failed',
  `sent_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'send_time',
  `cnt_sent` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  `cnt_failed` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  `content_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  `channel_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  PRIMARY KEY (`content_id`, `channel_id`),
  FOREIGN KEY (`content_id`) REFERENCES `tbl_content` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`channel_id`) REFERENCES `tbl_channel` (`id`) ON DELETE CASCADE
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_unicode_ci COMMENT 'recipients : channels';

/***********************************************/

CREATE TABLE IF NOT EXISTS `tbl_message`(
  `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `text` VARCHAR(2048) NOT NULL DEFAULT '',
  `attachments` VARCHAR(512) NOT NULL DEFAULT '' COMMENT 'attachments properties in JSON',
  `media_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1 : text , 2 : photo , 3 : video , 4 : audio , 5 : document , 6 : sticker , 7 : location',
  `type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '1 : direct , 2 : command , 3 : reply , 4 : mention',
  `status` TINYINT(4) NOT NULL DEFAULT 0,
  `created_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'create_time',
  `updated_at` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'update_time',
  -- `project_id` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT '',
  `channel_id` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT '',
  `member_id` MEDIUMINT(8) UNSIGNED DEFAULT NULL COMMENT '',
  `command_id` MEDIUMINT(8) UNSIGNED DEFAULT NULL COMMENT '',
  `content_id` MEDIUMINT(8) UNSIGNED DEFAULT NULL COMMENT 'replied to (for reply type)',
  PRIMARY KEY (`id`),
  -- FOREIGN KEY (`project_id`) REFERENCES `tbl_project` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`channel_id`) REFERENCES `tbl_channel` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`) ON DELETE SET NULL,
  FOREIGN KEY (`command_id`) REFERENCES `tbl_command` (`id`) ON DELETE SET NULL,
  FOREIGN KEY (`content_id`) REFERENCES `tbl_content` (`id`) ON DELETE SET NULL
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_general_ci COMMENT = 'received message from members';