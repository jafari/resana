<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Ali Irani <ali@irani.im>
 * @since 2.0
 */
class FrontPageAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/front-page';

    public $css = [
        'css/main.css',
    ];

    public $js = [
        'js/resana.js'
    ];
}
