<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Ali Irani <ali@irani.im>
 * @since 2.0
 */
class ClipboardAsset extends AssetBundle
{
    public $sourcePath = '@bower/clipboard/dist';

    public $js = [
        'clipboard.min.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}
