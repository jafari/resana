<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ChannelMember;

/**
 * ChatMemberSearch represents the model behind the search form about `common\models\ChannelMember`.
 */
class ChatMemberSearch extends ChannelMember
{
    public $chatName;
    public $chatUsername;
    public $chatType;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'chatType'], 'integer'],
            [['chatName', 'chatUsername'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ChannelMember::find()->where(['channel_id' => $this->channel_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'created_at',
                ],
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('chat');

        $query->andFilterWhere([
            'status' => $this->status,
            '{{%chat}}.type' => $this->chatType,
        ]);

        $query->andFilterWhere(['like', '{{%chat}}.name', $this->chatName]);
        $query->andFilterWhere(['like', '{{%chat}}.username', $this->chatUsername]);

        return $dataProvider;
    }
}
