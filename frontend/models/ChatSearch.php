<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Chat;

/**
 * ChatSearch represents the model behind the search form about `common\models\Chat`.
 */
class ChatSearch extends Chat
{
    public $channel_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['chat_id', 'type', 'member_id'], 'integer'],
            [['name', 'username'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Chat::find()->joinWith('channelMembers')->where(['channel_id' => $this->channel_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'chat_id' => $this->chat_id,
            'type' => $this->type,
            'member_id' => $this->member_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'username', $this->username]);

        return $dataProvider;
    }
}
