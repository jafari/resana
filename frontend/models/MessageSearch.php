<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace frontend\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use common\models\Message;

/**
 * MessageSearch represents the model behind the search form about `common\models\Message`.
 */
class MessageSearch extends Message
{
    /**
     * The message creator.
     */
    public $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['media_type', 'status'], 'integer'],
            [['text', 'user'], 'safe'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function scenarios()
    {
        return [
            parent::SCENARIO_DEFAULT => ['text', 'user', 'status', 'media_type'],
        ];
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param int $project_id
     *
     * @return ActiveDataProvider
     * @throws NotFoundHttpException
     */
    public function search($params, $project_id)
    {
        $query = Message::find()->project($project_id);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'created_at',
                ],
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if ($this->validate() === false) {
            return $dataProvider;
        }

        if ($this->user) {
            $query->joinWith('chat');
        }

        $query->andFilterWhere([
            'media_type' => $this->media_type,
            'status' => $this->status,
        ]);
        $query->andFilterWhere(['like', 'text', $this->text]);
        $query->andFilterWhere(['like', '{{%chat}}.name', $this->user]);

        return $dataProvider;
    }
}
