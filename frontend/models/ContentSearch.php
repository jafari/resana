<?php

namespace frontend\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\models\Content;
use yii\web\NotFoundHttpException;

/**
 * ContentSearch represents the model behind the search form about `common\models\Content`.
 */
class ContentSearch extends Content
{
    public $user;

    /**
     * @todo: fix rules
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['media_type', 'status'], 'integer'],
            [['text', 'user'], 'safe'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function scenarios()
    {
        return [
            parent::SCENARIO_DEFAULT => ['text', 'user', 'status', 'media_type'],
        ];
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (isset($params['project_id']) === false) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        $query = Content::find()->typePublic()->project($params['project_id']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'published_at',
                ],
                'defaultOrder' => [
                    'published_at' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'media_type' => $this->media_type,
            '{{%content}}.status' => $this->status,
        ]);

        $query->joinWith('createdBy');

        $query->andFilterWhere(['or', ['like', '{{%content}}.text', $this->text], ['like', '{{%user}}.name', $this->text]]);

        return $dataProvider;
    }
}
