<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ProjectUser;

/**
 * ProjectUserSearch represents the model behind the search form about `common\models\ProjectUser`.
 */
class ProjectUserSearch extends ProjectUser
{
    public $projectId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'role', 'project_id', 'user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProjectUser::find()->project($this->projectId);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'role' => $this->role,
            'project_id' => $this->project_id,
            'user_id' => $this->user_id,
        ]);

        return $dataProvider;
    }
}
