$(function() {
    var modal = $('#modal');

    $(document).on('beforeSubmit', '#ajaxForm', function (event, jqXHR, settings) {
        var form = $('#ajaxForm');
        $.ajax({
            type: 'post',
            url: form.attr('action'),
            data: new FormData(this),
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            context: this,
            success: function(data, status, xhr) {
                form.find('.form-loading').remove();
                if (data.success === true) {
                    modal.modal('hide');
                    $.pjax.reload({container: '#pjax'});  //Reload GridView
                } else {
                    $(this).find('.errors').remove();
                    if (data.errors !== undefined) {
                        var msg = '';
                        $.each(data.errors, function (key, value) {
                            msg += "<li>"+value+"</li>"
                        });
                        $(this).prepend("<div class='alert alert-danger errors'><ul>"+msg+"</ul></div>");
                    }
                }
            },
            beforeSend: function () {
                form.append("<div class='form-loading'></div>");
            }
        });
        return false;
    });

    $(document).on('click', '.showModalButton', function() {
        var title = $(this).data('title') === undefined ? '' : $(this).data('title');
        if (modal.data('bs.modal').isShown) {
            modal.find('#modalContent')
                .load($(this).data('url'));
            document.getElementById('modalHeader').innerHTML = '<h4>' + title + '</h4>';
        } else {
            modal.modal('show')
                .find('#modalContent')
                .load($(this).data('url'));
            document.getElementById('modalHeader').innerHTML = '<h4>' + title + '</h4>';
        }
    });
});
