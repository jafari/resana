<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$urlRules = require(__DIR__ . '/url-rules.php');

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['site/index'],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logVars' => [],
                    'categories' => ['bot'],
                    'logFile' => '@runtime/logs/bot.log',
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'view' => [
            'theme' => [
                'pathMap' => ['@app/views' => '@frontend/themes/adminlte'],
            ],
        ],
        'urlManager' => [
            'rules' => $urlRules,
        ],
        'request' => [
            'parsers' => ['application/json' => 'yii\web\JsonParser'],
        ],
        'session' => [
            'class' => 'yii\web\CacheSession',
            'timeout' => 21600, // 6 Hours
        ],
    ],
    'params' => $params,
];
