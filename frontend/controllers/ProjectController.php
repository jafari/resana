<?php

namespace frontend\controllers;

use Yii;
use common\models\Project;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['member'],
                        'actions' => ['index', 'create']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['readProject'],
                        'actions' => ['view'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['updateProject'],
                        'actions' => ['delete', 'update','add-auth-key', 'delete-auth-key', 'auth-key'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        $myProjects = Project::find()->active()->my()->all();
        $contributeProjects = Project::find()->active()->contribute()->all();

        return $this->render('index', compact('myProjects', 'contributeProjects'));
    }

    /**
     * Index of AuthKey.
     * @return mixed
     */
    public function actionAuthKey($id)
    {
        $model = $this->findModel($id);
        $model->scenario = Project::SCENARIO_GENERATE_AUTH_KEY;
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('auth-key', [
                'model' => $model,
            ]);
        }
        return $this->render('auth-key', [
            'model' => $model,
        ]);
    }

    /**
     * Add auth_key for project.
     * @return mixed
     */
    public function actionAddAuthKey($id)
    {
        $model = $this->findModel($id);
        $model->scenario = Project::SCENARIO_GENERATE_AUTH_KEY;
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->setAuthKey()) {
                return ['success' => true];
            }
            return ['success' => false, 'errors' => $model->errors];
        }
        return $this->redirect('auth-key');
    }

    /**
     * Delete auth_key field for capability of assign new token to user.
     * @return mixed
     */
    public function actionDeleteAuthKey($id)
    {
        $model = $this->findModel($id);
        $model->scenario = Project::SCENARIO_GENERATE_AUTH_KEY;
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->deleteAuthKey()) {
                return ['success' => true];
            }
            return ['success' => false, 'errors' => $model->errors];
        }
        return $this->redirect('auth-key');
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Project();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->disable();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
