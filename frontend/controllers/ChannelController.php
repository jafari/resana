<?php

namespace frontend\controllers;

use Yii;
use common\models\Project;
use common\models\Channel;
use common\models\WatermarkForm;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ChannelController implements the CRUD actions for Channel model.
 */
class ChannelController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['updateProject'],
                        'actions' => ['delete', 'create', 'update', 'watermark', 'signature', 'delete-watermark'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['readProject'],
                        'actions' => ['index'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Channel models.
     * @return mixed
     */
    public function actionIndex($project_id)
    {
        $project = $this->findProjectModel($project_id);

        $query = Channel::find()->active()->project($project_id);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'project' => $project,
        ]);
    }

    /**
     * Deletes an existing Channel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $project_id)
    {
        $deleteChannels = [
            Channel::TYPE_TWITTER,
            Channel::TYPE_TELEGRAM_CHANNEL,
        ];
        $model = $this->findModel($id);
        if (in_array($model->type, $deleteChannels, true)) {
            $model->delete();
        } else {
            $model->changeStatus(Channel::STATUS_DELETED);
        }

        return $this->redirect(['index', 'project_id' => $project_id]);
    }

    /**
     * Creates a new channel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($project_id)
    {
        $model = new Channel(compact('project_id'));
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'projectId' => $project_id,
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'projectId' => $project_id,
            ]);
        }
    }

    /**
     * Displays a single Project model.
     * @param $project_id
     * @param $id
     * @param string $type
     * @return array|string
     */
    public function actionWatermark($project_id, $id, $type = 'text')
    {
        $model = new WatermarkForm(['channelId' => $id]);
        if ($model->load(Yii::$app->request->post()) && $model->save($type)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => true];
        }

        return $this->renderAjax('watermark', [
            'project_id' => $project_id,
            'model' => $model,
            'id' => $id,
        ]);
    }

    /**
     * Delete channel's watermark setting
     * @param $project_id
     * @param $id
     * @return array|string
     */
    public function actionDeleteWatermark($project_id, $id)
    {
        $model = new WatermarkForm(['channelId' => $id]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($model->deleteWatermark()) {
            return ['success' => true];
        } else {
            return $this->renderAjax('watermark', [
                'project_id' => $project_id,
                'model' => $model,
                'id' => $id,
            ]);
        }
    }

   /* @param $project_id
    * @param $id
    * @return array|string | Response
    * @throws NotFoundHttpException
    */
    public function actionSignature($project_id, $id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isAjax === false) {
            return $this->render('signature', [
                'model' => $model,
                'project_id' => $project_id,
            ]);
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
            return ['error' => $model->errors];
        }
        return $this->renderAjax('_signatureForm', [
            'model' => $model,
            'project_id' => $project_id,
        ]);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Channel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findProjectModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Finds the Channel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Channel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Channel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
