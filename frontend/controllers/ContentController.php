<?php

namespace frontend\controllers;

use Yii;
use common\models\Group;
use common\models\Content;
use frontend\models\ContentSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ContentController implements the CRUD actions for Content model.
 */
class ContentController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['updateProject'],
                        'actions' => ['delete'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['readProject'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'delete-attachment' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Content models.
     * @return mixed
     */
    public function actionIndex($project_id)
    {
        $searchModel = new ContentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'projectId' => $project_id,
        ]);
    }

    /**
     * Displays a single Content model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($project_id, $id)
    {
        $model = $this->findModel($id, $project_id);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view', [
                'model' => $model,
                'projectId' => $project_id,
            ]);
        } else {
            return $this->render('view', [
                'model' => $model,
                'projectId' => $project_id,
            ]);
        }
    }

    /**
     * Creates a new Content model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($project_id)
    {
        $model = new Content(compact('project_id'));
        $model->scenario = Content::SCENARIO_CREATE;

        if (Yii::$app->request->getBodyParam('draft') !== null) {
            $model->status = Content::STATUS_PENDING;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (in_array($model->status, [Content::STATUS_SCHEDULED, Content::STATUS_PENDING], true)) {
                $urlParams = ['update', 'id' => $model->id, 'project_id' => $project_id];
            } else {
                $urlParams = ['index', 'project_id' => $project_id];
            }
            return $this->redirect($urlParams);
        } else {
            return $this->render('create', [
                'model' => $model,
                'projectId' => $project_id,
            ]);
        }
    }

    /**
     * Updates an existing Content model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($project_id, $id)
    {
        $model = $this->findModel($id, $project_id);
        $model->scenario = Content::SCENARIO_UPDATE;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'project_id' => $project_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'projectId' => $project_id,
            ]);
        }
    }

    /**
     * Deletes an existing Content model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($project_id, $id)
    {
        $this->findModel($id, $project_id)->delete();

        return $this->redirect(['index', 'project_id' => $project_id]);
    }

    /**
     * Delete an attachment file from attachments json field
     * @param $project_id
     * @return bool|int
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDeleteAttachment($project_id)
    {
        $request = Yii::$app->request;

        if ($request->isAjax) {
            $id = $request->getBodyParam('id');
            $url = $request->getBodyParam('key');
            $model = $this->findModel($id, $project_id);

            if ($model->attachments) {
                $files = json_decode($model->attachments);
                foreach ($files as $file) {
                    if ($file->url != $url) {
                        $newFiles[] = $file;
                    }
                }

                if (isset($newFiles)) {
                    $model->attachments = json_encode($newFiles);
                } else {
                    $model->attachments = '';
                    $model->media_type = Content::MEDIA_TYPE_TEXT;
                }

                return $model->update(false);
            }
        }

        return false;
    }

    /**
     * Returns groups of selected channels in json format
     * @return string
     */
    public function actionGetGroups($project_id, $id = null)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            if ($channelIds = $request->getBodyParam('channelIds')) {
                $groups = Group::findAllByChannels($channelIds);
                if ($id) {
                    $model = $this->findModel($id, $project_id);
                    $activeGroups = $model->groupsList;
                } else {
                    $activeGroups = [];
                }
                return json_encode(['items' => $groups, 'selected' => $activeGroups]);
            }
        }
        return '[]'; // array in json format
    }

    /**
     * Finds the Content model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Content the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $project_id)
    {
        if (($model = Content::findOne(compact('id', 'project_id'))) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
