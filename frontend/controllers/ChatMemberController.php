<?php

namespace frontend\controllers;

use Yii;
use common\models\ChannelMember;
use frontend\models\ChatMemberSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ChatMemberController implements the CRUD actions for ChannelMember model.
 */
class ChatMemberController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['readProject'],
                        'actions' => ['index', 'view']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['updateProject'],
                        'actions' => ['delete'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ChannelMember models.
     * @return mixed
     */
    public function actionIndex($project_id, $channel_id)
    {
        $searchModel = new ChatMemberSearch(compact('channel_id'));
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'channelId' => $channel_id,
            'projectId' => $project_id,
        ]);
    }

    /**
     * Displays a single ChannelMember model.
     * @param integer $channel_id
     * @param integer $member_id
     * @return mixed
     */
    public function actionView($project_id, $channel_id, $member_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($channel_id, $member_id),
        ]);
    }

    /**
     * Deletes an existing ChannelMember model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $channel_id
     * @param integer $member_id
     * @return mixed
     */
    public function actionDelete($project_id, $channel_id, $member_id)
    {
        $this->findModel($channel_id, $member_id)->delete();

        return $this->redirect(['index', 'channel_id' => $channel_id, 'project_id' => $project_id]);
    }

    /**
     * Finds the ChannelMember model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $channel_id
     * @param integer $member_id
     * @return ChannelMember the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($channel_id, $member_id)
    {
        if (($model = ChannelMember::findOne(['channel_id' => $channel_id, 'member_id' => $member_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
