<?php

namespace frontend\controllers;

use Yii;
use common\models\Project;
use common\models\TelegramChannel;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * TelegramChannelController implements the CRUD actions for TelegramChannel model.
 */
class TelegramChannelController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['updateProject'],
                        'actions' => ['create', 'update', 'view', 'delete'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Displays a single TelegramChannel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($project_id, $id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'projectId' => $project_id,
        ]);
    }

    /**
     * Creates a new TelegramChannel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($project_id)
    {
        $model = new TelegramChannel(['projectId' => $project_id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
            return $this->redirect(['view', 'id' => $model->channel_id, 'project_id' => $project_id]);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'projectId' => $project_id,
                'project' => $this->findProjectModel($project_id),
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'projectId' => $project_id,
                'project' => $this->findProjectModel($project_id),
            ]);
        }
    }

    /**
     * Updates an existing TelegramChannel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($project_id, $id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
            return $this->redirect(['view', 'id' => $model->channel_id, 'project_id' => $project_id]);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'projectId' => $project_id,
                'project' => $this->findProjectModel($project_id),
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'projectId' => $project_id,
                'project' => $this->findProjectModel($project_id),
            ]);
        }
    }

    /**
     * Finds the TelegramChannel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TelegramChannel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TelegramChannel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findProjectModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
