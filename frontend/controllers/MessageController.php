<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use common\models\Message;
use frontend\models\MessageSearch;

/**
 * MessageController implements the CRUD actions for Message model.
 */
class MessageController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['updateProject'],
                        'actions' => ['delete'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['readProject'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @param $project_id
     *
     * @return mixed
     */
    public function actionIndex($project_id)
    {
        $searchModel = new MessageSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $project_id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'projectId' => $project_id,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param integer $project_id
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($project_id, $id)
    {
        $model = $this->findModel($id, $project_id);
        $renderMethod = 'render';

        if (Yii::$app->request->isAjax) {
            $renderMethod = 'renderAjax';
        }

        return $this->{$renderMethod}('view', [
            'model' => $model,
            'projectId' => $project_id,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param integer $project_id
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($project_id, $id)
    {
        $this->findModel($id, $project_id)->delete();

        return $this->redirect(['message/index', 'project_id' => $project_id]);
    }

    /**
     * Finds the Content model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @param integer $project_id
     *
     * @return \common\models\Message
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $project_id)
    {
        if ($model = Message::find()->where(['{{%message}}.id' => $id])->project($project_id)->one()) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
