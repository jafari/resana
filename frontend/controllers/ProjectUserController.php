<?php

namespace frontend\controllers;

use Yii;
use common\models\Project;
use common\models\ProjectUser;
use frontend\models\ProjectUserSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ProjectUserController implements the CRUD actions for ProjectUser model.
 */
class ProjectUserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['updateProject'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProjectUser models.
     * @return mixed
     */
    public function actionIndex($project_id)
    {
        if (is_null($project = Project::findOne($project_id))) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        $searchModel = new ProjectUserSearch(['projectId' => $project_id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'project' => $project,
        ]);
    }

    /**
     * Displays a single ProjectUser model.
     * @param integer $project_id
     * @param integer $user_id
     * @return mixed
     */
    public function actionView($project_id, $user_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($project_id, $user_id),
        ]);
    }

    /**
     * Creates a new ProjectUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $project_id
     * @return mixed
     */
    public function actionCreate($project_id)
    {
        $model = new ProjectUser(['project_id' => $project_id]);
        $model->scenario = ProjectUser::SCENARIO_CREATE;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->addMember()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'New member successfully added to the project.'));
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['success' => true];
                }
                return $this->redirect(['view', 'project_id' => $model->project_id, 'user_id' => $model->user_id]);
            }

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => false, 'errors' => $model->errors];
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'projectId' => $project_id,
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'projectId' => $project_id,
            ]);
        }
    }

    /**
     * Updates an existing ProjectUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $project_id
     * @param integer $user_id
     * @return mixed
     */
    public function actionUpdate($project_id, $user_id)
    {
        $model = $this->findModel($project_id, $user_id);
        $model->scenario = ProjectUser::SCENARIO_UPDATE;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'project_id' => $model->project_id, 'user_id' => $model->user_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ProjectUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $project_id
     * @param integer $user_id
     * @return mixed
     */
    public function actionDelete($project_id, $user_id)
    {
        $model = $this->findModel($project_id, $user_id);
        if ($model->delete()) {
            $message = Yii::t('app', 'Member successfully deleted.');
        }

        $message = $model->firstErrors
        ;
        Yii::$app->session->setFlash('error', $message);

        return $this->redirect(['index', 'project_id' => $project_id]);
    }

    /**
     * Finds the ProjectUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $project_id
     * @param integer $user_id
     * @return ProjectUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($project_id, $user_id)
    {
        if (($model = ProjectUser::findOne(['project_id' => $project_id, 'user_id' => $user_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
