<?php

namespace frontend\controllers;

use Yii;
use common\models\Group;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GroupController implements the CRUD actions for Group model.
 */
class GroupController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['member'],
                        'actions' => ['index']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['readProject'],
                        'actions' => ['view'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['updateProject'],
                        'actions' => ['delete', 'update', 'create'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Group models.
     * @return mixed
     */
    public function actionIndex($project_id, $channel_id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Group::find()->channel($channel_id, $project_id),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'projectId' => $project_id,
            'channelId' => $channel_id,
        ]);
    }

    /**
     * Displays a single Group model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($project_id, $channel_id, $id)
    {
        return $this->render('view', [
            'model' => $this->findModel($project_id, $channel_id, $id),
        ]);
    }

    /**
     * Creates a new Group model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($project_id, $channel_id)
    {
        $model = new Group(['project_id' => $project_id, 'channel_id' => $channel_id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Your group successfully created.'));
            return $this->redirect(['update', 'project_id' => $project_id, 'channel_id' => $channel_id, 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Group model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($project_id, $channel_id, $id)
    {
        $model = $this->findModel($project_id, $channel_id, $id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Your group successfully updated.'));
            return $this->redirect(['update', 'project_id' => $project_id, 'channel_id' => $channel_id, 'id' => $id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Group model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($project_id, $channel_id, $id)
    {
        $this->findModel($project_id, $channel_id, $id)->delete();

        Yii::$app->session->setFlash('error', Yii::t('app', 'Your group successfully deleted.'));
        return $this->redirect(['index', 'project_id' => $project_id, 'channel_id' => $channel_id]);
    }

    /**
     * Finds the Group model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Group the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($project_id, $channel_id, $id)
    {
        $model = Group::findOne(compact('id', 'channel_id'));
        $model->project_id = $project_id;

        if ($model !== null && $model->validate()) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
