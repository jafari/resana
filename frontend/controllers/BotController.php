<?php

namespace frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use common\models\Channel;
use common\models\Bot;

/**
 * BotController implements the CRUD actions for Bot model.
 */
class BotController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['updateProject'],
                        'actions' => ['create', 'update', 'view'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Displays a single Bot model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $project_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'projectId' => $project_id,
        ]);
    }

    /**
     * Creates a new Bot model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $project_id
     * @return mixed
     */
    public function actionCreate($project_id)
    {
        /* @var Bot $model */
        /* @var Bot $bot */
        /* @var Channel $channel */
        $model = new Bot(['projectId' => $project_id]);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->setBotAttributes() && $model->save()) {
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['success' => true];
                }
                return $this->redirect(['view', 'id' => $model->channel_id, 'project_id' => $project_id]);
            } elseif (($bot = Bot::findOne(['bot_id' => $model->bot_id])) && ($channel = Channel::findOne(['id' => $bot->channel_id, 'status' => Channel::STATUS_DELETED]))) {
                $channel->project_id = $project_id;
                $channel->status = Channel::STATUS_ACTIVE;
                if ($channel->update()) {
                    if (Yii::$app->request->isAjax) {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return ['success' => true];
                    }
                    return $this->redirect(['view', 'id' => $channel->id, 'project_id' => $project_id]);
                }
            }
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => false, 'errors' => $model->errors];
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'projectId' => $project_id,
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'projectId' => $project_id,
            ]);
        }
    }

    /**
     * Updates an existing Bot model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $project_id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['success' => true];
                }
                return $this->redirect(['view', 'id' => $model->channel_id, 'project_id' => $project_id]);
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => false, 'errors' => $model->errors];
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'projectId' => $project_id,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'projectId' => $project_id,
            ]);
        }
    }

    /**
     * Finds the Bot model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bot the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bot::findActiveOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
