<?php

namespace frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use common\models\Project;
use common\models\Twitter;

/**
 * TwitterChannelController implements the CRUD actions for Twitter model.
 */
class TwitterController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['updateProject'],
                        'actions' => ['create', 'view', 'delete'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'actions' => ['callback'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param $project_id
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($project_id, $id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'projectId' => $project_id,
        ]);
    }

    /**
     * @param $project_id
     * @return string
     * @throws NotFoundHttpException
     * @throws \Abraham\TwitterOAuth\TwitterOAuthException
     */
    public function actionCreate($project_id)
    {
        $model = new Twitter();
        $connection = Twitter::getConnection();
        $requestToken = $connection->oauth('oauth/request_token');
        $url = $connection->url('oauth/authorize', ['oauth_token' => $requestToken['oauth_token']]);
        Yii::$app->session->set('token:' . $requestToken['oauth_token'], $project_id);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'projectId' => $project_id,
                'project' => $this->findProjectModel($project_id),
                'url' => $url,
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'projectId' => $project_id,
                'project' => $this->findProjectModel($project_id),
                'url' => $url,
            ]);
        }
    }

    /**
     * Take action when the twitter api want to callback the authorization request.
     * If model exists before, the browser will be redirected to the 'index' page of 'channel' controller,
     * Else the browser will be redirected to the 'view' page.
     * @param $oauth_token
     * @param $oauth_verifier
     * @return \yii\web\Response
     * @throws \Abraham\TwitterOAuth\TwitterOAuthException
     */
    public function actionCallback($oauth_token, $oauth_verifier)
    {
        $projectId = Yii::$app->session->get('token:' . $oauth_token);
        $connection = Twitter::getConnection();
        $accessToken = $connection->oauth("oauth/access_token", [
                "oauth_verifier" => $oauth_verifier,
                'oauth_token' => $oauth_token,
        ]);
        $connection->setOauthToken(
            $accessToken['oauth_token'],
            $accessToken['oauth_token_secret']
        );
        $user = $connection->get("account/verify_credentials");

        $model = new Twitter([
            'name' => $user->name,
            'username' => $user->screen_name,
            'user_id' => $user->id,
            'token' => $accessToken['oauth_token'],
            'token_secret' => $accessToken['oauth_token_secret'],
            'projectId' => $projectId,
        ]);

        if ($model->save()) {
            return $this->redirect(['view', 'id' => $model->channel_id, 'project_id' => $projectId]);
        }

        Yii::$app->session->setFlash('error', $model->getFirstError('token'));
        return $this->redirect(['channel/index', 'project_id' => $projectId]);
    }

    /**
     * Deletes an existing Twitter model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Twitter model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Twitter the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Twitter::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $id
     * @return null|static
     * @throws NotFoundHttpException
     */
    protected function findProjectModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
