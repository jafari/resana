<?php
namespace frontend\controllers;

use common\models\User;
use Yii;
use yii\helpers\Markdown;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'help'],
                'rules' => [
                    [
                        'actions' => ['logout', 'help'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'authCallback'],
            ],
        ];
    }

    /**
     * Auth client success callback for login by shenasa service
     * @param $client
     */
    public function authCallback($client)
    {
        User::login($client->userAttributes);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            $this->redirect('/project');
        }

        $this->layout = '@app/views/layouts/front-page-main.php';
        return $this->render('index');
    }

    /**
     * Display help page
     *
     * @return string
     */
    public function actionHelp()
    {
        $this->view->title = Yii::t('app', 'Resana Help');
        $content = $this->renderFile('@app/views/site/help.md');
        return $this->render('help', [
            'content' => Markdown::process($content, 'gfm-comment'),
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
