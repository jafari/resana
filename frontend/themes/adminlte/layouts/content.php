<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\widgets\Breadcrumbs;
use airani\widgets\Alert;
?>
<div class="content-wrapper">
    <?php if (isset($this->params['hasHeader']) === false) : ?>
    <section class="content-header content-box">
        <h1>
            <?php
            if ($icon = ArrayHelper::getValue($this->params, 'title-icon')) {
                echo Html::tag('i', '', ['class' => 'fa ' . $icon]) . ' ';
            }

            if ($this->title !== null) {
                echo $this->title;
            } else {
                echo Inflector::camel2words(Inflector::id2camel($this->context->module->id));
                echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
            } ?>
        </h1>

        <?= ArrayHelper::getValue($this->params, 'create-button') ? ArrayHelper::getValue($this->params, 'create-button') : '' ?>

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </section>
    <?php endif; ?>

    <section class="content content-box">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>