<?php
use airani\web\AdminLteRtlAsset;
use yii\helpers\Html;
use frontend\assets\AppAsset;
use yii\bootstrap\Modal;

/* @var $this \yii\web\View */
/* @var $content string */
if (Yii::$app->controller->action->id === 'login') {
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {
    $adminLte = AdminLteRtlAsset::register($this);
    AppAsset::register($this);
    $directoryAsset = $adminLte->baseUrl;
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="skin-purple sidebar-mini">
    <?php $this->beginBody() ?>
    <div class="wrapper">
        <?= $this->render(
            'header.php',
            ['directoryAsset' => $directoryAsset]
        ) ?>

        <?= $this->render(
            'left.php',
            ['directoryAsset' => $directoryAsset]
        )
        ?>

        <?= $this->render(
            'content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset]
        ) ?>

        <?= $this->render('footer.php'); ?>

    </div>

    <?php $this->endBody() ?>
    <?php
    Modal::begin([
        'headerOptions' => ['id' => 'modalHeader'],
        'id' => 'modal',
        'size' => Modal::SIZE_LARGE,
    ]);
    echo "<div id='modalContent'></div>";
    Modal::end();
    ?>
    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
