<?php
use common\components\AjaxModal;
use common\models\Channel;
use common\models\Group;
use common\models\Project;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

?>
<aside class="main-sidebar">

    <section class="sidebar">
    <?php if (Yii::$app->user->isGuest === false) : ?>

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-right image">
                <img src="<?= Yii::$app->user->identity->avatar ?>" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->name ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> <?= Yii::t('app', 'online') ?></a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="<?= Yii::t('app', 'Search') ?>..."/>
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <?php
        $controller = Yii::$app->controller;

        if (($controller->id === 'project') && (isset($controller->actionParams['id']))) {
            $projectId = $controller->actionParams['id'];
        } elseif (isset($controller->actionParams['project_id'])) {
            $projectId = $controller->actionParams['project_id'];
        }

        if (($controller->id === 'bot') && (isset($controller->actionParams['id']))) {
            $channelId = $controller->actionParams['id'];
        } elseif (isset($controller->actionParams['channel_id'])) {
            $channelId = $controller->actionParams['channel_id'];
        }

        if (isset($channelId)) {
            $channel = Channel::findOne($channelId);
        }
        ?>
        <ul class="sidebar-menu">
            <li class="treeview">
                <a href="<?= Url::to(['project/index']) ?>"><i class="fa fa-dashboard"></i> <span><?= Yii::t('app', 'Projects') ?></span> <i class="fa fa-angle-left pull-left"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <?= Html::a('<i class="fa fa-plus"></i> <span>'.Yii::t('app', 'Create Project').'</span>', '#', ['class' => 'showModalButton', 'data' => [
                            'url' => Url::to(['project/create']),
                            'title' => Yii::t('app', 'Create Project'),
                        ]]); ?>
                    </li>
                    <li>
                        <?= Html::a('<i class="fa fa-circle"></i> <span>'.Yii::t('app', 'Projects').'</span>', ['project/index']) ?>
                    </li>
                    <?php
                    if ($projects = Project::find()->active()->own()->all()) {
                        foreach ($projects as $project) {
                            echo Html::tag('li', Html::a(
                                '<i class="fa fa-circle-o"></i> ' . $project->name,
                                ['project/view', 'id' => $project->id]
                            ));
                        }
                    } ?>
                </ul>
            </li>
            <?php if (isset($projectId)) : ?>
            <?php $project = Project::findOne($projectId) ?>
            <li class="header"><i class="fa fa-star"></i> <strong><?= Html::a(Yii::t('app', 'Project') . ': ' . $project->name, ['project/view', 'id' => $project->id]) ?></strong></li>
            <li class="<?= ($controller->id === 'content' && $controller->action->id === 'index') ? 'active' : '' ?>">
                <a href="<?= Url::to(['content/index', 'project_id' => $projectId]) ?>">
                    <i class="fa fa-newspaper-o"></i> <span><?= Yii::t('app', 'Contents') ?></span>
                </a>
            </li>
            <li class="<?= ($controller->id === 'content' && $controller->action->id === 'create') ? 'active' : '' ?>">
                <?= Html::a('<i class="fa fa-edit"></i> <span>'.Yii::t('app', 'Create Content').'</span>', ['content/create', 'project_id' => $projectId]); ?>
            </li>
            <li class="<?= ($controller->id === 'message' && $controller->action->id === 'index') ? 'active' : '' ?>">
                <a href="<?= Url::to(['message/index', 'project_id' => $projectId]) ?>">
                    <i class="fa fa-comments-o"></i> <span><?= Yii::t('app', 'Messages') ?></span>
                </a>
            </li>
            <?php if (Yii::$app->user->can('updateProject')) : ?>
                <li class="treeview <?= $controller->id === 'project-user' ? 'active' : '' ?>">
                    <a href="<?= Url::to(['project-user/index', 'project_id' => $projectId]) ?>">
                        <i class="fa fa-users"></i> <span><?= Yii::t('app', 'Members') ?></span> <i class="fa fa-angle-left pull-left"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add Member'), ['project-user/create', 'project_id' => $projectId], [
                                'class' => 'showModalButton',
                                'onclick' => 'return false;',
                                'data' => [
                                    'title' => '<i class="fa fa-user"></i> '.Yii::t('app', 'Add Member'),
                                    'url' => Url::to(['project-user/create', 'project_id' => $projectId]),
                                ]
                            ]); ?></li>
                        <li><?= Html::a('<i class="fa fa-circle-o"></i> '.Yii::t('app', 'All Members'), ['project-user/index', 'project_id' => $projectId]); ?></li>
                    </ul>
                </li>
            <?php endif; ?>
            <li class="treeview <?= in_array($controller->id, ['bot', 'channel', 'telegram-channel']) ? 'active' : '' ?>">
                <a href="<?= Url::to(['channel/index', 'project_id' => $projectId]) ?>">
                    <i class="fa fa-road"></i> <span><?= Yii::t('app', 'Channels') ?></span> <i class="fa fa-angle-left pull-left"></i>
                </a>
                <ul class="treeview-menu">
                    <?php if (Yii::$app->user->can('updateProject')) : ?>
                    <li>
                        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add Channel'), ['channel/create', 'project_id' => $projectId], [
                            'class' => 'showModalButton',
                            'onClick' => 'return false;',
                            'data' => [
                            'url' => Url::to(['channel/create', 'project_id' => $projectId]),
                            'title' => Yii::t('app', 'Add Channel')
                            ]
                        ]) ?>
                    </li>
                    <?php endif ?>
                    <li><?= Html::a('<i class="fa fa-circle-o"></i> '.Yii::t('app', 'All Channels'), ['channel/index', 'project_id' => $projectId]); ?></li>
                    <?php
                    $channelClasses = [
                        Channel::TYPE_TELEGRAM_CHANNEL => 'send',
                        Channel::TYPE_TELEGRAM_BOT => 'simplybuilt',
                        Channel::TYPE_TWITTER => 'twitter',
                    ];
                    if ($channels = Channel::findAll(['project_id' => $projectId, 'status' => Channel::STATUS_ACTIVE])) {
                        foreach ($channels as $ch) {
                            echo Html::tag('li', Html::a(
                                '<i class="fa fa-'. $channelClasses[$ch->type] .'"></i> ' . $ch->name,
                                [$ch->actionName . '/view', 'project_id' => $projectId, 'id' => $ch->id]
                            ));
                        }
                    }
                    ?>
                </ul>
            </li>
            <?php endif; ?>

            <?php if (isset($channelId, $projectId)) : ?>
                <li class="header"><i class="fa fa-road"></i> <strong><?= $channel->name . ' (' . $channel->typeName . ')' ?></strong></li>
                <li class="<?= $controller->id === 'chat' ? 'active' : '' ?>">
                    <a href="<?= Url::to([$channel->memberType . '-member/index', 'project_id' => $projectId, 'channel_id' => $channelId]) ?>">
                        <i class="fa fa-book"></i> <span><?= Yii::t('app', ucfirst(Inflector::pluralize($channel->memberType))) ?></span>
                    </a>
                </li>
                <li class="treeview <?= $controller->id === 'group' ? 'active' : '' ?>">
                    <a href="<?= Url::to(['group/index', 'project_id' => $projectId, 'channel_id' => $channelId]) ?>">
                        <i class="fa fa-folder-open"></i> <span><?= Yii::t('app', 'Groups') ?></span> <i class="fa fa-angle-left pull-left"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if (Yii::$app->user->can('updateProject')) : ?>
                        <li><?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Create Group'), ['group/create', 'project_id' => $projectId, 'channel_id' => $channelId]); ?></li>
                        <?php endif; ?>
                        <li><?= Html::a('<i class="fa fa-circle"></i> '.Yii::t('app', 'All Groups'), ['group/index', 'project_id' => $projectId, 'channel_id' => $channelId]); ?></li>
                        <?php
                        if ($groups = Group::getList($channelId)) {
                            foreach ($groups as $id => $name) {
                                echo Html::tag('li', Html::a(
                                    '<i class="fa fa-circle-o"></i> ' . $name,
                                    ['group/view', 'project_id' => $projectId, 'channel_id' => $channelId, 'id' => $id]
                                ));
                            }
                        }
                        ?>
                    </ul>
                </li>
            <?php endif; ?>
        </ul>

        <?php endif; ?>
    </section>
</aside>
