<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <a href="<?= Yii::$app->homeUrl ?>" class="logo">
        <span class="logo-mini">
            <b>R</b>SA
        </span>
        <span class="logo-lg">
            <b>R</b>ESANA
        </span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <!-- User Account: style can be found in dropdown.less -->

                <?php if (Yii::$app->user->isGuest === false): ?>
                <li>
                    <a href="<?= Url::to(['site/help']) ?>"><i class="fa fa-question-circle"></i> <?= Yii::t('app', 'Help') ?></a>
                </li>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= Yii::$app->user->identity->avatar ?>" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?= Yii::$app->user->identity->name ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= Yii::$app->user->identity->avatar ?>" class="img-circle"
                                 alt="User Image"/>

                            <p>
                                <?= Yii::$app->user->identity->name ?>
                                <small><?= Yii::t('app', 'Member since') ?> <?= Yii::$app->formatter->asDate(Yii::$app->user->identity->created_at, 'php:d M Y') ?></small>
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-right">
                                <?= Html::a(
                                    Yii::t('app', 'Edit Profile'),
                                    ['user/edit'],
                                    ['class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                            <div class="pull-left">
                                <?= Html::a(
                                    Yii::t('app', 'Sign out'),
                                    ['site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>
                <?php else: ?>
                    <li>
                        <?= Html::a(Yii::t('app', 'Login'), ['site/auth', 'authclient' => 'shenasa']) ?>
                    </li>
                <?php endif; ?>

            </ul>
        </div>
    </nav>
</header>
