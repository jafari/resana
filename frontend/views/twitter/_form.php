<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Twitter */
/* @var $project common\models\Project */
/* @var $form yii\widgets\ActiveForm */
/* @var $projectId $project->id */
/* @var $url */

$action = Url::to(["twitter/create", 'project_id' => $projectId]);
?>

<div class="telegram-channel-form">
    <?php $form = ActiveForm::begin(['action' => $action, 'options' => ['id'=>'ajaxForm']]); ?>

    <a href="<?= $url ?>"><?= Html::button(Yii::t('app', 'LogIn With Twitter'), ['class'=>'btn btn-primary']) ?> </a>

    <?php ActiveForm::end(); ?>

</div>
