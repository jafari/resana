<?php

/* @var $this yii\web\View */
/* @var $model common\models\Twitter */
/* @var $project common\models\Project */
/* @var $projectId */
/* @var $url */

$this->title = Yii::t('app', 'Create Twitter');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Twitters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="twitter-create box box-primary">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
            'projectId' => $projectId,
            'project' => $project,
            'url' => $url,
        ]) ?>
    </div>
</div>
