<?php
/**
 * @var \common\models\Message[] $messages
 * @var $projectId
 */

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-comments"></i>
        <h3 class="box-title"><?= Html::a(Yii::t('app', 'Recent Messages'), ['message/index', 'project_id' => $projectId]) ?></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <ul class="products-list product-list-in-box">
            <?php if (count($messages) > 0) : ?>
            <?php foreach ($messages as $message) : ?>
            <li class="item">
                <img class="direct-chat-img" src="http://nmedia.ncdn.dev/v1/image/E3Fj-HubVO9iSPArtIDzuALUMC7h1e-GE71Tr1IM7b0/s/w200h200-c" alt="message user image">
                <div class="product-info">
                    <span class="direct-chat-timestamp pull-left"><?= Yii::$app->formatter->asDatetime($message->created_at, 'php:d F Y - H:i') ?></span>
                    <?= $message->member->name ?>
                    <span class="product-description"><?= HtmlPurifier::process($message->text) ?></span>
                </div>
            </li>
            <!-- /.item -->
            <?php endforeach; ?>
            <?php else : ?>
                <li class="item">
                    <?= Yii::t('app', 'Messages not found') ?>
                </li>
            <?php endif; ?>
        </ul>
    </div>
    <!-- /.box-body -->
</div><!-- /.box-primary -->

