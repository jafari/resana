<?php
/**
 * @var \common\models\ProjectUser[] $projectUsers
 * @var $projectId
 */

use yii\helpers\Html;
?>

<div class="box box-danger">
    <div class="box-header with-border">
        <i class="fa fa-users"></i>
        <?= Html::tag('h3', Html::a(Yii::t('app', 'Project Users'), ['project-user/index', 'project_id' => $projectId]), ['class' => 'box-title']) ?>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
        <ul class="users-list clearfix">
            <?php foreach ($projectUsers as $projectUser) : ?>
            <li>
                <?= Html::a(Html::img($projectUser->user->getAvatar(128, 128)), ['project-user/view', 'project_id' => $projectId, 'user_id' => $projectUser->user_id]); ?>
                <?= Html::a($projectUser->user->name, ['project-user/view', 'project_id' => $projectId, 'user_id' => $projectUser->user_id], ['class' => 'users-list-name']); ?>
                <span class="users-list-date"><?= $projectUser->roleName ?></span>
            </li>
            <?php endforeach; ?>
        </ul>
        <!-- /.users-list -->
    </div>
    <!-- /.box-body -->
</div><!-- /.box-danger -->
