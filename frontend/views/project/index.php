<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var array $myProjects */
/* @var array $contributeProjects */

$this->title = Yii::t('app', 'Projects');
$this->params['hasHeader'] = true;
?>
<div class="project-index">
    <?php if (count($myProjects) <= 0 && count($contributeProjects) <= 0) : ?>
        <?= $this->render('_no-post', [
            'title' => Yii::t('app', 'Contribute Projects'),
            'projects' => $contributeProjects,
        ]) ?>
    <?php else : ?>
        <?php if (count($myProjects) > 0) : ?>
            <?= $this->render('_my-projects', [
                'title' => Yii::t('app', 'My Projects'),
                'projects' => $myProjects,
            ]) ?>
        <?php endif; ?>

        <?php if (count($contributeProjects) > 0) : ?>
            <?= $this->render('_contribute-projects', [
                'title' => Yii::t('app', 'Contribute Projects'),
                'projects' => $contributeProjects,
            ]) ?>
        <?php endif; ?>
    <?php endif; ?>
</div>
