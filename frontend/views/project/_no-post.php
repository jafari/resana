<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var array $projects */
/* @var $project common\models\Project */
/* @var string $title */

?>
<div class="row no-post">
    <i class="fa fa-dashboard"></i>
    <h1><?= Yii::t('app', 'Start Now') ?><br><small><?= Yii::t('app', 'For creating new project just click on the below link') ?></small></h1>
    <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Create Project'), '#', ['class' => 'showModalButton btn btn-default', 'data' => [
        'url' => Url::to(['project/create']),
        'title' => Yii::t('app', 'Create Project'),
    ]]); ?>
</div>
