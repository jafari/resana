<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Project */

$this->title = Yii::t('app', 'Project') . ' '. $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['hasHeader'] = true;
?>
    <div class="row">
        <div class="col-md-8 col-sm-12">
            <div class="row new-button">
                <ul>
                    <li class="col-md-12 col-sm-12 col-xs-12">
                        <?= Html::a('<i class="fa fa-edit"></i> ' . Yii::t('app', 'Create Content'), ['content/create', 'project_id' => $model->id], ['class' => 'btn bg-orange col-md-12 col-sm-12 col-xs-12']) ?>
                    </li>
                    <?php if (Yii::$app->user->can('updateProject')) : ?>
                    <li class="col-md-12 col-sm-12 col-xs-12">
                        <?= Html::a('<i class="fa fa-code-fork"></i> ' . Yii::t('app', 'Add Channel'), '#', [
                            'class' => 'btn bg-navy col-md-12 col-sm-12 col-xs-12 showModalButton',
                            'data' => [
                                'url' => Url::to(['channel/create', 'project_id' => $model->id]),
                                'title' => Yii::t('app', 'Add Channel'),
                            ]
                        ]) ?>
                    </li>
                    <li class="col-md-12 col-sm-12 col-xs-12">
                        <?= Html::a('<i class="fa fa-user-plus"></i> ' . Yii::t('app', 'Add Member'), '#', [
                            'class' => 'btn bg-aqua col-md-12 col-sm-12 col-xs-12 showModalButton',
                            'data' => [
                                'url' => Url::to(['project-user/create', 'project_id' => $model->id]),
                                'title' => Yii::t('app', 'Add Member'),
                            ]
                        ]) ?>
                    </li>
                    <?php endif; ?>
                </ul>
            </div>
            
            <?= $this->render('_contents', [
                'projectId' => $model->id,
            ]); ?>
        </div><!-- /.col-md-8 -->
        <div class="col-md-4 col-sm-12">
            <?= $this->render('_project_info', [
                'model' => $model
            ]); ?>
            <?= $this->render('_channels', [
                'channels' => $model->activeChannels,
                'projectId' => $model->id,
            ]); ?>
            <?= $this->render('_project-users.php', [
                'projectUsers' => $model->projectUsers,
                'projectId' => $model->id,
            ]) ?>
            <?= $this->render('_messages.php', [
                'messages' => $model->getRecentMessages(5),
                'projectId' => $model->id,
            ]) ?>
        </div>
    </div>

