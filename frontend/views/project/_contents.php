<?php
/**
 * @var $projectId
 */

use common\models\Content;
use common\widgets\ContentWidget;
use yii\data\ActiveDataProvider;

$query = Content::find()->where(['project_id' => $projectId]);
$dataProvider = new ActiveDataProvider([
    'query' => $query,
    'pagination' => [
        'pageSize' => 5,
    ],
    'sort' => [
        'attributes' => [
            'published_at',
        ],
        'defaultOrder' => [
            'published_at' => SORT_DESC,
        ],
    ],
]);
?>

<section class="content">
    <div class="row">
        <div class="col-md-12 post-list-content">
            <?= ContentWidget::widget([
                'dataProvider' => $dataProvider,
                'layout' => '{items}',
            ]) ?>
        </div>
    </div>
</section>
