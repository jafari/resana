<?php
/**
 * @var \common\models\Project $model
 */
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="box box-widget widget-user-2">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header bg-yellow">
        <div class="widget-user-image">
            <img class="img-circle" src="<?= $model->projectOwner->user->avatar ?>" title="<?= $model->projectOwner->user->name ?>" alt="User Avatar">
        </div>
        <!-- /.widget-user-image -->
        <h3 class="widget-user-username"><?= $model->name ?></h3>
        <h5 class="widget-user-desc"><?= $model->description ?: '<br>' ?></h5>
    </div>
    <div class="box-footer no-padding">
        <ul class="nav nav-stacked">
            <li><a href="<?= Url::to(['content/index', 'project_id' => $model->id])?>"><?= Yii::t('app', 'Contents') ?><span class="pull-left badge bg-blue"><?= Yii::$app->formatter->asInteger($model->totalContents) ?></span></a></li>
            <li><a href="<?= Url::to(['channel/index', 'project_id' => $model->id])?>"><?= Yii::t('app', 'Channels') ?><span class="pull-left badge bg-aqua"><?= Yii::$app->formatter->asInteger($model->totalChannels) ?></span></a></li>
            <li><a href="<?= Url::to(['project-user/index', 'project_id' => $model->id])?>"><?= Yii::t('app', 'Members') ?><span class="pull-left badge bg-green"><?= Yii::$app->formatter->asInteger($model->totalUsers) ?></span></a>
            </li>
            <?php if (Yii::$app->user->can('updateProject')) : ?>
            <li>
                <br>

                <div class="col-md-12 col-sm-12 text-center">
                    <?= Html::a('<i class="fa fa-edit"></i>' . Yii::t('app', 'Edit'), ['update', 'id' => $model->id], [
                        'class' => 'btn btn-app showModalButton',
                        'style' => 'margin:0 0 10px 0 !important;',
                        'title' => Yii::t('app', 'Edit'),
                        'onclick' => 'return false;',
                        'data' => [
                            'url' => Url::to(['update', 'id' => $model->id]),
                        ]
                    ]) ?>
                    <?= Html::a('<i class="fa fa-key"></i>' . Yii::t('app', 'Auth Key'), ['auth-key', 'id' => $model->id], [
                        'class' => 'btn btn-app showModalButton',
                        'style' => 'margin:0 10px 10px 0 !important;',
                        'title' => Yii::t('app', 'Generate Auth Key'),
                        'onclick' => 'return false;',
                        'data' => [
                            'url' => Url::to(['auth-key', 'id' => $model->id]),
                            'title' => Yii::t('app', 'Generate Auth Key'),
                        ]
                    ]) ?>
                    <?= Html::a('<i class="fa fa-trash"></i>' . Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-app',
                        'style' => 'margin:0 10px 10px 0 !important;',
                        'title' => Yii::t('app', 'Delete'),
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            </li>
            <?php endif; ?>
        </ul>
    </div>
</div><!-- /.widget-user-2 -->
