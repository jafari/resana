<?php

use frontend\assets\ClipboardAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Project */
/* @var $form yii\widgets\ActiveForm */

$action = $model->auth_key ? Url::to(["project/delete-auth-key", 'id' => $model->id]) : Url::to(["project/add-auth-key", 'id' => $model->id]);
ClipboardAsset::register($this);

$this->registerJs(<<< JS
var clipboard = new Clipboard('.copy');
clipboard.on('success', function(e) {
    var copy = $('.copy');
    copy.html(copy.data('after'));
    e.clearSelection();
});
JS
);
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(['action' => $action, 'options' => ['id'=>'ajaxForm']]); ?>

    <?= $model->auth_key ? $form->field($model, 'auth_key')->textInput(['readonly' => true]) : '' ?>

    <div class="form-group">
        <?php if ($model->auth_key): ?>
            <?= Html::submitButton(Yii::t('app', 'Revoke Auth Key'), ['class' => 'btn btn-danger']) ?>
            <?= Html::button('<span class="glyphicon glyphicon-copy"></span> ' . Yii::t('app', 'Copy Token'), [
                'class' => 'btn btn-success copy',
                'data' => [
                    'clipboard-target' => '#project-auth_key',
                    'after' => '<span class="glyphicon glyphicon-copy"></span> ' . Yii::t('app', 'Copied!'),
                ],
            ])?>
        <?php else: ?>
            <?= Html::submitButton(Yii::t('app', 'Generate Auth Key'), ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>