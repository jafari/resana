<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var array $projects */
/* @var $project common\models\Project */
/* @var string $title */
?>

<h3 class="project-box-title"><i class="fa fa-dashboard "></i><?= $title ?></h3>
<?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Create new project'), '#', ['class' => 'btn btn-default showModalButton', 'data' => [
    'url' => Url::to(['project/create']),
    'title' => '<i class="fa fa-dashboard"></i> ' . Yii::t('app', 'Create Project'),
]]) ?>
<br>
<div class="row">
    <?php Pjax::begin(['id' => 'pjax']) ?>
    <?php if ($projects) : ?>
        <?php foreach ($projects as $project) : ?>
            <div class="col-md-4">
                <div class="box box-widget widget-user-2 my-project-box">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <a href="<?= Url::to(['project/view', 'id' => $project->id]) ?>" data-pjax="0">
                        <div class="widget-user-header bg-blue">
                            <h3 class="widget-user-username"><?= $project->name ?></h3>
                            <h5 class="widget-user-desc"><?= $project->description ?: '<br>' ?></h5>
                        </div>
                    </a>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header"><?= Yii::$app->formatter->asInteger($project->totalChannels) ?></h5>
                                    <span class="description-text"><?= Yii::t('app', 'Channel') ?></span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header"><?= Yii::$app->formatter->asInteger($project->totalContents) ?></h5>
                                    <span class="description-text"><?= Yii::t('app', 'Content') ?></span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div class="description-block">
                                    <h5 class="description-header"><?= Yii::$app->formatter->asInteger($project->totalUsers) ?></h5>
                                    <span class="description-text"><?= Yii::t('app', 'Member') ?></span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
            </div><!-- ./col -->
        <?php endforeach; ?>
    <?php endif; ?>
    <?php Pjax::end() ?>
</div>
