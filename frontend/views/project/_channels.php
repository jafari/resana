<?php
/**
 * @var $projectId
 * @var $channels
 */

use common\models\Channel;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

$channelName = function ($model) {
    return Html::a($model->name, [$model->actionName . '/view', 'id' => $model->id, 'project_id' => $model->project_id]);
};

$type = function ($model) {
    $icons = [
        Channel::TYPE_TELEGRAM_BOT => ['icon' => 'fa-simplybuilt', 'bg' => 'red'],
        Channel::TYPE_TELEGRAM_CHANNEL => ['icon' => 'fa-send', 'bg' => 'green'],
        Channel::TYPE_TWITTER => ['icon' => 'fa-twitter', 'bg' => 'aqua'],
    ];

    return Html::tag('span', Html::tag('i', '', ['class' => 'fa fa-fw ' . $icons[$model->type]['icon'], 'title' => $model->typeName]), ['class' => 'info-box-icon bg-'.$icons[$model->type]['bg']]);
};

$channelMembers = function ($model) {
    if ($model->members) {
        $title = Yii::t('app', ucfirst(Inflector::pluralize($model->memberType)));
        return Html::a('<i class="fa fa-users"></i> ' . $title, [$model->memberType . '-member/index', 'project_id' => $model->project_id, 'channel_id' => $model->id], ['class' => 'btn btn-default btn-xs']);
    }
    return '';
};

$groups = function ($model) {
    if ($model->groups) {
        return Html::a('<i class="fa fa-folder"></i> ' . Yii::t('app', 'Groups'), ['group/index', 'project_id' => $model->project_id, 'channel_id' => $model->id], ['class' => 'btn btn-default btn-xs']);
    }
    return '';
};

$hiddenButton = function ($name, $icon, $action, $model) use ($projectId) {
    $url = Url::to([$action, 'project_id' => $projectId, 'id' => $model->id]);
    $i = Html::tag('i', '', [
        'class' => 'fa fa-lg '. $icon,
        'data' => [
            'toggle' => 'tooltip',
            'original-title' => $name,
        ]
    ]);
    return Html::a($i, $url, [
        'class' => 'showModalButton',
        'onclick' => 'return false;',
        'data' => [
            'url' => $url,
            'title' => $name,
        ]
    ]);
};
?>

<ul class="todo-list ui-sortable route-dis">
    <?php foreach ($channels as $channel) : ?>
    <li>
        <div class="info-box">
            <?php if (Yii::$app->user->can('updateProject')) : ?>
            <div class="tools hidden-action">
                <?= $hiddenButton(Yii::t('app', 'Watermark'), 'fa-plug', 'channel/watermark', $channel); ?>
                <?= $hiddenButton(Yii::t('app', 'Signature'), 'fa-paw', 'channel/signature', $channel); ?>
                <?= $hiddenButton(Yii::t('app', 'Edit'), 'fa-edit', $channel->actionName.'/update', $channel); ?>
                <?= Html::a('<i class="fa fa-trash-o fa-lg" data-original-title="'. Yii::t('app', 'Delete') .'" data-toggle="tooltip"></i>', ['channel/delete', 'project_id' => $projectId, 'id' => $channel->id], [
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
            <?php endif; ?>
            <?= $type($channel) ?>
            <div class="info-box-content">
                <span class="info-box-text"><?= $channel->typeName ?></span>
                <span class="info-box-number"><?= $channelName($channel) ?></span>
                <?= $groups($channel) ?>
                <?= $channelMembers($channel) ?>
            </div>
            <!-- /.info-box-content -->
        </div>
    </li>
    <?php endforeach; ?>
</ul><!-- /.todo-list -->