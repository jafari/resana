<?php

use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\bootstrap\Html;
use common\models\Message;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\MessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $projectId */

$this->title = Yii::t('app', 'Messages');
$this->params['breadcrumbs'][] = $this->title;

$mediaType = function ($model) use ($projectId) {
    $iconClass = [
        Message::MEDIA_TYPE_TEXT => 'align-right',
        Message::MEDIA_TYPE_IMAGE => 'picture-o',
        Message::MEDIA_TYPE_VIDEO => 'video-camera',
        Message::MEDIA_TYPE_AUDIO => 'music',
        Message::MEDIA_TYPE_DOCUMENT => 'book',
    ];
    $title = Html::tag('span', '', ['class' => 'fa fa-fw fa-lg fa-' . $iconClass[$model->media_type]]);

    return Html::a($title, ['index', 'MessageSearch[media_type]' => $model->media_type, 'project_id' => $projectId]);
};

$status = function ($model) use ($projectId) {
    $statusClass = [
        Message::STATUS_ACTIVE => 'label-success',
        Message::STATUS_DISABLED => 'label-default',
    ];
    $title = Html::tag('span', $model->statusName, ['class' => 'text-md-size label ' . $statusClass[$model->status]]);
    return Html::a($title, ['index', 'MessageSearch[status]' => $model->status, 'project_id' => $projectId]);
};

$urlCreator = function ($action, $model, $key, $index) use ($projectId) {
    return Url::toRoute([
        $action,
        'project_id' => $projectId,
        'id' => $key,
    ]);
};

$channelLink = function ($model) use ($projectId) {
    $channel = $model->channel;

    return $channelLink = Html::a("{$channel->name} ({$channel->typeName})", [
        "{$channel->actionName}/view",
        'id' => $channel->id,
        'project_id' => $projectId,
    ]);
};
?>

<div class="content-index  box box-primary">
    <div class="box-body">
        <?php Pjax::begin(['id' => 'pjax']); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'options' => ['class' => 'table-responsive'],
            'tableOptions' => ['class' => 'table table-striped'],
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                ],
                [
                    'content' => function ($model) use ($projectId) {
                        $title = Html::tag('span', '', ['class' => 'fa fa-fw fa-lg fa-eye']);
                        $link = Url::to(['message/view', 'id' => $model->id, 'project_id' => $projectId]);
                        return Html::a($title, $link, ['data' => ['url' => $link], 'class' => 'showModalButton', 'onClick' => 'return false;']);
                    }
                ],
                [
                    'attribute' => 'media_type',
                    'filter' => Message::getMediaTypes(),
                    'format' => 'html',
                    'value' => $mediaType,
                    'label' => '',
                    'contentOptions' => ['class' => 'text-center'],
                ],
                [
                    'attribute' => 'text',
                    'value' => 'text',
                    'format' => 'html',
                    'contentOptions' => ['width' => '45%', 'dir' => 'auto'],
                ],
                [
                    'label' => Yii::t('app', 'Author'),
                    'attribute' => 'user',
                    'value' => 'member.name',
                ],
                [
                    'label' => Yii::t('app', 'Channels'),
                    'value' => $channelLink ?: '',
                    'format' => 'html',
                    'contentOptions' => ['width' => '15%'],
                ],
                [
                    'attribute' => 'status',
                    'filter' => Message::getAllStatus(),
                    'format' => 'html',
                    'value' => $status,
                    'contentOptions' => ['class' => 'text-center'],
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['class' => 'text-center text-bold', 'dir' => 'auto'],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'urlCreator' => $urlCreator,
                    'template' => '{delete}',
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
