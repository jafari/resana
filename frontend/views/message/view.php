<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Message;

/* @var $this yii\web\View */
/* @var $model common\models\Message */
/* @var $projectId */

$mediaUrl = $this->title = '';
$media = function ($mediaUrl) {
    return [
        Message::MEDIA_TYPE_IMAGE => Html::img($mediaUrl, ['style' => 'max-width: 100%; margin-bottom: 10px;']),
        Message::MEDIA_TYPE_VIDEO => Html::tag('video', '', ['src' => $mediaUrl, 'controls'=>'', 'style' => 'max-width: 100%; margin-bottom: 10px;']),
        Message::MEDIA_TYPE_AUDIO => Html::tag('audio', '', ['src' => $mediaUrl, 'controls'=>'', 'style' => 'max-width: 100%; margin-bottom: 10px;']),
        Message::MEDIA_TYPE_DOCUMENT => Html::a(Yii::t('app', 'Download'), $mediaUrl, ['class' => 'btn btn-success']),
    ];
};

if ($channel = $model->channel) {
    $channelLink = Html::a("{$channel->name} ({$channel->typeName})", ["{$channel->actionName}/view", 'id' => $channel->id, 'project_id' => $projectId]);
}
?>
<div class="content-view">
    <div class="row">
        <div class="col-md-7 col-sm-12">
            <?= $model->text ?>
            <?php
            if ($attachments = $model->attachments) {
                $attachments = json_decode($attachments);
                foreach ($attachments as $attachment) {
                    echo $media($attachment->url)[$attachment->media_type];
                }
            }
            ?>
        </div>
        <div class="col-md-5 col-sm-12">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute' => 'channels',
                        'label' => Yii::t('app', 'Channels'),
                        'value' => isset($channelLink) ? $channelLink : '',
                        'format' => 'html',
                        'visible' => isset($channelLink) ? true : false,
                    ],
                    [
                        'label' => Yii::t('app', 'Author'),
                        'value' => $model->member->getName(),
                    ],
                    [
                        'attribute' => 'media_type',
                        'value' => $model->mediaTypeName,
                    ],
                    [
                        'attribute' => 'type',
                        'value' => $model->typeName,
                    ],
                    [
                        'attribute' => 'status',
                        'value' => $model->statusName,
                    ],
                    [
                        'attribute' => 'created_at',
                        'format' => ['todaytime', 'Y/M/d @ H:m'],
                    ],
                    [
                        'attribute' => 'updated_at',
                        'format' => ['todaytime', 'Y/M/d @ H:m'],
                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>
