<?php

use common\models\ChannelMember;
use common\models\Chat;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ChatMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $projectId */
/* @var $channelId */

$this->title = Yii::t('app', 'Chats');
$this->params['breadcrumbs'][] = $this->title;

$urlCreator = function ($action, $model, $key, $index) use ($projectId, $channelId) {
    return Url::toRoute(array_merge([
        "chat-member/$action",
        'project_id' => $projectId,
    ], $key));
};
?>
<div class="channel-member-index box box-primary">
    <div class="box-body">

        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'options' => ['class' => 'table-responsive'],
            'tableOptions' => ['class' => 'table table-striped'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'label' => Yii::t('app', 'Name'),
                    'attribute' => 'chatName',
                    'value' => 'chat.name',
                    'contentOptions' => ['class' => 'text-bold'],
                ],
                [
                    'label' => Yii::t('app', 'Username'),
                    'attribute' => 'chatUsername',
                    'value' => 'chat.username',
                ],
                [
                    'label' => Yii::t('app', 'Type'),
                    'attribute' => 'chatType',
                    'value' => 'chat.typeName',
                    'filter' => Chat::getTypes()
                ],
                [
                    'attribute' => 'status',
                    'value' => 'statusName',
                    'filter' => ChannelMember::getAllStatus()
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['class' => 'text-center', 'dir' => 'auto'],
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                    'urlCreator' => $urlCreator,
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
