<?php

use common\models\User;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

$previewAvatar = Html::img($model->avatar, ['style' => 'max-width:200px;']);
?>

<div class="user-form">
    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'gender')->textInput()->radioList(User::getGenders()) ?>

    <?= $form->field($model, 'avatar')->widget(FileInput::className(), [
        'options'=>[
            'multiple'=> false
        ],
        'pluginOptions' => [
            'append' => true,
            'showRemove' => false,
            'showUpload' => false,
            'showClose' => true,
            'maxFileCount' => 1,
            'initialPreview' => $previewAvatar ? $previewAvatar : '',
            //'initialPreviewConfig' => empty($previewsConfig) ? '' : $previewsConfig,
        ],
    ]); ?>

    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-3">
            <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
