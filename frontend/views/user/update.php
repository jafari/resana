<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = Yii::t('app', 'Edit Profile');
$this->params['breadcrumbs'][] = Yii::t('app', 'Edit Profile');
?>
<div class="user-update box box-primary">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
