<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Channel */
/* @var $form yii\widgets\ActiveForm */
$action = Url::to(["channel/signature", 'id' => $model->id, 'project_id' => $project_id]);

$this->registerJs(<<< JS
$(document).ready(function() {
    $('#btn-delete-signature').click(function() {
        $('#channel-signature').val('');
        $('#ajaxForm').submit();
    })
});
JS
);
?>
<div class="project-form">
    <?php $form = ActiveForm::begin(['action' => $action, 'options' => ['id'=>'ajaxForm']]); ?>
        <?= $form->field($model, 'signature')->textarea() ?>
        <div class="form-group">
            <?= Html::submitButton($model->signature ? Yii::t('app', 'Update signature') : Yii::t('app', 'Create signature'), ['class' => $model->signature ? 'btn btn-primary' : 'btn btn-success']) ?>

            <?= $model->signature ? Html::button(Yii::t('app', 'Delete signature'), ['class' => 'btn btn-danger', 'id' => 'btn-delete-signature']) : null; ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>