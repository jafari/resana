<?php
/* @var $this yii\web\View */
/* @var $model common\models\Channel */
/* @var $projectId */

$this->title = Yii::t('app', 'Add Channel');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Channels'), 'url' => ['index', 'project_id' => $projectId]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-create">
    <?= $this->render('_form', [
        'model' => $model,
        'projectId' => $projectId,
    ]) ?>
</div>
