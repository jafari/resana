<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $project common\models\Project */

$this->title = Yii::t('app', '{project} Channels', ['project' => $project->name]);
$this->params['breadcrumbs'][] = $this->title;
$projectId = $project->id;

$urlCreator = function ($action, $model, $key, $index) use ($projectId) {
    $url = $action === 'delete' ? "channel/$action" : $model->actionName . '/' . $action;
    return Url::toRoute([
        $url,
        'project_id' => $projectId,
        'id' => $key
    ]);
};
?>
<div class="channel-index box box-primary">
    <div class="box-body">
        <?php if (Yii::$app->user->can('updateProject')) : ?>
        <p>
            <?= Html::a(Yii::t('app', 'Add Channel'), ['channel/create', 'project_id' => $project->id], [
                'class' => 'btn btn-success showModalButton',
                'onClick' => 'return false;',
                'data' => [
                    'url' => Url::to(['channel/create', 'project_id' => $project->id]),
                    'title' => Yii::t('app', 'Add Channel')
                ]
            ]) ?>
        </p>
        <?php endif; ?>

        <?php Pjax::begin(['id' => 'pjax']); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'options' => ['class' => 'table-responsive'],
            'tableOptions' => ['class' => 'table table-striped'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'name',
                    'format' => 'html',
                    'value' => function ($model) {
                        return Html::tag('strong', Html::a($model->name, [$model->actionName . '/view', 'id' => $model->id, 'project_id' => $model->project_id]));
                    }
                ],
                'typeName',
                [
                    'attribute' => 'created_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['class' => 'text-center text-bold', 'dir' => 'auto'],
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{signature} {update} {delete}',
                    'buttons' => [
                        'signature' => function ($url, $model) {
                            $options = [
                                'onClick' => 'return false',
                                'data-pjax' => '0',
                                'class' => 'showModalButton',
                                'data-title' => Yii::t('app', 'Add/Update signature'),
                                'data-url' => Url::to(['channel/signature', 'id' => $model->id, 'project_id' => $model->project_id]),
                                'aria-label' => Yii::t('app', 'signature'),
                            ];
                            $url = Url::to(['Channel/signature', 'id' => $model->id, 'project_id' => $model->project_id]);

                            return Html::a('<span class="fa fa-flag"></span>', $url, $options);
                        }
                    ],
                    'urlCreator' => $urlCreator,
                    'visibleButtons' => [
                        'update' => function ($model) {
                            return $model->actionName !== 'twitter';
                        }
                    ],
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>

</div>
