<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\color\ColorInput;
use kartik\slider\Slider;
use kartik\file\FileInput;
use common\models\WatermarkForm;

// Basic horizontal slider with ActiveForm. Check the model validation for values 
// > 5 or < 1. You can also use arrow keys to navigate.


/* @var $this yii\web\View */
/* @var $model common\models\WatermarkForm */
/* @var $form ActiveForm */
/* @var int $project_id */

$this->title = Yii::t('app', 'Watermark');
$deleteUrl = Url::to(['channel/delete-watermark', 'id' => $id, 'project_id' => $project_id]);
$this->registerJs(<<< JS
$(document).ready(function() {
    jQuery.fn.exists = function(){return this.length>0;}
    var action = $('.nav.nav-tabs .active a').attr('data-url');
    $('#ajaxForm').attr('action', action);
    $(document).on('click', '.nav.nav-tabs a', function () {
        var action = $('.nav.nav-tabs .active a').attr('data-url');
        $('#ajaxForm').attr('action', action);
    });
    $('#btn-delete-watermark').click(function() {
        $('#ajaxForm').attr('action', "{$deleteUrl}");
        $('#ajaxForm').submit();
    })
});
JS
);
$fontPathArial = '@frontend/web/fonts/arial.ttf';
$fontPathVazir = '@frontend/web/fonts/Vazir.ttf';
$fontPathTimes = '@frontend/web/fonts/times.ttf';
?>

<?php $form = ActiveForm::begin(['action' => '', 'options' => ['id'=>'ajaxForm', 'project_id' => $project_id, 'enctype' => 'multipart/form-data']]); ?>

<?= $form->field($model, 'position')->dropDownList(WatermarkForm::getPositions())->label(Yii::t('app', 'Position')) ?>

<?= $form->field($model, 'size')->dropDownList(WatermarkForm::getSizes())->label(Yii::t('app', 'Size')) ?>

<?= $form->field($model, 'transparency')->widget(Slider::classname(), [
    'pluginOptions'=>[
        'min' => 1,
        'max' => 100,
        'step' => 1
    ]
])->label(Yii::t('app', 'Transparency')); ?>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li id="text-tab" class="<?= $model->isImage() ? '' : 'active' ?>"><?= Html::a(Yii::t('app', "Text"), '#text', ['data' => ['url' => Url::to(['channel/watermark', 'id' => $id, 'type' => 'text', 'project_id' => $project_id]), 'toggle' => 'tab']]) ?></li>
        <li id="img-tab" class="<?= $model->isImage() ? 'active' : '' ?>"><?= Html::a(Yii::t('app', "Image"), '#img', ['data' => ['url' => Url::to(['channel/watermark', 'id' => $id, 'type' => 'image', 'project_id' => $project_id]), 'toggle' => 'tab']]) ?></li>
    </ul>
    <div class="tab-content">
        <div id="text" class="tab-pane fade <?= $model->isImage()? '' : 'in active' ?>">
            <div class="text-watermark-form">
                <?= $form->field($model, 'text')->textarea(['maxlength' => true])->label(false) ?>
                <?= $form->field($model, 'color')->widget(ColorInput::classname(), [
                    'options' => ['placeholder' => Yii::t('app', 'Select color ...')],
                    'pluginOptions' => [
                        'showAlpha' => false,
                    ],
                ])->label(Yii::t('app', 'Color')) ?>
                <?= $form->field($model, 'font')->dropDownList([
                    $fontPathArial => Yii::t('app', 'Arial'),
                    $fontPathTimes => Yii::t('app', 'Times'),
                    $fontPathVazir => Yii::t('app', 'Vazir'),
                ])->label(Yii::t('app', 'Font'))?>
            </div>
        </div>
        <div id="img" class="tab-pane fade <?= $model->isImage()? 'in active' : '' ?>">
            <?= $form->field($model, 'isPreview')->hiddenInput(['class'=>'preview-image', 'value' => ($model->imageUrl) ? 'Preview' : '' ])->label(false); ?>
            <?= $form->field($model, 'image')->widget(FileInput::className(), [
                'options'=>[
                    'accept' => '.png',
                    'multiple'=> true,
                    'enctype' => 'multipart/form-data',
                ],
                'pluginEvents' => [
                    'fileclear' => "function() { $('.preview-image').attr('value', ''); }",
                    'fileselect' => "function() { $('.preview-image').attr('value', ''); }",
                ],
                'pluginOptions' => [
                    'showUpload' => false,
                    'showClose' => true,
                    'maxFileCount' => 1,
                    'initialPreview'=> $model->isImage()? [
                    Html::img($model->imageUrl, ['class'=>'file-preview-image', 'style' => 'width: 100%;']),
                    ] : false,
                    'overwriteInitial' => true
                ],
            ])->label(false); ?>
            <div id="image-holder"></div>
        </div>
    </div>
</div>

<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>

<?= Html::button(Yii::t('app', 'Delete Watermark'), ['id' => 'btn-delete-watermark', 'class' => 'btn btn-danger']) ?>

<?php ActiveForm::end(); ?>
