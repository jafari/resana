<?php

use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Channel */
/* @var $form yii\widgets\ActiveForm */
/* @var $projectId */

$items = [
    Url::to(['bot/create', 'project_id' => $projectId]) => Yii::t('app', 'Telegram Bot'),
    Url::to(['telegram-channel/create', 'project_id' => $projectId]) => Yii::t('app', 'Telegram Channel'),
    Url::to(['twitter/create', 'project_id' => $projectId]) => Yii::t('app', 'Twitter'),
    'facebook' => Yii::t('app', 'Facebook'),
];

$this->registerJs(<<<JS
var radio = $('input[name="channel-type"]');
function formLoad() {
    $.ajax({
        type: 'GET',
        url: this.value,
        success: function(data, status, xhr) {
            $('#channel-form').html(data);
        }
    });
};
radio.on('click', formLoad);
document.onload = $('input[name="channel-type"]:checked').click();
JS
);
?>

<div class="content-form">
    <p>
        <?= Html::radioList('channel-type', Url::to(['bot/create', 'project_id' => $projectId]), $items, [
            'item' => function ($index, $label, $name, $checked, $value) {
                $disabled = ($index > 2) ? true : false;
                return Html::tag('label', Html::radio($name, $checked, ['value' => $value, 'disabled' => $disabled]) . ' ' . $label, ['class' => $disabled ? 'text-gray' : '']);
            }
        ]);
        ?>
    </p>
    <div id="channel-form"></div>
</div>