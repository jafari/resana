<?php

use common\models\Project;
use common\models\ProjectUser;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ProjectUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $project common\models\Project */

$this->title = Yii::t('app', '{project} Members', ['project' => $project->name]);
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-10 col-md-offset-1 post-list-content">
        <div class="col-md-4">
                <div class="box box-primary user-box">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="http://nmedia.ncdn.dev/v1/image/E3Fj-HubVO9iSPArtIDzuALUMC7h1e-GE71Tr1IM7b0/s/w200h200-c" alt="User profile picture">
                        <h3 class="profile-username text-center">رامین جعفری</h3>
                        <p class="text-muted text-center">مالک</p>
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <p data-toggle="tooltip" data-original-title="زمان ایجاد"> ۲۸ تیر ۱۳۹۵ ه‍.ش.،‏ ۱۰:۱۰:۰۷</p>
                            </li>
                            <li class="list-group-item">
                                <p data-toggle="tooltip" data-original-title="ایمیل">raminjfariam@gmail.com</p>
                            </li>
                        </ul>
                        <a href="#" class="btn btn-danger btn-block"><b>حذف</b></a>
                    </div>
                    <!-- /.box-body -->
                </div><!-- /.user-box -->
            </div><!-- /.col-md-4 -->

            <div class="col-md-4">
                <div class="box box-primary user-box">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="http://nmedia.ncdn.ir/v1/image/fbSx4JqCl7ul3YZgUZTGwfVdfyzuRcpMWH88bIZaYWoILHeGUpxDdA/s/w128h128-c" alt="User profile picture">

                        <h3 class="profile-username text-center">علی ایرانی</h3>

                        <p class="text-muted text-center">مدیر</p>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <p data-toggle="tooltip" data-original-title="زمان ایجاد"> ۱۱ تیر ۱۳۹۵ ه‍.ش.،‏ ۱۰:۱۰:۰۷</p>
                            </li>
                            <li class="list-group-item">
                                <p data-toggle="tooltip" data-original-title="ایمیل">a.irani@gmail.com</p>
                            </li>
                        </ul>

                        <a href="#" class="btn btn-danger btn-block"><b>حذف</b></a>
                    </div>
                    <!-- /.box-body -->
                </div><!-- /.user-box -->
            </div><!-- /.col-md-4 -->

            <div class="col-md-4">
                <div class="box box-primary user-box">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="http://nmedia.ncdn.ir/v1/image/h9OxO7k6awUm3S7UKmPHKF7bYthAuiq_uV4y7jAYM9erDP7IPqE7AQ/s/w128h128-c" alt="User profile picture">

                        <h3 class="profile-username text-center">رضا نجفی</h3>

                        <p class="text-muted text-center">مدیر</p>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <p data-toggle="tooltip" data-original-title="زمان ایجاد"> ۵ تیر ۱۳۹۵ ه‍.ش.،‏ ۰۹:۰۴:۰۷</p>
                            </li>
                            <li class="list-group-item">
                                <p data-toggle="tooltip" data-original-title="ایمیل">r.najafi@gmail.com</p>
                            </li>
                        </ul>

                        <a href="#" class="btn btn-danger btn-block"><b>حذف</b></a>
                    </div>
                    <!-- /.box-body -->
                </div><!-- /.user-box -->
            </div><!-- /.col-md-4 -->
    </div>
</div>
