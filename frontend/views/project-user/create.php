<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProjectUser */

$this->title = Yii::t('app', 'Create Project User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Project Users'), 'url' => ['index', 'project_id' => $model->project_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-user-create box box-primary">

    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>
