<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ProjectUser */

$this->title = $model->user->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Project Users'), 'url' => ['index', 'project_id' => $model->project_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-user-view box box-primary">

    <div class="box-body">
        <p>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'project_id' => $model->project_id, 'user_id' => $model->user_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'label' => Yii::t('app', 'Project Name'),
                    'attribute' => 'project.name',
                ],
                [
                    'label' => Yii::t('app', 'User Name'),
                    'attribute' => 'user.name',
                ],
                'user.name',
                [
                    'label' => Yii::t('app', 'Role'),
                    'attribute' => 'roleName',
                ],
                'created_at:todayTime',
                'updated_at:todayTime',
            ],
        ]) ?>
    </div>

</div>
