<?php

use common\models\Project;
use common\models\ProjectUser;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ProjectUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $project common\models\Project */

$this->title = Yii::t('app', '{project} Members', ['project' => $project->name]);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-user-index box box-primary">

    <div class="box-body">
        <p>
            <?= Html::a(Yii::t('app', 'Add Member'), ['project-user/create', 'project_id' => $project->id], [
                'class' => 'showModalButton btn btn-success',
                'onclick' => 'return false;',
                'data' => [
                    'title' => '<i class="fa fa-user"></i> '.Yii::t('app', 'Add Member'),
                    'url' => Url::to(['project-user/create', 'project_id' => $project->id]),
                ]
            ]); ?>
        </p>

        <?php Pjax::begin(['id' => 'pjax']); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'options' => ['class' => 'table-responsive'],
            'tableOptions' => ['class' => 'table table-striped'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'content' => function ($model) {
                        return Html::img($model->user->getAvatar(70, 70));
                    },
                ],
                [
                    'attribute' => 'user.name',
                    'label' => Yii::t('app', 'User Name'),
                    'format' => 'html',
                    'value' => function ($model) {
                        return Html::tag('strong', Html::a($model->user->name, ['view', 'user_id' => $model->user_id, 'project_id' => $model->project_id]));
                    }
                ],
                'user.email',
                [
                    'attribute' => 'role',
                    'value' => 'roleName',
                    'filter' => ProjectUser::getRoles()
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['class' => 'text-center text-bold', 'dir' => 'auto'],
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>

</div>
