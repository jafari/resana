<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Bot */

$this->title = Yii::t('app', 'Create Bot');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Channels'), 'url' => ['channel/index', 'project_id' => $model->projectId]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bot-create box box-primary">

    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
            'projectId' => $projectId,
        ]) ?>
    </div>

</div>
