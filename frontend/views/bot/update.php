<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Bot */
/* @var $projectId */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Bot'),
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Channels'), 'url' => ['channel/index', 'project_id' => $projectId]];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->channel_id, 'project_id' => $projectId]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="bot-update box box-primary">

    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
            'projectId' => $projectId,
        ]) ?>
    </div>

</div>
