<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Bot */
/* @var $projectId */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Channels'), 'url' => ['channel/index', 'project_id' => $projectId]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bot-view box box-primary">

    <div class="box-body">
        <?php if (Yii::$app->user->can('updateProject')) : ?>
        <p>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->channel_id, 'project_id' => $projectId], [
                'class' => 'showModalButton btn btn-primary',
                'onclick' => 'return false;',
                'data' => [
                    'url' => Url::to(['update', 'id' => $model->channel_id, 'project_id' => $projectId]),
                ]
            ]); ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['channel/delete', 'id' => $model->channel_id, 'project_id' => $projectId], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <?php endif; ?>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'name',
                [
                    'attribute' => 'username',
                    'value' => Html::a('@' . $model->username, 'https://telegram.me/' . $model->username),
                    'format' => 'html'
                ],
                [
                    'label' => Yii::t('app', 'Owners'),
                    'attribute' => 'owner',
                    'value' => implode(', ', Json::decode($model->owner)),
                ],
                'channel.created_at:todayTime',
            ],
        ]) ?>
    </div>

</div>
