<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Bot */
/* @var $form yii\widgets\ActiveForm */
/* @var $projectId */

$action = $model->isNewRecord ? Url::to(["bot/create", 'project_id' => $projectId]) : Url::to(["bot/update", 'id' => $model->channel_id, 'project_id' => $projectId]);
?>

<div class="bot-form">

    <?php $form = ActiveForm::begin(['action' => $action, 'options' => ['id'=>'ajaxForm']]); ?>

    <?= $form->field($model, 'token')->textInput(['maxlength' => true, 'dir' => 'ltr'])
        ->hint(Yii::t('app', 'What is security token? How to create a bot on Telegram?') . ' ' . Html::a(Yii::t('app', 'See Here'), 'https://core.telegram.org/bots#3-how-do-i-create-a-bot')) ?>
    <?= $form->field($model, 'owners')->widget(Select2::className(), [
        'showToggleAll' => false,
        'options' => [
            'placeholder' => Yii::t('app', 'Enter owner usernames ...'),
            'multiple' => true
        ],
        'pluginOptions' => [
            'tags' => true,
        ],
    ])->hint(Yii::t('app', "Enter usernames of bot owners in Telegram")); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
