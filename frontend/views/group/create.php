<?php


/* @var $this yii\web\View */
/* @var $model common\models\Group */

$this->title = Yii::t('app', 'Create Group');
$actionParams = $this->context->actionParams;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Groups'), 'url' => ['index', 'project_id' => $actionParams['project_id'], 'channel_id' => $actionParams['channel_id']]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-create  box box-primary">

    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>
