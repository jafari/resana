<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Group */

$this->title = $model->name;
$actionParams = $this->context->actionParams;
$urlParams = ['project_id' => $actionParams['project_id'], 'channel_id' => $actionParams['channel_id']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Groups'), 'url' => array_merge(['index'], $urlParams)];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-view box box-primary">
    <div class="box-body">
        <p>
            <?php if (Yii::$app->user->can('updateProject')): ?>
                <?= Html::a(Yii::t('app', 'Update'), array_merge(['update', 'id' => $model->id], $urlParams), ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Delete'), array_merge(['delete', 'id' => $model->id], $urlParams), [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            <?php endif; ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'name',
                [
                    'label' => Yii::t('app', 'Type'),
                    'attribute' => 'typeName',
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['todaytime', 'Y/M/d - H:m'],
                ]
            ],
        ]) ?>
    </div>
</div>

<?= $this->render("_{$model->channel->memberType}_members_list", ['model' => $model]) ?>
