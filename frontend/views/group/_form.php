<?php

use common\models\Group;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Group */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="group-form row">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12">
        <?php $form->errorSummaryCssClass = 'callout callout-danger'; ?>
        <?= $form->errorSummary($model) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'type')->dropDownList(Group::getTypes()) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'membersList')->checkboxList($model->channel->getChannelMembersList()) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
