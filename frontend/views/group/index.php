<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $projectId */
/* @var $channelId */

$this->title = Yii::t('app', 'Groups');
$this->params['breadcrumbs'][] = $this->title;

$urlCreator = function ($action, $model, $key, $index) use ($channelId, $projectId) {
    return Url::toRoute([
        $action,
        'project_id' => $projectId,
        'channel_id' => $channelId,
        'id' => $key
    ]);
};

$name = function ($model) use ($projectId, $channelId) {
    return Html::a($model->name, [
        'view',
        'id' => $model->id,
        'project_id' => $projectId,
        'channel_id' => $channelId,
    ], ['class' => 'text-bold']);
};
?>
<div class="group-index box box-primary">

    <div class="box-body">
        <?php if (Yii::$app->user->can('updateProject')): ?>
        <p>
            <?= Html::a(Yii::t('app', 'Create Group'), ['group/create', 'project_id' => $projectId, 'channel_id' => $channelId], ['class' => 'btn btn-success']) ?>
        </p>
        <?php endif ?>

        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'options' => ['class' => 'table-responsive'],
            'tableOptions' => ['class' => 'table table-striped'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'name',
                    'format' => 'html',
                    'value' => $name,
                ],
                [
                    'attribute' => 'type',
                    'value' => 'typeName',
                ],
                'membersCount:integer',
                [
                    'attribute' => 'created_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['class' => 'text-center text-bold', 'dir' => 'auto'],
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'urlCreator' => $urlCreator,
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>

</div>
