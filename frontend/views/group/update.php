<?php

/* @var $this yii\web\View */
/* @var $model common\models\Group */

$this->title = Yii::t('app', 'Edit Group');
$actionParams = $this->context->actionParams;
$urlParams = ['project_id' => $actionParams['project_id'], 'channel_id' => $actionParams['channel_id']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Groups'), 'url' => array_merge(['index'], $urlParams)];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => array_merge(['view', 'id' => $model->id], $urlParams)];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="group-update box box-primary">

    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>
