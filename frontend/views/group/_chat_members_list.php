<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = Yii::t('app', 'Groups');
$this->params['breadcrumbs'][] = $this->title;
?>
<h3><?= Yii::t('app', 'Chats') ?></h3>
<div class="members-list-index box box-primary">
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => new ArrayDataProvider([
                'models' => $model->members
            ]),
            'options' => ['class' => 'table-responsive'],
            'tableOptions' => ['class' => 'table table-striped'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'chat.name',
                    'label' => Yii::t('app', 'Name'),
                ],
                [
                    'attribute' => 'chat.username',
                    'label' => Yii::t('app', 'Username'),
                ],
                [
                    'attribute' => 'chat.type',
                    'value' => 'chat.typeName',
                    'label' => Yii::t('app', 'Type'),
                ],
            ],
        ]); ?>
    </div>
</div>
