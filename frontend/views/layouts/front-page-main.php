<?php
use frontend\assets\FrontPageAsset;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

$asset = FrontPageAsset::register($this);
?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="designer" content="ramin jafari twitter:@raminps3">
    <meta name="copyright" content="noghteh.ir">

    <meta property="og:type" content="website">
    <meta property="og:title" content="رسانا سامانه توزیع پیامک">
    <meta property="og:description" content="سرویسی برای ارتباط،مدیریت و انتشار آسان پیام ها در شبکه های اجتماعی تلگرام،فیس بوک و توئیتر">
    <meta property="og:url" content="http://resana.im/">
    <meta name="theme-color" content="#311b92">
    <link rel="shortcut icon" sizes="196x196" href="<?= $asset->baseUrl ?>/images/icon-196x196.png">
	<meta name="mobile-web-app-capable" content="yes">
    <?= Html::csrfMetaTags() ?>
    <title>سامانه توزیع پیام رسانا</title>
    <?php $this->head() ?>
</head>
<body class="viewing_1">
<?php $this->beginBody() ?>

<!-- Main-Page -->
<header id="header">
    <section id="main_bg"></section>
    <!-- <nav id="arrow_down"></nav> -->
    <nav id="main_menu">
        <div class="menu_icon">
            <div class="close_line"></div>
        </div>
        <div class="button-main">
            <figure id="ic_logo">

            </figure>

            <span>سرویسی برای ارتباط،مدیریت و انتشار آسان پیام ها در شبکه های اجتماعی تلگرام،فیس بوک و توئیتر</span>
            <div class="all_sub">
                <li class="nav_li"><a class="nav_links sub_links" href="<?= Url::to(['site/auth', 'authclient' => 'shenasa']) ?>">ورود با شناسا</a></li>
                <li class="nav_li " ><a class="nav_links sub_links" href="<?= Url::to(['site/auth', 'authclient' => 'shenasa']) ?>">عضویت</a></li>
                <li class="nav_li resana" data-linked-page="2"><a href="#about" class="nav_links sub_links" style="background:#4b36a7">رسانا</a></li>
                <li class="nav_li feature" data-linked-page="3"><a href="#feature" class="nav_links sub_links" style="background:#897bc5">امکانات</a></li>
            </div>

            <!--<form method="post"  class="newsletter">

                <input type="email" name="email"  placeholder="از راه اندازی رسانا با خبر شوید" required=""/>
                <button class="submit">ثبت</button>
            </form>-->
        </div>
        <blockquote>
            <div class="permit">متولد شده در آزمایشگاه<strong><a href="http://www.noghteh.ir"> نقطه</a></strong></div>
            <div class="quoter">سامانه توزیع پیامک رسانا سال 1394</div>
        </blockquote>
        <ul class="main_nav" >

            <li class="nav_li" data-linked-page="1">
                <a href="#home" class="nav_links">خانه</a>
            </li>
            <li class="nav_li" data-linked-page="2">
                <a href="#about" class="nav_links">آشنایی</a>
            </li>
            <li class="nav_li" data-linked-page="3">
                <a href="#feature" class="nav_links">امکانات</a>
            </li>
            <li class="nav_li" data-linked-page="5">
                <a href="#price" class="nav_links">تعرفه ها</a>
            </li>
            <li class="nav_li" data-linked-page="7">
                <a href="#contact" class="nav_links">تماس با ما</a>
            </li>



        </ul>
    </nav>
</header>
<section class="side_page side_page-2 page_side">
    <div class="register_wrapper">

        <div class="register_body">
            <form method="post" id="registration_form" class="form">
                <input type="email" name="email"  placeholder="ایمیل" required=""/>
                <button class="submit">ثبت</button>
            </form>
        </div>
        <div class="register_title">
            <h3>عضویت<br>در خبرنامه</h3>
        </div>
        <div class="register_response_wrapper">
            <div class="register_response_box">با تشکر ایمیل شما با موفقیت ثبت شد.</div>
            <div class="register_retry">تلاش دوباره</div>
        </div>
    </div>
</section>
<section class="main">

    <!-- Page(1) -->
    <section class="pages page_1">
    </section>

    <!-- Page(2) -->
    <section class="pages page_2">
        <article class="p2_main">
            <h2><img src="<?= $asset->baseUrl ?>/images/Resana-logo.svg"></h2>
            <div class="line_wrapper">
                <p class="p2_paragraph">رساناامکان مدیریت آسان و منظم ومتمرکز اکانت های شبکه های اجتماعی و پیام رسان ها را در فضای کاملا گرافیکی برای شما فراهم کرده است.<br>
                    با وب اپ رسانا تنها با چند کلیک ساده می توانید پیام خود را ایجاد و در شبکه های اجتماعی (توئیتر ، فیس بوک و ...) پیام رسان تلگرام و در قالب پیامک در کمترین زمان ممکن ارسال کنید.
                    ما با فراهم کردن سرویس های متنوع بر بستر به روزترین تکنولوژی ها قصد داریم مدیریت توزیع پیام ها را برای شما آسان تر کنیم.

                </p>
            </div>


        </article>
    </section>

    <!-- Page(3) -->
    <section class="pages page_3 hand">
        <h2 class="p3_title">امکانات</h2>
        <div class="resana_slider">
            <article class="slides">
                <figure class="pic_wrapper">
                    <img src="<?= $asset->baseUrl ?>/images/1.png" alt="" class="face">
                </figure>
                <div class="info">
                    <h3 class="headings">ایجاد چند ربات به صورت همزمان</h3>

                </div>
            </article>
            <article class="slides">
                <figure class="pic_wrapper">
                    <img src="<?= $asset->baseUrl ?>/images/2.png" alt="" class="face">
                </figure>
                <div class="info">
                    <h3 class="headings">پنل مدیریت جداگانه برای هر ربات</h3>

                </div>
            </article>
            <article class="slides">
                <figure class="pic_wrapper">
                    <img src="<?= $asset->baseUrl ?>/images/3.png" alt="" class="face">
                </figure>
                <div class="info">
                    <h3 class="headings">دسته بندی موضوعی محتوا</h3>

                </div>
            </article>
            <article class="slides">
                <figure class="pic_wrapper">
                    <img src="<?= $asset->baseUrl ?>/images/4.png" alt="" class="face">
                </figure>
                <div class="info">
                    <h3 class="headings">ارسال موضوعی محتوا</h3>

                </div>
            </article>
            <article class="slides">
                <figure class="pic_wrapper">
                    <img src="<?= $asset->baseUrl ?>/images/1.png" alt="" class="face">
                </figure>
                <div class="info">
                    <h3 class="headings">ایجاد گروه های مختلف کاربری</h3>

                </div>
            </article>
            <article class="slides">
                <figure class="pic_wrapper">
                    <img src="<?= $asset->baseUrl ?>/images/1.png" alt="" class="face">
                </figure>
                <div class="info">
                    <h3 class="headings">ارسال به گروه های مختلف کاربری</h3>

                </div>
            </article>
            <article class="slides">
                <figure class="pic_wrapper">
                    <img src="<?= $asset->baseUrl ?>/images/1.png" alt="" class="face">
                </figure>
                <div class="info">
                    <h3 class="headings">دیلیوری پیام</h3>

                </div>
            </article>
            <article class="slides">
                <figure class="pic_wrapper">
                    <img src="<?= $asset->baseUrl ?>/images/1.png" alt="" class="face">
                </figure>
                <div class="info">
                    <h3 class="headings">آمار ارسال و دریافت پیام ها</h3>

                </div>
            </article>
            <article class="slides">
                <figure class="pic_wrapper">
                    <img src="<?= $asset->baseUrl ?>/images/1.png" alt="" class="face">
                </figure>
                <div class="info">
                    <h3 class="headings">دریافت پام و امکان پاسخ گویی</h3>

                </div>
            </article>
            <article class="slides">
                <figure class="pic_wrapper">
                    <img src="<?= $asset->baseUrl ?>/images/1.png" alt="" class="face">
                </figure>
                <div class="info">
                    <h3 class="headings">دارا بودن دستورات پیش فرض</h3>

                </div>
            </article>
            <article class="slides">
                <figure class="pic_wrapper">
                    <img src="<?= $asset->baseUrl ?>/images/1.png" alt="" class="face">
                </figure>
                <div class="info">
                    <h3 class="headings">امکان ایجاد دستورات دلخواه</h3>

                </div>
            </article>
            <article class="slides">
                <figure class="pic_wrapper">
                    <img src="<?= $asset->baseUrl ?>/images/1.png" alt="" class="face">
                </figure>
                <div class="info">
                    <h3 class="headings">ارسال متن ، صدا ، عکس ، فیلم و فایل داکیومنت</h3>

                </div>
            </article>
            <article class="slides">
                <figure class="pic_wrapper">
                    <img src="<?= $asset->baseUrl ?>/images/1.png" alt="" class="face">
                </figure>
                <div class="info">
                    <h3 class="headings">بدون نیاز به برنامه </h3>

                </div>
            </article>
            <article class="slides">
                <figure class="pic_wrapper">
                    <img src="<?= $asset->baseUrl ?>/images/1.png" alt="" class="face">
                </figure>
                <div class="info">
                    <h3 class="headings">ذخیره مکالمات هر ربات و نمایش آن در پنل مدیریت</h3>

                </div>
            </article>


            <nav class="slider_nav">
                <div class="backward">
                    <svg xml:space="preserve" preserveAspectRatio="xMidYMax meet" viewBox="0 0 30 80">
                            <polyline fill="none" points="30,0 0,40 30,80"/>
                        </svg>
                </div>
                <div class="forward">
                    <svg xml:space="preserve" preserveAspectRatio="xMidYMax meet" viewBox="0 0 30 80">
                            <polyline fill="none" points="0,0 30,40 0,80"/>
                        </svg>
                </div>
            </nav>
        </div>

    </section>

    <!-- Page(4) -->
    <section class="pages page_4">
        <h2> صرفه جویی در زمان و هزینه و ...</h2>
    </section>

    <!-- Page(5) -->
    <section class="pages page_5">
        <div class="mol_5"></div>
        <article class="p5_main">
            <!-- <h2>Quattro</h2> -->
            <h2>تعرفه ها</h2>
            <div class="line_wrapper">
                <p class="p5_paragraph">
                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیستری را برای طراحان رایانه ای و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان.
                </p>
            </div>
            <aside class="ball_wapper">
                <div class="balls ball_1" data-index="1" >
                    <div class="ball_cover">
                        <i>1</i>
                        <h4>طلایی</h4>
                    </div>
                </div>
                <div class="balls ball_2" data-index="2">
                    <div class="ball_cover">
                        <i>2</i>
                        <h4>نقره ای</h4>
                    </div>
                </div>
                <div class="balls ball_3" data-index="3">
                    <div class="ball_cover">
                        <i>3</i>
                        <h4>برنز</h4>
                    </div>
                </div>
                <div class="balls ball_4" data-index="4" >
                    <div class="ball_cover">
                        <i>1</i>
                        <h4>رایگان</h4>
                    </div>
                </div>

                <article class="balls_article b1_article">
                    <span class="balls_close_icon"></span>
                    <h3>طرح طلایی</h3>
                    <p class="balls_paragraph">

                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیستری را برای طراحان رایانه ای و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.

                    </p>
                    <h5><q>پیشنهاد رسانا برای شما</q></h5>
                </article>
                <article class="balls_article b2_article">
                    <span class="balls_close_icon"></span>
                    <h3>طرح تقره ای</h3>
                    <p class="balls_paragraph">
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیستری را برای طراحان رایانه ای و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
                    </p>
                    <h5><q></q></h5>
                </article>
                <article class="balls_article b3_article">
                    <span class="balls_close_icon"></span>
                    <h3>طرح برنز</h3>
                    <p class="balls_paragraph">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیستری را برای طراحان رایانه ای و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.

                    </p>
                    <h5><q></q></h5>
                </article>
                <article class="balls_article b4_article">
                    <span class="balls_close_icon"></span>
                    <h3>طرح رایگان</h3>
                    <p class="balls_paragraph">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیستری را برای طراحان رایانه ای و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.

                    </p>
                    <h5><q></q></h5>
                </article>

            </aside>
        </article>
    </section>
    <!-- Page(6) -->
    <section class="pages page_6">
        <h2>استفاده از مدرن ترین تکنولوژی های روز دنیا</h2>
    </section>

    <!-- Page(7) -->
    <section class="pages page_7">
        <article class="p7_main">
            <!-- <h2>Contact</h2> -->
            <blockquote>سوالات و پیشنهادات خود را با ما در میان بگذارید</blockquote>


        </article>
        <div class="p7_slide">
            <div class="contact">
                <div class="form">
                    <h3>تماس با ما</h3>
                    <form method="post" id="contact_form">

                        <input type="email" name="contact_email" id="email" placeholder="ایمیل" required=""/>
                        <input type="text" name="name" id="name" placeholder="نام" required=""/>
                        <textarea class="scrollable" name="message" id="message" cols="45" rows="2" placeholder="پیام" required=""></textarea>
                        <button class="submit">ارسال</button>
                    </form>
                </div>
                <div class="contact_response_wrapper">
                    <div class="contact_response_box"></div>
                    <div class="contact_retry">تلاش مجدد</div>
                </div>

                <h5>hi@resana.ir</h5>

            </div>
        </div>
    </section>

</section>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
