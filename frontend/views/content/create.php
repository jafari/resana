<?php


/* @var $this yii\web\View */
/* @var $model common\models\Content */
/* @var $projectId */

$this->title = Yii::t('app', 'Create Content');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contents'), 'url' => ['index', 'project_id' => $projectId]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-create">
    <?= $this->render('_form', [
        'model' => $model,
        'projectId' => $projectId,
    ]) ?>
</div>
