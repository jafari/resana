<?php

use common\models\Content;
use vova07\imperavi\Widget as ReadactorEditor;
use kartik\file\FileInput;
use mrlco\datepicker\Datepicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\Content */
/* @var $form yii\widgets\ActiveForm */
/* @var $projectId */

// @todo: Add this script to the js file and add to the asset bundle
$urlArgs = ['content/get-groups', 'project_id' => $projectId];
if ($model->isNewRecord === false) {
    $urlArgs['id'] = $model->id;
}
$urlGroups = Url::to($urlArgs);
$this->registerJs(<<< JS
$('[name="Content[channelsList][]"]').change(function() {
    var channelIds = $('[name="Content[channelsList][]"]:checked').map(function() {
        return this.value;
    }).get();

    $.ajax({
        type: "POST",
        dataType: 'json',
        url: "{$urlGroups}",
        data: {channelIds: channelIds},
        beforeSend: function () {
            $('#content-groupslist').html('در حال بارگذاری ...');
        },
        success: function (groups) {
            var items = '';
            if (typeof groups.items === 'undefined') {
                $('.field-content-groupslist').addClass('hidden');
            } else {
                if (Object.keys(groups.items).length > 0) {
                    var template = function(value, name, checked) {return '<div class="checkbox"><label><input type="checkbox" name="Content[groupsList][]" value="'+ value +'" '+ checked +'> '+ name +'</label></div>'};
                    $.each(groups.items, function(index, value){
                        var checked = $.inArray(parseInt(index), groups.selected) != -1 ? 'checked' : '';
                        items += template(index, value, checked);
                    });
                    $('.field-content-groupslist').removeClass('hidden');
                }
            }
            $('#content-groupslist').html(items);
        }
    });
});

$("form").submit(function() {
    var publish_time = Number($(".pub-date").val());
    var date = new Date(publish_time);
    var string_date = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes()
    $("#content-publishtime").val(string_date);
});

$(function() {
    var elCounter = $('#char-counter');
    String.prototype.toFaDigit = function() {
        return this.replace(/\d+/g, function(digit) {
            var ret = '';
            for (var i = 0, len = digit.length; i < len; i++) {
                ret += String.fromCharCode(digit.charCodeAt(i) + 1728);
            }
            return ret;
        });
    };
    count = $('.redactor-editor').text().length - 1;
    elCounter.text(count.toString().toFaDigit());
    $('.redactor-editor').keyup(function () {
        count = $('.redactor-editor').text().length - 1;
        elCounter.text(count.toString().toFaDigit());
    });
})
JS
);

// Create preview and preview configs for attachments
if ($attachments = json_decode($model->attachments)) {
    $deleteAttachmentUrl = Url::to(['content/delete-attachment', 'project_id' => $projectId]);
    foreach ($attachments as $attachment) {
        $previews[] = Html::img($attachment->url, ['style' => 'max-width:200px;']);
        $previewsConfig[] = [
            'caption' => $attachment->name,
            'url' => $deleteAttachmentUrl,
            'key' => $attachment->url,
            'extra' => ['id' => $model->id],
        ];
    }
}

$scheduledLabel = Yii::t('app', 'Schedule');
$publishLabel = Yii::t('app', 'Publish');
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<div class="content-form row">

    <div class="col-sm-8">
        <div class="box box-primary">
            <div class="box-body">
                <?php $form->errorSummaryCssClass = 'callout callout-danger'; ?>
                <?= $form->errorSummary($model) ?>

                <?= $form->field($model, 'text')->widget(ReadactorEditor::className(), [
                    'settings' => [
                        'buttons' => ['bold', 'italic', 'link', 'unorderedlist', 'html'],
                        'lang' => 'fa',
                        'direction' => 'auto',
                        'minHeight' => 400,
                        'plugins' => [
                            'fullscreen',
                        ]
                    ]
                ]) ?>

                <div class="controls">
                    <span><?= Yii::t('app', 'Charachter count:') ?></span>
                    <span class="dir-lrt-number"><span class="label bg-green" id="char-counter">0</span></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="box box-success">
            <div class="box-body">
                <div class="form-group clearfix">
                    <?php if ($model->isNewRecord || in_array($model->status, [Content::STATUS_SCHEDULED, Content::STATUS_PENDING, Content::STATUS_FAILED], true)) : ?>
                        <?= Html::submitButton('<i class="fa fa-send"></i> ' . Yii::t('app', 'Publish'), ['name' => 'publish', 'class' => 'btn btn-success']) ?>
                        <?= Html::submitButton('<i class="fa fa-pencil"></i> ' . Yii::t('app', 'Save to draft'), ['name' => 'draft', 'class' => 'btn btn-primary']) ?>
                    <?php endif; ?>

                    <?= !$model->isNewRecord ? Html::a('<i class="fa fa-trash"></i> ' . Yii::t('app', 'Delete'), ['delete', 'id' => $model->id, 'project_id' => $projectId], [
                        'class' => 'btn btn-danger pull-left',
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]) : ''; ?>
                </div>

                <?= $form->field($model, 'publishTime')->widget(Datepicker::className(), [
                    'theme' => 'dark',
                    'addon' => '<i class="fa fa-fw fa-calendar"></i>',
                    'options' => [
                        'placeholder' => Yii::t('app', 'Insert Puplish Date'),
                        'readOnly' => true,
                    ],
                    'clientOptions' => [
                        'altField' => '.pub-date',
                        'format' => 'dddd D MMMM YYYY @ H:m',
                        'maxDate' => (time()-(60*60*24))*1000, // yesterday timestamp date in javascript
                        'onSelect' => new JsExpression("function(self){if(Date.now() < parseInt(\$('[name=\'publish_date\']').val())){\$('button[name=\'publish\']').text('$scheduledLabel');}else{\$('button[name=\'publish\']').text('$publishLabel');}}"),
                        'navigator' => [
                            'text' => [
                                'btnNextText' => '>>',
                                'btnPrevText' => '<<',
                            ],
                        ],
                        'timePicker' => [
                            'enabled' => true,
                            'showSeconds' => false,
                            'showMeridian' => false,
                        ]
                    ],
                ]) ?>
                <?= Html::hiddenInput('publish_date', null, ['class' => 'pub-date']); ?>

                <?= $form->field($model, 'files[]')->widget(FileInput::className(), [
                    'options'=>[
                        'multiple'=> true
                    ],
                    'pluginOptions' => [
                        'append' => true,
                        'showRemove' => false,
                        'showUpload' => false,
                        'showClose' => true,
                        'maxFileCount' => 10,
                        'initialPreview' => empty($previews) ? '' : $previews,
                        'initialPreviewConfig' => empty($previewsConfig) ? '' : $previewsConfig,
                    ],
                ]); ?>

                <?= empty($model->project->channelsList) ? '' : $form->field($model, 'channelsList')->checkboxList($model->project->channelsList); ?>

                <?= $form->field($model, 'groupsList', ['options' => ['class' => 'hidden']])->checkboxList($model->channelGroups); ?>
            </div>
        </div>
    </div>

</div>
<?php ActiveForm::end(); ?>
