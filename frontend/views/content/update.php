<?php

/* @var $this yii\web\View */
/* @var $model common\models\Content */
/* @var $projectId */

$this->title = Yii::t('app', 'Update Content');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contents'), 'url' => ['index', 'project_id' => $projectId]];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id, 'project_id' => $projectId]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="content-update">
    <?= $this->render('_form', [
        'model' => $model,
        'projectId' => $projectId,
    ]) ?>
</div>
