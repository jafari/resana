<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\Pjax;
use common\models\Content;
use common\widgets\ContentWidget;

/* @var $searchModel frontend\models\ContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $projectId */

$this->title = Yii::t('app', 'Contents');
$this->params['breadcrumbs'][] = $this->title;
$this->params['title-icon'] = 'fa-newspaper-o';
$this->params['create-button'] = Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Create Content'), ['content/create', 'project_id' => $projectId], ['class' => 'btn btn-default']);

$this->registerJs(<<< JS
$('#myPjax').on('pjax:error', function (event) {
    console.log('Failed to load the page');
    event.preventDefault();
});
var intervalId = setInterval(function(){
    var sending = $('i.fa-truck').length;
    var inQueue = $('i.fa-ellipsis-h').length;

    if (sending || inQueue) {
        $.pjax.reload({"container": "#pjax", "push": true, "replace": false, "timeout":3000});
    } else {
        clearInterval(intervalId);
    }
}, 3000);
JS
, 5);
?>

<div class="row">
    <div class="col-md-10 col-md-offset-1 post-list-content">
        <div class="post-list-sort">
            <div class="box-body no-padding">
                <div class="mailbox-controls filters clearfix" id="w0-filters">
                    <div class="box-tools pull-left">
                        <div class="has-feedback">
                            <?= Html::activeInput("text", $searchModel, 'text', ['class' => 'form-control input-sm', 'placeholder' => Yii::t('app', 'Search')]) ?>
                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                    </div>
                    <div class="box-tools pull-left">
                        <div class="has-feedback">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-search form-control-feedback"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <?= Html::activeInput("text", $searchModel, 'text', ['class' => 'form-control input-sm', 'placeholder' => Yii::t('app', 'Search')]) ?>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="date-sort">
                            <button type="button" data-toggle="tooltip" data-original-title="نزولی" class="btn btn-default btn-sm"><i class="fa fa-sort-amount-asc" aria-hidden="true"></i></button>
                            <button type="button" data-toggle="tooltip" data-original-title="صعودی" class="btn btn-default btn-sm"><i class="fa fa-sort-amount-desc" aria-hidden="true"></i></button>
                        </div>
                        <!-- /.btn-group -->
                    </div>
                    <div class="pull-right" style="margin-left: 10px;">
                        <div class="has-feedback">
                            <?= Html::activeDropDownList($searchModel, 'status', ['' => Yii::t('app', 'Status')] + Content::getAllStatus(), ['class' => 'form-control input-sm']) ?>
                        </div>
                    </div>

                    <div class="pull-right">
                        <div class="has-feedback">
                            <?= Html::activeDropDownList($searchModel, 'media_type', ['' => Yii::t('app', 'Media Type')] + Content::getMediaTypes(), ['class' => 'form-control input-sm']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php Pjax::begin(['id'=>'pjax']); ?>
        <?= ContentWidget::widget([
            'dataProvider' => $dataProvider,
        ]) ?>
        <?php Pjax::end() ?>
    </div>
</div>
