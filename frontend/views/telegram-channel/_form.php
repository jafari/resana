<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TelegramChannel */
/* @var $project common\models\Project */
/* @var $form yii\widgets\ActiveForm */

$action = $model->isNewRecord ? Url::to(["telegram-channel/create", 'project_id' => $projectId]) : Url::to(["telegram-channel/update", 'id' => $model->channel_id, 'project_id' => $projectId]);
?>

<div class="telegram-channel-form">
    <?php $form = ActiveForm::begin(['action' => $action, 'options' => ['id'=>'ajaxForm']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bot_channel_id')->dropDownList($project->botsList)->label(Yii::t('app', 'Bot')); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
