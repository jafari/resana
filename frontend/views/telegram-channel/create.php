<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TelegramChannel */
/* @var $project common\models\Project */
/* @var $projectId */

$this->title = Yii::t('app', 'Create Telegram Channel');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Telegram Channels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="telegram-channel-create box box-primary">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
            'projectId' => $projectId,
            'project' => $project,
        ]) ?>
    </div>
</div>
