<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TelegramChannel */
/* @var $project common\models\Project */
/* @var $projectId */

$this->title = Yii::t('app', 'Update').' '. $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Telegram Channels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->channel_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="telegram-channel-update box box-primary">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
            'projectId' => $projectId,
            'project' => $project,
        ]) ?>
    </div>
</div>
