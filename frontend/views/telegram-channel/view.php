<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TelegramChannel */
/* @var $projectId */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Telegram Channels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="telegram-channel-view box box-primary">
    <div class="box-body">
        <p>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->channel_id, 'project_id' => $projectId], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['channel/delete', 'id' => $model->channel_id, 'project_id' => $projectId], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'name',
                'username',
                [
                    'label' => Yii::t('app', 'Bot'),
                    'attribute' => 'bot.name',
                ]
            ],
        ]) ?>
    </div>
</div>
