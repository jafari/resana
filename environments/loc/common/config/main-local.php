<?php
return [
    'components' => [
        'db' => [
            'dsn' => 'mysql:host=db1.noghteh.net;dbname=db_resana',
            'username' => 'usr_resana',
            'password' => '12345678',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'shenasa' => [
                    'class' => 'common\components\Shenasa',
                    'clientId' => '5p5lwH64C5-iW-0gAQHmzlKX7CDchkH1',
                    'clientSecret' => '$2y$13$/7FCJVcmwZ4lwpf6ySn3Lu1ktYL7ANtWgKcX/tpgs5Wi9/sC5OraG',
                    'authUrl' => 'http://shenasa2.dev/oauth2/authorize',
                    'tokenUrl' => 'http://shenasa2.dev/oauth2/token',
                    'apiBaseUrl' => 'http://shenasa2.dev/oauth2',
                ],
            ],
        ],
        'media' => [
            'appKey' => 'cde6691e87482d66c148699947ad7ee417e011ef',
            'baseUrl' => 'http://nmedia.noghtehservice.dev/api/v1/media/',
        ],
        'amqp' => [
            'class' => 'common\components\AMQP',
            'host' => 'web1',
            'port' => 5672,
            'user' => 'noghteh',
            'pass' => 'graph123',
        ]
    ],
];
