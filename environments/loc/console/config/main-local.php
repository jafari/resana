<?php
return [
    'bootstrap' => ['gii'],
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'log' => [
            'flushInterval' => 1,
            'targets' => [
                [
                    'class' => 'airani\log\TelegramTarget',
                    'levels' => ['error'],
                    'botToken' => '136445035:AAETDSSdJCl30Zs35zhS3E94X-d1LsEW34M',
                    'chatId' => '-125716072',
                    'exportInterval' => 1,
                ],
            ],
        ],
    ],
];
