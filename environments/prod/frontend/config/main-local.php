<?php
return [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'shenasa' => [
                    'class' => 'common\components\Shenasa',
                    'clientId' => 'faEECGDTdeDVZ67Kz3KYlZ9FAXLUcfYZ',
                    'clientSecret' => '$2y$13$FQcWi6v8tLikLQpqDD2QW.fD7AW6MySeyv4exCkXCkxsxtqUbjr0G',
                    'authUrl' => 'https://new.shenasaservice.com/oauth2/authorize',
                    'tokenUrl' => 'https://new.shenasaservice.com/oauth2/token',
                    'apiBaseUrl' => 'https://new.shenasaservice.com/oauth2',
                ],
            ],
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'airani\log\TelegramTarget',
                    'levels' => ['error'],
                    'botToken' => '136445035:AAETDSSdJCl30Zs35zhS3E94X-d1LsEW34M',
                    'chatId' => '-93095256',
                ],
            ],
        ],
    ],
];
