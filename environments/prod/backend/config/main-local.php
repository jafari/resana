<?php
return [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'shenasa' => [
                    'class' => 'common\components\Shenasa',
                    'clientId' => 'EFkClhZV6wUWXeXeO7aOJkXR1m3La8Sc',
                    'clientSecret' => '$2y$13$1Xir6QulnpmqCVwdWvxzGu4aLb9ksr6l4nZ253kW5n89mvv/Zvfh2',
                    'authUrl' => 'https://new.shenasaservice.com/oauth2/authorize',
                    'tokenUrl' => 'https://new.shenasaservice.com/oauth2/token',
                    'apiBaseUrl' => 'https://new.shenasaservice.com/oauth2',
                ],
            ],
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'airani\log\TelegramTarget',
                    'levels' => ['error'],
                    'botToken' => '136445035:AAETDSSdJCl30Zs35zhS3E94X-d1LsEW34M',
                    'chatId' => '-93095256',
                ],
            ],
        ],
    ],
];
