<?php
return [
    'components' => [
        'db' => [
            'dsn' => 'mysql:host=localhost;dbname=db_resana',
            'username' => 'usr_resana',
            'password' => '5UQ5wqA7aWGvWxNy27Y3',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
        ],
        'media' => [
            'appKey' => 'c083ca5c59a130fddf02bdc1d77ca822d65b2c7a',
            'baseUrl' => 'http://nmedia.noghtehservice.com/api/v1/media/',
        ],
        'amqp' => [
            'class' => 'common\components\AMQP',
            'host' => 'localhost',
            'port' => 5672,
            'user' => 'noghteh',
            'pass' => 'graph123',
        ]
    ],
];
