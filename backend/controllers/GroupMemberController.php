<?php

namespace backend\controllers;

use Yii;
use common\models\GroupMember;
use backend\models\GroupMemberSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * GroupMemberController implements the CRUD actions for GroupMember model.
 */
class GroupMemberController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['superAdmin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all GroupMember models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GroupMemberSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GroupMember model.
     * @param string $group_id
     * @param string $member_id
     * @return mixed
     */
    public function actionView($group_id, $member_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($group_id, $member_id),
        ]);
    }

    /**
     * Creates a new GroupMember model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GroupMember();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'group_id' => $model->group_id, 'member_id' => $model->member_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing GroupMember model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $group_id
     * @param string $member_id
     * @return mixed
     */
    public function actionUpdate($group_id, $member_id)
    {
        $model = $this->findModel($group_id, $member_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'group_id' => $model->group_id, 'member_id' => $model->member_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing GroupMember model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $group_id
     * @param string $member_id
     * @return mixed
     */
    public function actionDelete($group_id, $member_id)
    {
        $this->findModel($group_id, $member_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the GroupMember model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $group_id
     * @param string $member_id
     * @return GroupMember the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($group_id, $member_id)
    {
        if (($model = GroupMember::findOne(['group_id' => $group_id, 'member_id' => $member_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
