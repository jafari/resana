<?php

namespace backend\controllers;

use Yii;
use common\models\ContentGroup;
use backend\models\ContentGroupSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ContentGroupController implements the CRUD actions for ContentGroup model.
 */
class ContentGroupController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['superAdmin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all ContentGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContentGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ContentGroup model.
     * @param string $content_id
     * @param string $group_id
     * @return mixed
     */
    public function actionView($content_id, $group_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($content_id, $group_id),
        ]);
    }

    /**
     * Creates a new ContentGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ContentGroup();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'content_id' => $model->content_id, 'group_id' => $model->group_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ContentGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $content_id
     * @param string $group_id
     * @return mixed
     */
    public function actionUpdate($content_id, $group_id)
    {
        $model = $this->findModel($content_id, $group_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'content_id' => $model->content_id, 'group_id' => $model->group_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ContentGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $content_id
     * @param string $group_id
     * @return mixed
     */
    public function actionDelete($content_id, $group_id)
    {
        $this->findModel($content_id, $group_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ContentGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $content_id
     * @param string $group_id
     * @return ContentGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($content_id, $group_id)
    {
        if (($model = ContentGroup::findOne(['content_id' => $content_id, 'group_id' => $group_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
