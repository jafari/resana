<?php

namespace backend\controllers;

use Yii;
use common\models\ContentChannel;
use backend\models\ContentChannelSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ContentChannelController implements the CRUD actions for ContentChannel model.
 */
class ContentChannelController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['superAdmin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all ContentChannel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContentChannelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ContentChannel model.
     * @param string $content_id
     * @param string $channel_id
     * @return mixed
     */
    public function actionView($content_id, $channel_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($content_id, $channel_id),
        ]);
    }

    /**
     * Creates a new ContentChannel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ContentChannel();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'content_id' => $model->content_id, 'channel_id' => $model->channel_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ContentChannel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $content_id
     * @param string $channel_id
     * @return mixed
     */
    public function actionUpdate($content_id, $channel_id)
    {
        $model = $this->findModel($content_id, $channel_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'content_id' => $model->content_id, 'channel_id' => $model->channel_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ContentChannel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $content_id
     * @param string $channel_id
     * @return mixed
     */
    public function actionDelete($content_id, $channel_id)
    {
        $this->findModel($content_id, $channel_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ContentChannel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $content_id
     * @param string $channel_id
     * @return ContentChannel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($content_id, $channel_id)
    {
        if (($model = ContentChannel::findOne(['content_id' => $content_id, 'channel_id' => $channel_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
