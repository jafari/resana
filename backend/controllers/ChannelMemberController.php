<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\ChannelMember;
use backend\models\ChannelMemberSearch;

/**
 * ChannelMemberController implements the CRUD actions for ChannelMember model.
 */
class ChannelMemberController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['superAdmin'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Lists all ChannelMember models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ChannelMemberSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ChannelMember model.
     * @param string $channel_id
     * @param string $member_id
     * @return mixed
     */
    public function actionView($channel_id, $member_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($channel_id, $member_id),
        ]);
    }

    /**
     * Creates a new ChannelMember model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ChannelMember();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'channel_id' => $model->channel_id, 'member_id' => $model->member_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ChannelMember model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $channel_id
     * @param string $member_id
     * @return mixed
     */
    public function actionUpdate($channel_id, $member_id)
    {
        $model = $this->findModel($channel_id, $member_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'channel_id' => $model->channel_id, 'member_id' => $model->member_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ChannelMember model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $channel_id
     * @param string $member_id
     * @return mixed
     */
    public function actionDelete($channel_id, $member_id)
    {
        $this->findModel($channel_id, $member_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ChannelMember model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $channel_id
     * @param string $member_id
     * @return ChannelMember the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($channel_id, $member_id)
    {
        if (($model = ChannelMember::findOne(['channel_id' => $channel_id, 'member_id' => $member_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
