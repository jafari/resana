<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use common\models\TelegramChannel;

/**
 * TelegramChannelSearch represents the model behind the search form about `common\models\TelegramChannel`.
 */
class TelegramChannelSearch extends TelegramChannel
{
    public $projectName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'username', 'projectName'], 'safe'],
            [['channel_id', 'bot_channel_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TelegramChannel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->projectName) {
            $query->joinWith([
                'channel' => function (ActiveQuery $query) {
                    $query->joinWith('project');
                }
            ]);
        }

        $query->andFilterWhere([
            'channel_id' => $this->channel_id,
            'bot_channel_id' => $this->bot_channel_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', '{{%project}}.name', $this->projectName]);

        return $dataProvider;
    }
}
