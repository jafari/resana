<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ContentGroup;

/**
 * ContentGroupSearch represents the model behind the search form about `common\models\ContentGroup`.
 */
class ContentGroupSearch extends ContentGroup
{
    public $summaryText;
    public $groupLink;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'content_id', 'group_id'], 'integer'],
            [['summaryText', 'groupLink'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContentGroup::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'attributes' => [
                    'sent_at',
                    'cnt_sent',
                    'cnt_failed'
                ],
            ],
        ]);

        $this->load($params);

        if ($this->summaryText) {
            $query->joinWith('content');
        }

        if ($this->groupLink) {
            $query->joinWith('group');
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'status' => $this->status,
            'sent_at' => $this->sent_at,
            'cnt_sent' => $this->cnt_sent,
            'cnt_failed' => $this->cnt_failed,
            'content_id' => $this->content_id,
            'group_id' => $this->group_id,
        ]);

        $query->andFilterWhere(['like', '{{%content}}.text', $this->summaryText]);
        $query->andFilterWhere(['like', '{{%group}}.name', $this->groupLink]);

        return $dataProvider;
    }
}
