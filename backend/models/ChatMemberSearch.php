<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ChannelMember;

/**
 * ChatMemberSearch represents the model behind the search form about `common\models\ChannelMember`.
 */
class ChatMemberSearch extends ChannelMember
{
    public $chatName;
    public $chatUsername;
    public $chatType;
    public $channelName;
    public $channelProjectName;
    public $id;
    public $member_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'chatType'], 'integer'],
            [['id', 'chatName', 'chatUsername', 'channelName', 'channelProjectName', 'member_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ChannelMember::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'attributes' => [
                    'created_at',
                ],
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if ($this->chatName || $this->chatUsername || $this->id) {
            $query->joinWith('chat');
        }

        if ($this->channelName) {
            $query->joinWith('bot');
        }

        if ($this->channelProjectName) {
            $query->joinWith('projects');
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            '{{%channel_member}}.status' => $this->status,
            '{{%chat}}.type' => $this->chatType,
            '{{%chat}}.chat_id' => $this->id,
            '{{%channel_member}}.member_id' => $this->member_id,
        ]);

        $query->andFilterWhere(['like', '{{%chat}}.name', $this->chatName]);
        $query->andFilterWhere(['like', '{{%chat}}.username', $this->chatUsername]);
        $query->andFilterWhere(['like', '{{%bot}}.name', $this->channelName]);
        $query->andFilterWhere(['like', '{{%project}}.name', $this->channelProjectName]);

        return $dataProvider;
    }
}
