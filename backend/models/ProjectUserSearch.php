<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ProjectUser;

/**
 * ProjectUserSearch represents the model behind the search form about `common\models\ProjectUser`.
 */
class ProjectUserSearch extends ProjectUser
{
    public $projectName;
    public $userName;
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'role', 'project_id', 'user_id'], 'integer'],
            [['projectName','userName', 'email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProjectUser::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if ($this->projectName) {
            $query->joinWith('project');
        }

        if ($this->userName || $this->email) {
            $query->joinWith('user');
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            '{{%project_user}}.role' => $this->role,
            'project_id' => $this->project_id,
            'user_id' => $this->user_id,
        ]);
        $query->andFilterWhere(['like', '{{%project}}.name', $this->projectName]);
        $query->andFilterWhere(['like', '{{%user}}.name', $this->userName]);
        $query->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
