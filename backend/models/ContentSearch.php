<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Content;

/**
 * ContentSearch represents the model behind the search form about `common\models\Content`.
 */
class ContentSearch extends Content
{
    public $channelsCountAttr;
    public $groupsCountAttr;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'media_type', 'type', 'status', 'updated_at', 'created_by', 'project_id', 'f_chat_id', 'f_message_id', 'message_id'], 'integer'],
            [['text', 'attachments' , 'summaryText'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'attributes' => [
                    'id',
                    'created_at',
                    'published_at',
                    'channelsCountAttr',
                    'groupsCountAttr',
                ],
                'defaultOrder' => [
                    'published_at' => SORT_DESC,
                ],
            ],
        ]);
        $query->select([
            '{{%content}}.*',
            'channelsCountAttr',
            'groupsCountAttr',
        ]);
        $query->countJoin('{{%content_channel}}', '{{%content}}.id', 'content_id', 'channelsCountAttr');
        $query->countJoin('{{%content_group}}', '{{%content}}.id', 'content_id', 'groupsCountAttr');

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'media_type' => $this->media_type,
            'type' => $this->type,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'published_at' => $this->published_at,
            'cnt_sent' => $this->cnt_sent,
            'cnt_failed' => $this->cnt_failed,
            'created_by' => $this->created_by,
            'project_id' => $this->project_id,
            'f_chat_id' => $this->f_chat_id,
            'f_message_id' => $this->f_message_id,
            'message_id' => $this->message_id,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'attachments', $this->attachments]);

        return $dataProvider;
    }
}
