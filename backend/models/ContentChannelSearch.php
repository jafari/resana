<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ContentChannel;

/**
 * ContentChannelSearch represents the model behind the search form about `common\models\ContentChannel`.
 */
class ContentChannelSearch extends ContentChannel
{
    public $summaryText;
    public $channelName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'content_id', 'channel_id'], 'integer'],
            [['summaryText', 'channelName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContentChannel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if ($this->summaryText) {
            $query->joinWith('content');
        }

        if ($this->channelName) {
            $query->joinWith('bot')->joinWith('telegramChannel');
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'status' => $this->status,
            'sent_at' => $this->sent_at,
            'cnt_sent' => $this->cnt_sent,
            'cnt_failed' => $this->cnt_failed,
            'content_id' => $this->content_id,
            'channel_id' => $this->channel_id,
        ]);

        $query->andFilterWhere(['like', '{{%content}}.text', $this->summaryText]);
        $query->andFilterWhere(['like', '{{%telegram_channel}}.name', $this->channelName])->orFilterWhere(['like', '{{%bot}}.name', $this->channelName]);
        return $dataProvider;
    }
}
