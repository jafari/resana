<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace backend\models;

use Yii;
use common\models\User as UserModel;

/**
 * Model User
 *
 * @author Ali Irani <ali@irani.im>
 */
class User extends UserModel
{
    /**
     * Check superAdmin permission
     * This method call after login event which set on config file
     * @param $event
     * @return bool
     */
    public static function checkAccess($event = null)
    {
        if (Yii::$app->user->can(static::ROLE_SUPER_ADMIN)) {
            return true;
        }
        Yii::$app->session->setFlash('error', Yii::t('app', 'You don\'t have required permissions to login this page!'));
        Yii::$app->user->logout();
        return false;
    }

}