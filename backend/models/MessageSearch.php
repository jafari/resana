<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Message;

/**
 * MessageSearch represents the model behind the search form about `common\models\Message`.
 */
class MessageSearch extends Message
{
    public $channelName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'media_type', 'type', 'status', 'channel_id'], 'integer'],
            [['text', 'member_id', 'channelName'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Message::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'attributes' => [
                    'id',
                    'created_at',
                    'updated_at',
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->channelName) {
            $query->joinWith('bot')->joinWith('telegramChannel');
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'media_type' => $this->media_type,
            'type' => $this->type,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'channel_id' => $this->channel_id,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', '{{%telegram_channel}}.name', $this->channelName])->orFilterWhere(['like', '{{%bot}}.name', $this->channelName])
            ->andFilterWhere(['like', 'attachments', $this->attachments]);

        return $dataProvider;
    }
}
