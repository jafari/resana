<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use common\models\Twitter;

/**
 * TwitterSearch represents the model behind the search form about `common\models\Twitter`.
 */
class TwitterSearch extends Twitter
{
    public $status;
    public $projectName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'username', 'token', 'token_secret', 'projectName'], 'safe'],
            [[ 'channel_id', 'status', 'user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Twitter::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'attributes' => [
                    'created_at',
                    'updated_at',
                    'user_id',
                    'channel_id',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $sort = Yii::$app->request->getQueryParam('sort');
        if ($this->status || in_array($sort, ['update_at', 'created_at', '-update_at', '-created_at'])) {
            $query->joinWith('channel');
        }

        if ($this->projectName) {
            $query->joinWith([
                'channel' => function (ActiveQuery $query) {
                    $query->joinWith('project');
                }
            ]);
        }

        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'channel_id' => $this->channel_id,
            '{{%channel}}.status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'token', $this->token])
            ->andFilterWhere(['like', 'token_secret', $this->token_secret])
            ->andFilterWhere(['like', '{{%project}}.name', $this->projectName]);

        return $dataProvider;
    }
}
