<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Bot;

/**
 * BotSearch represents the model behind the search form about `common\models\Bot`.
 */
class BotSearch extends Bot
{
    public $groupsCountAttr;
    public $contentsCountAttr;
    public $channelMembersCountAttr;
    public $channelProjectId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['channel_id', 'channelProjectId'], 'integer'],
            [['name', 'username', 'token', 'owner'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'attributes' => [
                    'bot_id',
                    'groupsCountAttr',
                    'contentsCountAttr',
                    'channelMembersCountAttr',
                ],
                'defaultOrder' => [
                    'bot_id' => SORT_DESC,
                ],
            ],
        ]);
        $query->select([
            '{{%bot}}.*',
            '{{%channel}}.project_id',
            'groupsCountAttr',
            'contentsCountAttr',
            'channelMembersCountAttr',
        ]);
        $query->countJoin('{{%group}}', '{{%bot}}.channel_id', 'channel_id', 'groupsCountAttr');
        $query->countJoin('{{%content_channel}}', '{{%bot}}.channel_id', 'channel_id', 'contentsCountAttr');
        $query->countJoin('{{%channel_member}}', '{{%bot}}.channel_id', 'channel_id', 'channelMembersCountAttr');
        $query->joinWith('channel');

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'bot_id' => $this->bot_id,
            'channel_id' => $this->channel_id,
            'project_id' => $this->channelProjectId,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'token', $this->token])
            ->andFilterWhere(['like', 'owner', $this->owner]);

        return $dataProvider;
    }
}
