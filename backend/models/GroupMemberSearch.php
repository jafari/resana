<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\GroupMember;

/**
 * GroupMemberSearch represents the model behind the search form about `common\models\GroupMember`.
 */
class GroupMemberSearch extends GroupMember
{
    public $memberChatName;
    public $groupName;
    public $channelName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'group_id', 'member_id'], 'integer'],
            [['memberChatName', 'groupName', 'channelName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GroupMember::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if ($this->memberChatName) {
            $query->joinWith('chat');
        }

        if ($this->groupName) {
            $query->joinWith('group');
        }

        if ($this->channelName) {
            $query->joinWith('bot');
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            '{{%group_member}}.status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'group_id' => $this->group_id,
            '{{%group_member}}.member_id' => $this->member_id,
        ]);

        $query->andFilterWhere(['like', '{{%chat}}.name', $this->memberChatName]);
        $query->andFilterWhere(['like', '{{%group}}.name', $this->groupName]);
        $query->andFilterWhere(['like', '{{%bot}}.name', $this->channelName]);

        return $dataProvider;
    }
}
