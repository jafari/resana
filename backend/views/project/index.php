<?php

use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use common\models\project;
use common\models\User;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Projects');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index box box-primary">
    <div class="box-body">
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'id',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'attribute' => 'name',
                    'contentOptions' => ['dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control']
                ],
                [
                    'attribute' => 'description',
                    'contentOptions' => ['dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control']
                ],
                [
                    'attribute' => 'status',
                    'filter' => Project::getAllStatus(),
                    'value'=>'statusName',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'label' => Yii::t('app', 'Project Owner'),
                    'attribute' => 'userProjectOwner',
                    'value' => 'projectOwner.user.name',
                    'contentOptions' => ['dir' => 'auto', 'align' => 'center'],
                    'filter' => ArrayHelper::map(User::find()->asArray()->all(), 'id', 'name'),
                ],
                [
                    'label' => Yii::t('app', 'Project Users Count'),
                    'attribute' => 'projectUsersCountAttr',
                    'format' => 'decimal',
                    'value' => 'projectUsersCountAttr',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'label' => Yii::t('app', 'Contents Count'),
                    'attribute' => 'contentsCountAttr',
                    'format' => 'decimal',
                    'value' => 'contentsCountAttr',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['dir' => 'auto', 'align' => 'center'],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=>'{view} {delete} {update}'
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
