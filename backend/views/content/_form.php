<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Content;

/* @var $this yii\web\View */
/* @var $model common\models\Content */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="content-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'text')->textarea(['rows' => '6']) ?>

    <?= $form->field($model, 'attachments')->textarea() ?>

    <?= $form->field($model, 'media_type')->radioList(Content::getMediaTypes()) ?>

    <?= $form->field($model, 'type')->radioList(Content::getTypes()) ?>

    <?= $form->field($model, 'status')->radioList(Content::getAllStatus()) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'published_at')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'project_id')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
