<?php

use common\models\Content;
use common\models\Project;
use common\models\User;
use yii\grid\GridView;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contents');
$this->params['breadcrumbs'][] = $this->title;

$user = function ($model) {
    return Html::a($model->createdBy->name, Url::to(['user/view' , 'id' => $model->createdBy->id]));
};

$project = function ($model) {
    return Html::a($model->project->name, Url::to(['project/view' , 'id' => $model->project->id]));
};

$mediaType = function ($model) {
    $iconClass = [
        Content::MEDIA_TYPE_AUDIO => 'music',
        Content::MEDIA_TYPE_DOCUMENT => 'book',
        Content::MEDIA_TYPE_FORWARD => 'retweet',
        Content::MEDIA_TYPE_IMAGE => 'picture-o',
        Content::MEDIA_TYPE_TEXT => 'align-right',
        Content::MEDIA_TYPE_VIDEO => 'video-camera',
        Content::MEDIA_TYPE_GIF => 'film',
    ];
    $title = Html::tag('span', '', ['class' => 'fa fa-fw fa-lg fa-' . $iconClass[$model->media_type]]);
    return Html::a($title, ['index', 'ContentSearch[media_type]' => $model->media_type]);
};

$status = function ($model) {
    $statusClass = [
        Content::STATUS_PENDING => 'label-warning',
        Content::STATUS_FAILED => 'label-danger',
        Content::STATUS_SENT => 'label-success',
        Content::STATUS_SCHEDULED => 'label-primary',
        Content::STATUS_IN_QUEUE => 'label-default',
        Content::STATUS_SENDING => 'label-info',
    ];
    $title = Html::tag('span', $model->statusName, ['class' => 'text-md-size label ' . $statusClass[$model->status]]);
    return Html::a($title, ['index', 'ContentSearch[status]' => $model->status]);
};
?>
<div class="content-index box box-primary">
    <div class="box-body">
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'id',
                    'format' => 'integer',
                    'contentOptions' => ['align' => 'center'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
                [
                    'attribute' => 'media_type',
                    'filter' => Content::getMediaTypes(),
                    'format' => 'html',
                    'value' => $mediaType,
                    'contentOptions' => ['class' => 'text-center'],
                ],
                [
                    'attribute' => 'text',
                    'value' => 'summaryText',
                    'format' => 'html',
                    'contentOptions' => ['width' => '35%', 'dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
                [
                    'label' => Yii::t('app', 'Project Name'),
                    'format' => 'html',
                    'value' => $project,
                    'attribute' => 'project_id',
                    'filter' => ArrayHelper::map(Project::find()->asArray()->all(), 'id', 'name'),
                    'contentOptions' => ['class' => 'text-center'],
                ],
                [
                    'attribute' => 'created_by',
                    'format' => 'html',
                    'value' => $user,
                    'filter' => ArrayHelper::map(User::find()->asArray()->all(), 'id', 'name'),
                    'contentOptions' => ['class' => 'text-center'],
                ],
                [
                    'attribute' => 'type',
                    'value' => 'typeName',
                    'filter' =>  Content::getTypes(),
                ],
                [
                    'attribute' => 'status',
                    'value' => $status,
                    'format' => 'html',
                    'filter' =>  Content::getAllStatus(),
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'attribute' => 'cnt_sent',
                    'format' => 'decimal',
                    'contentOptions' => ['class' => 'text-center'],
                ],
                [
                    'attribute' => 'cnt_failed',
                    'format' => 'decimal',
                    'contentOptions' => ['align'=>'center'],
                ],
                [
                    'label' => Yii::t('app', 'Channels Count'),
                    'attribute'=>'channelsCountAttr',
                    'format' => 'decimal',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'label' => Yii::t('app', 'Groups Count'),
                    'attribute'=>'groupsCountAttr',
                    'format' => 'decimal',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['dir' => 'auto'],
                ],
                [
                    'attribute' => 'published_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=>'{view} {delete} {update}'
                ],
            ],
        ]);
        ?>
        <?php Pjax::end(); ?>
    </div>
</div>
