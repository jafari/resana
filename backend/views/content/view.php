<?php

use common\models\Content;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Content */

$mediaUrl = $this->title = '';
$media = function($mediaUrl) {
    return [
        Content::MEDIA_TYPE_IMAGE => Html::img($mediaUrl, ['style' => 'max-width: 100%; margin-bottom: 10px;']),
        Content::MEDIA_TYPE_GIF => Html::img($mediaUrl, ['style' => 'max-width: 100%; margin-bottom: 10px;']),
        Content::MEDIA_TYPE_VIDEO => Html::tag('video', '', ['src' => $mediaUrl, 'controls'=>'', 'style' => 'max-width: 100%; margin-bottom: 10px;']),
        Content::MEDIA_TYPE_AUDIO => Html::tag('audio', '', ['src' => $mediaUrl, 'controls'=>'', 'style' => 'max-width: 100%; margin-bottom: 10px;']),
    ];
};

if ($channels = $model->channels) {
    foreach ($channels as $channel) {
        $chLinks[] = Html::a("{$channel->name} ({$channel->typeName})", ["{$channel->actionName}/view", 'id' => $channel->id]);
    }
    $channelLinks = empty($chLinks) ? '' : implode('، ', $chLinks);
}

if ($groups = $model->groups) {
    foreach ($groups as $group) {
        $grLinks[] = Html::a("{$group->name} ({$group->channel->name})", ["group/view", 'id' => $group->id]);
    }
    $groupLinks = empty($grLinks) ? '' : implode('، ', $grLinks);
}
?>
<div class="content-view">
    <div class="row">
        <div class="col-md-7 col-sm-12">
            <?= $model->text ?>
            <?php
            if ($attachments = $model->attachments) {
                $attachments = json_decode($attachments);
                foreach ($attachments as $attachment) {
                    echo $media($attachment->url)[$attachment->media_type];
                }
            }
            ?>
        </div>
        <div class="col-md-5 col-sm-12">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute' => 'channels',
                        'label' => Yii::t('app', 'Channels'),
                        'value' => isset($channelLinks) ? $channelLinks : '',
                        'format' => 'html',
                        'visible' => isset($channelLinks) ? true : false,
                    ],
                    [
                        'attribute' => 'groups',
                        'label' => Yii::t('app', 'Groups'),
                        'value' => isset($groupLinks) ? $groupLinks : '',
                        'format' => 'html',
                        'visible' => isset($groupLinks) ? true : false,
                    ],
                    [
                        'attribute' => 'media_type',
                        'value' => $model->mediaTypeName,
                    ],
                    [
                        'attribute' => 'type',
                        'value' => $model->typeName,
                    ],
                    [
                        'attribute' => 'status',
                        'value' => $model->statusName,
                    ],
                    [
                        'attribute' => 'created_at',
                        'format' => ['todaytime', 'Y/M/d @ H:m'],
                    ],
                    [
                        'attribute' => 'updated_at',
                        'format' => ['todaytime', 'Y/M/d @ H:m'],
                    ],
                    [
                        'attribute' => 'published_at',
                        'format' => ['todaytime', 'Y/M/d @ H:m'],
                    ],
                    'cnt_sent:integer',
                    'cnt_failed:integer',
                    [
                        'label' => Yii::t('app', 'Author'),
                        'attribute' => 'user',
                        'value' => $model->createdBy->name,
                    ],
                    'f_chat_id:integer',
                    'f_message_id:integer',
                    'message_id',
                    'channelsCount:integer',
                    'groupsCount:integer',
                ],
            ]) ?>
        </div>
    </div>
</div>
