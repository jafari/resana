<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ContentChannel */

$this->title = Yii::t('app', 'Create Content Channel');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Content Channels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-channel-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
