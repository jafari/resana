<?php

/* @var $this yii\web\View */
/* @var $model common\models\ContentChannel */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Content Channels'),
]) . $model->content_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Content Channels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->content_id, 'url' => ['view', 'content_id' => $model->content_id, 'channel_id' => $model->channel_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="content-channel-update">
    <div class="bot-index box box-primary">
        <div class="box-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
