<?php

use common\models\ContentChannel;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ContentChannelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Content Channels');
$this->params['breadcrumbs'][] = $this->title;

$contentLink = function ($url, $model, $key) {
    return Html::a(Html::tag('span', '', ['class' => 'fa fa-link']), Url::to(['content/view' , 'id' => $model->content->id]));
};
$channelName = function ($model) {
    return Html::a($model->channel->name, Url::to([$model->channel->actionname.'/view' , 'id' => $model->channel->id]));
};
?>
<div class="content-channel-index box box-primary">
    <div class="box-body">
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=>'{contentLink}',
                    'buttons' => [
                        'contentLink' => $contentLink,
                    ]
                ],
                [
                    'attribute' => 'summaryText',
                    'format' => 'html',
                    'value' =>  'content.summaryText',
                    'contentOptions' => ['dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
                [
                    'attribute' => 'channelName',
                    'format' => 'html',
                    'value' => $channelName,
                    'contentOptions' => ['align' => 'center'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
//                [
//                    'attribute' => 'status',
//                    'value' => 'statusName',
//                    'filter' => ContentChannel::getAllStatus(),
//                ],
                [
                    'attribute' => 'sent_at',
                    'format' => 'decimal',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'attribute' => 'cnt_sent',
                    'format' => 'decimal',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'attribute' => 'cnt_failed',
                    'format' => 'decimal',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=>'{view} {delete} {update}'
                ],
            ],
        ]);
        ?>
        <?php Pjax::end(); ?>
    </div>
</div>
