<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ContentChannel */

$this->title = Yii::t('app', 'Content Channel');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Content Channels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-channel-view box box-primary">
    <div class="box-body">
        <p>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'content_id' => $model->content_id, 'channel_id' => $model->channel_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'content.text:html',
                [
                    'label' => Yii::t('app', 'Channel Name'),
                    'value' => $model->channel->name,
                ],
                'statusName',
                'sent_at:decimal',
                'cnt_sent:decimal',
                'cnt_failed:decimal',
            ],
        ]) ?>
    </div>
</div>
