<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ContentGroup */

$this->title = Yii::t('app', 'Create Content Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Content Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
