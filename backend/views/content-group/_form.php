<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ContentGroup;

/* @var $this yii\web\View */
/* @var $model common\models\ContentGroup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="content-group-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status')->radioList(ContentGroup::getAllStatus()) ?>

    <?= $form->field($model, 'sent_at')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cnt_sent')->textInput() ?>

    <?= $form->field($model, 'cnt_failed')->textInput() ?>

    <?= $form->field($model, 'content_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'group_id')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
