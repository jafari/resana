<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ContentGroup */

$this->title = Yii::t('app', 'Content Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Content Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-group-view box box-primary">
    <div class="box-body">
        <p>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'content_id' => $model->content_id, 'group_id' => $model->group_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'content.text',
                    'format' => 'html',
                    'contentOptions' => ['width' => '45%', 'dir' => 'auto'],
                ],
                'statusName',
                'sent_at',
                'cnt_sent:decimal',
                'cnt_failed:decimal',
                [
                    'label' => Yii::t('app', 'Group Name'),
                    'attribute' => 'group.name',
                ],
            ],
        ])
        ?>
    </div>
</div>
