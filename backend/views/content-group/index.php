<?php

use common\models\ProjectUser;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ContentGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Content Groups');
$this->params['breadcrumbs'][] = $this->title;

$contentLink = function ($url, $model, $key) {
    return Html::a(Html::tag('span', '', ['class' => 'fa fa-link']), Url::to(['content/view' , 'id' => $model->content->id]));
};
$groupLink = function ($model) {
    return Html::a($model->group->name, Url::to(['group/view' , 'id' => $model->group->id]));
};
?>
<div class="content-group-index box box-primary">
    <div class="box-body">
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=>'{contentLink}',
                    'buttons' => [
                        'contentLink' => $contentLink,
                    ],
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'attribute' => 'content_id',
                    'contentOptions' => ['class' => 'text-center', 'width' => '5%'],
                    'filterInputOptions' => ['class' => 'text-center form-control'],
                ],
                [
                    'attribute' => 'summaryText',
                    'format' => 'html',
                    'value' => 'content.summaryText',
                    'contentOptions' => ['width' => '45%', 'dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
                [
                    'attribute' => 'group_id',
                    'contentOptions' => ['class' => 'text-center', 'width' => '5%'],
                    'filterInputOptions' => ['class' => 'text-center form-control'],
                ],
                [
                    'label' => Yii::t('app', 'Group Name'),
                    'attribute' => 'groupLink',
                    'format' => 'html',
                    'value' => $groupLink,
                    'contentOptions' => ['dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
//                [
//                    'attribute' => 'status',
//                    'value' => 'statusName',
//                    'filter' => ProjectUser::getAllStatus(),
//                    'contentOptions' => ['align' => 'center'],
//                ],
                [
                    'attribute' => 'sent_at',
                    'format' => 'decimal',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'attribute' => 'cnt_sent',
                    'format' => 'decimal',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'attribute' => 'cnt_failed',
                    'format' => 'decimal',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=>'{view} {delete} {update}'
                ],
            ],
        ]);
        ?>
        <?php Pjax::end(); ?>
    </div>
</div>
