<?php

/* @var $this yii\web\View */
/* @var $model common\models\ContentGroup */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Content Groups'),
]) . $model->content_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Content Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->content_id, 'url' => ['view', 'content_id' => $model->content_id, 'group_id' => $model->group_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="content-group-update">
    <div class="bot-index box box-primary">
        <div class="box-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
