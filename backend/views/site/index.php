<?php

use yii\bootstrap\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Dashboard');
?>
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <?= Html::tag('h3', Html::a(Yii::t('app', 'Users').Html::tag('div', Html::tag('i', '', ['class' => 'fa fa-user']), ['class' => 'icon']), Url::to(['user/index']), ['class' => 'text-muted'])).Html::tag('p', Yii::t('app', 'Description')); ?>
                </div>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <?= Html::tag('h3', Html::a(Yii::t('app', 'Projects').Html::tag('div', Html::tag('i', '', ['class' => 'fa fa-dashboard']), ['class' => 'icon']), Url::to(['project/index']), ['class' => 'text-muted'])).Html::tag('p', Yii::t('app', 'Description')); ?>
                </div>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <?= Html::tag('h3', Html::a(Yii::t('app', 'Bots').Html::tag('div', Html::tag('i', '', ['class' => 'fa fa-android']), ['class' => 'icon']), Url::to(['bot/index']), ['class' => 'text-muted'])).Html::tag('p', Yii::t('app', 'Description')); ?>
                </div>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <?= Html::tag('h3', Html::a(Yii::t('app', 'Telegram Chats').Html::tag('div', Html::tag('i', '', ['class' => 'fa fa-comment']), ['class' => 'icon']), Url::to(['chat/index']), ['class' => 'text-muted'])).Html::tag('p', Yii::t('app', 'Description')); ?>
                </div>
            </div>
        </div><!-- ./col -->
    </div><!-- /.row -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <?= Html::tag('h3', Html::a(Yii::t('app', 'Groups').Html::tag('div', Html::tag('i', '', ['class' => 'fa fa-folder-open']), ['class' => 'icon']), Url::to(['group/index']), ['class' => 'text-muted'])).Html::tag('p', Yii::t('app', 'Description')); ?>
                </div>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <?= Html::tag('h3', Html::a(Yii::t('app', 'Contents').Html::tag('div', Html::tag('i', '', ['class' => 'fa fa-newspaper-o']), ['class' => 'icon']), Url::to(['content/index']), ['class' => 'text-muted'])).Html::tag('p', Yii::t('app', 'Description')); ?>
                </div>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <?= Html::tag('h3', Html::a(Yii::t('app', 'Project Users').Html::tag('div', Html::tag('i', '', ['class' => 'fa fa-users']), ['class' => 'icon']), Url::to(['project-user/index']), ['class' => 'text-muted'])).Html::tag('p', Yii::t('app', 'Description')); ?>
                </div>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <?= Html::tag('h3', Html::a(Yii::t('app', 'Group Members').Html::tag('div', Html::tag('i', '', ['class' => 'fa fa-users']), ['class' => 'icon']), Url::to(['group-member/index']), ['class' => 'text-muted'])).Html::tag('p', Yii::t('app', 'Description')); ?>
                </div>
            </div>
        </div><!-- ./col -->
    </div><!-- /.row -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <?= Html::tag('h3', Html::a(Yii::t('app', 'Chats').Html::tag('div', Html::tag('i', '', ['class' => 'fa fa-users']), ['class' => 'icon']), Url::to(['chat-member/index']), ['class' => 'text-muted'])).Html::tag('p', Yii::t('app', 'Description')); ?>
                </div>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <?= Html::tag('h3', Html::a(Yii::t('app', 'Content Group').Html::tag('div', Html::tag('i', '', ['class' => 'fa fa-newspaper-o']), ['class' => 'icon']), Url::to(['content-group/index']), ['class' => 'text-muted'])).Html::tag('p', Yii::t('app', 'Description')); ?>
                </div>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <?= Html::tag('h3', Html::a(Yii::t('app', 'Content Channel').Html::tag('div', Html::tag('i', '', ['class' => 'fa fa-newspaper-o']), ['class' => 'icon']), Url::to(['content-channel/index']), ['class' => 'text-muted'])).Html::tag('p', Yii::t('app', 'Description')); ?>
                </div>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <?= Html::tag('h3', Html::a(Yii::t('app', 'Telegram Channels').Html::tag('div', Html::tag('i', '', ['class' => 'fa fa-paper-plane']), ['class' => 'icon']), Url::to(['telegram-channel/index']), ['class' => 'text-muted'])).Html::tag('p', Yii::t('app', 'Description')); ?>
                </div>
            </div>
        </div><!-- ./col -->
    </div><!-- /.row -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <?= Html::tag('h3', Html::a(Yii::t('app', 'Twitters').Html::tag('div', Html::tag('i', '', ['class' => 'fa fa-twitter']), ['class' => 'icon']), Url::to(['twitter/index']), ['class' => 'text-muted'])).Html::tag('p', Yii::t('app', 'Description')); ?>
                </div>
            </div>
        </div><!-- ./col -->


        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <?= Html::tag('h3', Html::a(Yii::t('app', 'Messages').Html::tag('div', Html::tag('i', '', ['class' => 'fa fa-envelope']), ['class' => 'icon']), Url::to(['message/index']), ['class' => 'text-muted'])).Html::tag('p', Yii::t('app', 'Description')); ?>
                </div>
            </div>
        </div><!-- ./col -->


    </div><!-- /.row -->

</section>