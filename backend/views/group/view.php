<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Group */

$this->title = Yii::t('app', 'Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-view box box-primary">
        <div class="box-body">
            <p>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id:integer',
                    'name',
                    [
                        'attribute' => 'typeName',
                    ],
                    [
                        'attribute' => 'statusName',
                    ],
                    [
                        'attribute' => 'created_at',
                        'format' => ['todaytime', 'Y/M/d @ H:m'],
                        'contentOptions' => ['dir' => 'auto'],
                    ],
                    [
                        'attribute' => 'updated_at',
                        'format' => ['todaytime', 'Y/M/d @ H:m'],
                        'contentOptions' => ['dir' => 'auto'],
                    ],
                    'membersCount:integer',
                    'contentsCount:integer',
                ],
            ]) ?>
        </div>
</div>
