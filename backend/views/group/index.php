<?php

use common\models\Group;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Groups');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-index box box-primary">
    <div class="box-body">
        <?php Pjax::begin(); ?>
    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'id',
                    'format' => 'decimal',
                    'contentOptions' => ['align' => 'center', 'width' => '5%'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
                [
                    'attribute' => 'name',
                    'contentOptions' => ['align' => 'center'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
                [
                    'attribute' => 'type',
                    'filter' => Group::getTypes(),
                    'value' => 'typeName',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'attribute' => 'status',
                    'filter' => Group::getAllStatus(),
                    'value' => 'statusName',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'label' => Yii::t('app', 'Members Count'),
                    'attribute'=>'membersCountAttr',
                    'format' => 'decimal',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'label' => Yii::t('app', 'Contents Count'),
                    'attribute'=>'contentsCountAttr',
                    'format' => 'decimal',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=>'{view} {delete} {update}'
                ],
            ],
        ]);
    ?>
        <?php Pjax::end(); ?>
    </div>
</div>
