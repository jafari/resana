<?php

use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ChannelMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Channel Members');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="channel-member-index box box-primary">
    <div class="box-body">
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'statusName',
                [
                    'attribute' => 'created_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['dir' => 'auto'],
                ],
                [
                    'label' => Yii::t('app', 'Channel Name'),
                    'attribute' => 'channel.name',
                ],
                [
                    'label' => Yii::t('app', 'Channel Type'),
                    'attribute' => 'channel.typeName',
                ],
                [
                    'label' => Yii::t('app', 'Project id'),
                    'attribute' => 'channel.project.id',
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=>'{view} {delete} {update}'

                ],
            ],
        ]);
        ?>
        <?php Pjax::end(); ?>`
    </div>
</div>
