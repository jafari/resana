<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ChannelMember */

$this->title = $model->channel_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Channel Members'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="channel-member-view box box-primary">
    <div class="box-body">
        <p>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'channel_id' => $model->channel_id, 'member_id' => $model->member_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'statusName',
                [
                    'attribute' => 'created_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['dir' => 'auto'],
                ],
                [
                    'label' => Yii::t('app', 'Channel Name'),
                    'attribute' => 'channel.name',
                ],
                [
                    'label' => Yii::t('app', 'Channel type'),
                    'attribute' => 'channel.typeName',
                ],
            ],
        ])
        ?>
    </div>
</div>
