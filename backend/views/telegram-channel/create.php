<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TelegramChannel */

$this->title = Yii::t('app', 'Create Telegram Channel');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Telegram Channels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="telegram-channel-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
