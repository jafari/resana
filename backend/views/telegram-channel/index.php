<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TelegramChannelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$userName = function ($model) {
    return Html::a($model->user->name, Url::to(['user/view' , 'id' => $model->user->id]));
};

$projectName = function ($model) {
    return Html::a($model->channel->project->name, Url::to(['project/view' , 'id' => $model->channel->project->id]));
};

$this->title = Yii::t('app', 'Telegram Channels');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="telegram-channel-index box box-primary">
    <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => Yii::t('app', 'Channel Id'),
                'attribute' => 'bot_channel_id',
                'contentOptions' => ['align' => 'center'],
                'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                'contentOptions' => ['class' => 'text-center', 'width' => '5%'],
            ],
            [
                'attribute' => 'name',
                'contentOptions' => ['dir' => 'auto'],
                'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
            ],
            [
                'attribute' => 'username',
                'contentOptions' => ['dir' => 'auto'],
                'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
            ],
            [
                'label' => Yii::t('app', 'Project Name'),
                'attribute'=>'projectName',
                'format' => 'html',
                'value' => $projectName,
                'contentOptions' => ['align' => 'center'],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view} {delete} {update}'
            ],
        ],
    ]); ?>
    </div>
</div>
