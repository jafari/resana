<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TelegramChannel */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Telegram Channels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="telegram-channel-view box box-primary">
    <div class="box-body">
    <p>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->channel_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'channel_id',
            'name',
            'username',
            'channel.project.name',
            [
                'label' => Yii::t('app', 'Signature'),
                'attribute' => 'signature',
                'value' => $model->channel->signature,
            ],
        ],
    ]) ?>
    </div>
</div>
