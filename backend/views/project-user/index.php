<?php

use common\models\ProjectUser;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\Url;

$projectName = function ($model) {
    return Html::a($model->project->name, Url::to(['project/view' , 'id' => $model->project->id]));
};

$userName = function ($model) {
    return Html::a($model->user->name, Url::to(['user/view' , 'id' => $model->user->id]));
};

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProjectUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Project Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-user-index box box-primary">
    <div class="box-body">
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'user_id',
                    'contentOptions' => ['class' => 'text-center', 'width' => '5%'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
                [
                    'label' => Yii::t('app', 'User Name'),
                    'attribute' => 'userName',
                    'format' => 'html',
                    'value' => $userName,
                    'contentOptions' => ['dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
                [
                    'attribute' => 'email',
                    'format' => 'email'  ,
                    'value' => 'user.email',
                    'contentOptions' => ['dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto',   'class'=>'form-control'],
                ],
                [
                    'attribute' => 'project_id',
                    'contentOptions' => ['class' => 'text-center', 'width' => '5%'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
                [
                    'label' => Yii::t('app', 'Project Name'),
                    'format' => 'html',
                    'value' => $projectName,
                    'attribute' => 'projectName',
                    'contentOptions' => ['dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
//                [
//                    'attribute' => 'status',
//                    'value' => 'statusName',
//                    'filter' => ProjectUser::getAllStatus(),
//                    'contentOptions' => ['align' => 'center'],
//                ],
                [
                    'attribute' => 'role',
                    'value' => 'roleName',
                    'filter' => ProjectUser::getRoles(),
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['dir' => 'auto', 'align' => 'center'],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=>'{view} {delete} {update}'
                ],
            ],
        ]);
        ?>
        <?php Pjax::end(); ?>
    </div>
</div>
