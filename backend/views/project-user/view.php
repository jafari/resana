<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ProjectUser */

$this->title = Yii::t('app', 'Project User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Project Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-user-view box box-primary">
    <div class="box-body">
        <p>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'project_id' => $model->project_id, 'user_id' => $model->user_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'user.name',
                'statusName',
                [
                    'attribute' => 'created_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['dir' => 'auto'],
                ],
                'roleName',
                [
                    'label' => Yii::t('app', 'Project Name'),
                    'value' => $model->project->name,
                ],
            ],
        ]) ?>
    </div>
</div>
