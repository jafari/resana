<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ChannelMember */

$this->title = Yii::t('app', 'Chat');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Channel Members'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="channel-member-view box box-primary">
    <div class="box-body">
    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'channel_id' => $model->channel_id, 'member_id' => $model->member_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'channel_id' => $model->channel_id, 'member_id' => $model->member_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'statusName',
            [
                'attribute' => 'created_at',
                'format' => ['todaytime', 'Y/M/d @ H:m'],
                'contentOptions' => ['class' => 'text-center', 'dir' => 'auto'],
            ],
            [
                'attribute' => 'updated_at',
                'format' => ['todaytime', 'Y/M/d @ H:m'],
                'contentOptions' => ['class' => 'text-center', 'dir' => 'auto'],
            ],
            [
                'label' => Yii::t('app', 'Channel Name'),
                'value' => $model->channel->name
            ],
            [
                'label' => Yii::t('app', 'Project Name'),
                'value' => $model->channel->project->name
            ],
        ],
    ]) ?>
    </div>
</div>
