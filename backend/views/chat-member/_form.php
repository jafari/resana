<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ChannelMember;

/* @var $this yii\web\View */
/* @var $model common\models\ChannelMember */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="channel-member-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status')->radioList(ChannelMember::getAllStatus()) ?>
    
    <?= $form->field($model, 'created_at')->textInput() ?>
    
    <?= $form->field($model, 'updated_at')->textInput() ?>
   
    <?= $form->field($model, 'channel_id')->textInput() ?>
   
    <?= $form->field($model, 'member_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    
</div>
