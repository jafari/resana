<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\ChannelMember;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ChatMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $projectId */
/* @var $channelId */

$this->title = Yii::t('app', 'Chats');
$this->params['breadcrumbs'][] = $this->title;

$channelProjectName = function ($model) {
    return Html::a($model->channel->project->name, Url::to(['project/view' , 'id' => $model->channel->project->id]));
};

$chatName = function ($model) {
    if (isset($model->chat->name)) {
        return Html::a($model->chat->name, Url::to(['chat/view', 'id' => $model->chat->member_id]));
    }
    return null;
};

$chatUsername = function ($model) {
    if (isset($model->chat->name)) {
        return Html::a($model->chat->username, Url::to(['chat/view', 'id' => $model->chat->member_id]));
    }
    return null;
};

$channelName = function ($model) {
    return Html::a($model->channel->name, Url::to(['bot/view' , 'id' => $model->channel->id]));
};
?>
<div class="channel-member-index box box-primary">
    <div class="box-body">
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'options' => ['class' => 'table-responsive'],
            'tableOptions' => ['class' => 'table table-striped'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'id',
                    'format' => 'html',
                    'value' =>  'chat.chat_id',
                    'contentOptions' => ['class' => 'text-center', 'width' => '8%'],
                    'filterInputOptions' => ['class' => 'text-center form-control'],
                ],
                [
                    'attribute' => 'member_id',
                    'contentOptions' => ['class' => 'text-center', 'width' => '5%'],
                    'filterInputOptions' => ['class' => 'text-center form-control'],
                ],
                [
                    'label' => Yii::t('app', 'Name'),
                    'attribute' => 'chatName',
                    'format' => 'html',
                    'value' => $chatName,
                    'contentOptions' => ['class' => 'text-bold', 'dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
                [
                    'label' => Yii::t('app', 'Username'),
                    'attribute' => 'chatUsername',
                    'format' => 'html',
                    'value' => $chatUsername,
                    'contentOptions' => ['dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
                [
                    'attribute' => 'status',
                    'value' => 'statusName',
                    'filter' => ChannelMember::getAllStatus(),
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'label' => Yii::t('app', 'Channel Name'),
                    'attribute' => 'channelName',
                    'format' => 'html',
                    'value' => $channelName,
                    'contentOptions' => ['dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
                [
                    'label' => Yii::t('app', 'Project Name'),
                    'attribute' => 'channelProjectName',
                    'format' => 'html',
                    'value' => $channelProjectName,
                    'contentOptions' => ['dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['class' => 'text-center', 'dir' => 'auto'],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=>'{view} {delete} {update}'
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
