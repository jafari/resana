<?php

/* @var $this yii\web\View */
/* @var $model common\models\ChannelMember */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Chats'),
]) . $model->channel_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Chats Members'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->channel_id, 'url' => ['view', 'channel_id' => $model->channel_id, 'member_id' => $model->member_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="channel-member-update">
    <div class="bot-index box box-primary">
        <div class="box-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
