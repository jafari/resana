<?php

use common\models\Chat;
use common\models\Member;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\Url;

$Name = function ($model) {
    return Html::a($model->name, Url::to(['chat/view' , 'id' => $model->member_id]));
};

$userName = function ($model) {
    return Html::a($model->username, Url::to(['chat/view' , 'id' => $model->member_id]));
};

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ChatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Telegram Chats');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chat-index box box-primary">
    <div class="box-body">
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'chat_id',
                    'contentOptions' => ['align' => 'center', 'width' => '5%'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
                [
                    'label' => Yii::t('app', 'Name'),
                    'attribute' => 'name',
                    'format' => 'html',
                    'value' => $Name,
                    'contentOptions' => ['dir' => 'auto'],
                ],
                [
                    'label' => Yii::t('app', 'Username'),
                    'attribute' => 'username',
                    'format' => 'html',
                    'value' => $userName,
                    'contentOptions' => ['dir' => 'auto'],
                ],
                [
                    'attribute' => 'type',
                    'filter' => Chat::getTypes(),
                    'value' => 'typeName',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'label' => Yii::t('app', 'Status'),
                    'attribute' => 'status',
                    'filter' => Member::getAllStatus(),
                    'value' => 'member.statusName',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'label' => Yii::t('app', 'Member Created'),
                    'attribute' => 'member.created_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['dir' => 'auto', 'align' => 'center'],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=>'{view} {delete} {update}'
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>`
    </div>
</div>
