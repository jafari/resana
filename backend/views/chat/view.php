<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Chat */

$this->title = Yii::t('app', 'Chat');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Chats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chat-view box box-primary">
    <div class="box-body">
        <p>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->member_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'chat_id',
                'name',
                'username',
                'typeName',
                'member_id:integer',
                'member.statusName',
                [
                    'label' => Yii::t('app', 'Member Created'),
                    'attribute' => 'member.created_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['dir' => 'auto'],
                ],
            ],
        ]) ?>
    </div>
</div>
