<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Message;

/* @var $this yii\web\View */
/* @var $model common\models\Message */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="message-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'text')->textarea(['rows' => '6']) ?>

    <?= $form->field($model, 'attachments')->textarea() ?>

    <?= $form->field($model, 'media_type')->radioList(Message::getMediaTypes()); ?>

    <?= $form->field($model, 'type')->radioList(Message::getTypes()) ?>

    <?= $form->field($model, 'status')->radioList(Message::getAllStatus()) ?>

    <?= $form->field($model, 'created_at')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_at')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'channel_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'member_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'command_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content_id')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
