<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\models\Message;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Messages');
$this->params['breadcrumbs'][] = $this->title;

$mediaType = function ($model) {
    $iconClass = [
        Message::MEDIA_TYPE_AUDIO => 'music',
        Message::MEDIA_TYPE_DOCUMENT => 'book',
        Message::MEDIA_TYPE_IMAGE => 'picture-o',
        Message::MEDIA_TYPE_TEXT => 'align-right',
        Message::MEDIA_TYPE_VIDEO => 'video-camera',
    ];
    $title = Html::tag('span', '', ['class' => 'fa fa-fw fa-lg fa-' . $iconClass[$model->media_type]]);
    return Html::a($title, ['index', 'ContentSearch[media_type]' => $model->media_type]);
};
$status = function ($model) {
    $statusClass = [
        Message::STATUS_ACTIVE => 'label-success',
        Message::STATUS_DISABLED => 'label-primary',
    ];
    $title = Html::tag('span', $model->statusName, ['class' => 'text-md-size label ' . $statusClass[$model->status]]);
    return Html::a($title, ['index', 'ContentSearch[status]' => $model->status]);
};
$channelName = function ($model) {
    return Html::a($model->channel->name, Url::to([$model->channel->actionname.'/view' , 'id' => $model->channel->id]));
};
$memberName = function ($model) {
    if (isset($model->member)) {
        return Html::a($model->member->name, Url::to([$model->member->actionname.'/view' , 'id' => $model->member->id]));
    }
    return null;
};
?>

<div class="bot-index box box-primary">
    <div class="box-body">
        <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'id',
                        'contentOptions' => ['width' => '5%'],
                    ],
                    [
                        'attribute' => 'media_type',
                        'filter' => Message::getMediaTypes(),
                        'format' => 'html',
                        'value' => $mediaType,
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                    'text',
                    [
                        'attribute' => 'member_id',
                        'format' => 'html',
                        'value' => $memberName,
                    ],
                    [
                        'attribute' => 'type',
                        'value' => 'typeName',
                        'filter' =>  Message::getTypes(),
                    ],
                    [
                        'attribute' => 'status',
                        'value' => $status,
                        'format' => 'html',
                        'filter' =>  Message::getAllStatus(),
                        'contentOptions' => ['align' => 'center'],
                    ],
                    [
                        'label' => Yii::t('app', 'Channel Name'),
                        'attribute' => 'channelName',
                        'format' => 'html',
                        'value' => $channelName,
                    ],
                     'content_id',
                    [
                        'attribute' => 'created_at',
                        'format' => ['todaytime', 'Y/M/d @ H:m'],
                        'contentOptions' => ['align' => 'center'],
                    ],
                    [
                        'attribute' => 'updated_at',
                        'format' => ['todaytime', 'Y/M/d @ H:m'],
                        'contentOptions' => ['align' => 'center'],
                    ],
                    ['class' => 'yii\grid\ActionColumn',
                        'template'=>'{view} {delete} {update}'
                    ],
                ],
            ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>

