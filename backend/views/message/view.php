<?php

use yii\widgets\DetailView;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Message;

/* @var $this yii\web\View */
/* @var $model common\models\Message */

$this->title = $model->id;

$mediaUrl = $this->title = '';
$media = function ($mediaUrl) {
    return [
        Message::MEDIA_TYPE_IMAGE => Html::img($mediaUrl, ['style' => 'max-width: 100%; margin-bottom: 10px;']),
        Message::MEDIA_TYPE_VIDEO => Html::tag('video', '', ['src' => $mediaUrl, 'controls'=>'', 'style' => 'max-width: 100%; margin-bottom: 10px;']),
        Message::MEDIA_TYPE_AUDIO => Html::tag('audio', '', ['src' => $mediaUrl, 'controls'=>'', 'style' => 'max-width: 100%; margin-bottom: 10px;']),
        Message::MEDIA_TYPE_DOCUMENT  => Html::tag('document', '', ['src' => $mediaUrl, 'controls'=>'', 'style' => 'max-width: 100%; margin-bottom: 10px;']),
    ];
};

$channelName = Html::a($model->channel->name, Url::to([$model->channel->actionname.'/view' , 'id' => $model->channel->id]));
$memberName = Html::a($model->member->name, Url::to([$model->member->actionname.'/view' , 'id' => $model->member->id]));
?>

<div class="content-view box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-7 col-sm-12">
                <?= $model->text ?>
                <?php
                if ($attachments = $model->attachments) {
                    $attachments = json_decode($attachments);
                    foreach ($attachments as $attachment) {
                        echo $media($attachment->url)[$attachment->media_type];
                    }
                }
                ?>
            </div>
            <div class="col-md-5 col-sm-12">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'text',
                        [
                            'attribute' => 'media_type',
                            'format' => 'html',
                            'value' => $model->mediaTypeName,
                        ],
                        [
                            'attribute' => 'type',
                            'value' => $model->typeName,
                        ],
                        [
                            'attribute' => 'status',
                            'value' => $model->statusName,
                        ],
                        [
                            'attribute' => 'created_at',
                            'format' => ['todaytime', 'Y/M/d @ H:m'],
                            'contentOptions' => ['dir' => 'auto'],
                        ],
                        [
                            'attribute' => 'updated_at',
                            'format' => ['todaytime', 'Y/M/d @ H:m'],
                            'contentOptions' => ['align' => 'center'],
                        ],
                        [
                            'label' => Yii::t('app', 'Channel Name'),
                            'attribute' => 'channelName',
                            'format' => 'html',
                            'value' => $channelName,
                        ],
                        [
                            'attribute' => 'member_id',
                            'format' => 'html',
                            'value' => isset($model->member) ? $memberName : null,
                        ],
                        'command_id',
                        'content_id',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>