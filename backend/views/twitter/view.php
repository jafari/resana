<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Twitter */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Twitters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="twitter-view">
    <div class="bot-index box box-primary">
        <div class="box-body">
            <p>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->channel_id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'name',
                    'username',
                    'user_id',
                    'token',
                    'token_secret',
                    [
                        'label' => Yii::t('app', 'Channel Name'),
                        'attribute' => 'channel_id',
                        'value' => $model->channel->name,
                    ],
                    [
                        'label' => Yii::t('app', 'Create Time'),
                        'attribute' => 'created_at',
                        'value' => $model->channel->created_at,
                        'format' => ['todaytime', 'Y/M/d @ H:m'],
                        'contentOptions' => ['align' => 'center'],
                    ],
                    [
                        'label' => Yii::t('app', 'Update Time'),
                        'attribute' => 'created_at',
                        'value' => $model->channel->updated_at,
                        'format' => ['todaytime', 'Y/M/d @ H:m'],
                        'contentOptions' => ['align' => 'center'],
                    ],
                    [
                        'label' => Yii::t('app', 'Signature'),
                        'attribute' => 'signature',
                        'value' => $model->channel->signature,
                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>
