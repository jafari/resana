<?php

/* @var $this yii\web\View */
/* @var $model common\models\Twitter */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Twitters'),
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Twitters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->channel_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="twitter-update">
    <div class="bot-index box box-primary">
        <div class="box-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>        
</div>
