<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Channel;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TwitterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Twitters');
$this->params['breadcrumbs'][] = $this->title;

$channelProjectName = function ($model) {
    return Html::a($model->channel->project->name, Url::to(['project/view' , 'id' => $model->channel->project->id]));
};
$status = function ($model) {
    $statusClass = [
        Channel::STATUS_ACTIVE => 'label-info',
        Channel::STATUS_DELETED => 'label-primary',
    ];
    $title = Html::tag('span', $model->channel->statusName, ['class' => 'text-md-size label ' . $statusClass[$model->channel->status]]);
    return Html::a($title, ['index', 'ContentSearch[status]' => $model->channel->statusName]);
};
?>
<div class="bot-index box box-primary">
    <div class="box-body">
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => Yii::t('app', 'Channel Id'),
                    'attribute' => 'channel_id',
                    'contentOptions' => ['align' => 'center'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                    'contentOptions' => ['class' => 'text-center', 'width' => '5%'],
                ],
                'user_id',
                [
                    'label' => Yii::t('app', 'Status'),
                    'attribute' => 'status',
                    'format' => 'html',
                    'filter' => Channel:: getAllStatus(),
                    'value' => $status,
                    'contentOptions' => ['align' => 'center'],
                ],
                'name',
                'username',
                [
                    'label' => Yii::t('app', 'Project Name'),
                    'format' => 'html',
                    'attribute' => 'projectName',
                    'value' => $channelProjectName,
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'label' => Yii::t('app', 'Create Time'),
                    'attribute' => 'created_at',
                    'value' => 'channel.created_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'label' => Yii::t('app', 'Update Time'),
                    'attribute' => 'updated_at',
                    'value' => 'channel.updated_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['align' => 'center'],
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'template'=>'{view} {delete} {update}'
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
