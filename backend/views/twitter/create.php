<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Twitter */

$this->title = Yii::t('app', 'Create Twitter');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Twitters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="twitter-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
