<?php

use common\models\Project;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\BotSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Bots');
$this->params['breadcrumbs'][] = $this->title;

$channelProjectName = function ($model) {
    return Html::a($model->channel->project->name, Url::to(['project/view' , 'id' => $model->channel->project->id]));
};

?>
<div class="bot-index box box-primary">
    <div class="box-body">
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'bot_id',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'attribute' => 'name',
                    'contentOptions' => ['dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control']
                ],
                [
                    'attribute' => 'username',
                    'contentOptions' => ['dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control']
                ],
                [
                    'attribute' => 'owner',
                    'contentOptions' => ['dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control']
                ],
                [
                    'label' => Yii::t('app', 'Project Name'),
                    'format' => 'html',
                    'attribute' => 'channelProjectId',
                    'value' => $channelProjectName,
                    'filter' => ArrayHelper::map(Project::find()->asArray()->all(), 'id', 'name'),
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'label' => Yii::t('app', 'Channel Members'),
                    'attribute'=>'channelMembersCountAttr',
                    'format' => 'decimal',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'label' => Yii::t('app', 'Groups Count'),
                    'attribute'=>'groupsCountAttr',
                    'format' => 'decimal',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'label' => Yii::t('app', 'Contents Count'),
                    'attribute'=>'contentsCountAttr',
                    'format' => 'decimal',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=>'{view} {delete} {update}'
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>`
    </div>
</div>
