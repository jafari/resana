<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Bot */

$this->title = Yii::t('app', 'Bot');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bots'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bot-view box box-primary">
    <div class="box-body">
        <p>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->channel_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'bot_id',
                'name',
                'username',
                'token',
                'owner',
                'groupsCount:integer',
                'channel.contentsCount:integer',
                'channel.ChannelMembersCount:integer',
                'channel.project.name',
                'channel.project_id:integer',
                [
                    'label' => Yii::t('app', 'Signature'),
                    'attribute' => 'signature',
                    'value' => $model->channel->signature,
                ],
            ],
        ]) ?>
    </div>
</div>
