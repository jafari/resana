<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = Yii::t('app', 'User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view box box-primary">
    <div class="box-body">
    <p>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id:integer',
            'sid',
            [
                'attribute' => 'status',
                'value' => $model->statusName,
            ],
            'created_at:date',
            'email:email',
            'mobile:integer',
            'name',
            'first_name',
            'last_name',
            'text',
            [
                'attribute' => 'gender',
                'value' => $model->genderName,
            ],
            [
                'label' => Yii::t('app', 'Projects Count'),
                'format' => 'integer',
                'value' => $model->projectsCount,
            ],
            [
                'label' => Yii::t('app', 'Contents Count'),
                'format' => 'integer',
                'value' => $model->contentsCount,
            ],
         ],
    ]) ?>
     </div>
</div>
