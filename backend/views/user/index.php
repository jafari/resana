<?php

use common\models\User;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
?>
<div class="user-index box box-primary">
    <div class="box-body">
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'id',
                    'contentOptions' => ['align' => 'center', 'width' => '5%'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
                [
                    'attribute' => 'name',
                    'contentOptions' => ['dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control']
                ],
                [
                    'attribute' => 'email',
                    'contentOptions' => ['dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
                [
                    'label' => Yii::t('app', 'Gender'),
                    'attribute' => 'gender',
                    'filter' => User::getGenders(),
                    'value' => 'genderName',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'attribute' => 'status',
                    'filter' => User::getAllStatus(),
                    'value' => 'statusName',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'label' => Yii::t('app', 'Projects Count'),
                    'format' => 'integer',
                    'attribute' => 'projectsCountAttr',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'label' => Yii::t('app', 'Contents Count'),
                    'format' => 'integer',
                    'attribute' => 'contentsCountAttr',
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['dir' => 'auto', 'align' => 'center'],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=>'{view} {delete} {update}',
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
