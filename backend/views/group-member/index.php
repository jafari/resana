<?php

use common\models\GroupMember;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\GroupMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Group Members');
$this->params['breadcrumbs'][] = $this->title;

$memberChatName = function ($model) {
    if (isset($model->member->chat->name)) {
        return Html::a($model->member->chat->name, Url::to(['chat/view' , 'id' => $model->member->id]));
    }
    return null;
};

$groupName = function ($model) {
    return Html::a($model->group->name, Url::to(['group/view' , 'id' => $model->group->id]));
};

$channelName = function ($model) {
    /* Second parameter must changed in future */
    return Html::a($model->group->channel->name, Url::to(['bot/view' , 'id' => $model->group->channel->id]));
};
?>
<div class="group-member-index box box-primary">
    <div class="box-body">
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'member_id',
                    'contentOptions' => ['class' => 'text-center', 'width' => '5%'],
                    'filterInputOptions' => ['class'=>'form-control text-center'],
                ],
                [
                    'label' => Yii::t('app', 'Name'),
                    'format' => 'html',
                    'attribute' => 'memberChatName',
                    'value' => $memberChatName,
                    'contentOptions' => ['dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
                [
                    'attribute' => 'group_id',
                    'contentOptions' => ['class' => 'text-center', 'width' => '5%'],
                    'filterInputOptions' => ['class'=>'form-control text-center'],
                ],
                [
                    'label' => Yii::t('app', 'Group Name'),
                    'attribute' => 'groupName',
                    'format' => 'html',
                    'value' => $groupName,
                    'contentOptions' => ['dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
                [
                    'label' => Yii::t('app', 'Channel Name'),
                    'attribute' => 'channelName',
                    'format' => 'html',
                    'value' => $channelName,
                    'contentOptions' => ['dir' => 'auto'],
                    'filterInputOptions' => ['dir' => 'auto', 'class'=>'form-control'],
                ],
                [
                    'attribute' => 'status',
                    'value' => 'statusName',
                    'filter' => GroupMember::getAllStatus(),
                    'contentOptions' => ['align' => 'center'],
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['dir' => 'auto', 'align' => 'center'],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=>'{view} {delete} {update}'
                ],
            ],
        ]);
        ?>
        <?php Pjax::end(); ?>
    </div>
</div>
