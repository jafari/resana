<?php

/* @var $this yii\web\View */
/* @var $model common\models\GroupMember */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Group Members'),
]) . $model->group_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Group Members'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->group_id, 'url' => ['view', 'group_id' => $model->group_id, 'member_id' => $model->member_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="group-member-update">
    <div class="bot-index box box-primary">
        <div class="box-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
