<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\GroupMember */

$this->title = Yii::t('app', 'Group Member');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Group Members'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-member-view box box-primary">
    <div class="box-body">
        <p>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'group_id' => $model->group_id, 'member_id' => $model->member_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'statusName',
                [
                    'attribute' => 'created_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['dir' => 'auto'],
                ],
                [
                    'attribute' => 'updated_at',
                    'format' => ['todaytime', 'Y/M/d @ H:m'],
                    'contentOptions' => ['dir' => 'auto'],
                ],
                'group_id:integer',
                [
                    'label' => Yii::t('app', 'Group Name'),
                    'attribute' => 'group.name',
                ],

            ],
        ])
        ?>
    </div>
</div>
