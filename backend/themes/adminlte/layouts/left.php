<?php
use yii\helpers\Url;

$controller = Yii::$app->controller;
?>
<aside class="main-sidebar">

    <section class="sidebar">
    <?php if (Yii::$app->user->isGuest === false) : ?>
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-right image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->name ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> <?= Yii::t('app', 'online') ?></a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="<?= Yii::t('app', 'Search') ?>..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <ul class="sidebar-menu">
            <li class="treeview <?= $controller->id === 'user' ? 'active' : '' ?>">
                <a href="<?= Url::to(['user/index']) ?>">
                    <i class="fa fa-user"></i> <span><?= Yii::t('app', 'Users') ?></span>
                </a>
            </li>
            <li class="treeview <?= $controller->id === 'project' ? 'active' : '' ?>">
                <a href="<?= Url::to(['project/index']) ?>">
                    <i class="fa fa-dashboard"></i> <span><?= Yii::t('app', 'Projects') ?></span>
                </a>
            </li>
            <li class="treeview <?= $controller->id === 'bot' ? 'active' : '' ?>">
                <a href="<?= Url::to(['bot/index']) ?>">
                    <i class="fa fa-android"></i> <span><?= Yii::t('app', 'Bots') ?></span>
                </a>
            </li>
            <li class="treeview <?= $controller->id === 'twitter' ? 'active' : '' ?>">
                <a href="<?= Url::to(['twitter/index']) ?>">
                    <i class="fa fa-twitter"></i> <span><?= Yii::t('app', 'Twitters') ?></span>
                </a>
            </li>
            <li class="treeview <?= $controller->id === 'chat' ? 'active' : '' ?>">
                <a href="<?= Url::to(['chat/index']) ?>">
                    <i class="fa fa-comment"></i> <span><?= Yii::t('app', 'Telegram Chats') ?></span>
                </a>
            </li>
            <li class="treeview <?= $controller->id === 'group' ? 'active' : '' ?>">
                <a href="<?= Url::to(['group/index']) ?>">
                    <i class="fa fa-folder-open"></i> <span><?= Yii::t('app', 'Groups') ?></span>
                </a>
            </li>
            <li class="treeview <?= $controller->id === 'content' ? 'active' : '' ?>">
                <a href="<?= Url::to(['content/index']) ?>">
                    <i class="fa fa-newspaper-o"></i> <span><?= Yii::t('app', 'Contents') ?></span>
                </a>
            </li>
            <li class="treeview <?= $controller->id === 'project-user' ? 'active' : '' ?>">
                <a href="<?= Url::to(['project-user/index']) ?>">
                    <i class="fa fa-users"></i> <span><?= Yii::t('app', 'Project Users') ?></span>
                </a>
            </li>
            <li class="treeview <?= $controller->id === 'group-member' ? 'active' : '' ?>">
                <a href="<?= Url::to(['group-member/index']) ?>">
                    <i class="fa fa-users"></i> <span><?= Yii::t('app', 'Group Members') ?></span>
                </a>
            </li>
            <li class="treeview <?= $controller->id === 'chat-member' ? 'active' : '' ?>">
                <a href="<?= Url::to(['chat-member/index']) ?>">
                    <i class="fa fa-users"></i> <span><?= Yii::t('app', 'Chats') ?></span>
                </a>
            </li>
            <li class="treeview <?= $controller->id === 'content-group' ? 'active' : '' ?>">
                <a href="<?= Url::to(['content-group/index']) ?>">
                    <i class="fa fa-newspaper-o"></i> <span><?= Yii::t('app', 'Content Groups') ?></span>
                </a>
            </li>
            <li class="treeview <?= $controller->id === 'content-channel' ? 'active' : '' ?>">
                <a href="<?= Url::to(['content-channel/index']) ?>">
                    <i class="fa fa-newspaper-o"></i> <span><?= Yii::t('app', 'Content Channels') ?></span>
                </a>
            </li>
            <li class="treeview <?= $controller->id === 'telegram-channel' ? 'active' : '' ?>">
                <a href="<?= Url::to(['telegram-channel/index']) ?>">
                    <i class="fa fa-paper-plane"></i> <span><?= Yii::t('app', 'Telegram Channels') ?></span>
                </a>
            </li>
            <li class="treeview <?= $controller->id === 'message' ? 'active' : '' ?>">
                <a href="<?= Url::to(['message/index']) ?>">
                    <i class="fa fa-envelope"></i> <span><?= Yii::t('app', 'Messages') ?></span>
                </a>
            </li>
        </ul>
    <?php endif; ?>
    </section>
</aside>
