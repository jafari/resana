<?php
return [
    'name' => 'Resana',
    'language' => 'fa-IR',
    'timeZone' => 'Asia/Tehran',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'charset' => 'utf8mb4',
            'enableSchemaCache' => true,
            'tablePrefix' => 'tbl_',
        ],
        'cache' => [
            'class' => 'yii\caching\MemCache',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'urlManagerService' => [
            'class' => 'yii\web\urlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => 'https://resana.noghtehservice.com/',
            'rules' => [
                'api/hook/<tokenId:[\d]+>/<tokenString:[^\/]+>' => 'api/hook',
            ],
        ],
        'formatter' => [
            'class' => 'common\components\NoghtehFormatter',
            'calendar' => 'persian',
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'shenasa' => [
                    'class' => 'common\components\Shenasa',
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\GettextMessageSource',
                    'basePath' => '@common/messages',
                ],
            ],
        ],
        'media' => [
            'class' => 'noghteh\services\MediaService',
        ],
    ],
];
