<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2016 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\validators\content;

use Yii;
use yii\validators\Validator;
use common\models\Content;

/**
 * Class TextLengthValidator
 * @package common\validators\content
 *
 * @author Morteza Pouretemadi <e.morteza94@yahoo.com>
 */
class TwitterFilesValidator extends Validator
{
    const MAX_TWITTER_VIDEO_SIZE = 15000000;
    const MAX_TWITTER_IMAGE_SIZE = 5000000;

    /**
     * @param Content $model
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute)
    {
        if (!$model->attachments || $model->isTwitterSelected() === false) {
            return true;
        }

        $file = $model->lastFilePath;
        $fileSize = filesize($file);
        $fileType = $model->media_type;
        $fileMime = mime_content_type($file);
        $acceptedMimeTypes = [
            'image/jpg',
            'image/jpeg',
            'image/gif',
            'image/png',
            'image/webp',
            'video/mp4',
        ];

        if (!in_array($fileMime, $acceptedMimeTypes, true)) {
            $model->addError('files', Yii::t('app', 'Media file not supported in twitter channels.'));
            return false;
        }
        if (($fileType === Content::MEDIA_TYPE_IMAGE || $fileType === Content::MEDIA_TYPE_GIF) && $fileSize > self::MAX_TWITTER_IMAGE_SIZE) {
            $model->addError('files', Yii::t('app', 'Image size in twitter channels must be fewer than 5MB.'));
            return false;
        }
        if ($fileType === Content::MEDIA_TYPE_VIDEO && $fileSize > self::MAX_TWITTER_VIDEO_SIZE) {
            $model->addError('files', Yii::t('app', 'Video size in twitter channels must be fewer than 15MB.'));
            return false;
        }
        return true;
    }
}
