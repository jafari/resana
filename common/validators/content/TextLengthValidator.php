<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2016 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\validators\content;

use Yii;
use yii\validators\Validator;
use common\models\Content;

/**
 * Class TextLengthValidator
 * @package common\validators\content
 *
 * @author Morteza Pouretemadi <e.morteza94@yahoo.com>
 */
class TextLengthValidator extends Validator
{
    const MAX_TWITTER_TEXT_LENGTH = 140;

    /**
     * @param Content $model
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute)
    {
        $channels = $model->selectedChannels;

        foreach ($channels as $channel) {
            if ($model->isTwitterSelected() && mb_strlen($model->twitterText . $channel->signature) > self::MAX_TWITTER_TEXT_LENGTH) {
                $model->addError('text', Yii::t('app', 'the maximum number of text and signature for twitter channel is 140'));
                return false;
            }

            if (($model->isTelegramSelected() || $model->isTelegramBotSelected()) &&
                $model->media_type === Content::MEDIA_TYPE_AUDIO &&
                mb_strlen($model->telegramText . $channel->signature) > Content::MAX_TELEGRAM_CAPTION_LENGTH) {
                $model->addError('text', Yii::t('app', 'the maximum number of text and signature for telegram channel is 200'));
                return false;
            }
        }

        return true;
    }
}
