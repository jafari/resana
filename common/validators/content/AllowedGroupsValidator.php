<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2016 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\validators\content;

use Yii;
use yii\validators\Validator;
use yii\helpers\ArrayHelper;
use common\models\Channel;
use common\models\Group;
use common\validators\IsListAllowedValidator;

/**
 * Class GroupsListValidator
 * @package common\components\validators
 *
 * @author Morteza Pouretemadi <e.morteza94@yahoo.com>
 */
class AllowedGroupsValidator extends Validator
{
    /**
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute)
    {
        $allGroups = $model->groupsList;
        if ($allGroups === null) {
            return true;
        }

        $allowedGroups = $this->getAllowedGroupsIds($model);
        foreach ($allGroups as $groupId) {
            if (in_array($groupId, $allowedGroups, false) === false) {
                $this->addError($model, $attribute, Yii::t('app', 'GroupsList is invalid'));
                return false;
            }
        }
        return true;
    }

    /**
     * returns the allowed groups ids.
     * @return array groups
     */
    public function getAllowedGroupsIds($model)
    {
        $allowedChannels = Channel::findAll(['project_id' => $model->project_id]);
        $allowedChannels = ArrayHelper::getColumn(ArrayHelper::toArray($allowedChannels), 'id');
        $allowedGroups = Group::findAll(['channel_id' => $allowedChannels]);
        $allowedGroups = ArrayHelper::getColumn(ArrayHelper::toArray($allowedGroups), 'id');
        return $allowedGroups;
    }
}
