<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2016 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\validators\content;

use Yii;
use yii\validators\Validator;
use yii\helpers\ArrayHelper;
use common\models\Channel;
use common\validators\IsListAllowedValidator;

/**
 * Class ChannelsListValidator
 * @package common\validators\content
 *
 * @author Morteza Pouretemadi <e.morteza94@yahoo.com>
 */
class AllowedChannelsValidator extends Validator
{
    /**
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute)
    {
        $allChannels = $model->channelsList;
        if ($allChannels === null) {
            $this->addError($model, 'channels', Yii::t('app', 'You must select least 1 channel'));
        }
        
        $allowedChannels = $this->getAllowedChannelsIds($model);
        foreach ($allChannels as $channelId) {
            if (in_array($channelId, $allowedChannels, false) === false) {
                $this->addError($model, $attribute, Yii::t('app', 'ChannelsList is invalid'));
                return false;
            }
        }
        return true;
    }

    /**
     * returns the allowed channels ids.
     * @return array channels
     */
    public function getAllowedChannelsIds($model)
    {
        $allowedChannels = Channel::findAll(['project_id' => $model->project_id]);
        $allowedChannels = ArrayHelper::getColumn(ArrayHelper::toArray($allowedChannels), 'id');
        return $allowedChannels;
    }
}
