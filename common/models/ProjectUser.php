<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\traits\ModelHelperTrait;

/**
 * This is the model class for table "{{%project_user}}".
 *
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $role
 * @property integer $project_id
 * @property integer $user_id
 *
 * @property Project $project
 * @property User $user
 */
class ProjectUser extends ActiveRecord
{
    use ModelHelperTrait;

    const ROLE_OWNER = 1;
    const ROLE_ADMIN = 2;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'email'],
            ['email', 'required'],
            ['email', 'string', 'max' => 64],
            ['email', 'unique', 'targetAttribute' => ['project_id', 'user_id']],
            ['role', 'isChangeableRole'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => [],
            self::SCENARIO_CREATE => ['email'],
            self::SCENARIO_UPDATE => ['role'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_user}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status' => Yii::t('app', 'Status'),
            'statusName' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Create Time'),
            'updated_at' => Yii::t('app', 'Update Time'),
            'role' => Yii::t('app', 'Role'),
            'roleName' => Yii::t('app', 'Role'),
            'project_id' => Yii::t('app', 'Project ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'email' => Yii::t('app', 'Email'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if ($this->isChangeableRole()) {
                return true;
            }
            return false;
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Add new member to the project
     * @return bool
     */
    public function addMember()
    {
        if (is_null($this->email) || is_null($this->project_id)) {
            return false;
        }

        $user = User::findByEmail($this->email);

        if (is_null($user)) {
            $this->addError('email', Yii::t('app', 'Email is not exists'));
            return false;
        }

        $this->user_id = $user->id;
        $this->role = static::ROLE_ADMIN;

        return $this->insert();
    }

    /**
     * Return role name
     * @return string
     */
    public function getRoleName()
    {
        $roles = static::getRoles();

        if (array_key_exists($this->role, $roles)) {
            return $roles[$this->role];
        }

        return 'Unknown_role_' . $this->role;
    }

    /**
     * Check user project is owner or not
     * @return bool
     */
    public function isOwner()
    {
        return $this->role === self::ROLE_OWNER;
    }

    /**
     * Check role is changeable or not
     * Owner role can't edit or delete
     * @return bool
     */
    public function isChangeableRole()
    {
        if ($this->role == self::ROLE_OWNER || $this->oldAttributes['role'] == self::ROLE_OWNER) {
            $this->addError('role', Yii::t('app', 'Owner role can not change.'));
            return false;
        }
        return true;
    }

    /**
     * Return all roles
     * @return array
     */
    public static function getRoles()
    {
        return [
            static::ROLE_ADMIN => Yii::t('app', 'Admin'),
            static::ROLE_OWNER => Yii::t('app', 'Owner'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new ProjectUserQuery(get_called_class());
    }
}
