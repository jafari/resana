<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[\common\models\Channel]].
 *
 * @see \common\models\Channel
 */
class ChannelQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['status' => Channel::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     * @return \common\models\Channel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Channel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Scope project
     * @param integer $id
     * @return $this
     */
    public function project($id)
    {
        return $this->andWhere(['project_id' => $id]);
    }
}
