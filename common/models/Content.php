<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\models;

use Yii;
use CURLFile;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Html;
use yii\web\ServerErrorHttpException;
use yii\web\UploadedFile;
use common\traits\ModelHelperTrait;
use common\components\telegram\Sender;
use common\behaviors\CleanTextBehavior;
use common\components\Watermark;
use common\validators\content\AllowedChannelsValidator;
use common\validators\content\AllowedGroupsValidator;
use common\validators\content\TextLengthValidator;
use common\validators\content\TwitterFilesValidator;

/**
 * This is the model class for table "{{%content}}".
 *
 * @property integer $id
 * @property string $text
 * @property string $attachments
 * @property integer $media_type
 * @property integer $type
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $published_at
 * @property integer $cnt_sent
 * @property integer $cnt_failed
 * @property integer $created_by
 * @property integer $project_id
 * @property integer $f_chat_id
 * @property integer $f_message_id
 * @property integer $message_id
 * @property string $uploadPath
 * @property string $telegramText
 * @property string $twitterText
 * @property array $selectedChannelsType
 * @property array $selectedChannels
 * @property array $selectedGroups
 * @property array $files
 * @property string $lastFilePath
 *
 * @property User $createdBy
 * @property Project $project
 * @property ContentChannel[] $contentChannels
 * @property Channel[] $channels
 * @property ContentGroup[] $contentGroups
 * @property Group[] $groups
 * @property ContentMember[] $contentMembers
 * @property Member[] $members
 * @property Bot[] $bots
 * @property TelegramChannel[] $telegramChannels
 * @property Twitter[] $twitters
 * @property Message[] $messages
 *
 * @author Ali Irani <ali@irani.im>
 */
class Content extends ActiveRecord
{
    use ModelHelperTrait;

    const MEDIA_TYPE_TEXT = 1;
    const MEDIA_TYPE_IMAGE = 2;
    const MEDIA_TYPE_VIDEO = 3;
    const MEDIA_TYPE_AUDIO = 4;
    const MEDIA_TYPE_DOCUMENT = 5;
    const MEDIA_TYPE_FORWARD = 6;
    const MEDIA_TYPE_GIF = 7;

    const TYPE_PUBLIC = 1;
    const TYPE_PRIVATE = 2;

    const STATUS_IN_QUEUE = 1;
    const STATUS_SENDING = 2;
    const STATUS_SENT = 3;
    const STATUS_FAILED = 4;
    const STATUS_PENDING = 5;
    const STATUS_SCHEDULED = 6;

    const UPLOAD_PATH_DIR = '/uploads/';
    const TEXT_SUMMARY_LENGTH = 150;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    const MAX_TELEGRAM_CAPTION_LENGTH = 200;
    const MAX_UPLOAD_FILE_SIZE = 50000000; // 50MB

    private $_channels;
    private $_groups;
    private $_files;
    private $_telegramText;
    private $_twitterText;
    private $_selectedChannels;
    private $_selectedGroups;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%content}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['media_type', 'type', 'status', 'published_at'], 'integer'],
            ['text', TextLengthValidator::className()],
            ['text', 'required',
                'when' => function ($model) {
                    return $model->_files === null && $model->attachments === null;
                },
                'whenClient' => 'function(attribute, value){return $("#content-files").val().length == 0 && $(".file-input-new").length != 0;}'
            ],
            ['channelsList', 'required', 'message' => Yii::t('app', 'You must select least 1 channel'), 'skipOnEmpty' => true],
            ['channelsList', AllowedChannelsValidator::className(), 'skipOnEmpty' => false],
            ['status', 'in', 'range' => array_keys(self::getAllStatus())],
            ['type', 'in', 'range' => array_keys(self::getTypes())],
            ['status', 'default', 'value' => self::STATUS_IN_QUEUE],
            ['type', 'default', 'value' => self::TYPE_PUBLIC],
            ['files', 'file', 'maxSize' => self::MAX_UPLOAD_FILE_SIZE, 'maxFiles' => 1],
            ['files', TwitterFilesValidator::className()],
            ['published_at', 'default', 'value' => time()],
            ['publishTime', 'date', 'format' => 'yyyy-MM-dd HH:mm'],
            ['groupsList', 'safe'],
            ['groupsList', AllowedGroupsValidator::className(), 'skipOnEmpty' => false],
            ['media_type', 'default', 'value' => self::MEDIA_TYPE_TEXT],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => CleanTextBehavior::className(),
                'allowedTags' => ['ul', 'li', 'a', 'b', 'strong', 'em', 'i', 'p'],
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_by',
                ],
                'value' => function () {
                    if (Yii::$app->user->isGuest === false) {
                        return Yii::$app->user->id;
                    }
                    return isset($this->created_by) ? $this->created_by : null;
                }
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'status',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'status',
                ],
                'value' => function () {
                    if ($this->published_at > time()) {
                        return self::STATUS_SCHEDULED;
                    } elseif (in_array($this->status, [self::STATUS_FAILED, self::STATUS_PENDING], true) && $this->scenario === self::SCENARIO_UPDATE) {
                        return self::STATUS_IN_QUEUE;
                    }
                    return $this->status;
                }
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'published_at',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'published_at',
                ],
                'value' => function () {
                    return $this->published_at < time() ? time() : $this->published_at;
                }
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'text' => Yii::t('app', 'Text'),
            'attachments' => Yii::t('app', 'Attachments'),
            'media_type' => Yii::t('app', 'Media Type'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'published_at' => Yii::t('app', 'Published At'),
            'cnt_sent' => Yii::t('app', 'Cnt Sent'),
            'cnt_failed' => Yii::t('app', 'Cnt Failed'),
            'created_by' => Yii::t('app', 'Created By'),
            'project_id' => Yii::t('app', 'Project ID'),
            'f_chat_id' => Yii::t('app', 'F Chat ID'),
            'f_message_id' => Yii::t('app', 'F Message ID'),
            'message_id' => Yii::t('app', 'Message ID'),
            'channelsList' => Yii::t('app', 'Channels'),
            'files' => Yii::t('app', 'Files'),
            'summaryText' => Yii::t('app', 'Summary Text'),
            'publishTime' => Yii::t('app', 'Publish Time'),
            'groupsList' => Yii::t('app', 'Groups List'),
            'channelsCount' => Yii::t('app', 'Channel Count'),
            'groupsCount' => Yii::t('app', 'Group Count'),
        ];
    }

    /**
     * @inheritDoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => ['status', 'published_at', 'publish_time'],
            self::SCENARIO_CREATE => ['text', 'attachments', 'media_type', 'type', 'status', 'published_at', 'channelsList', 'files', 'groupsList', 'publishTime'],
            self::SCENARIO_UPDATE => ['text', 'attachments', 'media_type', 'type', 'status', 'published_at', 'channelsList', 'files', 'groupsList', 'publishTime'],
        ];
    }


    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();

        $this->on(self::EVENT_AFTER_INSERT, [$this, 'setChannels']);
        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'setChannels']);

        $this->on(self::EVENT_AFTER_INSERT, [$this, 'setGroups']);
        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'setGroups']);

        $this->on(self::EVENT_AFTER_INSERT, [$this, 'sendToQueue']);
        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'sendToQueue']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentChannels()
    {
        return $this->hasMany(ContentChannel::className(), ['content_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannels()
    {
        return $this->hasMany(Channel::className(), ['id' => 'channel_id'])->viaTable('{{%content_channel}}', ['content_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannelsCount()
    {
        return $this->getChannels()->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBots()
    {
        return $this->hasMany(Bot::className(), ['channel_id' => 'channel_id'])->viaTable('{{%content_channel}}', ['content_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTelegramChannels()
    {
        return $this->hasMany(TelegramChannel::className(), ['channel_id' => 'channel_id'])->viaTable('{{%content_channel}}', ['content_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTwitters()
    {
        return $this->hasMany(Twitter::className(), ['channel_id' => 'channel_id'])->viaTable('{{%content_channel}}', ['content_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentGroups()
    {
        return $this->hasMany(ContentGroup::className(), ['content_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::className(), ['id' => 'group_id'])->viaTable('{{%content_group}}', ['content_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupsCount()
    {
        return $this->getGroups()->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentMembers()
    {
        return $this->hasMany(ContentMember::className(), ['content_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMembers()
    {
        return $this->hasMany(Member::className(), ['id' => 'member_id'])->viaTable('{{%content_member}}', ['content_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['content_id' => 'id']);
    }

    /**
     * Returns media type constant value or id
     * @param $type
     * @return int|null|string
     */
    public static function getMediaTypeId($type)
    {
        $mediaTypes = [
            'gif' => self::MEDIA_TYPE_GIF,
            'image' => self::MEDIA_TYPE_IMAGE,
            'mp4' => self::MEDIA_TYPE_VIDEO,
            'audio' => self::MEDIA_TYPE_AUDIO,
            'document' => self::MEDIA_TYPE_DOCUMENT,
            'pdf' => self::MEDIA_TYPE_DOCUMENT,
            'video' => self::MEDIA_TYPE_DOCUMENT,
        ];

        foreach ($mediaTypes as $key => $typeId) {
            if (strpos($type, $key) !== false) {
                return $typeId;
            }
        }
        return null;
    }

    /**
     * Returns all media types
     * @return array
     */
    public static function getMediaTypes()
    {
        return [
            static::MEDIA_TYPE_TEXT => Yii::t('app', 'Text'),
            static::MEDIA_TYPE_IMAGE => Yii::t('app', 'Image'),
            static::MEDIA_TYPE_VIDEO => Yii::t('app', 'Video'),
            static::MEDIA_TYPE_AUDIO => Yii::t('app', 'Audio'),
            static::MEDIA_TYPE_DOCUMENT => Yii::t('app', 'Document'),
            static::MEDIA_TYPE_GIF => Yii::t('app', 'Gif'),
            static::MEDIA_TYPE_FORWARD => Yii::t('app', 'Forward'),
        ];
    }

    /**
     * Returns all status
     * @return array
     */
    public static function getAllStatus()
    {
        return [
            static::STATUS_SENDING => Yii::t('app', 'Sending'),
            static::STATUS_IN_QUEUE => Yii::t('app', 'In Queue'),
            static::STATUS_SENT => Yii::t('app', 'Sent'),
            static::STATUS_FAILED => Yii::t('app', 'Failed'),
            static::STATUS_PENDING => Yii::t('app', 'Pending'),
            static::STATUS_SCHEDULED => Yii::t('app', 'Scheduled'),
        ];
    }

    /**
     * Return all content types
     * @return array
     */
    public static function getTypes()
    {
        return [
            static::TYPE_PRIVATE => Yii::t('app', 'Private'),
            static::TYPE_PUBLIC => Yii::t('app', 'Public'),
        ];
    }

    /**
     * Returns media type property
     * @return string
     */
    public function getMediaTypeName()
    {
        return $this->getNameOf('media_type', self::getMediaTypes());
    }

    /**
     * Returns upload path directory
     * @return string
     */
    public function getUploadPath()
    {
        return Yii::$aliases['@frontend'] . self::UPLOAD_PATH_DIR;
    }

    /**
     * Save files
     */
    public function setFiles()
    {
        if ($files = UploadedFile::getInstances($this, 'files')) {
            foreach ($files as $file) {
                $media = Yii::$app->media->save($file);
                if (array_key_exists('success', $media) && $media['success']) {
                    $mediaInfo = $media['data']['media'][0];
                    $this->media_type = static::getMediaTypeId($file->type);
                    $attachments[] = [
                        'url' => $mediaInfo['uriMin'],
                        'name' => $mediaInfo['original_file_name'],
                        'media_type' => $this->media_type,
                    ];
                } elseif (array_key_exists('message', $media)) {
                    Yii::error($media['message'], __METHOD__);
                }

                $file->saveAs($this->uploadPath . $file->name);
                chmod($this->uploadPath . $file->name, 0666);
            }

            if (isset($attachments)) {
                if ($this->attachments) {
                    $attachments = array_merge(json_decode($this->attachments), $attachments);
                }

                $this->attachments = json_encode($attachments);
            }
        }
    }

    /**
     * Returns attachment files
     * @return array
     */
    public function getFiles()
    {
        if (!$this->attachments) {
            return [];
        }

        if ($this->_files === null) {
            return $this->_files = json_decode($this->attachments);
        }

        return $this->_files;
    }

    /**
     * Returns uploaded file object
     * @return \CURLFile
     */
    public function getLastFile()
    {
        if (!$this->files) {
            return null;
        }

        foreach ($this->_files as $file) {
            if ($this->media_type === $file->media_type) {
                if (isset($file->path)) {
                    return new CURLFile($file->path);
                }
                
                $results = glob($this->uploadPath . $file->name);
                if (array_key_exists(0, $results)) {
                    return new CURLFile($results[0]);
                }
            }
        }

        return null;
    }

    /**
     * Return selected channels
     * @return Channel[]
     */
    public function getSelectedChannels()
    {
        if ($this->_selectedChannels === null) {
            return $this->_selectedChannels = Channel::findAll(['id' => $this->_channels]);
        }
        return $this->_selectedChannels;
    }

    /**
     * Return selected channels
     * @return Channel[]
     */
    public function getSelectedGroups()
    {
        if ($this->_selectedGroups === null) {
            return $this->_selectedGroups = Channel::findAll(['id' => $this->_groups]);
        }
        return $this->_selectedGroups;
    }

    /**
     * Return channels list in an array
     * @return array
     */
    public function getChannelsList()
    {
        $contentChannel = $this->_channels ? $this->selectedChannels : $this->channels;
        return ArrayHelper::getColumn(ArrayHelper::toArray($contentChannel), 'id');
    }

    /**
     * Set channels list
     * @param $channels
     * @throws \Exception
     */
    public function setChannelsList($channels)
    {
        $this->_channels = $channels;
    }

    /**
     * Set content channels list, add new channels and remove old channels
     * @throws \Exception
     */
    public function setChannels()
    {
        if (in_array($this->scenario, [self::SCENARIO_CREATE, self::SCENARIO_UPDATE], true) === false) {
            return false;
        }
        ContentChannel::deleteAll(['content_id' => $this->id]);
        $channels = $this->_channels?: [];
        foreach ($channels as $channel) {
            $contentChannel= new ContentChannel([
                'content_id' => $this->id,
                'channel_id' => $channel,
            ]);
            $contentChannel->save();
        }
        return true;
    }

    /**
     * @return array
     */
    public function getSelectedChannelsType()
    {
        $types = [];
        if ($this->_channels) {
            foreach ($this->_channels as $channelId) {
                $channel = Channel::findOne($channelId);
                $types[] = $channel ? $channel->type : null;
            }
        }
        return $types;
    }

    /**
     * Checks if a twitter channel is selected or not
     * @return bool
     */
    public function isTwitterSelected()
    {
        return in_array(Channel::TYPE_TWITTER, $this->selectedChannelsType, true);
    }

    /**
     * Checks if a telegram channel is selected or not
     * @return bool
     */
    public function isTelegramSelected()
    {
        return in_array(Channel::TYPE_TELEGRAM_CHANNEL, $this->selectedChannelsType, true);
    }

    /**
     * Checks if a telegram bot channel is selected or not
     * @return bool
     */
    public function isTelegramBotSelected()
    {
        return in_array(Channel::TYPE_TELEGRAM_BOT, $this->selectedChannelsType, true);
    }

    /**
     * Return groups list in an array
     * @return array
     */
    public function getGroupsList()
    {
        $contentGroup = $this->_groups ? $this->selectedGroups : $this->groups;
        return ArrayHelper::getColumn(ArrayHelper::toArray($contentGroup), 'id');
    }

    /**
     * Set content groups list, add new groups and remove old channels
     * @param $groups
     * @throws \Exception
     */
    public function setGroupsList($groups)
    {
        $this->_groups = $groups;
    }

    /**
     * Set content groups, add new groups and remove old groups
     * @throws \Exception
     */
    public function setGroups()
    {
        if (in_array($this->scenario, [self::SCENARIO_CREATE, self::SCENARIO_UPDATE], true) === false) {
            return false;
        }
        ContentGroup::deleteAll(['content_id' => $this->id]);
        $groups = $this->_groups?: [];
        foreach ($groups as $group) {
            $contentGroup = new ContentGroup([
                'content_id' => $this->id,
                'group_id' => $group,
            ]);
            $contentGroup->save();
        }
        return true;
    }

    public function getChannelGroups()
    {
        $channelIds = ArrayHelper::getColumn(ArrayHelper::toArray($this->channels), 'id');

        return Group::findAllByChannels($channelIds);
    }

    /**
     * Returns publish time string from published_at
     * @return string
     */
    public function getPublishTime()
    {
        if ($this->published_at) {
            return date('Y-m-d H:i', $this->published_at);
        }
        return null;
    }

    /**
     * Set publish time to published_at with timestamp format
     * @param integer $time
     */
    public function setPublishTime($time)
    {
        if ($time) {
            $this->published_at = strtotime($time);
        }
    }

    /**
     * Returns summary of text
     * @return string
     */
    public function getSummaryText()
    {
        return $this->text ? StringHelper::truncate($this->text, self::TEXT_SUMMARY_LENGTH) : '';
    }

    /**
     * Returns stripped text for telegram
     * @return string
     */
    public function getTelegramText()
    {
        if ($this->_telegramText === null) {
            $text = str_replace("\n", '', $this->text);
            $text = strip_tags($text, '<a><b><strong><i><em><pre><code><p>');
            return $this->_telegramText = rtrim(str_replace(['<p>', '</p>'], ['',"\n"], $text));
        }
        return $this->_telegramText;
    }

    /**
     * Returns stripped text for twitter
     * @return string
     */
    public function getTwitterText()
    {
        if ($this->_twitterText === null) {
            $text = str_replace("\n", '', $this->text);
            $text = strip_tags($text, '<p>');
            return $this->_twitterText = rtrim(str_replace(['<p>', '</p>'], ['',"\n"], $text));
        }
        return $this->_twitterText;
    }

    /**
     * Returns last file path
     * @return string|null
     */
    public function getLastFilePath()
    {
        if (count($this->files) === 0) {
            return null;
        }

        if (isset($this->files[0]->path)) {
            return $this->files[0]->path;
        }

        $results = glob($this->uploadPath . $this->files[0]->name);

        return isset($results[0]) ? $results[0] : null;
    }

    /**
     * If watermark config is set, returns watermarked image file path else returns the original image file path
     * @param array $watermarkConfig
     * @return mixed|null|string
     * @throws ServerErrorHttpException
     */
    public function getWatermarkedFilePath($watermarkConfig = null)
    {
        if (!$this->attachments) {
            return null;
        }

        if ($this->media_type !== self::MEDIA_TYPE_IMAGE) {
            return $this->lastFilePath;
        }

        if ($watermarkConfig !== null) {
            $watermark = new Watermark([
                'baseImage' => (array)$this->files[0],
                'config' => $watermarkConfig,
            ]);
            
            if ($watermark->finalImage !== null) {
                return $this->uploadPath . $watermark->finalImage['name'];
            }
        }
        return $this->lastFilePath;
    }

    /**
     * Send content to the telegram chats
     * @return boolean
     */
    public function sendToTelegramChats()
    {
        if ($groups = $this->groups) {
            $handlers = $groups;
        } elseif ($bots = $this->bots) {
            $handlers = $bots;
        } else {
            return false;
        }

        foreach ($handlers as $handler) {
            /* @var Bot $bot */
            if ($handler instanceof Group) {
                $bot = $handler->bot;
                $handlerChats = $handler->activeChats;
            } else {
                $bot = $handler;
                $handlerChats = $bot->activeChats;
            }

            if (!$handlerChats) {
                continue;
            }

            $bots[$bot->bot_id] = $bot;

            foreach ($handlerChats as $chat) {
                $botChats[$bot->bot_id][] = $chat->chat_id;
                $activeChats[$chat->chat_id] = $chat;
            }
        }
        $this->cnt_failed = $this->cnt_failed ?: 0;
        $this->cnt_sent = $this->cnt_sent ?: 0;
        if (isset($botChats)) {
            foreach ($botChats as $botId => $chats) {
                $chats = array_unique($chats);
                $channel = $bots[$botId]->channel;
                $longCaptionLink = $this->isLongCaption($channel->signature) ? Html::a(' ', $this->getFiles()[0]->url) : '';
                $telegramSender = new Sender([
                    'text' => $longCaptionLink . $this->telegramText . $channel->signature,
                    'file' => new CURLFile($this->getWatermarkedFilePath($channel->getConfig('watermark', true))),
                    'bot' => $bots[$botId],
                    'media_type' => $this->isLongCaption($channel->signature) ? self::MEDIA_TYPE_TEXT : $this->media_type,
                    'forward_chat_id' => $this->f_chat_id,
                    'forward_message_id' => $this->f_message_id,
                ]);

                $sentCount = $failedCount = 0;
                foreach ($chats as $chatId) {
                    $sent = $telegramSender->sendToChat($activeChats[$chatId]);
                    $this->cnt_sent++;
                    $sentCount++;
                    if ($sent === false) {
                        $this->cnt_failed++;
                        $failedCount++;
                    }
                    $this->updateAttributes(['cnt_sent' => $this->cnt_sent, 'cnt_failed' => $this->cnt_failed]);
                }
                ContentChannel::updateAll([
                    'cnt_sent' => $sentCount,
                    'cnt_failed' => $failedCount,
                    'sent_at' => time(),
                ], [
                    'content_id' => $this->id,
                    'channel_id' => $bots[$botId]->channel_id
                ]);
            }
        }
        return true;
    }

    /**
     * Send content
     * @return bool
     */
    public function sendToTelegramChannels()
    {
        if (!($telegramChannels = $this->telegramChannels)) {
            return false;
        }
        $this->cnt_failed = $this->cnt_failed ?: 0;
        $this->cnt_sent = $this->cnt_sent ?: 0;
        /* @var TelegramChannel $telegramChannel */
        foreach ($telegramChannels as $telegramChannel) {
            $failedCount = 0;
            $channel = $telegramChannel->channel;
            $longCaptionLink = $this->isLongCaption($channel->signature) ? Html::a(' ', $this->getFiles()[0]->url) : '';
            $telegramSender = new Sender([
                'text' => $longCaptionLink . $this->telegramText . $channel->signature,
                'file' => new CURLFile($this->getWatermarkedFilePath($channel->getConfig('watermark', true))),
                'bot' => $telegramChannel->bot,
                'media_type' => $this->isLongCaption($channel->signature) ? self::MEDIA_TYPE_TEXT : $this->media_type,
                'forward_chat_id' => $this->f_chat_id,
                'forward_message_id' => $this->f_message_id,
            ]);
            $sent = $telegramSender->sendToChannel($telegramChannel->username);
            $this->cnt_sent++;
            if ($sent === false) {
                $this->cnt_failed++;
                $failedCount++;
            }
            ContentChannel::updateAll([
                'cnt_sent' => 1,
                'cnt_failed' => $failedCount,
                'sent_at' => time(),
            ], [
                'content_id' => $this->id,
                'channel_id' => $telegramChannel->channel_id
            ]);
            $this->updateAttributes(['cnt_sent' => $this->cnt_sent, 'cnt_failed' => $this->cnt_failed]);
        }
        return true;
    }

    /**
     * Send content to twitter
     * @return bool
     */
    public function sendToTwitter()
    {
        if (!($twitters = $this->twitters)) {
            return false;
        }
        $this->cnt_failed = $this->cnt_failed ?: 0;
        $this->cnt_sent = $this->cnt_sent ?: 0;
        $connection = Twitter::getConnection();

        foreach ($twitters as $twitter) {
            $failedCount = 0;
            if (!$twitter) {
                continue;
            }
            $connection->setOauthToken($twitter->token, $twitter->token_secret);
            $signature = $twitter->channel->signature;
            if ($this->media_type === self::MEDIA_TYPE_TEXT) {
                $message = $this->twitterText . $signature;
                $status = $connection->post('statuses/update', ['status' => $message]);
            } else {
                $channel = $twitter->channel;
                $attachments = $this->getWatermarkedFilePath($channel->getConfig('watermark', true));
                if ($this->media_type === self::MEDIA_TYPE_VIDEO) {
                    $media = $connection->upload('media/upload', ['media' => $attachments, 'media_type' => 'video/mp4'], true);
                } else {
                    $media = $connection->upload('media/upload', ['media' => $attachments]);
                }
                $status = $connection->post('statuses/update', [
                    'status' => $this->twitterText . $signature,
                    'media_ids' => $media->media_id_string,
                ]);
            }
            $sent = $status ? true : false;
            $this->cnt_sent++;
            if ($sent === false) {
                $this->cnt_failed++;
                $failedCount++;
            }
            ContentChannel::updateAll([
                'cnt_sent' => 1,
                'cnt_failed' => $failedCount,
                'sent_at' => time(),
            ], [
                'content_id' => $this->id,
                'channel_id' => $twitter->channel_id
            ]);
            $this->updateAttributes(['cnt_sent' => $this->cnt_sent, 'cnt_failed' => $this->cnt_failed]);
        }
        return true;
    }


    /**
     * Send content to Resana channels
     * @return bool
     * @throws \Exception
     */
    public function sendToChannels()
    {
        $this->sendToTelegramChats();
        $this->sendToTelegramChannels();
        $this->sendToTwitter();
        $this->status = ($this->cnt_failed === $this->cnt_sent) ? self::STATUS_FAILED : self::STATUS_SENT;
    }

    /**
     * Sending content to AMQP queue after saving content
     * @return bool
     */
    public function sendToQueue()
    {
        if (in_array($this->status, [self::STATUS_SCHEDULED, self::STATUS_IN_QUEUE], true) === false) {
            return false;
        }

        $delayedTime = 0;
        if ($this->status === self::STATUS_SCHEDULED) {
            $delayedTime = ($this->published_at - time()) * 1000; // delay time in milliseconds
        }

        Yii::$app->amqp->sendMessage($this->id, $delayedTime);
        return true;
    }

    /**
     * Change content status
     * @param int $status
     * @return bool
     */
    public function changeStatus($status)
    {
        return $this->updateAttributes(['status' => $status]) ? true : false;
    }

    /**
     * Check length of caption (whether is to 200 character or more)
     * @param string $signature
     * @return bool
     */
    public function isLongCaption($signature = '')
    {
        return mb_strlen($this->telegramText . $signature) > self::MAX_TELEGRAM_CAPTION_LENGTH && in_array($this->media_type, [self::MEDIA_TYPE_IMAGE, self::MEDIA_TYPE_VIDEO, self::MEDIA_TYPE_GIF], true) ? true : false;
    }

    /**
     * @inheritdoc
     * @return ContentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ContentQuery(get_called_class());
    }
}
