<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\models;

use common\traits\ModelHelperTrait;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $type
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $mobile
 * @property string $name
 * @property string $first_name
 * @property string $last_name
 * @property string $text
 * @property integer $gender
 * @property string $config
 *
 * @property Content[] $contents
 * @property ProjectUser[] $projectUsers
 * @property Project[] $projects
 *
 * @author Ali Irani <ali@irani.im>
 */
class User extends ActiveRecord implements IdentityInterface
{
    use ModelHelperTrait;

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_PENDING = 2;

    const ROLE_MEMBER = 'member';
    const ROLE_SUPER_ADMIN = 'superAdmin';

    const GENDER_UNKNOWN = 0;
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;
    const GENDER_LEGAL_PERSON = 3;

    const SCENARIO_OAUTH_LOGIN = 'oauth_login';
    const SCENARIO_UPDATE = 'update';

    const AVATAR_PATH = '/uploads/avatar/';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'name',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'name',
                ],
                'value' => function($event) {
                    $name = $event->sender->first_name;
                    $name .= $event->sender->last_name ? ' ' . $event->sender->last_name : '';
                    return $name;
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['text', 'first_name', 'last_name', 'text'], 'default', 'value' => ''],
            ['gender', 'in', 'range' => array_keys(static::getGenders())],
            ['mobile', 'default', 'value' => NULL],
            ['first_name', 'required', 'on' => self::SCENARIO_UPDATE],
            ['avatar', 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => [],
            self::SCENARIO_OAUTH_LOGIN => ['sid', 'email', 'username', 'name', 'first_name', 'last_name', 'text', 'status'],
            self::SCENARIO_UPDATE => ['name', 'first_name', 'last_name', 'gender', 'mobile', 'avatar'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sid' => Yii::t('app', 'Shenasa ID'),
            'username' => Yii::t('app', 'Username'),
            'auth_key' => Yii::t('app', 'Authentication Key'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'role' => Yii::t('app', 'Role'),
            'created_at' => Yii::t('app', 'Create Time'),
            'updated_at' => Yii::t('app', 'Update Time'),
            'email' => Yii::t('app', 'Email'),
            'mobile' => Yii::t('app', 'Mobile'),
            'name' => Yii::t('app', 'Name'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'text' => Yii::t('app', 'Text'),
            'gender' => Yii::t('app', 'Gender'),
            'config' => Yii::t('app', 'Config'),
            'avatar' => Yii::t('app', 'Avatar'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
        ];
    }

    /**
     * Returns all status
     * @return array
     */
    public static function getAllStatus()
    {
        return [
            static::STATUS_DELETED => Yii::t('app', 'Deleted'),
            static::STATUS_ACTIVE => Yii::t('app', 'Active'),
            static::STATUS_PENDING => Yii::t('app', 'Pending'),
        ];
    }

    /**
     * Returns Genders
     * @return array
     */
    public static function getGenders()
    {
        return [
            static::GENDER_UNKNOWN => Yii::t('app', 'Unknown'),
            static::GENDER_MALE => Yii::t('app', 'Male'),
            static::GENDER_FEMALE => Yii::t('app', 'Female'),
            static::GENDER_LEGAL_PERSON => Yii::t('app', 'Legal Person'),
        ];
    }

    /**
     * Returns gender property
     * @return string
     */
    public function getGenderName()
    {
        return $this->getNameOf('gender', self::getGenders());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContents()
    {
        return $this->hasMany(Content::className(), ['created_by' => 'id']);
    }

    /**
     * Returns count of contents
     * @return int|string
     */
    public function getContentsCount()
    {
        return $this->getContents()->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectUsers()
    {
        return $this->hasMany(ProjectUser::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['id' => 'project_id'])->viaTable('{{%project_user}}', ['user_id' => 'id']);
    }

    /**
     * Returns count of projects
     * @return int|string
     */
    public function getProjectsCount()
    {
        return $this->getProjects()->count();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds user by Shenasa id
     *
     * @param string $sid
     * @return static|null
     */
    public static function findBySid($sid)
    {
        return static::findOne(['sid' => $sid, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function getConfigParam($key)
    {
        $configs = json_decode($this->config);
        return isset($configs->$key) ? $configs->$key : null;
    }

    public function setConfigParam($key, $value)
    {
        $configs = json_decode($this->config, true);
        $configs[$key] = $value;
        $this->config = json_encode($configs);
    }

    public function setAvatar()
    {
        if ($file = UploadedFile::getInstance($this, 'avatar')) {
            $media = Yii::$app->media->save($file);
            if (isset($media['success']) && $media['success']) {
                $mediaInfo = $media['data']['media'][0];
                $this->setConfigParam('avatar', $mediaInfo['uriMin']);
            } elseif (isset($media['message'])) {
                Yii::error($media['message'], __METHOD__);
            }
        }
    }

    public function getAvatar($width = 200, $height = 200)
    {
        $default = "http://nmedia.ncdn.ir/v1/image/S2n4gNINReIkz5UrbgE22yNje7ay7raHU7cTLRe04UQdv7qw0Ju7JQ/s/w{$width}h{$height}-c";
        $avatar = $this->getConfigParam('avatar');
        return $avatar ? $avatar . "w{$width}h{$height}-c": $default;
    }

    /**
     * Login user by Auth client
     *
     * @param array $client
     * @return null
     */
    public static function login($client)
    {
        if ($user = self::findBySid($client['id'])) {
            return Yii::$app->user->login($user);
        }

        if (!($user = self::findByEmail($client['email']))) {
            $user = new User();
        }

        $user->scenario = self::SCENARIO_OAUTH_LOGIN;
        $user->setAttributes($client);
        $user->status = User::STATUS_ACTIVE;

        if ($user->save() === false) {
            return false;
        }

        $auth = Yii::$app->authManager;
        if ($auth->getAssignment(self::ROLE_MEMBER, $user->id) === null) {
            $authorRole = $auth->getRole(self::ROLE_MEMBER);
            $auth->assign($authorRole, $user->id);
        }

        return Yii::$app->user->login($user);
    }

    /**
     * Add new user by email address and returns user object
     * @param $email
     * @return bool|int
     * @throws \Exception
     */
    public static function addByEmail($email)
    {
        $user = new User(['email' => $email, 'status' => self::STATUS_PENDING]);

        return $user->insert() ? $user : false;
    }

    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }
}
