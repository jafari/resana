<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\traits\ModelHelperTrait;

/**
 * This is the model class for table "{{%channel_member}}".
 *
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $channel_id
 * @property integer $member_id
 *
 * @property Channel $channel
 * @property Member $member
 * @property Chat $chat
 *
 * @author Ali Irani <ali@irani.im>
 */
class ChannelMember extends \yii\db\ActiveRecord
{
    use ModelHelperTrait;

    const STATUS_ACTIVE = 1;
    const STATUS_DISABLED = 2;
    const STATUS_TELEGRAM_BLOCKED = 3;
    const STATUS_TELEGRAM_LEFT_GROUP = 4;
    const STATUS_TELEGRAM_UNSUBSCRIBED = 5;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%channel_member}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status' => Yii::t('app', 'Status'),
            'statusName' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Create Time'),
            'updated_at' => Yii::t('app', 'Update Time'),
            'channel_id' => Yii::t('app', 'Channel ID'),
            'member_id' => Yii::t('app', 'Member ID'),
            'id' => Yii::t('app', 'ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannel()
    {
        return $this->hasOne(Channel::className(), ['id' => 'channel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(Member::className(), ['id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChat()
    {
        return $this->hasOne(Chat::className(), ['member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBot()
    {
        return $this->hasOne(Bot::className(), ['channel_id' => 'channel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['id' => 'project_id'])->viaTable('{{%channel}}', ['id' => 'channel_id']);
    }

    /**
     * Returns all status
     * @return array
     */
    public static function getAllStatus()
    {
        return [
            static::STATUS_ACTIVE => Yii::t('app', 'Active'),
            static::STATUS_DISABLED => Yii::t('app', 'Disabled'),
            static::STATUS_TELEGRAM_BLOCKED => Yii::t('app', 'Blocked'),
            static::STATUS_TELEGRAM_LEFT_GROUP => Yii::t('app', 'Left Group'),
            static::STATUS_TELEGRAM_UNSUBSCRIBED => Yii::t('app', 'Unsubscribed'),
        ];
    }

    /**
     * Change status
     * @param $status
     * @return int
     */
    public function changeStatusTo($status)
    {
        return $this->updateAttributes(['status' => $status]);
    }

    /**
     * Change status to telegram left group
     * @return int
     */
    public function leftGroup()
    {
        return $this->changeStatusTo(self::STATUS_TELEGRAM_LEFT_GROUP);
    }

    /**
     * Change status to active
     * @return int
     */
    public function activate()
    {
        return $this->changeStatusTo(self::STATUS_ACTIVE);
    }
}
