<?php

namespace common\models;

use Yii;
use common\traits\ModelHelperTrait;

/**
 * This is the model class for table "{{%content_group}}".
 *
 * @property integer $status
 * @property integer $sent_at
 * @property integer $cnt_sent
 * @property integer $cnt_failed
 * @property integer $content_id
 * @property integer $group_id
 *
 * @property Content $content
 * @property Group $group
 */
class ContentGroup extends \yii\db\ActiveRecord
{
    use ModelHelperTrait;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%content_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'sent_at', 'cnt_sent', 'cnt_failed', 'content_id', 'group_id'], 'integer'],
            [['content_id', 'group_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status' => Yii::t('app', 'Status'),
            'statusName' => Yii::t('app', 'Status'),
            'sent_at' => Yii::t('app', 'Sent At'),
            'cnt_sent' => Yii::t('app', 'Cnt Sent'),
            'cnt_failed' => Yii::t('app', 'Cnt Failed'),
            'content_id' => Yii::t('app', 'Content ID'),
            'group_id' => Yii::t('app', 'Group ID'),
            'group_name' => Yii::t('app', 'Group Name'),
            'content_text' => Yii::t('app', 'Group'),
            'summaryText' => Yii::t('app', 'Summary Text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContent()
    {
        return $this->hasOne(Content::className(), ['id' => 'content_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }
}
