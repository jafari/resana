<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Content]].
 *
 * @see Content
 */
class ContentQuery extends \yii\db\ActiveQuery
{

    /**
     * @inheritdoc
     * @return Group[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Group|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function typePublic()
    {
        return $this->andWhere(['{{%content}}.type' => Content::TYPE_PUBLIC]);
    }

    public function project($project_id)
    {
        return $this->andWhere(compact('project_id'));
    }

    /**
     * Add a join with a query contains count attribute a field
     * Example on search model:
     * ```
     * $query->select([
     *  '{{%user}}.*',
     *  'projectsCountAttr',
     *  'contentsCountAttr',
     * ]);
     * $query->countJoin('{{%project_user}}', '{{%user}}.id', 'user_id', 'projectsCountAttr');
     * $query->countJoin('{{%content}}', '{{%user}}.id', 'created_by', 'contentsCountAttr');
     * ```
     * @author Ali Irani <ali@irani.im>
     *
     * @param $tableName {{%table_name}}
     * @param $on {{%on_table_name}}.id
     * @param string $attr attribute name
     * @param string $countAttr count attribute name
     * @return \yii\db\Query
     */
    public function countJoin($tableName, $on, $attr, $countAttr)
    {
        return $this->join('LEFT JOIN', "(SELECT $attr, COUNT(*) AS $countAttr FROM $tableName GROUP BY $attr) $tableName ON $on = $tableName.$attr");
    }
}
