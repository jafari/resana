<?php

namespace common\models;

use common\traits\ModelHelperTrait;
use Yii;

/**
 * This is the model class for table "{{%content_channel}}".
 *
 * @property integer $status
 * @property integer $sent_at
 * @property integer $cnt_sent
 * @property integer $cnt_failed
 * @property integer $content_id
 * @property integer $channel_id
 *
 * @property Content $content
 * @property Channel $channel
 */
class ContentChannel extends \yii\db\ActiveRecord
{
    use ModelHelperTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%content_channel}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'sent_at', 'cnt_sent', 'cnt_failed', 'content_id', 'channel_id'], 'integer'],
            [['content_id', 'channel_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status' => Yii::t('app', 'Status'),
            'statusName' => Yii::t('app', 'Status'),
            'sent_at' => Yii::t('app', 'Sent At'),
            'cnt_sent' => Yii::t('app', 'Cnt Sent'),
            'cnt_failed' => Yii::t('app', 'Cnt Failed'),
            'content_id' => Yii::t('app', 'Content ID'),
            'channel_id' => Yii::t('app', 'Channel ID'),
            'summaryText' => Yii::t('app', 'Summary Text'),
            'channelName' => Yii::t('app', 'Channel Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContent()
    {
        return $this->hasOne(Content::className(), ['id' => 'content_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannel()
    {
        return $this->hasOne(Channel::className(), ['id' => 'channel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBot()
    {
        return $this->hasOne(Bot::className(), ['channel_id' => 'channel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTelegramChannel()
    {
        return $this->hasOne(TelegramChannel::className(), ['channel_id' => 'channel_id']);
    }
}
