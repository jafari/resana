<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%telegram_channel}}".
 *
 * @property string $name
 * @property string $username
 * @property integer $channel_id
 * @property integer $bot_channel_id
 *
 * @property Channel $channel
 */
class TelegramChannel extends \yii\db\ActiveRecord
{
    public $projectId;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%telegram_channel}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'username', 'bot_channel_id'], 'required'],
            ['bot_channel_id', 'integer'],
            [['name'], 'string', 'max' => 256, 'min' => 3],
            [['username'], 'string', 'max' => 128],
            [['username'], 'unique'],
            ['username', 'match', 'pattern' => '/@/', 'not' => true, 'message' => Yii::t('app', '@ is not valid!')],
        ];
    }

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();

        $this->on(self::EVENT_BEFORE_INSERT, [$this, 'setChannelId']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'username' => Yii::t('app', 'Username'),
            'channel_id' => Yii::t('app', 'Channel ID'),
            'bot_channel_id' => Yii::t('app', 'Bot Channel Id'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannel()
    {
        return $this->hasOne(Channel::className(), ['id' => 'channel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBot()
    {
        return $this->hasOne(Bot::className(), ['channel_id' => 'bot_channel_id']);
    }

    /**
     * Add channel record and set channel_id
     * @return bool
     */
    public function setChannelId()
    {
        $channel = new Channel([
            'project_id' => $this->projectId,
            'type' => Channel::TYPE_TELEGRAM_CHANNEL,
        ]);

        if ($channel->save()) {
            $this->channel_id = $channel->id;
            return true;
        }

        return false;
    }
}
