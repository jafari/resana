<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use Abraham\TwitterOAuth\TwitterOAuth;

/**
 * This is the model class for table "{{%twitter}}".
 *
 * @property string $name
 * @property string $username
 * @property string $token
 * @property string $token_secret
 * @property string $channel_id
 *
 * @property Channel $channel
 */
class Twitter extends ActiveRecord
{
    public $projectId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['token', 'token_secret'], 'required'],
            [['token', 'token_secret'], 'unique', 'targetAttribute' => ['token', 'token_secret']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%twitter}}';
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->on(self::EVENT_BEFORE_INSERT, [$this, 'setChannelId']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'username' => Yii::t('app', 'Username'),
            'token' => Yii::t('app', 'Token'),
            'token_secret' => Yii::t('app', 'Token Secret'),
            'channel_id' => Yii::t('app', 'Channel ID'),
            'user_id' => Yii::t('app', 'Twitter User Id'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannel()
    {
        return $this->hasOne(Channel::className(), ['id' => 'channel_id']);
    }

    /**
     * @inheritdoc
     * @return TwitterQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TwitterQuery(get_called_class());
    }

    public function setChannelId()
    {
        $channel = new Channel([
            'project_id' => $this->projectId,
            'type' => Channel::TYPE_TWITTER,
        ]);

        if ($channel->save()) {
            $this->channel_id = $channel->id;
            return true;
        }

        return false;
    }

    /**
     * Returns a twitter connection
     * @param bool $proxy enable proxy mode
     * @return TwitterOAuth
     */
    public static function getConnection($proxy = false)
    {
        $twitterParams = Yii::$app->params['twitter'];
        $connection = new TwitterOAuth(
            $twitterParams['CONSUMER_KEY'],
            $twitterParams['CONSUMER_SECRET']
        );
        $connection->setTimeouts(150, 150);
        if ($proxy) {
            $connection->setProxy([
                'CURLOPT_PROXY' => $twitterParams['CURLOPT_PROXY'],
                'CURLOPT_PROXYPORT' => $twitterParams['CURLOPT_PROXYPORT'],
                'CURLOPT_PROXYUSERPWD' => '',
            ]);
        }
        return $connection;
    }
}
