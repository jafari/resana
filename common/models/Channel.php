<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\models;

use Yii;
use yii\helpers\Json;
use yii\db\ActiveRecord;
use common\traits\ModelHelperTrait;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%channel}}".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $config
 * @property integer $project_id
 *
 * @property Bot $bot
 * @property Project $project
 * @property ChannelMember[] $channelMembers
 * @property Member[] $members
 * @property Command[] $commands
 * @property ContentChannel[] $contentChannels
 * @property Content[] $contents
 * @property Group[] $groups
 * @property Mail $mail
 * @property Message[] $messages
 * @property Twitter $twitter
 *
 * @author Ali Irani <ali@irani.im>
 */
class Channel extends ActiveRecord
{
    use ModelHelperTrait;

    const TYPE_TELEGRAM_BOT = 1;
    const TYPE_TELEGRAM_CHANNEL = 2;
    const TYPE_EMAIL = 3;
    const TYPE_SMS = 4;
    const TYPE_GCM = 5;
    const TYPE_TWITTER = 6;
    const TYPE_FACEBOOK = 7;
    const TYPE_AFSARAN = 8;

    const STATUS_ACTIVE = 1;
    const STATUS_DELETED = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%channel}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['signature', 'string'],
            ['type', 'in', 'range' => array_keys(self::getTypes())],
            ['project_id', 'exist', 'targetClass' => '\common\models\Project', 'targetAttribute' => 'id'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Create Time'),
            'updated_at' => Yii::t('app', 'Update Time'),
            'config' => Yii::t('app', 'Config'),
            'project_id' => Yii::t('app', 'Project ID'),
            'name' => Yii::t('app', 'Name'),
            'typeName' => Yii::t('app', 'Type'),
            'signature' => Yii::t('app', 'Signature'),
            'contentsCount' => Yii::t('app', 'Content Count'),
            'ChannelMembersCount' => Yii::t('app', 'Member Count'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBot()
    {
        return $this->hasOne(Bot::className(), ['channel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTelegramChannel()
    {
        return $this->hasOne(TelegramChannel::className(), ['channel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannelMembers()
    {
        return $this->hasMany(ChannelMember::className(), ['channel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannelMembersCount()
    {
        return $this->getChannelMembers()->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMembers()
    {
        return $this->hasMany(Member::className(), ['id' => 'member_id'])->viaTable('{{%channel_member}}', ['channel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommands()
    {
        return $this->hasMany(Command::className(), ['channel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentChannels()
    {
        return $this->hasMany(ContentChannel::className(), ['channel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContents()
    {
        return $this->hasMany(Content::className(), ['id' => 'content_id'])->viaTable('{{%content_channel}}', ['channel_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentsCount()
    {
        return $this->getContents()->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::className(), ['channel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMail()
    {
        return $this->hasOne(Mail::className(), ['channel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['channel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTwitter()
    {
        return $this->hasOne(Twitter::className(), ['channel_id' => 'id']);
    }

    /**
     * Returns all types in an array
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_TELEGRAM_BOT => Yii::t('app', 'Telegram Bot'),
            self::TYPE_TELEGRAM_CHANNEL => Yii::t('app', 'Telegram Channel'),
            self::TYPE_AFSARAN => Yii::t('app', 'Afsaran'),
            self::TYPE_EMAIL => Yii::t('app', 'Email'),
            self::TYPE_FACEBOOK => Yii::t('app', 'Facebook'),
            self::TYPE_GCM => Yii::t('app', 'Google Cloud Message'),
            self::TYPE_SMS => Yii::t('app', 'SMS'),
            self::TYPE_TWITTER => Yii::t('app', 'Twitter'),
        ];
    }

    /**
     * Returns all status
     * @return array
     */
    public static function getAllStatus()
    {
        return [
            static::STATUS_ACTIVE => Yii::t('app', 'Active'),
            static::STATUS_DELETED => Yii::t('app', 'Deleted'),
        ];
    }

    /**
     * Return channel config with attributeName = $key and return string.
     * @param string $key
     * @return string
     */
    public function getConfig($key, $asArray = false)
    {
        $config = Json::decode($this->config, $asArray);
        if ($asArray) {
            return $config !== null & isset($config[$key]) ? $config[$key] : null;
        }
        return $config !== null & isset($config->$key) ? $config->$key : null;
    }

    /**
     * Set channel config with attributeName = key and value = val.
     * @param string $key
     * @param string $val
     */
    public function setConfig($key, $val)
    {
        $config = Json::decode($this->config);
        $config[$key] = $val;
        $this->config = json_encode($config);
    }

    /**
     * Return signature in a array
     * @return string
     */
    public function getSignature($asArray = false)
    {
        return  $this->getConfig('signature', $asArray);
    }

    /**
     * Set signature config with attributeName = val.
     * @param string $val
     */
    public function setSignature($val)
    {
        $this->setConfig('signature', $val);
    }

    /**
     * Return watermark in a array
     * @return string
     */
    public function getWatermark($asArray = false)
    {
        return  $this->getConfig('watermark', $asArray);
    }

    /**
     * Set watermark config with attributeName = val.
     * @param string $val
     */
    public function setWatermark($val)
    {
        $this->setConfig('watermark', $val);
    }

    /**
     * Returns channel members in an array with key as id and value as name
     * @return array
     */
    public function getChannelMembersList()
    {
        $channelMembers = $this->channelMembers;
        $memberType = $this->memberType;
        $members = [];

        if (is_null($memberType) || empty($channelMembers)) {
            return $members;
        }

        foreach ($channelMembers as $channelMember) {
            $members[$channelMember->member_id] = $channelMember->$memberType->name;
        }

        return $members;
    }

    /**
     * Returns channel name
     * @return string
     */
    public function getName()
    {
        switch ($this->type) {
            case self::TYPE_TELEGRAM_BOT:
                $name = $this->bot->name;
                break;
            case self::TYPE_TELEGRAM_CHANNEL:
                $name = $this->telegramChannel->name;
                break;
            case self::TYPE_TWITTER:
                $name = $this->twitter->name;
                break;
            default:
                $name = 'Unknown';
                break;
        }

        return $name;
    }

    public function getActionName()
    {
        switch ($this->type) {
            case self::TYPE_TELEGRAM_BOT:
                $name = 'bot';
                break;
            case self::TYPE_TELEGRAM_CHANNEL:
                $name = 'telegram-channel';
                break;
            case self::TYPE_TWITTER:
                $name = 'twitter';
                break;
            default:
                $name = null;
                break;
        }

        return $name;
    }

    public function getMemberType()
    {
        $types = [
            self::TYPE_TELEGRAM_BOT => 'chat',
        ];

        return isset($types[$this->type]) ? $types[$this->type] : null;
    }

    /**
     * Change channel status to disabled
     * @return bool
     */
    public function disable()
    {
        if ($this->type === self::TYPE_TELEGRAM_BOT) {
            $this->bot->unsetWebhook();
        }
        
        return $this->updateAttributes(['status' => self::STATUS_DELETED]) ? true : false;
    }

    /**
     * @inheritdoc
     * @return ChannelQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ChannelQuery(get_called_class());
    }
}
