<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use common\traits\ModelHelperTrait;

/**
 * This is the model class for table "{{%project}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $auth_key
 *
 * @property Channel[] $channels
 * @property Content[] $contents
 * @property ProjectUser[] $projectUsers
 * @property User[] $users
 *
 * @author Ali Irani <ali@irani.im>
 */
class Project extends ActiveRecord
{
    use ModelHelperTrait;

    const STATUS_ACTIVE = 1;
    const STATUS_DISABLED = 2;

    const SCENARIO_GENERATE_AUTH_KEY = 'gen_auth_key';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'length' => [4, 128]],
            [['description'], 'string', 'max' => 1000],
            [['status'], 'default', 'value' => self::STATUS_ACTIVE],
            [['status'], 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DISABLED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => ['name' , 'description', 'status'],
            self::SCENARIO_GENERATE_AUTH_KEY => ['auth_key'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'status' => Yii::t('app', 'Status'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'statusName' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Create Time'),
            'updated_at' => Yii::t('app', 'Update Time'),
            'totalUsers' => Yii::t('app', 'Total Users'),
            'ProjectUsersCount' => Yii::t('app', 'Project Users Count'),
            'projectOwner.user.name' => Yii::t('app', 'Project Owner Name'),
            'projectOwner.user.id' => Yii::t('app', 'Project Owner Id'),
            'ContentsCount' => Yii::t('app', 'Contents Count'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            // Add project owner to {{%project_user}}
            $projectUser = new ProjectUser();
            $projectUser->project_id = $this->id;
            $projectUser->user_id = Yii::$app->user->id;
            $projectUser->role = ProjectUser::ROLE_OWNER;
            $projectUser->insert(false);
        }
    }

    /**
     * Returns all status
     * @return array
     */
    public static function getAllStatus()
    {
        return [
            static::STATUS_ACTIVE => Yii::t('app', 'Active'),
            static::STATUS_DISABLED => Yii::t('app', 'Disabled'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::OP_INSERT => self::SCENARIO_DEFAULT,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannels()
    {
        return $this->hasMany(Channel::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveChannels()
    {
        return $this->hasMany(Channel::className(), ['project_id' => 'id'])->where(['{{%channel}}.status' => Channel::STATUS_ACTIVE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBots()
    {
        return $this->hasMany(Bot::className(), ['channel_id' => 'id'])->viaTable('{{%channel}}', ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContents()
    {
        return $this->hasMany(Content::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentsCount()
    {
        return $this->hasMany(Content::className(), ['project_id' => 'id'])->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectUsers()
    {
        return $this->hasMany(ProjectUser::className(), ['project_id' => 'id']);
    }

    /**
     * Returns count of users of project
     * @return int|string
     */
    public function getProjectUsersCount()
    {
        return $this->getProjectUsers()->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectOwner()
    {
        return $this->hasOne(ProjectUser::className(), ['project_id' => 'id'])->where(['role' => ProjectUser::ROLE_OWNER]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('{{%project_user}}', ['project_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ProjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProjectQuery(get_called_class());
    }

    /**
     * Change status project to the disabled
     * @return boolean
     */
    public function disable()
    {
        foreach ($this->channels as $channel) {
            $channel->disable();
        }

        return $this->updateAttributes(['status' => self::STATUS_DISABLED]) ? true : false;
    }

    /**
     * Change status project to the activate
     * @return boolean
     */
    public function activate()
    {
        return $this->updateAttributes(['status' => self::STATUS_ACTIVE]) ? true : false;
    }

    /**
     * Returns count of project members
     * @return int|string
     */
    public function getTotalUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('{{%project_user}}', ['project_id' => 'id'])->count();
    }

    /**
     * Returns count of project contents
     * @return int|string
     */
    public function getTotalContents()
    {
        return $this->hasMany(Content::className(), ['project_id' => 'id'])->count();
    }

    /**
     * Returns count of project channels
     * @return int|string
     */
    public function getTotalChannels()
    {
        return $this->hasMany(Channel::className(), ['project_id' => 'id'])->andWhere(['status' => Channel::STATUS_ACTIVE])->count();
    }

    /**
     * Returns recent project messages
     * @param int $limit
     * @return array|Message[]
     */
    public function getRecentMessages($limit = 10)
    {
        $channelIds = ArrayHelper::getColumn($this->activeChannels, 'id');
        $messages = Message::find()->where(['channel_id' => $channelIds])
            ->orderBy(['created_at' => SORT_DESC, 'id' => SORT_DESC])
            ->limit($limit)
            ->all();
        return $messages;
    }

    /**
     * Returns all owner projects in an array
     * @return array
     */
    public static function getAllOwner()
    {
        $projects = Project::find()->joinWith([
            'projectUsers' => function ($query) {
                return $query->owner();
            }
        ])->asArray()->all();

        return ArrayHelper::map($projects, 'id', 'name');
    }

    /**
     * Check logged in user is project owner
     * @return bool
     */
    public function isOwner()
    {
        return $this->projectOwner->user_id === Yii::$app->user->id;
    }

    /**
     * Returns channels in an array with key as id and value as name
     * @return array
     */
    public function getChannelsList()
    {
        $channels = $this->activeChannels;
        $result = [];
        if (empty($channels)) {
            return $result;
        }

        foreach ($channels as $channel) {
            $result[$channel->id] = "{$channel->name} ({$channel->typeName})";
        }

        return $result;
    }

    /**
     * Returns project bots in an array with key as id and value as name
     * @return array
     */
    public function getBotsList()
    {
        $bots = $this->bots;
        $result = [];
        if (empty($bots)) {
            return $result;
        }

        foreach ($bots as $bot) {
            $result[$bot->channel_id] = $bot->name;
        }

        return $result;
    }

    /**
     * Insert auth_Key value(32 bit token)
     * @return boolean
     */
    public function setAuthKey()
    {
        return $this->updateAttributes(['auth_key' => $this->generateAuthKey()]) ? true : false;
    }

    /**
     * Check exist of auth_key value
     * @return boolean
     */
    public function hasAuthKey()
    {
        return ($this->auth_key) ? true : false;
    }

    /**
     * Generate random Auth Key(32 bit)
     * @return boolean
     */
    public function generateAuthKey()
    {
        return $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Delete auth_key field for capability of get new token.
     * @return boolean
     */
    public function deleteAuthKey()
    {
        return $this->updateAttributes(['auth_key' => null]);
    }
}
