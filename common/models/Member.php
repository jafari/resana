<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\traits\ModelHelperTrait;

/**
 * This is the model class for table "{{%member}}".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ChannelMember[] $channelMembers
 * @property Channel[] $channels
 * @property Chat $chat
 * @property ContentMember[] $contentMembers
 * @property Content[] $contents
 * @property Email $email
 * @property GroupMember[] $groupMembers
 * @property Group[] $groups
 * @property Message[] $messages
 * @property Mobile $mobile
 *
 * @author Ali Irani <ali@irani.im>
 */
class Member extends \yii\db\ActiveRecord
{
    use ModelHelperTrait;

    const TYPE_TELEGRAM_BOT = 1;
    const TYPE_EMAIL = 2;
    const TYPE_SMS = 3;
    const TYPE_GCM = 4;

    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%member}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'statusName' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Create Time'),
            'updated_at' => Yii::t('app', 'Update Time'),
        ];
    }

    /**
     * Returns all chat types
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_TELEGRAM_BOT => Yii::t('app', 'Telegram Bot'),
            self::TYPE_EMAIL => Yii::t('app', 'Email'),
            self::TYPE_SMS => Yii::t('app', 'SMS'),
            self::TYPE_GCM => Yii::t('app', 'Google Cloud Messaging'),
        ];
    }

    /**
     * Returns all status
     * @return array
     */
    public static function getAllStatus()
    {
        return [
            static::STATUS_ACTIVE => Yii::t('app', 'Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannelMembers()
    {
        return $this->hasMany(ChannelMember::className(), ['member_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannels()
    {
        return $this->hasMany(Channel::className(), ['id' => 'channel_id'])->viaTable('{{%channel_member}}', ['member_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChat()
    {
        return $this->hasOne(Chat::className(), ['member_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentMembers()
    {
        return $this->hasMany(ContentMember::className(), ['member_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContents()
    {
        return $this->hasMany(Content::className(), ['id' => 'content_id'])->viaTable('{{%content_member}}', ['member_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmail()
    {
        return $this->hasOne(Email::className(), ['member_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupMembers()
    {
        return $this->hasMany(GroupMember::className(), ['member_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::className(), ['id' => 'group_id'])->viaTable('{{%group_member}}', ['member_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['member_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMobile()
    {
        return $this->hasOne(Mobile::className(), ['member_id' => 'id']);
    }

    /**
     * Return action name
     * @return null|string
     */
    public function getActionName()
    {
        switch ($this->type) {
            case self::TYPE_TELEGRAM_BOT:
                $name = 'chat';
                break;
            case self::TYPE_EMAIL:
                $name = 'email';
                break;
            case self::TYPE_SMS:
                $name = 'sms';
                break;
            case self::TYPE_GCM:
                $name = 'gcm';
                break;
            default:
                $name = null;
                break;
        }

        return $name;
    }

    /**
     * Returns Member name based on its type
     * @return string
     */
    public function getName()
    {
        switch ($this->type) {
            case self::TYPE_TELEGRAM_BOT:
                return $this->chat->name;
            default:
                return Yii::t('app', 'Unknown');
        }
    }
}
