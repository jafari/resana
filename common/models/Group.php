<?php

namespace common\models;

use common\traits\ModelHelperTrait;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%group}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $channel_id
 * @property integer $command_id
 * @property array $membersList
 * @property integer $membersCount
 *
 * @property integer $project_id
 *
 * @property ContentGroup[] $contentGroups
 * @property Content[] $contents
 * @property Channel $channel
 * @property Command $command
 * @property GroupMember[] $groupMembers
 * @property Member[] $members
 */
class Group extends \yii\db\ActiveRecord
{
    use ModelHelperTrait;

    const TYPE_PUBLIC = 1;
    const TYPE_PRIVATE = 2;

    const STATUS_DISABLED = 0;
    const STATUS_ACTIVE = 1;

    public $project_id;
    private $_members;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'status', 'channel_id', 'project_id'], 'integer'],
            [['channel_id', 'project_id', 'name', 'type'], 'required'],
            [['channel_id'], 'exist', 'targetAttribute' => ['channel_id' => 'id', 'project_id'], 'targetClass' => '\common\models\Channel'],
            [['name'], 'string', 'length' => [3, 128]],
            ['type', 'in', 'range' => array_keys(self::getTypes())],
            ['type', 'default', 'value' => self::TYPE_PUBLIC],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['membersList', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'typeName' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'statusName' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Create Time'),
            'updated_at' => Yii::t('app', 'Update Time'),
            'channel_id' => Yii::t('app', 'Channel ID'),
            'command_id' => Yii::t('app', 'Command ID'),
            'project_id' => Yii::t('app', 'Project ID'),
            'membersList' => Yii::t('app', 'Members List'),
            'membersCount' => Yii::t('app', 'Members Count'),
            'contentsCount' => Yii::t('app', 'Content Count'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();

        $this->on(self::EVENT_AFTER_INSERT, [$this, 'setMembers']);
        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'setMembers']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentGroups()
    {
        return $this->hasMany(ContentGroup::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContents()
    {
        return $this->hasMany(Content::className(), ['id' => 'content_id'])->viaTable('{{%content_group}}', ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentsCount()
    {
        return $this->getContents()->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannel()
    {
        return $this->hasOne(Channel::className(), ['id' => 'channel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBot()
    {
        return $this->hasOne(Bot::className(), ['channel_id' => 'channel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommand()
    {
        return $this->hasOne(Command::className(), ['id' => 'command_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupMembers()
    {
        return $this->hasMany(GroupMember::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMembers()
    {
        return $this->hasMany(Member::className(), ['id' => 'member_id'])->viaTable('{{%group_member}}', ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMembersCount()
    {
        return $this->getMembers()->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveChats()
    {
        $channelId = $this->channel_id;
        return $this->hasMany(Chat::className(), ['member_id' => 'member_id'])
            ->viaTable('{{%channel_member}}', ['member_id' => 'member_id'], function ($query) use($channelId) {
                /* @var $query \yii\db\ActiveQuery */
                return $query->andWhere([
                    'status' => ChannelMember::STATUS_ACTIVE,
                    'channel_id' => $channelId,
                ]);
            })
            ->viaTable('{{%group_member}}', ['group_id' => 'id']);

    }

    /**
     * Returns all groups specific channel in a list
     * @param $channel_id
     */
    public static function getList($channel_id)
    {
        $groups = static::find()->where(compact('channel_id'))->asArray()->all();
        return ArrayHelper::map($groups, 'id', 'name');
    }

    /**
     * Returns group types
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_PUBLIC => Yii::t('app', 'Public'),
            self::TYPE_PRIVATE => Yii::t('app', 'Private'),
        ];
    }

    /**
     * Returns all status
     * @return array
     */
    public static function getAllStatus()
    {
        return [
            static::STATUS_ACTIVE => Yii::t('app', 'Active'),
            static::STATUS_DISABLED => Yii::t('app', 'Disabled'),
        ];
    }

    /**
     * Return group members list in in an array
     * @return array
     */
    public function getMembersList()
    {
        return ArrayHelper::getColumn(ArrayHelper::toArray($this->members), 'id');
    }

    /**
     * Set group members list, add new members and remove old members
     * @param $members
     * @throws \Exception
     */
    public function setMembersList($members)
    {
        $this->_members = $members;
    }

    /**
     * Add new and delete old members for group
     * @throws \Exception
     */
    public function setMembers()
    {
        $members = $this->_members ?: [];
        $oldMembers = $this->membersList;
        $deletedMembers = array_diff($oldMembers, $members);
        $newMembers = array_diff($members, $oldMembers);

        if ($deletedMembers) {
            foreach ($deletedMembers as $member) {
                $groupMember = GroupMember::findOne([
                    'group_id' => $this->id,
                    'member_id' => $member,
                ]);

                $groupMember->delete();
            }
        }

        if ($newMembers) {
            foreach ($newMembers as $member) {
                $groupMember = new GroupMember([
                    'group_id' => $this->id,
                    'member_id' => $member,
                ]);

                $groupMember->save();
            }
        }
    }

    public static function findAllByChannels(array $ids)
    {
        $groups = static::find()->where(['channel_id' => $ids])->with('channel')->all();

        if ($groups) {
            foreach ($groups as $group) {
                $results[$group->id] = $group->name . ' (' . $group->channel->name . ')';
            }
        }

        return isset($results) ? $results : [];
    }

    /**
     * @inheritdoc
     * @return GroupQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GroupQuery(get_called_class());
    }
}
