<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use common\traits\ModelHelperTrait;

/**
 * This is the model class for table "{{%message}}".
 *
 * @property integer $id
 * @property string $text
 * @property string $attachments
 * @property integer $media_type
 * @property integer $type
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $channel_id
 * @property integer $member_id
 * @property integer $command_id
 * @property integer $content_id
 *
 * @property Channel $channel
 * @property Member $member
 * @property Content $content
 */
class Message extends ActiveRecord
{
    use ModelHelperTrait;

    /**
     * Media Type constants.
     */
    const MEDIA_TYPE_TEXT = 1;
    const MEDIA_TYPE_IMAGE = 2;
    const MEDIA_TYPE_VIDEO = 3;
    const MEDIA_TYPE_AUDIO = 4;
    const MEDIA_TYPE_DOCUMENT = 5;

    /**
     * Status constants.
     */
    const STATUS_ACTIVE = 1;
    const STATUS_DISABLED = 2;

    /**
     * Type constants.
     */
    const TYPE_DIRECT = 1;
    const TYPE_COMMAND = 2;
    const TYPE_REPLY = 3;
    const TYPE_MENTION = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%message}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['media_type', 'integer'],
            ['media_type', 'in', 'range' => array_keys(self::getMediaTypes())],
            ['media_type', 'default', 'value' => self::MEDIA_TYPE_TEXT],

            ['channel_id', 'required'],
            ['channel_id', 'integer'],

            ['status', 'integer'],
            ['status', 'in', 'range' => array_keys(self::getAllStatus())],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],

            ['type', 'integer'],
            ['type', 'in', 'range' => array_keys(self::getTypes())],
            ['type', 'default', 'value' => self::TYPE_DIRECT],

            ['text', 'string', 'max' => 2048],

            ['attachments', 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'text' => Yii::t('app', 'Text'),
            'attachments' => Yii::t('app', 'Attachments'),
            'media_type' => Yii::t('app', 'Media type'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Create time'),
            'updated_at' => Yii::t('app', 'Update time'),
            'channel_id' => Yii::t('app', 'Channel'),
            'member_id' => Yii::t('app', 'Member'),
            'command_id' => Yii::t('app', 'Command'),
            'content_id' => Yii::t('app', 'Replied to'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannel()
    {
        return $this->hasOne(Channel::className(), ['id' => 'channel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(Member::className(), ['id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContent()
    {
        return $this->hasOne(Content::className(), ['id' => 'content_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChat()
    {
        return $this->hasOne(Chat::className(), ['member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBot()
    {
        return $this->hasOne(Bot::className(), ['channel_id' => 'channel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTelegramChannel()
    {
        return $this->hasOne(TelegramChannel::className(), ['channel_id' => 'channel_id']);
    }

    /**
     * Returns all media types
     *
     * @return array
     */
    public static function getMediaTypes()
    {
        return [
            static::MEDIA_TYPE_TEXT => Yii::t('app', 'Text'),
            static::MEDIA_TYPE_IMAGE => Yii::t('app', 'Image'),
            static::MEDIA_TYPE_VIDEO => Yii::t('app', 'Video'),
            static::MEDIA_TYPE_AUDIO => Yii::t('app', 'Audio'),
            static::MEDIA_TYPE_DOCUMENT => Yii::t('app', 'Document'),
        ];
    }

    /**
     * Returns media type constant value or id
     *
     * @param $type
     *
     * @return int|null|string
     */
    public static function getMediaTypeId($type)
    {
        $mediaTypes = [
            'image' => self::MEDIA_TYPE_IMAGE,
            'mp4' => self::MEDIA_TYPE_VIDEO,
            'audio' => self::MEDIA_TYPE_AUDIO,
            'document' => self::MEDIA_TYPE_DOCUMENT,
            'pdf' => self::MEDIA_TYPE_DOCUMENT,
            'video' => self::MEDIA_TYPE_DOCUMENT,
        ];

        foreach ($mediaTypes as $key => $typeId) {
            if (strpos($type, $key) !== false) {
                return $typeId;
            }
        }

        return null;
    }

    /**
     * Returns media type property
     * @return string
     */
    public function getMediaTypeName()
    {
        return $this->getNameOf('media_type', self::getMediaTypes());
    }

    /**
     * Return all content types.
     * @return array
     */
    public static function getTypes()
    {
        return [
            static::TYPE_DIRECT => Yii::t('app', 'Direct'),
            static::TYPE_COMMAND => Yii::t('app', 'Command'),
            static::TYPE_REPLY => Yii::t('app', 'Reply'),
            static::TYPE_MENTION => Yii::t('app', 'Mention'),
        ];
    }

    /**
     * Returns all status
     * @return array
     */
    public static function getAllStatus()
    {
        return [
            static::STATUS_ACTIVE => Yii::t('app', 'Active'),
            static::STATUS_DISABLED => Yii::t('app', 'Disabled'),
        ];
    }

    /**
     * @inheritdoc
     * @return MessageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MessageQuery(get_called_class());
    }
}
