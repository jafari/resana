<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[\common\models\Bot]].
 *
 * @see \common\models\Bot
 */
class BotQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\Bot[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Bot|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Add a join with a query contains count attribute a field
     * Example on search model:
     * ```
     * $query->select([
     *  '{{%user}}.*',
     *  'projectsCountAttr',
     *  'contentsCountAttr',
     * ]);
     * $query->countJoin('{{%project_user}}', '{{%user}}.id', 'user_id', 'projectsCountAttr');
     * $query->countJoin('{{%content}}', '{{%user}}.id', 'created_by', 'contentsCountAttr');
     * ```
     * @author Ali Irani <ali@irani.im>
     *
     * @param $tableName {{%table_name}}
     * @param $on {{%on_table_name}}.id
     * @param string $attr attribute name
     * @param string $countAttr count attribute name
     * @return \yii\db\Query
     */
    public function countJoin($tableName, $on, $attr, $countAttr)
    {
        return $this->join('LEFT JOIN', "(SELECT $attr, COUNT(*) AS $countAttr FROM $tableName GROUP BY $attr) $tableName ON $on = $tableName.$attr");
    }
}
