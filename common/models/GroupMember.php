<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\traits\ModelHelperTrait;

/**
 * This is the model class for table "{{%group_member}}".
 *
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $group_id
 * @property integer $member_id
 *
 * @property Group $group
 * @property Member $member
 */
class GroupMember extends \yii\db\ActiveRecord
{
    use ModelHelperTrait;

    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%group_member}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status' => Yii::t('app', 'Status'),
            'statusName' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Create Time'),
            'updated_at' => Yii::t('app', 'Update Time'),
            'group_id' => Yii::t('app', 'Group ID'),
            'member_id' => Yii::t('app', 'Member ID'),
        ];
    }

    /**
     * Returns all status
     * @return array
     */
    public static function getAllStatus()
    {
        return [
            static::STATUS_ACTIVE => Yii::t('app', 'Active'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(Member::className(), ['id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChat()
    {
        return $this->hasOne(Chat::className(), ['member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBot()
    {
        return $this->hasOne(Bot::className(), ['channel_id' => 'channel_id'])->viaTable(Group::tableName(), ['id' => 'group_id']);
    }
}
