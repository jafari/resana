<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%content_member}}".
 *
 * @property integer $status
 * @property integer $sent_at
 * @property integer $content_id
 * @property integer $member_id
 *
 * @property Content $content
 * @property Member $member
 */
class ContentMember extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%content_member}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status' => Yii::t('app', 'Status'),
            'sent_at' => Yii::t('app', 'Sent At'),
            'content_id' => Yii::t('app', 'Content ID'),
            'member_id' => Yii::t('app', 'Member ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContent()
    {
        return $this->hasOne(Content::className(), ['id' => 'content_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(Member::className(), ['id' => 'member_id']);
    }
}
