<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Project]].
 *
 * @see Project
 * @author Ali Irani <ali@irani.im>
 */
class ProjectQuery extends ActiveQuery
{
    public function active()
    {
        $this->andWhere(['{{%project}}.status' => Project::STATUS_ACTIVE]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return Project[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Project|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Returns projects that user is owner or member
     * @return $this
     */
    public function own()
    {
        return $this->joinWith(['projectUsers' => function ($query) {
            return $query->andWhere(['user_id' => Yii::$app->user->id]);
        }]);
    }

    public function my()
    {
        return $this->joinWith(['projectUsers' => function ($query) {
            return $query->andWhere(['user_id' => Yii::$app->user->id, 'role' => ProjectUser::ROLE_OWNER]);
        }]);
    }

    public function contribute()
    {
        return $this->joinWith(['projectUsers' => function ($query) {
            return $query->andWhere(['user_id' => Yii::$app->user->id, 'role' => ProjectUser::ROLE_ADMIN]);
        }]);
    }

    /**
     * Add a join with a query contains count attribute a field
     * Example on search model:
     * ```
     * $query->select([
     *  '{{%user}}.*',
     *  'projectsCountAttr',
     *  'contentsCountAttr',
     * ]);
     * $query->countJoin('{{%project_user}}', '{{%user}}.id', 'user_id', 'projectsCountAttr');
     * $query->countJoin('{{%content}}', '{{%user}}.id', 'created_by', 'contentsCountAttr');
     * ```
     * @author Ali Irani <ali@irani.im>
     *
     * @param $tableName {{%table_name}}
     * @param $on {{%on_table_name}}.id
     * @param string $attr attribute name
     * @param string $countAttr count attribute name
     * @return \yii\db\Query
     */
    public function countJoin($tableName, $on, $attr, $countAttr)
    {
        return $this->join('LEFT JOIN', "(SELECT *, COUNT(*) AS $countAttr FROM $tableName GROUP BY $attr) $tableName ON $on = $tableName.$attr");
    }
}
