<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Group]].
 *
 * @see Group
 */
class GroupQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Group[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Group|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Scope channel
     * @param integer $id
     * @return $this
     */
    public function channel($id, $project_id)
    {
        return $this->andWhere(['channel_id' => $id])
            ->with([
                'channel' => function ($query) use ($project_id) {
                    /** @var \common\models\Channel $query */
                    return $query->project($project_id);
                }
            ]);
    }

    /**
     * Add a join with a query contains count attribute a field
     * Example on search model:
     * ```
     * $query->select([
     *  '{{%user}}.*',
     *  'projectsCountAttr',
     *  'contentsCountAttr',
     * ]);
     * $query->countJoin('{{%project_user}}', '{{%user}}.id', 'user_id', 'projectsCountAttr');
     * $query->countJoin('{{%content}}', '{{%user}}.id', 'created_by', 'contentsCountAttr');
     * ```
     * @author Ali Irani <ali@irani.im>
     *
     * @param $tableName {{%table_name}}
     * @param $on {{%on_table_name}}.id
     * @param string $attr attribute name
     * @param string $countAttr count attribute name
     * @return \yii\db\Query
     */
    public function countJoin($tableName, $on, $attr, $countAttr)
    {
        return $this->join('LEFT JOIN', "(SELECT $attr, COUNT(*) AS $countAttr FROM $tableName GROUP BY $attr) $tableName ON $on = $tableName.$attr");
    }
}
