<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\models;

use Yii;
use common\traits\ModelHelperTrait;

/**
 * This is the model class for table "{{%chat}}".
 *
 * @property integer $chat_id
 * @property string $name
 * @property string $username
 * @property integer $type
 * @property integer $member_id
 *
 * @property Member $member
 *
 * @author Ali Irani <ali@irani.im>
 */
class Chat extends \yii\db\ActiveRecord
{
    use ModelHelperTrait;

    const TYPE_USER = 1;
    const TYPE_GROUP = 2;
    const TYPE_SUPER_GROUP = 3;
    const TYPE_CHANNEL = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%chat}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'chat_id' => Yii::t('app', 'Chat ID'),
            'name' => Yii::t('app', 'Name'),
            'username' => Yii::t('app', 'Username'),
            'type' => Yii::t('app', 'Type'),
            'typeName' => Yii::t('app', 'Type'),
            'member_id' => Yii::t('app', 'Member ID'),
        ];
    }

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();

        $this->on(self::EVENT_BEFORE_INSERT, function () {
            $member = new Member([
                'type' => Member::TYPE_TELEGRAM_BOT,
                'status' => Member::STATUS_ACTIVE,
            ]);

            if ($member->save()) {
                $this->member_id = $member->id;
                return true;
            }

            return false;
        });
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(Member::className(), ['id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannelMembers()
    {
        return $this->hasMany(ChannelMember::className(), ['member_id' => 'member_id']);
    }

    /**
     * Returns all chat types
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_USER => Yii::t('app', 'User'),
            self::TYPE_GROUP => Yii::t('app', 'Group'),
            self::TYPE_SUPER_GROUP => Yii::t('app', 'Super Group'),
            self::TYPE_CHANNEL => Yii::t('app', 'Channel'),
        ];
    }

    /**
     * @inheritdoc
     * @return ChatQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ChatQuery(get_called_class());
    }

    /**
     * Finds chat by id.
     *
     * @param integer $chat_id
     *
     * @return static|null
     */
    public static function findByChatId($chat_id)
    {
        return static::findOne(['chat_id' => $chat_id]);
    }
}
