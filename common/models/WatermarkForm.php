<?php
namespace common\models;

use Yii;
use yii\base\Model;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Watermark form
 */
class WatermarkForm extends Model
{
    const SIZE_SMALL = 'small';
    const SIZE_MEDIUM = 'medium';
    const SIZE_LARGE = 'large';

    const POSITION_TOP_RIGHT = 'top-right';
    const POSITION_TOP_LEFT = 'top-left';
    const POSITION_CENTER = 'center';
    const POSITION_BOTTOM_RIGHT = 'bottom-right';
    const POSITION_BOTTOM_LEFT = 'bottom-left';

    public $text;
    public $color;
    public $size;
    public $font;
    public $transparency;
    public $position;
    public $channelId;
    public $imageUrl;
    public $isPreview;

    private $_image;
    private $_channel;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['position', 'in', 'range' => array_keys(self::getPositions())],
            ['size', 'in', 'range' => array_keys(self::getSizes())],
            ['transparency', 'integer', 'min' => 0, 'max' => 100],
            [['text', 'font'], 'string'],
            ['color', 'string', 'length' => 7],
            ['color', 'match', 'pattern' => '/^#\w+/i'],
            ['image', 'file', 'skipOnEmpty' => true, 'extensions' => 'png'],
            ['image', 'required', 'when' => function () {
                return ($this->image || $this->text) === false;
            },'whenClient' => 'function(attribute, value){return $("#img-tab").hasClass("active") && $(".file-input-new").length != 0;}'],
            ['text', 'required', 'when' => function () {
                return ($this->image || $this->text) === false;
            },'whenClient' => 'function(attribute, value){return $("#text-tab").hasClass("active");}'],
            ['imageUrl', 'string'],
            ['isPreview', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'text' => Yii::t('app', 'Text'),
            'image' => Yii::t('app', 'Image'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $model = $this->getChannel();
        $config = $model->getConfig('watermark');
        if ($config) {
            $this->size = isset($config->size) ? $config->size : null;
            $this->transparency = isset($config->transparency) ? $config->transparency : null;
            $this->position = isset($config->position) ? $config->position : null;
            $this->text = isset($config->text->content) ? $config->text->content : null;
            $this->font = isset($config->text->font) ? $config->text->font : null;
            $this->color = isset($config->text->color) ? '#'.$config->text->color : null;
            $this->imageUrl = isset($config->image->url) ? $config->image->url : null ;
        }
    }

    /**
     * Returns Whether watermark is image or no
     * @return boolean
     */
    public function isImage()
    {
        return $this->imageUrl === null ? false : true;
    }

    /**
     * Returns Watermark sizes
     * @return array
     */
    public static function getSizes()
    {
        return [
            self::SIZE_SMALL => Yii::t('app', 'Small'),
            self::SIZE_MEDIUM => Yii::t('app', 'Medium'),
            self::SIZE_LARGE => Yii::t('app', 'Large'),
        ];
    }

    /**
     * Returns Watermark positions
     * @return array
     */
    public static function getPositions()
    {
        return [
            self::POSITION_TOP_RIGHT => Yii::t('app', 'Top-Right'),
            self::POSITION_TOP_LEFT => Yii::t('app', 'Top-Left'),
            self::POSITION_CENTER => Yii::t('app', 'Center'),
            self::POSITION_BOTTOM_RIGHT => Yii::t('app', 'Bottom-Right'),
            self::POSITION_BOTTOM_LEFT => Yii::t('app', 'Bottom-Left'),
        ];
    }

    /**
     * Returns get specific Channel by id
     * @return Channel
     */
    public function getChannel()
    {
        if ($this->_channel) {
            return $this->_channel;
        }

        $channel = Channel::findOne($this->channelId);
        if ($channel === null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        return $this->_channel= $channel;
    }

    /**
     * Return image
     * @return mixed
     */
    public function getImage()
    {
        if ($this->_image !== null) {
            return $this->_image;
        } else {
            if (isset($this->channel->getWatermark(false)->image->url)) {
                return $this->channel->getWatermark(false)->image->url;
            } else {
                return null;
            }
        }
    }

    /**
     * Set image and give url of image
     * @return string
     */
    public function setImage()
    {
        if ($file = UploadedFile::getInstance($this, 'image')) {
            $media = Yii::$app->media->save($file);
            if (isset($media['success']) && $media['success']) {
                $mediaInfo = $media['data']['media'][0];
                $this->_image = $this->imageUrl = $mediaInfo['uriMin'];
            } elseif (isset($media['message'])) {
                Yii::error($media['message'], __METHOD__);
            }
        }
    }

    /**
     * Check whether submit data is image or text and save it
     * @param $type
     * @return bool
     */
    public function save($type)
    {
        if ($this->validate() === false) {
            return false;
        }
        /* @var \api\models\Channel $channel */
        $config = [];
        $channel = $this->channel;
        switch ($type) {
            case 'text':
                if ($this->text) {
                    $config = [
                        'size' => $this->size,
                        'transparency' => $this->transparency,
                        'position' => $this->position,
                        'text' => [
                            'content' => $this->text,
                            'color' => $this->color ? substr($this->color, 1) : null,
                            'font' => $this->font,
                        ]
                    ];
                }
                break;

            case 'image':
                if ($this->isPreview === 'Preview') {
                    $config = $channel->getConfig('watermark');
                }
                $config = [
                    'size' => $this->size,
                    'transparency' => $this->transparency,
                    'position' => $this->position,
                    'image' => [
                        'url' => $this->isPreview === 'Preview' ? $config->image->url : $this->getImage(),
                    ]
                ];
                break;

            default:
                return false;
        }
        $channel->setConfig('watermark', $config);
        return $channel->save(false);
    }

    /**
     * Deletes channel's watermark
     * @return bool
     */
    public function deleteWatermark()
    {
        /* @var \common\models\Channel $channel */
        $channel = $this->channel;
        $channel->setConfig('watermark', '');
        return $channel->save(false);
    }
}
