<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use airani\telegram\TelegramBot;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%bot}}".
 *
 * @property integer $bot_id
 * @property string $name
 * @property string $username
 * @property string $token
 * @property string $owner
 * @property integer $channel_id
 * @property integer $userOwnerId
 *
 * @property Channel $channel
 *
 * @author Ali Irani <ali@irani.im>
 */
class Bot extends ActiveRecord
{
    public $projectId;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bot}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['token', 'required'],
            ['token', 'match', 'pattern' => '/\d+\:.*/i'],
            ['token', 'validateToken'],
            [['token', 'username'], 'unique'],
            [['token', 'owner'], 'string', 'max' => 128],
            ['bot_id', 'unique'],
            //['owners', 'each', 'rule' => ['match', 'pattern' => '/@/', 'not' => true, 'message' => Yii::t('app', '@ is not valid!')]],
            ['owners', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bot_id' => Yii::t('app', 'Bot ID'),
            'name' => Yii::t('app', 'Name'),
            'username' => Yii::t('app', 'Username'),
            'token' => Yii::t('app', 'Token'),
            'owner' => Yii::t('app', 'Owner'),
            'owners' => Yii::t('app', 'Owners'),
            'channel_id' => Yii::t('app', 'Channel ID'),
            'groupsCount' => Yii::t('app', 'Group Count'),
            'channel.project.name' => Yii::t('app', 'Project Name'),
        ];
    }

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();

        $this->on(self::EVENT_BEFORE_INSERT, [$this, 'setBotAttributes']);
        $this->on(self::EVENT_BEFORE_UPDATE, [$this, 'setBotAttributes']);

        $this->on(self::EVENT_BEFORE_INSERT, [$this, 'setChannelId']);

        $this->on(self::EVENT_AFTER_INSERT, [$this, 'setWebhook']);
        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'setWebhook']);

        $this->on(self::EVENT_AFTER_DELETE, [$this, 'unsetWebhook']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannel()
    {
        return $this->hasOne(Channel::className(), ['id' => 'channel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChats()
    {
        return $this->hasMany(Chat::className(), ['member_id' => 'member_id'])->viaTable('{{%channel_member}}', ['channel_id' => 'channel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::className(), ['channel_id' => 'channel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupsCount()
    {
        return $this->getGroups()->count();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveChats()
    {
        return $this->hasMany(Chat::className(), ['member_id' => 'member_id'])
            ->viaTable('{{%channel_member}}', ['channel_id' => 'channel_id'], function ($query) {
            /* @var $query \yii\db\ActiveQuery */
            return $query->andWhere(['status' => ChannelMember::STATUS_ACTIVE]);
        });
    }

    /**
     * Returns user owner id
     * @return int|null
     */
    public function getUserOwnerId()
    {
        /* @var ProjectUser $projectUser */
        $project_id = $this->channel->project_id;
        $projectUser = ProjectUser::findOne(['project_id' => $project_id, 'role' => ProjectUser::ROLE_OWNER]);
        return $projectUser ? $projectUser->user_id : null;
    }

    /**
     * @inheritdoc
     * @return BotQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BotQuery(get_called_class());
    }

    /**
     * Return model with active status
     * @param $id
     * @return array|Bot|null
     */
    public static function findActiveOne($id)
    {
        return static::find()->joinWith('channel')->where(['id' => $id, '{{%channel}}.status' => Channel::STATUS_ACTIVE])->one();
    }

    /**
     * Validate telegram bot token
     * @return bool
     */
    public function validateToken()
    {
        $bot = $this->getBotInfo();

        if (isset($bot->ok) === false) {
            $this->addError('connection', Yii::t('app', 'Connection is timeout, Please try again.'));
            Yii::warning(['error' => 'Telegram connection is timeout.', 'botToken' => $bot->token], __METHOD__);
            return false;
        } elseif ($bot->ok == false) {
            $this->addError('token', Yii::t('app', 'Token is invalid!'));
            return false;
        }

        return true;
    }

    /**
     * Return bot owners in array
     * @return array
     */
    public function getOwners()
    {
        $owners = json_decode($this->owner);
        return is_array($owners) ? $owners : [];
    }

    /**
     * Set bot owners with Json format in owner field
     * @param array $items Owners username
     */
    public function setOwners($items)
    {
        $this->owner = is_array($items) ? Json::encode($items) : '';
    }

    /**
     * Returns bot information
     * @return mixed
     */
    public function getBotInfo()
    {
        $bot = new TelegramBot(['authKey' => $this->token]);
        return $bot->getMe();
    }

    /**
     * Returns webhook url
     * @return mixed
     */
    public function getWebhookUrl()
    {
        $tokens = explode(':', $this->token);
        $urlManager = Yii::$app->urlManagerService;

        return $urlManager->createAbsoluteUrl([
            'api/hook',
            'tokenId' => $tokens[0],
            'tokenString' => $tokens[1]
        ], 'https');
    }

    /**
     * Set webhook url for bot
     * @return bool
     */
    public function setWebhook($webhook = null)
    {
        $webhook = $webhook === null ? $webhook : $this->webhookUrl;
        $telegram = new TelegramBot(['authKey' => $this->token]);
        $response = $telegram->setWebhook($webhook);

        return isset($response->ok) ? $response->ok : false;
    }

    /**
     * Unset bot webhook
     * @return bool
     */
    public function unsetWebhook()
    {
        return $this->setWebhook('');
    }

    /**
     * Get bot info and set to attributes
     * @return bool
     */
    public function setBotAttributes()
    {
        if (isset($this->botInfo->result) === false || !($bot = $this->botInfo->result)) {
            return false;
        }

        $this->setAttributes([
            'bot_id' => $bot->id,
            'name' => $bot->first_name,
            'username' => $bot->username,
        ], false);

        return true;
    }

    /**
     * Add channel record and set channel_id
     * @return bool
     */
    public function setChannelId()
    {
        if ($this->bot_id === null) {
            return false;
        }

        $channel = new Channel([
            'project_id' => $this->projectId,
            'type' => Channel::TYPE_TELEGRAM_BOT,
        ]);

        if ($channel->save()) {
            $this->channel_id = $channel->id;
            return true;
        }

        return false;
    }
}
