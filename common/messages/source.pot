msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: messages\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgctxt "app"
msgid "(Admin Only)"
msgstr ""

msgctxt "app"
msgid "@ is not valid!"
msgstr ""

msgctxt "app"
msgid "A command to save contents as drafts"
msgstr ""

msgctxt "app"
msgid "A command to send contents to your channels"
msgstr ""

msgctxt "app"
msgid "Active"
msgstr ""

msgctxt "app"
msgid "Add Channel"
msgstr ""

msgctxt "app"
msgid "Add Member"
msgstr ""

msgctxt "app"
msgid "Add New Member"
msgstr ""

msgctxt "app"
msgid "Add/Update signature"
msgstr ""

msgctxt "app"
msgid "Admin"
msgstr ""

msgctxt "app"
msgid "Afsaran"
msgstr ""

msgctxt "app"
msgid "All Channels"
msgstr ""

msgctxt "app"
msgid "All Groups"
msgstr ""

msgctxt "app"
msgid "All Members"
msgstr ""

msgctxt "app"
msgid "Are you sure you want to delete this item?"
msgstr ""

msgctxt "app"
msgid "Arial"
msgstr ""

msgctxt "app"
msgid "Attachments"
msgstr ""

msgctxt "app"
msgid "Audio"
msgstr ""

msgctxt "app"
msgid "Auth Key"
msgstr ""

msgctxt "app"
msgid "Authentication Key"
msgstr ""

msgctxt "app"
msgid "Author"
msgstr ""

msgctxt "app"
msgid "Avatar"
msgstr ""

msgctxt "app"
msgid "Blocked"
msgstr ""

msgctxt "app"
msgid "Bot"
msgstr ""

msgctxt "app"
msgid "Bot Channel Id"
msgstr ""

msgctxt "app"
msgid "Bot ID"
msgstr ""

msgctxt "app"
msgid "Bots"
msgstr ""

msgctxt "app"
msgid "Bottom-Left"
msgstr ""

msgctxt "app"
msgid "Bottom-Right"
msgstr ""

msgctxt "app"
msgid "Center"
msgstr ""

msgctxt "app"
msgid "Channel"
msgstr ""

msgctxt "app"
msgid "Channel Count"
msgstr ""

msgctxt "app"
msgid "Channel ID"
msgstr ""

msgctxt "app"
msgid "Channel Id"
msgstr ""

msgctxt "app"
msgid "Channel Members"
msgstr ""

msgctxt "app"
msgid "Channel Name"
msgstr ""

msgctxt "app"
msgid "Channel Type"
msgstr ""

msgctxt "app"
msgid "Channel type"
msgstr ""

msgctxt "app"
msgid "Channels"
msgstr ""

msgctxt "app"
msgid "Channels Count"
msgstr ""

msgctxt "app"
msgid "ChannelsList is invalid"
msgstr ""

msgctxt "app"
msgid "Charachter count:"
msgstr ""

msgctxt "app"
msgid "Chat"
msgstr ""

msgctxt "app"
msgid "Chat ID"
msgstr ""

msgctxt "app"
msgid "Chats"
msgstr ""

msgctxt "app"
msgid "Chats Members"
msgstr ""

msgctxt "app"
msgid "Cnt Failed"
msgstr ""

msgctxt "app"
msgid "Cnt Sent"
msgstr ""

msgctxt "app"
msgid "Color"
msgstr ""

msgctxt "app"
msgid "Command"
msgstr ""

msgctxt "app"
msgid "Command ID"
msgstr ""

msgctxt "app"
msgid "Config"
msgstr ""

msgctxt "app"
msgid "Connection is timeout, Please try again."
msgstr ""

msgctxt "app"
msgid "Content"
msgstr ""

msgctxt "app"
msgid "Content Channel"
msgstr ""

msgctxt "app"
msgid "Content Channels"
msgstr ""

msgctxt "app"
msgid "Content Count"
msgstr ""

msgctxt "app"
msgid "Content Group"
msgstr ""

msgctxt "app"
msgid "Content Groups"
msgstr ""

msgctxt "app"
msgid "Content ID"
msgstr ""

msgctxt "app"
msgid "Content successfully saved and add to sending queue"
msgstr ""

msgctxt "app"
msgid "Content successfully saved and added to queue for sending"
msgstr ""

msgctxt "app"
msgid "Content successfully saved as draft"
msgstr ""

msgctxt "app"
msgid "Contents"
msgstr ""

msgctxt "app"
msgid "Contents Count"
msgstr ""

msgctxt "app"
msgid "Contribute Projects"
msgstr ""

msgctxt "app"
msgid "Copied!"
msgstr ""

msgctxt "app"
msgid "Copy Token"
msgstr ""

msgctxt "app"
msgid "Create"
msgstr ""

msgctxt "app"
msgid "Create Bot"
msgstr ""

msgctxt "app"
msgid "Create Channel Member"
msgstr ""

msgctxt "app"
msgid "Create Chat"
msgstr ""

msgctxt "app"
msgid "Create Content"
msgstr ""

msgctxt "app"
msgid "Create Content Channel"
msgstr ""

msgctxt "app"
msgid "Create Content Group"
msgstr ""

msgctxt "app"
msgid "Create Group"
msgstr ""

msgctxt "app"
msgid "Create Group Member"
msgstr ""

msgctxt "app"
msgid "Create Message"
msgstr ""

msgctxt "app"
msgid "Create Project"
msgstr ""

msgctxt "app"
msgid "Create Project User"
msgstr ""

msgctxt "app"
msgid "Create Telegram Channel"
msgstr ""

msgctxt "app"
msgid "Create Time"
msgstr ""

msgctxt "app"
msgid "Create Twitter"
msgstr ""

msgctxt "app"
msgid "Create User"
msgstr ""

msgctxt "app"
msgid "Create new project"
msgstr ""

msgctxt "app"
msgid "Create signature"
msgstr ""

msgctxt "app"
msgid "Create time"
msgstr ""

msgctxt "app"
msgid "Created At"
msgstr ""

msgctxt "app"
msgid "Created By"
msgstr ""

msgctxt "app"
msgid "Currently we don't support your message media content type."
msgstr ""

msgctxt "app"
msgid "Dashboard"
msgstr ""

msgctxt "app"
msgid "Delete"
msgstr ""

msgctxt "app"
msgid "Delete Watermark"
msgstr ""

msgctxt "app"
msgid "Delete signature"
msgstr ""

msgctxt "app"
msgid "Deleted"
msgstr ""

msgctxt "app"
msgid "Description"
msgstr ""

msgctxt "app"
msgid "Direct"
msgstr ""

msgctxt "app"
msgid "Disabled"
msgstr ""

msgctxt "app"
msgid "Document"
msgstr ""

msgctxt "app"
msgid "Download"
msgstr ""

msgctxt "app"
msgid "Edit"
msgstr ""

msgctxt "app"
msgid "Edit Group"
msgstr ""

msgctxt "app"
msgid "Edit Profile"
msgstr ""

msgctxt "app"
msgid "Email"
msgstr ""

msgctxt "app"
msgid "Email is not exists"
msgstr ""

msgctxt "app"
msgid "Enter owner usernames ..."
msgstr ""

msgctxt "app"
msgid "Enter usernames of bot owners in Telegram"
msgstr ""

msgctxt "app"
msgid "F Chat ID"
msgstr ""

msgctxt "app"
msgid "F Message ID"
msgstr ""

msgctxt "app"
msgid "Facebook"
msgstr ""

msgctxt "app"
msgid "Failed"
msgstr ""

msgctxt "app"
msgid "Feedback lets you send your suggestions about our products. We welcome problem reports, feature ideas and general comments."
msgstr ""

msgctxt "app"
msgid "Female"
msgstr ""

msgctxt "app"
msgid "Files"
msgstr ""

msgctxt "app"
msgid "First Name"
msgstr ""

msgctxt "app"
msgid "Font"
msgstr ""

msgctxt "app"
msgid "For creating new project just click on the below link"
msgstr ""

msgctxt "app"
msgid "Forward"
msgstr ""

msgctxt "app"
msgid "Forward a content to the all bot subscribers"
msgstr ""

msgctxt "app"
msgid "Gender"
msgstr ""

msgctxt "app"
msgid "Generate Auth Key"
msgstr ""

msgctxt "app"
msgid "Gif"
msgstr ""

msgctxt "app"
msgid "Google Cloud Message"
msgstr ""

msgctxt "app"
msgid "Google Cloud Messaging"
msgstr ""

msgctxt "app"
msgid "Group"
msgstr ""

msgctxt "app"
msgid "Group Count"
msgstr ""

msgctxt "app"
msgid "Group ID"
msgstr ""

msgctxt "app"
msgid "Group Member"
msgstr ""

msgctxt "app"
msgid "Group Members"
msgstr ""

msgctxt "app"
msgid "Group Name"
msgstr ""

msgctxt "app"
msgid "Groups"
msgstr ""

msgctxt "app"
msgid "Groups Count"
msgstr ""

msgctxt "app"
msgid "Groups List"
msgstr ""

msgctxt "app"
msgid "GroupsList is invalid"
msgstr ""

msgctxt "app"
msgid "Help"
msgstr ""

msgctxt "app"
msgid "ID"
msgstr ""

msgctxt "app"
msgid "Image"
msgstr ""

msgctxt "app"
msgid "Image size in twitter channels must be fewer than 5MB."
msgstr ""

msgctxt "app"
msgid "In Queue"
msgstr ""

msgctxt "app"
msgid "Insert Puplish Date"
msgstr ""

msgctxt "app"
msgid "It's a caption command for add caption under image/video or document"
msgstr ""

msgctxt "app"
msgid "It's a sample command"
msgstr ""

msgctxt "app"
msgid "Large"
msgstr ""

msgctxt "app"
msgid "Last Name"
msgstr ""

msgctxt "app"
msgid "Left Group"
msgstr ""

msgctxt "app"
msgid "Legal Person"
msgstr ""

msgctxt "app"
msgid "LogIn With Twitter"
msgstr ""

msgctxt "app"
msgid "Login"
msgstr ""

msgctxt "app"
msgid "Login by Shenasa service"
msgstr ""

msgctxt "app"
msgid "Male"
msgstr ""

msgctxt "app"
msgid "Meanwhile, you may <a href=\"{homeUrl}\">return to dashboard</a> or try using the search form."
msgstr ""

msgctxt "app"
msgid "Media Type"
msgstr ""

msgctxt "app"
msgid "Media file not supported in twitter channels."
msgstr ""

msgctxt "app"
msgid "Media type"
msgstr ""

msgctxt "app"
msgid "Medium"
msgstr ""

msgctxt "app"
msgid "Member"
msgstr ""

msgctxt "app"
msgid "Member Count"
msgstr ""

msgctxt "app"
msgid "Member Created"
msgstr ""

msgctxt "app"
msgid "Member ID"
msgstr ""

msgctxt "app"
msgid "Member since"
msgstr ""

msgctxt "app"
msgid "Member successfully deleted."
msgstr ""

msgctxt "app"
msgid "Members"
msgstr ""

msgctxt "app"
msgid "Members Count"
msgstr ""

msgctxt "app"
msgid "Members List"
msgstr ""

msgctxt "app"
msgid "Mention"
msgstr ""

msgctxt "app"
msgid "Message ID"
msgstr ""

msgctxt "app"
msgid "Messages"
msgstr ""

msgctxt "app"
msgid "Messages not found"
msgstr ""

msgctxt "app"
msgid "Mobile"
msgstr ""

msgctxt "app"
msgid "My Projects"
msgstr ""

msgctxt "app"
msgid "Name"
msgstr ""

msgctxt "app"
msgid "New member successfully added to the project."
msgstr ""

msgctxt "app"
msgid "No Channels"
msgstr ""

msgctxt "app"
msgid "Owner"
msgstr ""

msgctxt "app"
msgid "Owner role can not change."
msgstr ""

msgctxt "app"
msgid "Owners"
msgstr ""

msgctxt "app"
msgid "Password Hash"
msgstr ""

msgctxt "app"
msgid "Password Reset Token"
msgstr ""

msgctxt "app"
msgid "Pending"
msgstr ""

msgctxt "app"
msgid "Please contact us if you think this is a server error. Thank you."
msgstr ""

msgctxt "app"
msgid "Please send /forward command reply to the message you want forward"
msgstr ""

msgctxt "app"
msgid "Please send /save command reply to the message you want save"
msgstr ""

msgctxt "app"
msgid "Please send /send command reply to the message you want send"
msgstr ""

msgctxt "app"
msgid "Position"
msgstr ""

msgctxt "app"
msgid "Private"
msgstr ""

msgctxt "app"
msgid "Project"
msgstr ""

msgctxt "app"
msgid "Project ID"
msgstr ""

msgctxt "app"
msgid "Project Name"
msgstr ""

msgctxt "app"
msgid "Project Owner"
msgstr ""

msgctxt "app"
msgid "Project Owner Id"
msgstr ""

msgctxt "app"
msgid "Project Owner Name"
msgstr ""

msgctxt "app"
msgid "Project User"
msgstr ""

msgctxt "app"
msgid "Project Users"
msgstr ""

msgctxt "app"
msgid "Project Users Count"
msgstr ""

msgctxt "app"
msgid "Project id"
msgstr ""

msgctxt "app"
msgid "Projects"
msgstr ""

msgctxt "app"
msgid "Projects Count"
msgstr ""

msgctxt "app"
msgid "Public"
msgstr ""

msgctxt "app"
msgid "Publish"
msgstr ""

msgctxt "app"
msgid "Publish Time"
msgstr ""

msgctxt "app"
msgid "Published At"
msgstr ""

msgctxt "app"
msgid "Recent Messages"
msgstr ""

msgctxt "app"
msgid "Replied to"
msgstr ""

msgctxt "app"
msgid "Reply"
msgstr ""

msgctxt "app"
msgid "Resana Help"
msgstr ""

msgctxt "app"
msgid "Reset"
msgstr ""

msgctxt "app"
msgid "Resize image failed, please check your watermark image"
msgstr ""

msgctxt "app"
msgid "Revoke Auth Key"
msgstr ""

msgctxt "app"
msgid "Role"
msgstr ""

msgctxt "app"
msgid "SMS"
msgstr ""

msgctxt "app"
msgid "Save"
msgstr ""

msgctxt "app"
msgid "Save to draft"
msgstr ""

msgctxt "app"
msgid "Schedule"
msgstr ""

msgctxt "app"
msgid "Scheduled"
msgstr ""

msgctxt "app"
msgid "Search"
msgstr ""

msgctxt "app"
msgid "See Here"
msgstr ""

msgctxt "app"
msgid "Select color ..."
msgstr ""

msgctxt "app"
msgid "Select one of the below channels:"
msgstr ""

msgctxt "app"
msgid "Sending"
msgstr ""

msgctxt "app"
msgid "Sent"
msgstr ""

msgctxt "app"
msgid "Sent At"
msgstr ""

msgctxt "app"
msgid "Sent to:"
msgstr ""

msgctxt "app"
msgid "Shenasa"
msgstr ""

msgctxt "app"
msgid "Shenasa ID"
msgstr ""

msgctxt "app"
msgid "Showing a list of all available commands"
msgstr ""

msgctxt "app"
msgid "Sign out"
msgstr ""

msgctxt "app"
msgid "Signature"
msgstr ""

msgctxt "app"
msgid "Size"
msgstr ""

msgctxt "app"
msgid "Small"
msgstr ""

msgctxt "app"
msgid "Something wrong! please try again!"
msgstr ""

msgctxt "app"
msgid "Start Now"
msgstr ""

msgctxt "app"
msgid "Status"
msgstr ""

msgctxt "app"
msgid "Summary Text"
msgstr ""

msgctxt "app"
msgid "Super Group"
msgstr ""

msgctxt "app"
msgid "Telegram Bot"
msgstr ""

msgctxt "app"
msgid "Telegram Channel"
msgstr ""

msgctxt "app"
msgid "Telegram Channels"
msgstr ""

msgctxt "app"
msgid "Telegram Chats"
msgstr ""

msgctxt "app"
msgid "Text"
msgstr ""

msgctxt "app"
msgid "Thank you for your feedback. We value every piece of feedback we receive. We cannot respond individually to every one, but we will use your comments as we strive to improve your Google experience."
msgstr ""

msgctxt "app"
msgid "The above error occurred while the Web server was processing your request."
msgstr ""

msgctxt "app"
msgid "The requested page does not exist."
msgstr ""

msgctxt "app"
msgid "There is no content."
msgstr ""

msgctxt "app"
msgid "There was a problem to send your message. Try again later"
msgstr ""

msgctxt "app"
msgid "There was an error in saving your message. Please try again!"
msgstr ""

msgctxt "app"
msgid "This command take the responsibility of taking and storing Feedback message."
msgstr ""

msgctxt "app"
msgid "Times"
msgstr ""

msgctxt "app"
msgid "Today"
msgstr ""

msgctxt "app"
msgid "Token"
msgstr ""

msgctxt "app"
msgid "Token Secret"
msgstr ""

msgctxt "app"
msgid "Token is invalid!"
msgstr ""

msgctxt "app"
msgid "Top-Left"
msgstr ""

msgctxt "app"
msgid "Top-Right"
msgstr ""

msgctxt "app"
msgid "Total Users"
msgstr ""

msgctxt "app"
msgid "Transparency"
msgstr ""

msgctxt "app"
msgid "Twitter"
msgstr ""

msgctxt "app"
msgid "Twitter Channels"
msgstr ""

msgctxt "app"
msgid "Twitter User Id"
msgstr ""

msgctxt "app"
msgid "Twitters"
msgstr ""

msgctxt "app"
msgid "Type"
msgstr ""

msgctxt "app"
msgid "Unknown"
msgstr ""

msgctxt "app"
msgid "Unsubscribed"
msgstr ""

msgctxt "app"
msgid "Update"
msgstr ""

msgctxt "app"
msgid "Update Content"
msgstr ""

msgctxt "app"
msgid "Update Project"
msgstr ""

msgctxt "app"
msgid "Update Time"
msgstr ""

msgctxt "app"
msgid "Update signature"
msgstr ""

msgctxt "app"
msgid "Update time"
msgstr ""

msgctxt "app"
msgid "Update {modelClass}: "
msgstr ""

msgctxt "app"
msgid "Updated At"
msgstr ""

msgctxt "app"
msgid "User"
msgstr ""

msgctxt "app"
msgid "User ID"
msgstr ""

msgctxt "app"
msgid "User Name"
msgstr ""

msgctxt "app"
msgid "Username"
msgstr ""

msgctxt "app"
msgid "Users"
msgstr ""

msgctxt "app"
msgid "Vazir"
msgstr ""

msgctxt "app"
msgid "Video"
msgstr ""

msgctxt "app"
msgid "Video size in twitter channels must be fewer than 15MB."
msgstr ""

msgctxt "app"
msgid "Watermark"
msgstr ""

msgctxt "app"
msgid "What is security token? How to create a bot on Telegram?"
msgstr ""

msgctxt "app"
msgid "You don't have required permissions to login this page!"
msgstr ""

msgctxt "app"
msgid "You have no channels."
msgstr ""

msgctxt "app"
msgid "You must select least 1 channel"
msgstr ""

msgctxt "app"
msgid "Your group successfully created."
msgstr ""

msgctxt "app"
msgid "Your group successfully deleted."
msgstr ""

msgctxt "app"
msgid "Your group successfully updated."
msgstr ""

msgctxt "app"
msgid "caption just for video/image or document"
msgstr ""

msgctxt "app"
msgid "for caption you must be reply a image/video or document message"
msgstr ""

msgctxt "app"
msgid "now write caption"
msgstr ""

msgctxt "app"
msgid "online"
msgstr ""

msgctxt "app"
msgid "signature"
msgstr ""

msgctxt "app"
msgid "the maximum number of text and signature for telegram channel is 200"
msgstr ""

msgctxt "app"
msgid "the maximum number of text and signature for twitter channel is 140"
msgstr ""

msgctxt "app"
msgid "{project} Channels"
msgstr ""

msgctxt "app"
msgid "{project} Members"
msgstr ""

msgctxt "yii"
msgid "An internal server error occurred."
msgstr ""

msgctxt "yii"
msgid "Error"
msgstr ""

