msgid ""
msgstr ""
"Project-Id-Version: Resana\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Ali Irani <ali@irani.im>\n"
"Language-Team: Noghteh\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"

msgctxt "app"
msgid "(Admin Only)"
msgstr "(فقط مدیر)"

msgctxt "app"
msgid "@ is not valid!"
msgstr "مجاز به استفاده از کاراکتر @ نمی‌باشید"

msgctxt "app"
msgid "A command to save contents as drafts"
msgstr ""
"از این دستور برای ذخیره‌ی مطالب به عنوان پیش نویس در رسانا استفاده می شود"

msgctxt "app"
msgid "A command to send contents to your channels"
msgstr "از این دستور برای ارسال مطالب به مسیرهای توزیع رسانا استفاده می شود"

msgctxt "app"
msgid "Active"
msgstr "فعال"

msgctxt "app"
msgid "Add Channel"
msgstr "افزودن مسیر توزیع"

msgctxt "app"
msgid "Add Member"
msgstr "افزودن عضو جدید"

msgctxt "app"
msgid "Add New Member"
msgstr "افزودن عضو جدید"

msgctxt "app"
msgid "Add/Update signature"
msgstr "افزودن/ویرایش امضاء"

msgctxt "app"
msgid "Admin"
msgstr "مدیر"

msgctxt "app"
msgid "Afsaran"
msgstr "افسران"

msgctxt "app"
msgid "All Channels"
msgstr "همه مسیرهای توزیع"

msgctxt "app"
msgid "All Groups"
msgstr "همه گروه‌ها"

msgctxt "app"
msgid "All Members"
msgstr "همه اعضا"

msgctxt "app"
msgid "Are you sure you want to delete this item?"
msgstr "از حذف این مورد اطمینان دارید؟"

msgctxt "app"
msgid "Arial"
msgstr "آریال"

msgctxt "app"
msgid "Attachments"
msgstr "ضمائم"

msgctxt "app"
msgid "Audio"
msgstr "صدا"

msgctxt "app"
msgid "Auth Key"
msgstr "کلید اعتبارسنجی"

msgctxt "app"
msgid "Authentication Key"
msgstr "کلید اعتبارسنجی"

msgctxt "app"
msgid "Author"
msgstr "نویسنده"

msgctxt "app"
msgid "Avatar"
msgstr "تصویر"

msgctxt "app"
msgid "Blocked"
msgstr "مسدود شده"

msgctxt "app"
msgid "Bot"
msgstr "ربات"

msgctxt "app"
msgid "Bot Channel Id"
msgstr "شناسه مسیر توزیع ربات"

msgctxt "app"
msgid "Bot ID"
msgstr "شناسه‌ی ربات"

msgctxt "app"
msgid "Bots"
msgstr "ربات‌ها"

msgctxt "app"
msgid "Bottom-Left"
msgstr "پائین-چپ"

msgctxt "app"
msgid "Bottom-Right"
msgstr "پائین-راست"

msgctxt "app"
msgid "Center"
msgstr "مرکز"

msgctxt "app"
msgid "Channel"
msgstr "مسیر توزیع"

msgctxt "app"
msgid "Channel Count"
msgstr "تعداد مسیر توزیع"

msgctxt "app"
msgid "Channel ID"
msgstr "شناسه‌ی مسیر توزیع"

msgctxt "app"
msgid "Channel Id"
msgstr "شناسه‌ی کانال"

msgctxt "app"
msgid "Channel Members"
msgstr "مخاطبین مسیر توزیع"

msgctxt "app"
msgid "Channel Name"
msgstr "نام مسیر توزیع"

msgctxt "app"
msgid "Channel Type"
msgstr "نوع مسیر توزیع"

msgctxt "app"
msgid "Channel type"
msgstr "نوع مسیر توزیع"

msgctxt "app"
msgid "Channels"
msgstr "مسیرهای توزیع"

msgctxt "app"
msgid "Channels Count"
msgstr "تعداد مسیرهای توزیع"

msgctxt "app"
msgid "ChannelsList is invalid"
msgstr "لیست کانال ها مجاز نیست"

msgctxt "app"
msgid "Charachter count:"
msgstr "تعداد حروف:"

msgctxt "app"
msgid "Chat"
msgstr "گپ"

msgctxt "app"
msgid "Chat ID"
msgstr "شناسه‌ی گپ"

msgctxt "app"
msgid "Chats"
msgstr "گپ‌های تلگرام"

msgctxt "app"
msgid "Chats Members"
msgstr "مشترکین ربات"

msgctxt "app"
msgid "Cnt Failed"
msgstr "ناموفق‌ها"

msgctxt "app"
msgid "Cnt Sent"
msgstr "فرستاده‌شده‌ها"

msgctxt "app"
msgid "Color"
msgstr "رنگ"

msgctxt "app"
msgid "Command"
msgstr "دستور"

msgctxt "app"
msgid "Command ID"
msgstr "شناسه‌ی دستور"

msgctxt "app"
msgid "Config"
msgstr "تنظیمات"

msgctxt "app"
msgid "Connection is timeout, Please try again."
msgstr "خطا در برقراری ارتباط، دوباره تلاش کنید!"

msgctxt "app"
msgid "Content"
msgstr "مطلب"

msgctxt "app"
msgid "Content Channel"
msgstr "مطالب مسیر توزیع"

msgctxt "app"
msgid "Content Channels"
msgstr "مطالب مسیرهای توزیع"

msgctxt "app"
msgid "Content Count"
msgstr "تعداد مطالب"

msgctxt "app"
msgid "Content Group"
msgstr "مطالب گروه"

msgctxt "app"
msgid "Content Groups"
msgstr "مطالب گروه‌ها"

msgctxt "app"
msgid "Content ID"
msgstr "شناسه مطلب"

msgctxt "app"
msgid "Content successfully saved and add to sending queue"
msgstr "پیام جهت فوروارد با موفقیت ذخیره شد و در صف ارسال قرار گرفت"

msgctxt "app"
msgid "Content successfully saved and added to queue for sending"
msgstr "پیام جهت ارسال به مسیرهای توزیع انتخابی ذخیره و در صف ارسال قرار گرفت"

msgctxt "app"
msgid "Content successfully saved as draft"
msgstr "پیام با موفقیت به عنوان پیش نویس ذخیره شد"

msgctxt "app"
msgid "Contents"
msgstr "مطالب"

msgctxt "app"
msgid "Contents Count"
msgstr "تعداد مطالب"

msgctxt "app"
msgid "Contribute Projects"
msgstr "پروژه های مشارکت شده"

msgctxt "app"
msgid "Copied!"
msgstr "کپی شد!"

msgctxt "app"
msgid "Copy Token"
msgstr "کپی کردن کلید اعتبار سنجی"

msgctxt "app"
msgid "Create"
msgstr "ایجاد"

msgctxt "app"
msgid "Create Bot"
msgstr "افزودن ربات تلگرام"

msgctxt "app"
msgid "Create Channel Member"
msgstr "ایجاد عضو مسیر توزیع"

msgctxt "app"
msgid "Create Chat"
msgstr "ایجاد گپ جدید"

msgctxt "app"
msgid "Create Content"
msgstr "مطلب جدید"

msgctxt "app"
msgid "Create Content Channel"
msgstr "ایجاد مطلب جدید برای کانال"

msgctxt "app"
msgid "Create Content Group"
msgstr "ایجاد مطلب جدید برای گروه"

msgctxt "app"
msgid "Create Group"
msgstr "ساخت گروه"

msgctxt "app"
msgid "Create Group Member"
msgstr "ایجاد گروه کاربری جدید"

msgctxt "app"
msgid "Create Message"
msgstr "ایجاد پیام جدید"

msgctxt "app"
msgid "Create Project"
msgstr "ایجاد پروژه"

msgctxt "app"
msgid "Create Project User"
msgstr "افزودن کاربر به پروژه"

msgctxt "app"
msgid "Create Telegram Channel"
msgstr "افزودن کانال تلگرام"

msgctxt "app"
msgid "Create Time"
msgstr "زمان ایجاد"

msgctxt "app"
msgid "Create Twitter"
msgstr "افزودن حساب توییتر"

msgctxt "app"
msgid "Create User"
msgstr "ایجاد کاربر جدید"

msgctxt "app"
msgid "Create new project"
msgstr "ساخت پروژه جدید"

msgctxt "app"
msgid "Create signature"
msgstr "ایجاد امضاء"

msgctxt "app"
msgid "Create time"
msgstr "تاریخ ایجاد"

msgctxt "app"
msgid "Created At"
msgstr "تاریخ ایجاد"

msgctxt "app"
msgid "Created By"
msgstr "ایجاد شده توسط"

msgctxt "app"
msgid "Currently we don't support your message media content type."
msgstr ""
"در حال حاضر ما از نوع محتوای چند رسانه ای ارسالی شما پشتیبانی نمی کنیم. از "
"این رو پیام شما ذخیره نگردید. لطفا محتوای دیگری را ارسال نمایید."

msgctxt "app"
msgid "Dashboard"
msgstr "پیشخوان"

msgctxt "app"
msgid "Delete"
msgstr "حذف"

msgctxt "app"
msgid "Delete Watermark"
msgstr "حذف واترمارک"

msgctxt "app"
msgid "Delete signature"
msgstr "حذف امضا"

msgctxt "app"
msgid "Deleted"
msgstr "حذف شده"

msgctxt "app"
msgid "Description"
msgstr "توضیح"

msgctxt "app"
msgid "Direct"
msgstr "مستقیم"

msgctxt "app"
msgid "Disabled"
msgstr "غیرفعال"

msgctxt "app"
msgid "Document"
msgstr "سند"

msgctxt "app"
msgid "Download"
msgstr "دانلود"

msgctxt "app"
msgid "Edit"
msgstr "ویرایش"

msgctxt "app"
msgid "Edit Group"
msgstr "ویرایش گروه"

msgctxt "app"
msgid "Edit Profile"
msgstr "ویرایش مشخصات"

msgctxt "app"
msgid "Email"
msgstr "ایمیل"

msgctxt "app"
msgid "Email is not exists"
msgstr "برای این ایمیل حساب کاربری ثبت نشده است."

msgctxt "app"
msgid "Enter owner usernames ..."
msgstr "نام کاربری مدیران را وارد کنید ..."

msgctxt "app"
msgid "Enter usernames of bot owners in Telegram"
msgstr ""
"در این محل باید نام کاربری تلگرام کاربرانی که به عنوان مدیر ربات در تلگرام "
"تعریف می‌شوند وارد شود"

msgctxt "app"
msgid "F Chat ID"
msgstr "شناسه‌ی گپ فورواردی"

msgctxt "app"
msgid "F Message ID"
msgstr "شناسه‌ی پیام فورواردی"

msgctxt "app"
msgid "Facebook"
msgstr "فیس‌بوک"

msgctxt "app"
msgid "Failed"
msgstr "شکست خورد"

msgctxt "app"
msgid ""
"Feedback lets you send your suggestions about our products. We welcome "
"problem reports, feature ideas and general comments."
msgstr ""
"بازخورد امکان ارسال پیام‌ها و نظرات شما را به مدیران ربات فراهم می‌کند، لطفا "
"متن پیام خود را وارد نمایید:"

msgctxt "app"
msgid "Female"
msgstr "زن"

msgctxt "app"
msgid "Files"
msgstr "پرونده‌ها"

msgctxt "app"
msgid "First Name"
msgstr "نام"

msgctxt "app"
msgid "Font"
msgstr "فونت"

msgctxt "app"
msgid "For creating new project just click on the below link"
msgstr "برای ساخت یک پروژه روی لینک زیر کلیک کنید."

msgctxt "app"
msgid "Forward"
msgstr "بازنشر"

msgctxt "app"
msgid "Forward a content to the all bot subscribers"
msgstr "فوروارد پیام به تمام مشترکان ربات"

msgctxt "app"
msgid "Gender"
msgstr "جنسیت"

msgctxt "app"
msgid "Generate Auth Key"
msgstr "تولید کلید اعتبار سنجی"

msgctxt "app"
msgid "Gif"
msgstr "گیف"

msgctxt "app"
msgid "Google Cloud Message"
msgstr "سرویس پیام ابری گوگل"

msgctxt "app"
msgid "Google Cloud Messaging"
msgstr "پیام ابری گوگل"

msgctxt "app"
msgid "Group"
msgstr "گروه"

msgctxt "app"
msgid "Group Count"
msgstr "تعداد گروه"

msgctxt "app"
msgid "Group ID"
msgstr "شناسه‌ی گروه"

msgctxt "app"
msgid "Group Member"
msgstr "عضو گروه"

msgctxt "app"
msgid "Group Members"
msgstr "اعضای گروه"

msgctxt "app"
msgid "Group Name"
msgstr "نام گروه"

msgctxt "app"
msgid "Groups"
msgstr "گروه‌ها"

msgctxt "app"
msgid "Groups Count"
msgstr "تعداد گروه‌ها"

msgctxt "app"
msgid "Groups List"
msgstr "گروه‌ها"

msgctxt "app"
msgid "GroupsList is invalid"
msgstr "لیست گروه ها مجاز نیست."

msgctxt "app"
msgid "Help"
msgstr "راهنما"

msgctxt "app"
msgid "ID"
msgstr "شناسه"

msgctxt "app"
msgid "Image"
msgstr "تصویر"

msgctxt "app"
msgid "Image size in twitter channels must be fewer than 5MB."
msgstr "حجم تصاویر برای ارسال به توییتر باید کمتر از ۵ مگابایت باشد"

msgctxt "app"
msgid "In Queue"
msgstr "صف ارسال"

msgctxt "app"
msgid "Insert Puplish Date"
msgstr "زمان انتشار"

msgctxt "app"
msgid "It's a caption command for add caption under image/video or document"
msgstr ""
"این دستور متن را به تصاویر، ویدئوها و فایل ها متصل می‌کند و پیام جدیدی "
"برایتان می‌سازد"

msgctxt "app"
msgid "It's a sample command"
msgstr "یک دستور برای نمونه"

msgctxt "app"
msgid "Large"
msgstr "بزرگ"

msgctxt "app"
msgid "Last Name"
msgstr "نام خانوادگی"

msgctxt "app"
msgid "Left Group"
msgstr "خروج از گروه"

msgctxt "app"
msgid "Legal Person"
msgstr "شخصیت حقوقی"

msgctxt "app"
msgid "LogIn With Twitter"
msgstr "افزودن مسیر توزیع توییتر"

msgctxt "app"
msgid "Login"
msgstr "ورود"

msgctxt "app"
msgid "Login by Shenasa service"
msgstr "ورود با سرویس شناسا"

msgctxt "app"
msgid "Male"
msgstr "مرد"

msgctxt "app"
msgid ""
"Meanwhile, you may <a href=\"{homeUrl}\">return to dashboard</a> or try "
"using the search form."
msgstr "همچنین، می‌توانید به <a href=\"{homeUrl}\">صفحه پیش‌خوان</a> بازگردید."

msgctxt "app"
msgid "Media Type"
msgstr "نوع چندرسانه"

msgctxt "app"
msgid "Media file not supported in twitter channels."
msgstr "ارسال این فایل در توییتر مجاز نیست."

msgctxt "app"
msgid "Media type"
msgstr "نوع چندرسانه ای"

msgctxt "app"
msgid "Medium"
msgstr "متوسط"

msgctxt "app"
msgid "Member"
msgstr "عضو"

msgctxt "app"
msgid "Member Count"
msgstr "تعداد اعضا"

msgctxt "app"
msgid "Member Created"
msgstr "عضو اضافه شد"

msgctxt "app"
msgid "Member ID"
msgstr "شناسه‌ی عضو"

msgctxt "app"
msgid "Member since"
msgstr "عضو شده از تاریخ"

msgctxt "app"
msgid "Member successfully deleted."
msgstr "کاربر عضو از پروژه حذف شد."

msgctxt "app"
msgid "Members"
msgstr "اعضا"

msgctxt "app"
msgid "Members Count"
msgstr "تعداد اعضا"

msgctxt "app"
msgid "Members List"
msgstr "فهرست اعضا"

msgctxt "app"
msgid "Mention"
msgstr "رونوشت"

msgctxt "app"
msgid "Message ID"
msgstr "شناسه‌ی پیام"

msgctxt "app"
msgid "Messages"
msgstr "پیام ها"

msgctxt "app"
msgid "Messages not found"
msgstr "پیامی وجود ندارد."

msgctxt "app"
msgid "Mobile"
msgstr "موبایل"

msgctxt "app"
msgid "My Projects"
msgstr "پروژه‌های من"

msgctxt "app"
msgid "Name"
msgstr "نام"

msgctxt "app"
msgid "New member successfully added to the project."
msgstr "عضو جدید با موفقیت به پروژه افزوده شد."

msgctxt "app"
msgid "No Channels"
msgstr "بدون مسیر توزیع"

msgctxt "app"
msgid "Owner"
msgstr "مالک"

msgctxt "app"
msgid "Owner role can not change."
msgstr "تغییر مالک پروژه امکان پذیر نمی‌باشد."

msgctxt "app"
msgid "Owners"
msgstr "مالکین"

msgctxt "app"
msgid "Password Hash"
msgstr "رمز کدگذاری شده"

msgctxt "app"
msgid "Password Reset Token"
msgstr "رمز اعتباری سنجی جدید"

msgctxt "app"
msgid "Pending"
msgstr "در انتظار بررسی"

msgctxt "app"
msgid "Please contact us if you think this is a server error. Thank you."
msgstr ""
"اگر فکر می‌کنید این یک خطای مربوط به سرور است لطفا به مدیر سایت اطلاع دهید. "
"با تشکر."

msgctxt "app"
msgid "Please send /forward command reply to the message you want forward"
msgstr "لطفا دستور /forward را به پیامی که می‌خواهید ارسال کنید reply نمائید"

msgctxt "app"
msgid "Please send /save command reply to the message you want save"
msgstr "لطفا دستور /save را به پیامی که می‌خواهید ارسال کنید reply نمائید"

msgctxt "app"
msgid "Please send /send command reply to the message you want send"
msgstr "لطفا دستور /send را به پیامی که می‌خواهید ارسال کنید reply نمائید"

msgctxt "app"
msgid "Position"
msgstr "موقعیت"

msgctxt "app"
msgid "Private"
msgstr "خصوصی"

msgctxt "app"
msgid "Project"
msgstr "پروژه"

msgctxt "app"
msgid "Project ID"
msgstr "شناسه‌ی پروژه"

msgctxt "app"
msgid "Project Name"
msgstr "نام پروژه"

msgctxt "app"
msgid "Project Owner"
msgstr "مالک پروژه"

msgctxt "app"
msgid "Project Owner Id"
msgstr "شناسه مالک پروژه"

msgctxt "app"
msgid "Project Owner Name"
msgstr "مالک پروژه"

msgctxt "app"
msgid "Project User"
msgstr "عضو پروژه"

msgctxt "app"
msgid "Project Users"
msgstr "اعضای پروژه"

msgctxt "app"
msgid "Project Users Count"
msgstr "تعداد اعضای پروژه"

msgctxt "app"
msgid "Project id"
msgstr "شناسه‌ی پروژه"

msgctxt "app"
msgid "Projects"
msgstr "پروژه‌ها"

msgctxt "app"
msgid "Projects Count"
msgstr "تعداد پروژه‌ها"

msgctxt "app"
msgid "Public"
msgstr "عمومی"

msgctxt "app"
msgid "Publish"
msgstr "انتشار "

msgctxt "app"
msgid "Publish Time"
msgstr "زمان انتشار"

msgctxt "app"
msgid "Published At"
msgstr "منتشر شده در"

msgctxt "app"
msgid "Recent Messages"
msgstr "پیام‌های اخیر"

msgctxt "app"
msgid "Replied to"
msgstr "پاسخ داده شده به"

msgctxt "app"
msgid "Reply"
msgstr "پاسخ"

msgctxt "app"
msgid "Resana Help"
msgstr "راهنمای سرویس پیام رسان رسانا"

msgctxt "app"
msgid "Reset"
msgstr "راه‌اندازی مجدد"

msgctxt "app"
msgid "Resize image failed, please check your watermark image"
msgstr ""
"تغییر اندازه تصویر با خطا مواجه شده است لطفا تصویر واترمارک را بررسی کنید."

msgctxt "app"
msgid "Revoke Auth Key"
msgstr "ابطال کلید اعتبار سنجی"

msgctxt "app"
msgid "Role"
msgstr "نقش"

msgctxt "app"
msgid "SMS"
msgstr "پیامک"

msgctxt "app"
msgid "Save"
msgstr "ذخیره"

msgctxt "app"
msgid "Save to draft"
msgstr "ذخیره پیش‌نویس"

msgctxt "app"
msgid "Schedule"
msgstr "زمان‌بندی"

msgctxt "app"
msgid "Scheduled"
msgstr "زمان‌بندی‌شده"

msgctxt "app"
msgid "Search"
msgstr "جستجو"

msgctxt "app"
msgid "See Here"
msgstr "اینجا را ببینید"

msgctxt "app"
msgid "Select color ..."
msgstr "انتخاب رنگ ..."

msgctxt "app"
msgid "Select one of the below channels:"
msgstr "مسیر توزیع مورد نظر را انتخاب کنید:"

msgctxt "app"
msgid "Sending"
msgstr "در حال فرستادن"

msgctxt "app"
msgid "Sent"
msgstr "ارسال شد"

msgctxt "app"
msgid "Sent At"
msgstr "ارسال شده در"

msgctxt "app"
msgid "Sent to:"
msgstr "ارسال شده در:"

msgctxt "app"
msgid "Shenasa"
msgstr "شناسا"

msgctxt "app"
msgid "Shenasa ID"
msgstr "شناسه شناسا"

msgctxt "app"
msgid "Showing a list of all available commands"
msgstr "نمایش فهرست همه دستورات موجود"

msgctxt "app"
msgid "Sign out"
msgstr "بیرون رفتن"

msgctxt "app"
msgid "Signature"
msgstr "امضاء"

msgctxt "app"
msgid "Size"
msgstr "اندازه"

msgctxt "app"
msgid "Small"
msgstr "کوچک"

msgctxt "app"
msgid "Something wrong! please try again!"
msgstr "مشکلی پیش آمده! لطفا دوباره امتحان کنید."

msgctxt "app"
msgid "Start Now"
msgstr "همین حالا شروع کنید"

msgctxt "app"
msgid "Status"
msgstr "وضعیت"

msgctxt "app"
msgid "Summary Text"
msgstr "خلاصه متن"

msgctxt "app"
msgid "Super Group"
msgstr "گروه عظیم"

msgctxt "app"
msgid "Telegram Bot"
msgstr "ربات تلگرام"

msgctxt "app"
msgid "Telegram Channel"
msgstr "کانال تلگرام"

msgctxt "app"
msgid "Telegram Channels"
msgstr "کانال‌های تلگرام"

msgctxt "app"
msgid "Telegram Chats"
msgstr "مشترکان تلگرام"

msgctxt "app"
msgid "Text"
msgstr "متن"

msgctxt "app"
msgid ""
"Thank you for your feedback. We value every piece of feedback we receive. We "
"cannot respond individually to every one, but we will use your comments as "
"we strive to improve your Google experience."
msgstr "پیام شما با موفقیت ذخیره شد."

msgctxt "app"
msgid ""
"The above error occurred while the Web server was processing your request."
msgstr "خطای فوق زمانی رخ داده است که سرور در حال پردازش درخواست شما بوده است."

msgctxt "app"
msgid "The requested page does not exist."
msgstr "برگه درخواستی شما وجود ندارد."

msgctxt "app"
msgid "There is no content."
msgstr "مطلبی برای نمایش وجود ندارد."

msgctxt "app"
msgid "There was a problem to send your message. Try again later"
msgstr "در ارسال پیام شما مشکلی به وجود آمده است. لطفا بعدا دوباره سعی کنید."

msgctxt "app"
msgid "There was an error in saving your message. Please try again!"
msgstr "در ذخیره پیام شما مشکلی به وجود آمده است. لطفا بعدا دوباره سعی کنید."

msgctxt "app"
msgid ""
"This command take the responsibility of taking and storing Feedback message."
msgstr "جهت ارتباط و ارسال پیام به مدیر ربات از این دستور استفاده نمایید"

msgctxt "app"
msgid "Times"
msgstr "تایمز"

msgctxt "app"
msgid "Today"
msgstr "امروز"

msgctxt "app"
msgid "Token"
msgstr "نشانه امنیتی"

msgctxt "app"
msgid "Token Secret"
msgstr "نشانه امنیتی مخفی"

msgctxt "app"
msgid "Token is invalid!"
msgstr "نشانه امنیتی معتبر نیست!"

msgctxt "app"
msgid "Top-Left"
msgstr "بالا-چپ"

msgctxt "app"
msgid "Top-Right"
msgstr "بالا-راست"

msgctxt "app"
msgid "Total Users"
msgstr "تعداد اعضای پروژه"

msgctxt "app"
msgid "Transparency"
msgstr "شفافیت"

msgctxt "app"
msgid "Twitter"
msgstr "توییتر"

msgctxt "app"
msgid "Twitter Channels"
msgstr "مسیرتوزیع توییتر"

msgctxt "app"
msgid "Twitter User Id"
msgstr "شناسه کاربری توییتر"

msgctxt "app"
msgid "Twitters"
msgstr "توییترها"

msgctxt "app"
msgid "Type"
msgstr "نوع"

msgctxt "app"
msgid "Unknown"
msgstr "نامشخص"

msgctxt "app"
msgid "Unsubscribed"
msgstr "لغو اشتراک"

msgctxt "app"
msgid "Update"
msgstr "به‌روزرسانی"

msgctxt "app"
msgid "Update Content"
msgstr "ویرایش مطلب"

msgctxt "app"
msgid "Update Project"
msgstr "ویرایش پروژه"

msgctxt "app"
msgid "Update Time"
msgstr "زمان آخرین تغییر"

msgctxt "app"
msgid "Update signature"
msgstr "ویرایش امضاء"

msgctxt "app"
msgid "Update time"
msgstr "زمان آخرین تغییر"

msgctxt "app"
msgid "Update {modelClass}: "
msgstr "ویرایش {modelClass}: "

msgctxt "app"
msgid "Updated At"
msgstr "به‌روز شده در"

msgctxt "app"
msgid "User"
msgstr "کاربر"

msgctxt "app"
msgid "User ID"
msgstr "شناسه کاربر"

msgctxt "app"
msgid "User Name"
msgstr "نام‌ کاربر"

msgctxt "app"
msgid "Username"
msgstr "نام‌کاربری"

msgctxt "app"
msgid "Users"
msgstr "کاربران"

msgctxt "app"
msgid "Vazir"
msgstr "وزیر"

msgctxt "app"
msgid "Video"
msgstr "ویدئو"

msgctxt "app"
msgid "Video size in twitter channels must be fewer than 15MB."
msgstr "حجم ویدئوها برای ارسال به توییتر باید کمتر از ۱۵ مگابایت باشد"

msgctxt "app"
msgid "Watermark"
msgstr "واترمارک"

msgctxt "app"
msgid "What is security token? How to create a bot on Telegram?"
msgstr "نشانه امنیتی چیست؟ و چگونه یک ربات در تلگرام بسازم؟"

msgctxt "app"
msgid "You don't have required permissions to login this page!"
msgstr "شما برای مشاهده این صفحه دسترسی کافی ندارید!"

msgctxt "app"
msgid "You have no channels."
msgstr "هیچ مسیر توزیعی ثبت نشده است"

msgctxt "app"
msgid "You must select least 1 channel"
msgstr "لازم است حداقل یک مسیر توزیع انتخاب شود"

msgctxt "app"
msgid "Your group successfully created."
msgstr "گروه شما با موفقیت اضافه شد."

msgctxt "app"
msgid "Your group successfully deleted."
msgstr "گروه شما با موفقیت حذف شد."

msgctxt "app"
msgid "Your group successfully updated."
msgstr "گروه شما با موفقیت به‌روز شد."

msgctxt "app"
msgid "caption just for video/image or document"
msgstr "این دستور را فقط می‌توانید به تصویر، ویدئو، یا سند ارسال نمائید."

msgctxt "app"
msgid "for caption you must be reply a image/video or document message"
msgstr ""
"این دستور را به تصویر، ویدئو، یا سندی که می‌خواهید به آن متنی را متصل کنید "
"reply نمائید."

msgctxt "app"
msgid "now write caption"
msgstr "متن عنوان را وارد نمایید"

msgctxt "app"
msgid "online"
msgstr "آنلاین"

msgctxt "app"
msgid "signature"
msgstr "امضاء"

msgctxt "app"
msgid "the maximum number of text and signature for telegram channel is 200"
msgstr ""
"تعداد حروف متن بعلاوه امضاء برای ارسال به مسیر تلگرامی باید کمتر از ۲۰۰ باشد"

msgctxt "app"
msgid "the maximum number of text and signature for twitter channel is 140"
msgstr ""
"تعداد حروف متن بعلاوه امضاء برای ارسال به مسیر توییتر باید کمتر از ۱۴۰ باشد"

msgctxt "app"
msgid "{project} Channels"
msgstr "مسیرهای توزیع {project}"

msgctxt "app"
msgid "{project} Members"
msgstr "اعضای {project}"

msgctxt "yii"
msgid "An internal server error occurred."
msgstr "خطای داخلی سرور رخ داده است."

msgctxt "yii"
msgid "Error"
msgstr "خطا"

#~ msgctxt "app"
#~ msgid "Add/Update watermark"
#~ msgstr "افزودن/ویرایش واترمارک"

#~ msgctxt "app"
#~ msgid "Please create a new project."
#~ msgstr "شما پروژه ای ندارید لطفا یک پروژه جدید ایجاد کنید."

#~ msgctxt "app"
#~ msgid "Recent Contents"
#~ msgstr "آخرین مطالب"

#~ msgctxt "app"
#~ msgid "Without Projects"
#~ msgstr "پروژه ای وجود ندارد"

#~ msgctxt "app"
#~ msgid "Publish & Send"
#~ msgstr "انتشار و ارسال"

#~ msgctxt "app"
#~ msgid "List of your channels:\n"
#~ msgstr "لیست همه‌ی مسیرهای توزیع\n"

#~ msgctxt "app"
#~ msgid "Project Admin"
#~ msgstr "مدیر پروژه"

#~ msgctxt "app"
#~ msgid "Char numbers in twitter channels should be fewer than 140"
#~ msgstr "تعداد حروف برای ارسال به توییتر باید کمتر از ۱۴۰ حرف باشد"

#~ msgctxt "app"
#~ msgid "Char numbers should fewer of 200"
#~ msgstr ""
#~ "تعداد حروف برای ارسال فایل چند رسانه ای به تلگرام باید کمتر از ۲۰۰ حرف "
#~ "باشد"

#~ msgctxt "app"
#~ msgid "Error happened while saving file to upload directory"
#~ msgstr "خطایی در هنگام ذخیره فایل در پوشه آپلودها رخ داده است."

#~ msgctxt "app"
#~ msgid "in saving file to directory has a problem"
#~ msgstr "مشکلی در ذخیره سازی فایل در شاخه مورد نظر رخ داده است."

#~ msgctxt "app"
#~ msgid "mime type: {mimeType} not supported"
#~ msgstr "نوع پرونده ی {mimeType} پشتیبانی نمی شود"

#~ msgctxt "app"
#~ msgid ""
#~ "the maximum number of text and signature for TelegramBot channel is 200"
#~ msgstr ""
#~ "تعداد حروف متن بعلاوه امضاء برای ارسال به ربات تلگرام باید کمتر از ۲۰۰ "
#~ "باشد"

#~ msgctxt "app"
#~ msgid "Mirza"
#~ msgstr "میرزا"

#~ msgctxt "app"
#~ msgid "Location"
#~ msgstr "مکان"

#~ msgctxt "app"
#~ msgid "Message"
#~ msgstr "پیام"

#~ msgctxt "app"
#~ msgid "Sticker"
#~ msgstr "استیکر"

#~ msgctxt "app"
#~ msgid "Resize image faild, please check your watermark image detail"
#~ msgstr ""
#~ "تغییر سایز واترمارک با مشکل مواجه شده است ، لطفا جزئیات واترمارک ارسالی "
#~ "را بررسی کنید"

#~ msgctxt "app"
#~ msgid "maximum text length for caption is 200 character, try again"
#~ msgstr "برای عنوان حداکثر تعداد 200 حرف مجاز می باشد ، لطفا مجدد سعی کنید"

#~ msgctxt "yii"
#~ msgid "Add/Update signature"
#~ msgstr "افزودن/ ایجاد امضاء"

#~ msgctxt "yii"
#~ msgid "signature"
#~ msgstr "امضاء"

#~ msgctxt "app"
#~ msgid "Image Format in twitter channels must be 'JPG' or 'PNG' or 'WEBP'"
#~ msgstr "فرمت تصاویر در کانال های توئیتری باید 'JPG'، 'PNG' یا 'WEBP' باشد"

#~ msgctxt "app"
#~ msgid "Video Format in twitter channels must be '.mp4' or '.mov'"
#~ msgstr "فرمت ویدئوها در کانال های توئیتری باید 'mp4' یا 'mov' باشد"

#~ msgctxt "app"
#~ msgid "Project Auth Key"
#~ msgstr "کلید اعتبار سنجی پروژه"

#~ msgctxt "app"
#~ msgid "All Contents"
#~ msgstr "همه مطالب"

#~ msgctxt "app"
#~ msgid "All Projects"
#~ msgstr "همه‌ پروژه‌ها"

#~ msgctxt "app"
#~ msgid "Add Bot"
#~ msgstr "افزودن ربات"

#~ msgctxt "app"
#~ msgid "Not Found (#404)"
#~ msgstr "(#۴۰۴) یافت نشد"

#~ msgctxt "app"
#~ msgid "Forbidden (#403)"
#~ msgstr "(#۴۰۳) دسترسی غیرمجاز"
