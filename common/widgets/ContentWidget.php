<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\widgets;

use Yii;
use yii\grid\GridViewAsset;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\BaseListView;

/**
 * Class ContentWidget
 * @package common\widgets
 * @author Ali Irani <ali@irani.im>
 */
class ContentWidget extends BaseListView
{
    public $layout = "{items}\n{pager}";

    public $filterRowOptions = ['class' => 'filters'];

    /**
     * Initializes the grid view.
     * This method will initialize required property values and instantiate [[columns]] objects.
     */
    public function init()
    {
        parent::init();
        if (!isset($this->filterRowOptions['id'])) {
            $this->filterRowOptions['id'] = $this->options['id'] . '-filters';
        }
    }

    /**
     * Runs the widget.
     */
    public function run()
    {
        $id = $this->options['id'];
        $options = Json::htmlEncode($this->getClientOptions());
        $view = $this->getView();
        GridViewAsset::register($view);
        $view->registerJs("jQuery('#$id').yiiGridView($options);");
        parent::run();
    }

    /**
     * Returns the options for the grid view JS widget.
     * @return array the options
     */
    protected function getClientOptions()
    {
        $filterUrl = isset($this->filterUrl) ? $this->filterUrl : Yii::$app->request->url;
        $id = $this->filterRowOptions['id'];
        $filterSelector = "#$id input, #$id select";
        if (isset($this->filterSelector)) {
            $filterSelector .= ', ' . $this->filterSelector;
        }

        return [
            'filterUrl' => Url::to($filterUrl),
            'filterSelector' => $filterSelector,
        ];
    }

    /**
     * @inheritDoc
     */
    public function renderItems()
    {
        $models = $this->dataProvider->getModels();
        $submitDates = [];

        /**
         * @var \common\models\Content $model
         */
        foreach ($models as $model) {
            $dateKey = date('d-m-Y', $model->published_at);
            if (in_array($dateKey, $submitDates, true) === false) {
                $date = Yii::$app->formatter->asDate($model->published_at, 'php:j M Y');
                $items[] = Html::tag('li', Html::tag('span', $date, ['class' => 'bg-red']), ['class' => 'time-label']);
                $submitDates[] = $dateKey;
            }

            $adapter = new ContentWidgetAdapter($model);
            $items[] = Html::tag('li', $adapter->render());
        }

        $inlineItems = implode("\n", $items);
        return Html::tag('ul', $inlineItems, ['class' => 'timeline post-list todo-list', 'encode' => false]);
    }

    /**
     * @inheritDoc
     */
    public function renderPager()
    {
        $pager = parent::renderPager();

        return Html::tag('div', Html::tag('div', $pager, [
            'class' => 'dataTables_paginate paging_simple_numbers',
            'style' => 'text-align:center;'
        ]), ['class' => 'post-list-footer col-md-12']);
    }

    /**
     * @inheritDoc
     */
    public function renderEmpty()
    {
        return Html::tag('div', '<i class="fa fa-newspaper-o"></i>' . Html::tag('h3', Yii::t('app', 'There is no content.')), ['class' => 'row no-post col-md-12']);
    }
}
