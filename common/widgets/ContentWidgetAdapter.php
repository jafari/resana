<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\widgets;

use Yii;
use common\models\Content;
use yii\helpers\Html;

class ContentWidgetAdapter
{
    public $model;

    /**
     * @inheritDoc
     */
    public function __construct(Content $model)
    {
        $this->model = $model;
    }

    public function render()
    {
        return "{$this->renderType()}\n{$this->renderStatus()}\n{$this->renderItem()}";
    }

    public function renderItem()
    {
        return Html::tag('div', "{$this->renderBody()}\n{$this->renderFooter()}", ['class' => 'timeline-item']);
    }

    public function renderBody()
    {
        return Html::tag('div', "{$this->renderActions()}\n{$this->renderChannels()}\n{$this->model->text}\n{$this->renderAttachments()}", ['class' => 'timeline-body']);
    }

    public function renderFooter()
    {
        return Html::tag('div', "{$this->renderStats()}\n{$this->renderAuthor()}\n{$this->renderPublishTime()}", ['class' => 'timeline-footer']);
    }
    
    public function renderType()
    {
        $mediaType = $this->model->media_type;
        return Html::tag('i', '', ['class' => 'fa ' . $this->getIconMediaTypeClass($mediaType) . ' ' . $this->getColorMediaType($mediaType)]);
    }

    public function renderStatus()
    {
        $icon = $this->getStatusIcon($this->model->status);
        return Html::tag('i', '', ['class' => 'status fa ' . $icon, 'data' => ['toggle' => 'tooltip', 'original-title' => $this->model->statusName]]);
    }

    public function renderActions()
    {
        $actions = [
            'delete' => Html::a('<i class="fa fa-trash"></i> ' . Yii::t('app', 'Delete'), [
                'content/delete',
                'id' => $this->model->id,
                'project_id' => $this->model->project_id
            ], [
                'class' => 'btn btn-xs btn-bbg btn-danger-bbg text-red',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ]
            ]),
            'edit' => Html::a('<i class="fa fa-edit"></i> ' . Yii::t('app', 'Edit'), [
                'content/update',
                'id' => $this->model->id,
                'project_id' => $this->model->project_id,
            ], ['class' => 'btn btn-xs btn-bbg btn-primary-bbg text-blue']),
        ];

        if (in_array($this->model->status, [Content::STATUS_SENDING, Content::STATUS_IN_QUEUE])) {
            $actions = [];
        } elseif (in_array($this->model->status, [Content::STATUS_SENT])) {
            unset($actions['edit']);
        }
        
        return Html::tag('div', implode("\n", $actions), ['class' => 'visible-action tools']);
    }

    public function renderStats()
    {
        $cntFailed = Yii::$app->formatter->asInteger($this->model->cnt_failed);
        $cntSuccess = Yii::$app->formatter->asInteger($this->model->cnt_sent);

        $stats = [
            'failed' => Html::tag('span', '<i class="fa fa-times-circle text-red"></i> ' . $cntFailed, ['class' => 'unsuccess-send pull-right text-red', 'data' => ['toggle' => 'tooltip', 'original-title' => Yii::t('app', 'Failed')]]),
            'success' => Html::tag('span', '<i class="fa fa-check-circle text-green"></i> ' . $cntSuccess, ['class' => 'success-send pull-right text-green', 'data' => ['toggle' => 'tooltip', 'original-title' => Yii::t('app', 'Sent')]])
        ];
        
        return implode("\n", $stats);
    }

    public function renderAuthor()
    {
        return Html::tag('span', '<i class="fa fa-user"></i> ' . $this->model->createdBy->name, [
            'class' => 'author',
            'data' => [
                'toggle' => 'tooltip',
                'original-title' => Yii::t('app', 'Author')
            ]
        ]);
    }

    public function renderPublishTime()
    {
        $time = Yii::$app->formatter->asTodaytime($this->model->published_at);

        return Html::tag('span', '<i class="fa fa-clock-o"></i> ' . $time, [
            'class' => 'time',
            'data' => [
                'toggle' => 'tooltip',
                'original-title' => Yii::t('app', 'Publish Time')
            ]
        ]);
    }

    public function renderChannels()
    {
        if ($channels = $this->model->channels) {
            foreach ($channels as $channel) {
                $links[] = Html::tag('p', Html::a($channel->name, ["{$channel->actionName}/view", 'id' => $channel->id, 'project_id' => $this->model->project_id], [
                    'data' => [
                        'toggle' => 'tooltip',
                        'original-title' => $channel->typeName,
                    ]
                ]), ['class' => 'bg-route-color']);
            }
            $targets = implode("\n", $links);
        } else {
            $targets = Html::tag('p', Yii::t('app', 'No Channels'), ['class' => 'bg-route-color']);
        }
        
        $title = Html::tag('i', '', ['class' => 'fa fa-code-fork']) . Yii::t('app', 'Sent to:');
        return Html::tag('div', "$title\n$targets", ['class' => 'post-route-dis']);
    }

    public function renderAttachments()
    {
        $files = $this->model->files;
        
        if (count($files) === 0) {
            return '';
        }
        
        foreach ($files as $file) {
            $links[] = Html::a($file->name . ' ' . Html::tag('i', '', ['class' => 'fa fa-picture-o']), $file->url, ['class' => 'btn post-attachment']);
        }
        
        return implode("\n", $links);
    }

    public function getIconMediaTypes()
    {
        return [
            Content::MEDIA_TYPE_AUDIO => 'fa-microphone',
            Content::MEDIA_TYPE_DOCUMENT => 'fa-book',
            Content::MEDIA_TYPE_FORWARD => 'fa-retweet',
            Content::MEDIA_TYPE_IMAGE => 'fa-picture-o',
            Content::MEDIA_TYPE_TEXT => 'fa-align-right',
            Content::MEDIA_TYPE_VIDEO => 'fa-video-camera',
            Content::MEDIA_TYPE_GIF => 'fa-film',
        ];
    }

    public function getIconMediaTypeClass($mediaType)
    {
        if (array_key_exists($mediaType, $this->getIconMediaTypes())) {
            return $this->getIconMediaTypes()[$mediaType];
        }
        return 'fa-align-right';
    }

    public function getColorMediaTypes()
    {
        return [
            Content::MEDIA_TYPE_AUDIO => 'bg-maroon',
            Content::MEDIA_TYPE_DOCUMENT => 'bg-green',
            Content::MEDIA_TYPE_FORWARD => 'bg-navy',
            Content::MEDIA_TYPE_IMAGE => 'bg-yellow',
            Content::MEDIA_TYPE_TEXT => 'bg-blue',
            Content::MEDIA_TYPE_VIDEO => 'bg-teal',
            Content::MEDIA_TYPE_GIF => 'bg-purple',
        ];
    }

    public function getColorMediaType($mediaType)
    {
        if (array_key_exists($mediaType, $this->getColorMediaTypes())) {
            return $this->getColorMediaTypes()[$mediaType];
        }
        return 'fa-align-right';
    }

    public function getAllStatus()
    {
        return [
            Content::STATUS_PENDING => 'fa-pencil-square text-orange',
            Content::STATUS_FAILED => 'fa-close text-red',
            Content::STATUS_SENT => 'fa-check text-green',
            Content::STATUS_SCHEDULED => 'fa-clock-o text-clock-o',
            Content::STATUS_IN_QUEUE => 'fa-ellipsis-h text-purple',
            Content::STATUS_SENDING => 'fa-truck text-teal',
        ];
    }

    public function getStatusIcon($status)
    {
        if (array_key_exists($status, $this->getAllStatus())) {
            return $this->getAllStatus()[$status];
        }
        return '';
    }
}
