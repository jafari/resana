<?php
namespace common\traits;

/**
 * Class ModelHelperTrait
 * Useful methods in ActiveRecord models
 *
 * @author Ali Irani <ali@irani.im>
 */
trait ModelHelperTrait
{
    /**
     * @return array
     */
    public static function getAllStatus()
    {
        return [];
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [];
    }

    /**
     * Returns status name
     * @return string
     */
    public function getStatusName()
    {
        return $this->getNameOf('status', self::getAllStatus());
    }

    /**
     * Returns type name
     * @return string
     */
    public function getTypeName()
    {
        return $this->getNameOf('type', self::getTypes());
    }

    /**
     * Returns name of constant property in items array
     * like: status, types
     * @param $property
     * @param $items
     * @return string
     */
    public function getNameOf($property, $items)
    {
        if (array_key_exists($this->$property, $items)) {
            return $items[$this->$property];
        }

        return 'Unknown_' . $this->$property;
    }

    /**
     * Change status
     * @param int $status New status id
     */
    public function changeStatus($status)
    {
        $this->updateAttributes(['status' => $status]);
    }
}