<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\HtmlPurifier;

/**
 * Class CleanTextBehavior
 * Add a filter before save model to cleaning text attribute like strip tags and html purifier
 *
 * @package common\behaviors
 * @author Ali Irani <ali@irani.im>
 */
class CleanTextBehavior extends Behavior
{
    public $attribute = 'text';

    public $allowedTags = [];

    /**
     * @inheritDoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'run',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'run',
        ];
    }

    /**
     * Run html purifier on text attribute and if exist allowed tags
     * then strip_tags on text with specified allowed tags
     * @param $event
     */
    public function run($event)
    {
        $attribute = $this->attribute;
        $text = HtmlPurifier::process($event->sender->$attribute);
        if ($this->allowedTags) {
            $text = strip_tags($text, $this->stringAllowedTags);
        }
        $event->sender->$attribute = $text;
    }

    /**
     * Returns allowed tags for strip_tags
     * @return string
     */
    public function getStringAllowedTags()
    {
        if (!($tags = $this->allowedTags)) {
            return null;
        }
        array_walk($tags, function(&$item) {
            $item = "<$item>";
        });
        return implode('', $tags);
    }
}