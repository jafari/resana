<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\rbac\rules;

use Yii;
use yii\rbac\Rule;
use common\models\Project;
use common\models\ProjectUser;

/**
 * Project Owner Rule
 * @author Ali Irani <ali@irani.im>
 */
class ProjectAdminRule extends Rule
{
    public $name = 'isProjectAdmin';

    /**
     * Executes the rule.
     *
     * @param string|integer $user the user ID. This should be either an integer or a string representing
     * the unique identifier of a user. See [[\yii\web\User::id]].
     * @param \yii\rbac\Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to [[ManagerInterface::checkAccess()]].
     * @return boolean a value indicating whether the rule permits the auth item it is associated with.
     */
    public function execute($user, $item, $params)
    {
        $controller = Yii::$app->controller->id;
        $projectIdName = $controller === 'project' ? 'id' : 'project_id';
        $queryParams = Yii::$app->getRequest()->queryParams;
        if (isset($queryParams[$projectIdName]) === false) {
            return false;
        }

        $projectId = $queryParams[$projectIdName];
        $projectUser = ProjectUser::find()
            ->joinWith('project')
            ->where([
                'project_id' => $projectId,
                'user_id' => $user,
                '{{%project}}.status' => Project::STATUS_ACTIVE,
            ])
            ->one();

        return $projectUser ? true : false;
    }
}
