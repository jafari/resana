<?php
use Yii;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $projectName */

?>
<div class="invite-user">
    <p>Hello dear friend,</p>

    <p>You are invited to the <?= $projectName ?> a project of <?= Yii::$app->name ?>, for complete your account please go to the below link:</p>

    <p><a href="<?= Url::toRoute(['site/auth', 'authclient' => 'shenasa'], true); ?>">Confirm and signup to the Resana</a></p>
</div>
