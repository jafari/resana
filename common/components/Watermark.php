<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2016 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\web\UploadedFile;
use WideImage\WideImage;
use Imagick;
use Quince\PersianGD\PersianStringDecorator;

/**
 * Watermark generator
 *
 * @author Reza Ebrahimi <reza_2ebt@yahoo.com>
 */
class Watermark extends Component
{
    const UPLOAD_PATH_DIR = '@frontend/uploads/';
    const FONT_PATH_ARIAL = '@frontend/web/fonts/arial.ttf';

    const TEXT_SIZE_SMALL = 12;
    const TEXT_SIZE_MEDIUM = 16;
    const TEXT_SIZE_LARGE = 20;

    const WATERMARK_SIZE = 'medium';
    const WATERMARK_COLOR = '000000';
    const WATERMARK_POSITION = 'center';
    const WATERMARK_TRANSPARENCY = 50;
    const WATERMARK_MARGIN = 10;

    const MINIMUM_LENGTH_HEX = 3;
    const MAXIMUM_TRANSPARENCY = 100;

    /*
     * This property must be set and then class is run
     * @var array of "name" => original file name, "url" => path of file, "media_type" => type of image file LIKE (jpeg or png or gif)
     *
     * Example :
     * ```php
     * $baseImage = [
     *       'url' => 'your_image_path', //path can a directory or http url
     *       'name' => 'image_name.image_format', // your image file name LIKE (image.jpeg)
     * ```
     */
    public $baseImage;

    /*
     * This property must be set and then class is run.
     * @var array that sent by client and include config text/image watermark
     *
     * Example :
     * ```php
     * ***** when a watermark text is set *****
     * $watermarkConfig = [
     *       'size' => 'watermark_size', //  example : small, medium, large
     *       'position' => 'watermark_position', //  example : center,top-left,bottom-right
     *       'transparency' => watermark_opacity, // this is a integer number between 0-100
     *       'text' => [
     *           'content' => 'watermark_content', // this is a string text
     *           'color' => 'watermark_text_color', // this is a hexadecimal code, example : #000000 or 000000
     *           'font_name' => 'watermark_text_font_name' // a font name that exist in /frontend/web/fonts
     *       ],
     *   ];
     * ***** when a watermark image is set *****
     * $config = [
     *       'size' => 'watermark_size', //  example : small, medium, large
     *       'position' => 'watermark_position', //  example : center,top-left,bottom-right
     *       'transparency' => watermark_opacity, // this is a integer number between 0-100
     *       'image' => [
     *           'url' => 'your_watermark_path', //path can a directory or http url
     *       ],
     *   ];
     * ```
     */
    public $config;
    public $errors;

    protected $isImage = false;
    protected $isText = false;
    protected $imageUrl;
    protected $text;
    protected $color = self::WATERMARK_COLOR;
    protected $size = self::WATERMARK_SIZE;
    protected $transparency = self::WATERMARK_TRANSPARENCY;
    protected $baseImageUrl;
    protected $baseImageName;
    protected $mediaUploaded;

    private $_textSize;
    private $_watermarkSize;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setAttributes();
        $this->createImageWatermarked();
    }

    /**
     * Set all properties that we need to run my classes ,
     * This attributes send to this class whit config $originalImage & $config
     */
    public function setAttributes()
    {
        if ($this->baseImage === null || $this->config === null) {
            throw new Exception('The properties originalImage and config must be set.');
        }

        $originalImage = $this->baseImage;
        $config = $this->config;

        $this->baseImageUrl = isset($originalImage['url']) ? $originalImage['url'] : null;
        $this->baseImageName = isset($originalImage['name']) ? $originalImage['name'] : null;
        $this->isText = isset($config['text']) && $config['text'] ? true : false;
        $this->isImage = isset($config['image']) && $config['image'] ? true : false;
        $this->imageUrl = isset($config['image']['url']) && $config['image']['url'] ? $config['image']['url'] : null;
        $this->text = isset($config['text']['content']) && $config['text']['content'] ? $config['text']['content'] : null;
        $this->color = isset($config['text']['color']) && $config['text']['color'] ? $config['text']['color'] : self::WATERMARK_COLOR;
        $this->size = isset($config['size']) ? strtolower($config['size']) : self::WATERMARK_SIZE;
        $this->transparency = isset($config['transparency']) ? $config['transparency'] : self::WATERMARK_TRANSPARENCY;

        $type = $this->isText ? 'text' : 'image';
        $requirements = [
            'text' => ['baseImageUrl', 'baseImageName', 'text'],
            'image' => ['baseImageUrl', 'baseImageName', 'imageUrl'],
        ];

        foreach ($requirements[$type] as $property) {
            if ($this->$property === null) {
                throw new InvalidParamException(self::className() . "::$property must be set for this class");
            }
        }
    }

    /**
     * This method set a font path for watermark text
     * @return string
     */
    public function getFontPath()
    {
        if (isset($this->config['text']['font']) && file_exists(Yii::getAlias($this->config['text']['font']))) {
            return Yii::getAlias($this->config['text']['font']);
        }

        return Yii::getAlias(self::FONT_PATH_ARIAL);
    }

    /**
     * This operation calculate and compare image size(width,height) and watermark size.
     * @return array
     */
    public function getImageSize()
    {
        if (isset($this->_watermarkSize)) {
            return $this->_watermarkSize;
        }

        $fileSize = getimagesize($this->baseImageUrl);
        switch ($this->size) {
            case "small" :
                $w = floor($fileSize[0] * 5 / 100);
                $h = floor($fileSize[1] * 5 / 100);
                $size = ['width' => (int)$w, 'height' => (int)$h];
                break;
            case "medium" :
                $w = floor($fileSize[0] * 10 / 100);
                $h = floor($fileSize[1] * 10 / 100);
                $size = ['width' => (int)$w, 'height' => (int)$h];
                break;
            case "large" :
                $w = floor($fileSize[0] * 20 / 100);
                $h = floor($fileSize[1] * 20 / 100);
                $size = ['width' => (int)$w, 'height' => (int)$h];
                break;
            default :
                $size = ['width' =>(int)$fileSize[0], 'height' => (int)$fileSize[0]];
        }
        return $this->_watermarkSize = $size;
    }

    /**
     * This method return folder that in @frontend/uploads directory saving image watermark
     * @return string
     */
    public function getUploadPath()
    {
        return Yii::getAlias(self::UPLOAD_PATH_DIR);
    }

    /**
     * This method return image name in uploadedPath directory
     * @return string
     */
    public function getImagePath()
    {
        return $this->getUploadPath() . $this->baseImageName;
    }

    /**
     * While the operation don success return an string from file uploaded (directory and name)
     * While the operation is fail and do not exist file uploaded return null
     * the operation is resize image watermark(compared to main image)
     * @return null|string
     */
    public function getResizedImage()
    {
        $imagePath = $this->getUploadPath() . 'watermark_resize' . $this->baseImageName . '.png';
        $image = WideImage::load($this->imageUrl);
        $image->resize($this->getImageSize()['width'], $this->getImageSize()['height'])->saveToFile($imagePath);

        if (file_exists($imagePath)) {
            return $imagePath;
        }

        $this->errors[] = Yii::t('app', 'Resize image failed, please check your watermark image');
        return null;
    }

    /**
     * This method keep transparent image background when changed opacity
     * When processed images exists in path return file path and name else return null
     * @return null|string
     */
    public function getTransparentWatermark()
    {
        $watermark = new Imagick($this->getResizedImage());
        $watermark->evaluateImage(Imagick::EVALUATE_MULTIPLY, $this->transparency/self::MAXIMUM_TRANSPARENCY, Imagick::CHANNEL_ALPHA);
        $watermark->setImageFormat('png');
        $watermarkPath = $this->getUploadPath() . 'transparency_watermark_resize' . $this->baseImageName . '.png';
        $watermark->writeImage($watermarkPath);

        if (file_exists($watermarkPath)) {
            return $watermarkPath;
        }
        return null;
    }

    /**
     * On this method return a font size
     * @return array
     */
    public function getTextSize()
    {
        if (isset($this->_textSize)) {
            return $this->_textSize;
        }

        $sizes = [
            'small' => self::TEXT_SIZE_SMALL,
            'medium' => self::TEXT_SIZE_MEDIUM,
            'large' => self::TEXT_SIZE_LARGE
        ];
        return isset($sizes[$this->size]) ? $sizes[$this->size] : self::WATERMARK_SIZE;
    }

    /**
     * This method set position watermark in main image and return position in width & height
     * @return array
     */
    public function getPosition()
    {
        $position = isset($this->config['position']) ? strtolower($this->config['position']) : self::WATERMARK_POSITION;
        switch ($position) {
            case "top-left" :
                $x = 'left +' . self::WATERMARK_MARGIN;
                $y = 'top +' . self::WATERMARK_MARGIN;
                $position = ['width' => $x, 'height' => $y];
                break;
            case "top-right" :
                $x = 'right -' . self::WATERMARK_MARGIN;
                $y = 'top +' . self::WATERMARK_MARGIN;
                $position = ['width' => $x, 'height' => $y];
                break;
            case "bottom-left" :
                $x = 'left +' . self::WATERMARK_MARGIN;
                $y = 'bottom -' . self::WATERMARK_MARGIN;
                $position = ['width' => $x, 'height' => $y];
                break;
            case "bottom-right" :
                $x = 'right -' . self::WATERMARK_MARGIN;
                $y = 'bottom -' . self::WATERMARK_MARGIN;
                $position = ['width' => $x, 'height' => $y];
                break;
            case "center" :
                $x = 'center';
                $y = 'center';
                $position = ['width' => $x, 'height' => $y];
                break;
            default :
                $x = 'right -' . self::WATERMARK_MARGIN;
                $y = 'bottom -' . self::WATERMARK_MARGIN;
                $position = ['width' => $x, 'height' => $y];
        }
        return $position;
    }

    /**
     * This method convert hexadecimal colors to RGB(r,g,b)
     * @return array
     */
    public function getRgbColor()
    {
        $hex = $this->color;
        if ($hex[0] === '#') {
            $hex = str_replace("#", "", $hex);
        }

        if (strlen($hex) == self::MINIMUM_LENGTH_HEX) {
            $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
            $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
            $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
        } else {
            $r = hexdec(substr($hex, 0, 2));
            $g = hexdec(substr($hex, 2, 2));
            $b = hexdec(substr($hex, 4, 2));
        }
        return ['R' => $r, 'G' => $g, 'B' => $b];
    }

    /**
     * Save image with watermark in media service and set this saved to mediaUploaded , or set error
     * @return bool
     */
    public function saveToMediaService()
    {
        if (file_exists($this->getImagePath()) === false) {
            throw new Exception('Image file not exists!');
        }

        $file = new UploadedFile([
            'name' => null,
            'type' => null,
            'tempName' => $this->getImagePath(),
        ]);

        $media = Yii::$app->media->save($file);

        if (isset($media['success']) && $media['success']) {
            $mediaInfo = $media['data']['media'][0];
            $this->mediaUploaded = [
                'url' => $mediaInfo['uriMin'],
                'name' => $mediaInfo['original_file_name'],
                'message' => 'success',
            ];
            return true;
        } elseif (isset($media['message'])) {
            Yii::error($media['message']);
            $this->errors[] = $media['message'];
        }
        return false;
    }

    /**
     * This method set final result into array $mediaUploaded and can work with variable
     * @return bool
     */
    public function createImageWatermarked()
    {
        $finalImage = WideImage::load($this->baseImageUrl);
        if ($this->isImage) {
            if ($this->getResizedImage() === null) {
                return false;
            }
            $watermark = WideImage::load($this->getTransparentWatermark());
            $finalImage = $finalImage->merge(
                $watermark,
                $this->getPosition()['width'],
                $this->getPosition()['height'],
                self::MAXIMUM_TRANSPARENCY
            );
        } elseif ($this->isText) {
            $opacity = self::MAXIMUM_TRANSPARENCY - $this->transparency;
            $canvas = $finalImage->getCanvas();
            $canvas->useFont(
                $this->getFontPath(),
                $this->getTextSize(),
                $finalImage->allocateColorAlpha(
                    $this->getRgbColor()['R'],
                    $this->getRgbColor()['G'],
                    $this->getRgbColor()['B'],
                    $opacity
                )
            );
            $farsiGD = new PersianStringDecorator();
            $text = $farsiGD->decorate($this->text);
            $canvas->writeText($this->getPosition()['width'], $this->getPosition()['height'], $text);
        }
        $finalImage->saveToFile($this->getImagePath());
        $this->saveToMediaService();
        return true;
    }

    /**
     * Returns final image with watermark
     * @return array | null.
     */
    public function getFinalImage()
    {
        return $this->mediaUploaded;
    }
}
