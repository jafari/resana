<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\components\telegram;

use Yii;
use CURLFile;
use common\models\ChannelMember;
use common\models\Content;
use common\models\Chat;
use airani\telegram\TelegramBot;
use yii\base\Exception;
use yii\base\Model;

/**
 * Class TelegramSender
 *
 * @author Ali Irani <ali@irani.im>
 */
class Sender extends Model
{
    public $text;
    public $file;
    public $bot;
    public $media_type;
    public $forward_chat_id;
    public $forward_message_id;
    public $reply_to_message_id;
    public $reply_markup;
    public $parse_mode = 'HTML';
    public $response;

    private $_telegram_bot;
    private $_params;

    /**
     * Returns telegram method names for each media type in an array
     * @return array
     */
    protected static function getActions()
    {
        return [
            Content::MEDIA_TYPE_TEXT => 'sendMessage',
            Content::MEDIA_TYPE_IMAGE => 'sendPhoto',
            Content::MEDIA_TYPE_AUDIO => 'sendAudio',
            Content::MEDIA_TYPE_VIDEO => 'sendVideo',
            Content::MEDIA_TYPE_DOCUMENT => 'sendDocument',
            Content::MEDIA_TYPE_GIF => 'sendDocument',
            Content::MEDIA_TYPE_FORWARD => 'forwardMessage',
        ];
    }

    /**
     * Returns method name specific for media type
     * @return mixed
     */
    protected function getAction()
    {
        return static::getActions()[$this->media_type];
    }

    /**
     * Return action params
     * @return mixed
     */
    protected function getParams()
    {
        if ($this->_params === null || $this->fileChanged()) {
            $params = [
                Content::MEDIA_TYPE_TEXT => [$this->text, $this->reply_to_message_id, $this->reply_markup, $this->parse_mode],
                Content::MEDIA_TYPE_IMAGE => [$this->file, $this->text, $this->reply_to_message_id, $this->reply_markup],
                Content::MEDIA_TYPE_VIDEO => [$this->file, $this->text, $this->reply_to_message_id, $this->reply_markup],
                Content::MEDIA_TYPE_DOCUMENT => [$this->file, $this->text, $this->reply_to_message_id, $this->reply_markup],
                Content::MEDIA_TYPE_GIF => [$this->file, $this->text, $this->reply_to_message_id, $this->reply_markup],
                Content::MEDIA_TYPE_AUDIO => [$this->file, $this->reply_to_message_id, $this->reply_markup],
                Content::MEDIA_TYPE_FORWARD => [$this->forward_chat_id, $this->forward_message_id],
            ];

            return $this->_params = $params[$this->media_type];
        }

        return $this->_params;
    }

    /**
     * Return file changed or not
     * @return bool
     */
    protected function fileChanged()
    {
        return $this->file && $this->_params && $this->_params[0] !== $this->file;
    }

    /**
     * Returns instance of telegram bot class
     * @return TelegramBot
     */
    protected function getTelegramBot()
    {
        if ($this->_telegram_bot === null) {
            return $this->_telegram_bot = new TelegramBot(['authKey' => $this->bot->token]);
        }

        return $this->_telegram_bot;
    }

    /**
     * Send anything to the telegram chats or channels
     * @param integer $to destination chat_id
     * @return bool
     */
    public function send($to)
    {
        try {
            $response = call_user_func_array([$this->telegramBot, $this->action], array_merge([$to], $this->params));
        } catch (Exception $e) {
            Yii::error(['message' => $e->getMessage(), 'file' => $e->getFile().':'.$e->getLine()], __METHOD__);
            return false;
        }

        if (isset($response->ok) === false) {
            return false;
        }

        if ($response->ok) {
            // @todo: Add more data for audio
            if ($this->media_type === Content::MEDIA_TYPE_AUDIO && $this->text) {
                $this->telegramBot->sendMessage($to, $this->text, $response->result->message_id);
            }

            if ($this->file && $this->file instanceof CURLFile) {
                $type = strtolower(ltrim($this->action, 'send'));
                if (isset($response->result->$type)) {
                    $file = $response->result->$type;
                    $this->file = is_array($file) ? end($file)->file_id : $file->file_id;
                }
            }

            return true;
        }

        $this->response = $response;
        return false;
    }

    /**
     * Send content to the telegram chat and handle channel_member status
     * @param $chat_id
     * @return bool
     * @throws \Exception
     */
    public function sendToChat($chat)
    {
        $chat_id = $chat->chat_id;
        if ($this->send($chat_id)) {
            return true;
        }

        if ($this->response === null) {
            return false;
        }

        /* @var ChannelMember $channelMember */
        $channelMember = ChannelMember::findOne(['channel_id' => $this->bot->channel_id, 'member_id' => $chat->member_id]);
        if ($channelMember === null) {
            return false;
        }
        switch ($this->response->description) {
            case '[Error]: Bot was blocked by the user':
            case 'Bot was blocked by the user':
                $channelMember->changeStatusTo(ChannelMember::STATUS_TELEGRAM_BLOCKED);
                break;
            case '[Error]: Forbidden: bot was kicked from the channel chat':
            case '[Error]: Forbidden: bot was kicked from the supergroup chat':
            case '[Error]: Forbidden: bot was kicked from the group chat':
            case 'Forbidden: bot was kicked from the channel chat':
            case 'Forbidden: bot was kicked from the supergroup chat':
            case 'Forbidden: bot was kicked from the group chat':
                $channelMember->changeStatusTo(ChannelMember::STATUS_TELEGRAM_LEFT_GROUP);
                break;
            case '[Error]: Bad Request: group chat is migrated to supergroup chat':
            case 'Bad Request: group chat is migrated to a supergroup chat':
                $superGroupId = $this->response->parameters->migrate_to_chat_id;
                /* @var Chat $newChat */
                if ($newChat = Chat::findOne(['chat_id' => $superGroupId])) {
                    ChannelMember::deleteAll(['member_id' => $newChat->member_id]);
                    ChannelMember::updateAll(['member_id' => $newChat->member_id], ['member_id' => $chat->member_id]);
                    Chat::deleteAll(['chat_id' => $chat_id]);
                } else {
                    $update = Chat::updateAll([
                        'chat_id' => $superGroupId,
                        'type' => Chat::TYPE_SUPER_GROUP,
                    ], ['chat_id' => $chat_id]);
                    if ($update) {
                        $chat->chat_id = $superGroupId;
                        return $this->sendToChat($chat);
                    }
                }
                break;
            case '[Error]: PEER_ID_INVALID':
            case '[Error]: Bad Request: chat not found':
            case '[Error]: Bad Request: group is deactivated':
            case '[Error]: Bad Request: group chat is deactivated':
            case '[Error]: Forbidden: can\'t write to chat with deleted user':
            case 'Forbidden: user is deleted':
            case 'PEER_ID_INVALID':
            case 'Bad Request: chat not found':
            case 'Bad Request: group is deactivated':
            case 'Bad Request: group chat is deactivated':
            case 'Forbidden: can\'t write to chat with deleted user':
                $channelMember->delete();
                break;
            case '[Error]: Too many requests: retry later':
            case 'Too many requests: retry later':
                sleep(60);
                break;
            default:
                Yii::warning([
                    'message' => "[Telegram] Unknown Response on `{$this->action}` with chat_id '$chat_id'",
                    'response' => $this->response
                ], __METHOD__);
                break;
        }
        return false;
    }

    /**
     * Send content to the telegram channel
     * @param $channelUsername
     * @return bool
     * @throws \Exception
     */
    public function sendToChannel($channelUsername)
    {
        if ($this->send('@'.$channelUsername)) {
            return true;
        }

        if ($this->response === null) {
            return false;
        }

        switch ($this->response->description) {
            case '[Error]: Too many requests: retry later':
                sleep(60);
                break;
            case '[Error]: Bad Request: channel not found':
                //break;
            default:
                Yii::warning([
                    'message' => "[Telegram] Unknown Response on `{$this->action}` with chat_id '$channelUsername'",
                    'response' => $this->response
                ], __METHOD__);
                break;
        }
        return false;
    }
}
