<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\components\telegram\commands;

use Yii;

/**
 * Help Command
 * Show a list of all available commands
 * @property \common\components\telegram\types\Update $update
 *
 * @author Ali Irani <ali@irani.im>
 */
class HelpCommand extends Command
{
    protected $name = 'help';
    protected $description = 'Showing a list of all available commands';
    protected $pattern = '/help';
    protected $public = true;

    /**
     * @inheritdoc
     */
    public function execute()
    {
        $this->sendMessage(implode("\n", $this->getAvailableCommands()));
    }

    /**
     * Returns visible commands for showing to output of help command
     * @return array
     */
    public function getVisibleCommands()
    {
        return ['caption', 'feedback', 'help', 'save', 'send', 'forward'];
    }

    /**
     * Returns all available commands
     * @return array
     */
    public function getAvailableCommands()
    {
        $path = Yii::getAlias('@common/components/telegram/commands');
        $namespace = '\common\components\telegram\commands';
        $files = glob($path.'/*.php');
        $commandsList = [];

        foreach ($files as $file) {
            if (preg_match('/.*\/([\w]+Command).php/', $file, $matches)) {
                $className = $namespace . '\\' . $matches[1];
                /* @var Command $command */
                $command = new $className();
                if (in_array($command->name, $this->getVisibleCommands(), true) && $this->userCan($command)) {
                    $message = "{$command->getPattern()} - {$command->getDescription()}";
                    $message .= $command->isPublic() ? '' : ' ' . Yii::t('app', '(Admin Only)');
                    $commandsList[] = $message;
                }
            }
        }
        return $commandsList;
    }

    /**
     * @inheritDoc
     */
    public function getDescription()
    {
        return Yii::t('app', 'Showing a list of all available commands');
    }

    /**
     * Whether user can run this command
     * @param Command $command
     * @return bool
     */
    public function userCan($command)
    {
        return $command->isPublic() ? true : $this->isUserOwner();
    }
}
