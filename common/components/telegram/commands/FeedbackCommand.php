<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\components\telegram\commands;

use Yii;
use common\models\Chat;
use common\models\Message;
use common\traits\TelegramCommandTrait;

/**
 * This command take the responsibility of taking and storing Feedback message.
 *
 * @author Mohammad Davaee <mdavaee@gmail.com>
 */
class FeedbackCommand extends Command
{
    use TelegramCommandTrait;

    protected $name = 'feedback';
    protected $description = 'This command take the responsibility of taking and storing Feedback message.';
    protected $pattern = '/feedback';
    protected $public = true;

    /**
     * @inheritdoc
     */
    public function execute()
    {
        if ($this->isResponse && $this->saveFeedback()) {
            return $this->sendMessage(self::EMOJI_SUCCESS . ' ' . Yii::t('app', 'Thank you for your feedback. We value every piece of feedback we receive. We cannot respond individually to every one, but we will use your comments as we strive to improve your Google experience.'));
        }

        $this->setResponse();

        return $this->sendMessage(Yii::t('app', 'Feedback lets you send your suggestions about our products. We welcome problem reports, feature ideas and general comments.'));
    }

    /**
     * Store feedback message
     * @return boolean
     */
    public function saveFeedback()
    {
        $message = $this->update->message;

        $model = new Message([
            'text' => $message->text ?: '',
            'channel_id' => $this->bot->channel_id,
            'member_id' => $this->memberId,
            'attachments' => $this->attachments,
        ]);

        if ($mediaType = $message->getMimeType()) {
            $model->media_type = Message::getMediaTypeId($mediaType);
        }

        return $model->save();
    }

    /**
     * Get message attachments
     * @return boolean|string
     */
    public function getMemberId()
    {
        $chat = Chat::findByChatId($this->update->message->from->id);

        if ($chat) {
            return $chat->member_id;
        }

        return null;
    }

    /**
     * Get message attachments
     * @return string
     */
    public function getAttachments()
    {
        $message = $this->update->message;
        if ($file = $this->getAttachedFile($message)) {
            $attachments[] = [
                'path' => $file->fullPath,
                'url' => $file->url,
                'name' => $file->basename,
                'media_type' => Message::getMediaTypeId($message->getMimeType()),
            ];

            return json_encode($attachments);
        }

        return '';
    }

    /**
     * @inheritDoc
     */
    public function getDescription()
    {
        return Yii::t('app', 'This command take the responsibility of taking and storing Feedback message.');
    }
}
