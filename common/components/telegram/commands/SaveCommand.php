<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\components\telegram\commands;

use Yii;
use yii\helpers\Json;
use common\models\Content;

/**
 * Class SaveCommand
 * @property \airani\telegram\TelegramBot $api
 * @property \common\components\telegram\types\Update $update
 * @property integer $fileId
 *
 * @author Morteza Pouretemadi <e.morteza94@yahoo.com>
 */
class SaveCommand extends SendCommand
{
    protected $name = 'save';
    protected $description = 'A command to save contents as drafts';
    protected $pattern = '/save';
    protected $public = false;

    /**
     * @inheritdoc
     */
    public function execute()
    {
        if ($this->isReplyToMessage() === false) {
            $this->sendMessage(self::EMOJI_ALERT . ' ' . Yii::t('app', 'Please send /save command reply to the message you want save'));
            return false;
        }

        $message = $this->saveContent() ? self::EMOJI_SUCCESS . ' ' . Yii::t('app', 'Content successfully saved as draft') : self::EMOJI_ALERT . ' ' . Yii::t('app', 'Something wrong! please try again!');
        $this->sendMessage($message);

        return true;
    }

    /**
     * @inheritDoc
     */
    public function getDescription()
    {
        return Yii::t('app', 'A command to save contents as drafts');
    }

    /**
     * @inheritdoc
     */
    public function saveContent()
    {
        $message = $this->update->message->reply_to_message;
        $mediaType = Content::MEDIA_TYPE_TEXT;
        $attachments = '';

        if ($this->attachments) {
            $attachments = $this->attachments;
            $mediaType = Json::decode($attachments)[0]['media_type'];
        }

        $content = new Content([
            'text' => parent::getTextParagraphed($message->content),
            'attachments' => $attachments,
            'media_type' => $mediaType,
            'created_by' => $this->bot->userOwnerId,
            'project_id' => $this->bot->channel->project_id,
            'status' => Content::STATUS_PENDING,
            'type' => Content::TYPE_PUBLIC,
            'scenario' => Content::SCENARIO_CREATE,
        ]);
        $content->setChannelsList([]);
        return $content->insert(false);
    }
}
