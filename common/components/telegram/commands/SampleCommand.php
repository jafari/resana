<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\components\telegram\commands;

use Yii;

/**
 * Sample Command for guide purpose
 * @property \common\components\telegram\types\Update $update
 *
 * @author Ali Irani <ali@irani.im>
 */
class SampleCommand extends Command
{
    protected $name = 'sample';
    protected $description = 'It\'s a sample command';
    protected $pattern = '/sample';
    protected $public = true;

    /**
     * @inheritdoc
     */
    public function execute()
    {
        if ($this->isResponse) {
            $this->sendMessage($this->response . ' Is it reply message.');
        } else {
            $this->setResponse(self::EMOJI_SUCCESS);
            $this->sendMessage('Sample command write for tutorial purpose.');
        }
    }

    /**
     * @inheritDoc
     */
    public function getDescription()
    {
        return Yii::t('app', 'It\'s a sample command');
    }
}
