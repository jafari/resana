<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\components\telegram\commands;

use Yii;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use common\models\Content;
use common\models\Channel;
use common\traits\TelegramCommandTrait;

/**
 * Class SendCommand
 * @property \airani\telegram\TelegramBot $api
 * @property \common\components\telegram\types\Update $update
 *
 * @author Morteza Pouretemadi <e.morteza94@yahoo.com>
 */
class SendCommand extends Command
{
    use TelegramCommandTrait;

    protected $name = 'send';
    protected $description = 'A command to send contents to your channels';
    protected $pattern = '/send';
    protected $public = false;

    private $_selectedChannels;

    /**
     * @inheritdoc
     */
    public function execute()
    {
        $commands = explode(' ', $this->update->message->text);
        $activeChannels = $this->bot->channel->project->activeChannels;

        if ($activeChannels === null) {
            $this->sendMessage(self::EMOJI_WARNING . ' ' . Yii::t('app', 'You have no channels.'));
            return false;
        }

        if ($this->isResponse === false && $this->isReplyToMessage() === false) {
            $this->sendMessage(self::EMOJI_WARNING . ' ' . Yii::t('app', 'Please send /send command reply to the message you want send'));
            return false;
        }

        if ($this->isResponse === false && $this->isSetChannelsList($commands) === false) {
            $id = $this->sendContent(Content::STATUS_PENDING);
            if ($id) {
                $message = $this->makeChannelsList($activeChannels);
                $this->setResponse(['id' => $id]);
            } else {
                $message = self::EMOJI_ALERT . ' ' . Yii::t('app', 'There was an error in saving your message. Please try again!');
            }
            $this->sendMessage($message);
            return true;
        }

        if ($this->setSelectedChannels() && $this->validateChannels() === false) {
            $this->sendMessage(self::EMOJI_ALERT . Yii::t('app', 'ChannelsList is invalid'));
            return false;
        }

        if ($this->isResponse) {
            $message = $this->setContentChannels() ? self::EMOJI_SUCCESS . ' ' . Yii::t('app', 'Content successfully saved and added to queue for sending') : self::EMOJI_ALERT . ' ' . Yii::t('app', 'There was a problem to send your message. Try again later');
            $this->sendMessage($message);
            return true;
        }

        $message = $this->sendContent() ? self::EMOJI_SUCCESS . ' ' . Yii::t('app', 'Content successfully saved and added to queue for sending') : self::EMOJI_ALERT . ' ' . Yii::t('app', 'There was a problem to send your message. Try again later');
        $this->sendMessage($message);
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getDescription()
    {
        return Yii::t('app', 'A command to send contents to your channels');
    }

    /**
     * Check if the message is replied or not
     * @return bool
     */
    public function isReplyToMessage()
    {
        return $this->update->message->reply_to_message !== null;
    }

    /**
     * Checks if there is channels list in command or not
     * @param $commands
     * @return bool
     */
    public function isSetChannelsList($commands)
    {
        return array_key_exists(1, $commands);
    }

    /**
     * Get message attachments
     * @return string
     */
    public function getAttachments()
    {
        $message = $this->update->message->reply_to_message;
        if ($file = $this->getAttachedFile($message)) {
            $attachments[] = [
                'path' => $file->fullPath,
                'url' => $file->url,
                'name' => $file->basename,
                'media_type' => Content::getMediaTypeId($message->getMimeType()),
            ];

            return json_encode($attachments);
        }

        return '';
    }

    /**
     * Gives the list of channels
     * @param Channel[] $channels
     * @return string $message
     */
    public function makeChannelsList($channels)
    {
        $message = Yii::t('app', "Select one of the below channels:") . "\n\n";
        $message .= "1. " . Yii::t('app', 'All Channels') . "\n";
        for ($i = 2; $i <= count($channels) + 1; $i++) {
            $message .= $i . '. ' . $channels[$i - 2]->name . "  ({$channels[$i - 2]->typeName})" . "\n";
        }
        return $message;
    }

    /**
     * Checks if the selected channels are valid or not.
     * @param $toBeSentChannels array
     * @return bool
     */
    public function validateChannels()
    {
        $activeChannels = $this->bot->channel->project->activeChannels;
        $arrayActiveChannels = ArrayHelper::getColumn(ArrayHelper::toArray($activeChannels), 'id');
        for ($i = 2; $i <= count($activeChannels) + 1; $i++) {
            $numberedChannels[$i] = $arrayActiveChannels[$i - 2];
        }

        for ($i = 0; $i <= count($this->_selectedChannels) - 1; $i++) {
            if ($this->_selectedChannels[$i] === 1) {
                $this->_selectedChannels = $arrayActiveChannels;
                return true;
            }
            if (isset($numberedChannels[$this->_selectedChannels[$i]]) === false) {
                return false;
            }
            $this->_selectedChannels[$i] = $numberedChannels[$this->_selectedChannels[$i]];
        }
        return true;
    }

    /**
     * Sets private property selected channels
     * @return bool
     */
    public function setSelectedChannels()
    {
        $toBeSentChannels = explode(' ', $this->update->message->text);
        $start = 0;
        if ($this->isResponse && $toBeSentChannels[0] !== $this->pattern) {
            $start = 0;
        } elseif ($this->isResponse && $toBeSentChannels[0] === $this->pattern && $this->isSetChannelsList($toBeSentChannels)) {
            $start = 1;
        } elseif ($this->isResponse === false && $this->isSetChannelsList($toBeSentChannels)) {
            $start = 1;
        }
        if ($toBeSentChannels[$start] === '1') {
            $this->_selectedChannels[0] = 1;
        } else {
            for ($i = $start; $i <= count($toBeSentChannels) - 1; $i++) {
                $this->_selectedChannels[] = $toBeSentChannels[$i];
            }
        }
        return true;
    }

    /**
     * Returns selected Channels
     * @return mixed
     */
    public function getSelectedChannels()
    {
        return $this->_selectedChannels;
    }

    /**
     * Send a content by bot owner as In Queue
     * @param int $status
     * @return bool|string
     * @throws \Exception
     */
    public function sendContent($status = Content::STATUS_IN_QUEUE)
    {
        $message = $this->update->message->reply_to_message;
        $mediaType = Content::MEDIA_TYPE_TEXT;
        $attachments = '';

        if ($this->attachments) {
            $attachments = $this->attachments;
            $mediaType = Json::decode($attachments)[0]['media_type'];
        }

        $content = new Content([
            'text' => self::getTextParagraphed($message->content),
            'attachments' => $attachments,
            'media_type' => $mediaType,
            'created_by' => $this->bot->userOwnerId,
            'project_id' => $this->bot->channel->project_id,
            'status' => $status,
            'type' => Content::TYPE_PUBLIC,
            'scenario' => Content::SCENARIO_CREATE,
        ]);
        $status === Content::STATUS_IN_QUEUE ? $content->setChannelsList($this->selectedChannels) : $content->setChannelsList([]);

        $id = $content->insert(false) ? implode(',', array_values($content->getPrimaryKey(true))) : false;
        return $id;
    }

    /**
     * Sets the content channels and updates it's status and scenario
     * @return bool
     */
    public function setContentChannels()
    {
        $content = Content::findOne(['id' => $this->response['id']]);
        $content->scenario = Content::SCENARIO_UPDATE;
        $content->status = Content::STATUS_IN_QUEUE;
        $content->setChannelsList($this->selectedChannels);
        return $content->save(false);
    }

    /**
     * Add paragraph html tags instead of new line (\n)
     * @param $text
     * @return mixed
     */
    public static function getTextParagraphed($text)
    {
        preg_match_all("/([^\n]*)\n?/", $text, $matches);
        if (ArrayHelper::getValue($matches, 1)) {
            array_walk($matches[1], function (&$item) {
                $item = "<p>$item</p>";
            });
            return $text = implode('', $matches[1]);
        }
        return $text;
    }
}
