<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\components\telegram\commands;

use Yii;
use yii\base\Component;
use airani\telegram\TelegramBot;

/**
 * Telegram Command Core
 *
 * @property TelegramBot $api
 * @property mixed $response
 *
 * @author Ali Irani <ali@irani.im>
 */
abstract class Command extends Component
{
    const EMOJI_ALERT = '⛔️';
    const EMOJI_IRAN_FLAG = '🇮🇷';
    const EMOJI_WARNING = '⚠️';
    const EMOJI_SUCCESS = '✅';
    const EMOJI_BULLET = '🔹';

    const CACHE_DURATION_RESPONSE = 300; // 5 Minutes

    /*
     * Command name
     * @var string
     */
    protected $name;

    /*
     * Command descriptions
     * @var string
     */
    protected $description;

    /*
     * Run command after user sent exactly this string
     * @var string
     */
    protected $pattern;

    /*
     * Whether command accessibility for public users or just owners, default is true
     * @var bool
     */
    protected $public = true;

    /*
     * Whether command use as a menu item or just a command
     * @var bool
     */
    protected $menuItem = false;

    /**
     * Whether message is to replay the message
     * @var bool
     */
    protected $isResponse = false;

    /**
     * Update object
     * @var \common\components\telegram\types\Update
     */
    public $update;

    /**
     * Bot model
     * @var \common\models\Bot
     */
    public $bot;

    /**
     * Run for execute command
     */
    abstract public function execute();

    /**
     * Runner method
     */
    public function run()
    {
        if ($this->beforeExecute()) {
            $this->execute();
            $this->afterExecute();
        }
    }

    /**
     * Run before execute()
     * @return bool
     */
    public function beforeExecute()
    {
        if ($this->isPublic() === false && $this->isUserOwner() === false) {
            return false;
        }

        if ($this->response !== null) {
            $this->isResponse = true;
        }
        return true;
    }

    /**
     * Run before execute()
     */
    public function afterExecute()
    {
        if ($this->isResponse) {
            $this->endResponse();
        }
    }

    /*
     * Whether command accessibility for public users or just owners
     * @var bool
     */
    public function isPublic()
    {
        return $this->public;
    }

    /*
     * Whether command use as a menu item or just a command
     * @var bool
     */
    public function isMenuItem()
    {
        return $this->menuItem;
    }

    /**
     * Whether chat is user owner
     * @return bool
     */
    public function isUserOwner()
    {
        if ($this->bot === null) {
            return false;
        }
        $username = strtolower($this->update->message->chat->username);
        $owners = $this->bot->owners;
        array_walk($owners, function (&$value) {
            $value = strtolower($value);
        });
        return in_array($username, $owners, true);
    }

    /**
     * Returns command description
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Returns command name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Returns command pattern string
     * @return string
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * Returns telegram bot api object
     * @return TelegramBot
     */
    public function getApi()
    {
        return new TelegramBot(['authKey' => $this->bot->token]);
    }

    /**
     * Returns response value
     * @return mixed | null
     */
    public function getResponse()
    {
        $reply = Yii::$app->cache->get('reply:' . $this->update->message->chat->id);
        return isset($reply['value']) ? $reply['value'] : null;
    }

    /**
     * Set response cache for when need to reply command
     * @param $value
     * @return bool
     */
    public function setResponse($value = true)
    {
        $reply = [
            'command' => $this->pattern,
            'value' => $value,
        ];
        return Yii::$app->cache->set('reply:'.$this->update->message->chat->id, $reply, self::CACHE_DURATION_RESPONSE);
    }

    /**
     * Delete reply cache
     * @return bool
     */
    public function endResponse()
    {
        return Yii::$app->cache->delete('reply:'.$this->update->message->chat->id);
    }

    /**
     * Send message to the target chat
     * @param string $text
     * @param int|null $replyMessageId
     * @return mixed
     */
    public function sendMessage($text, $replyMessageId = null)
    {
        return $this->api->sendMessage($this->update->message->chat->id, $text, $replyMessageId, null, 'HTML');
    }
}
