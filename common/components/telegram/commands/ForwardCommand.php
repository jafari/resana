<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\components\telegram\commands;

use Yii;
use common\models\Content;

/**
 * Forward Command
 * Forward a content to the all bot subscribers
 * @property \common\components\telegram\types\Update $update
 *
 * @author Ali Irani <ali@irani.im>
 */
class ForwardCommand extends Command
{
    protected $name = 'forward';
    protected $description = 'Forward a content to the all bot subscribers';
    protected $pattern = '/forward';
    protected $public = false;

    /**
     * @inheritdoc
     */
    public function execute()
    {
        if ($this->update->message->reply_to_message === null) {
            $message = self::EMOJI_WARNING . ' ' . Yii::t('app', 'Please send /forward command reply to the message you want forward');
        } else {
            $message = $this->forwardContent() ? self::EMOJI_SUCCESS . ' ' .Yii::t('app', 'Content successfully saved and add to sending queue') : self::EMOJI_ALERT . ' ' . Yii::t('app', 'Something wrong! please try again!');
        }
        $this->sendMessage($message);
    }

    /**
     * @inheritDoc
     */
    public function getDescription()
    {
        return Yii::t('app', 'Forward a content to the all bot subscribers');
    }

    /**
     * Save a content by bot owner as forward media type
     * @return bool
     */
    public function forwardContent()
    {
        if ($this->update->message->reply_to_message === null) {
            false;
        }
        $message = $this->update->message->reply_to_message;
        $content = new Content([
            'text' => $message->content,
            'f_chat_id' => $message->chat->id,
            'f_message_id' => $message->message_id,
            'channelsList' => [$this->bot->channel_id],
            'created_by' => $this->bot->userOwnerId,
            'project_id' => $this->bot->channel->project_id,
            'media_type' => Content::MEDIA_TYPE_FORWARD,
            'status' => Content::STATUS_IN_QUEUE,
            'type' => Content::TYPE_PUBLIC,
            'published_at' => time(),
            'scenario' => Content::SCENARIO_CREATE,
        ]);
        return $content->insert(false);
    }
}
