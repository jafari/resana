<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\components\telegram\commands;

use Yii;
use yii\helpers\Html;
use common\traits\TelegramCommandTrait;

/**
 * This Class CaptionCommand add a caption to the image or video or document
 * @property \airani\telegram\TelegramBot $api
 * @property \common\components\telegram\types\Update $update
 * @property string $repliedMessageType
 * @property string $repliedMessageFileId
 * @property string $longCaptionLink
 * @property string $fileUrl
 * @property string $fileId
 *
 * @author Reza Ebrahimi <reza_2ebt@yahoo.com>
 */
class CaptionCommand extends Command
{
    use TelegramCommandTrait;

    const MESSAGE_TYPE_PHOTO = 'photo';
    const MESSAGE_TYPE_VIDEO = 'video';
    const MESSAGE_TYPE_DOCUMENT = 'document';
    const MESSAGE_TYPE_TEXT = 'text';

    const MAX_CAPTION_LENGTH = 200;

    protected $name = 'caption';
    protected $description = 'It\'s a caption command';
    protected $pattern = '/caption';
    protected $public = false;
    protected $fileId;

    /**
     * @inheritdoc
     */
    public function execute()
    {
        if ($this->isResponse) {
            $this->fileId = $this->response['file_id'];
            $caption = $this->update->message->text;
            if ($this->isLongCaption($caption)) {
                $text = $this->longCaptionLink . $caption;
                $this->sendMessage($text);
            } else {
                $type = $this->response['type'];
                $this->sendWithCaption($type, $caption);
            }
        } else {
            if ($this->isRepliedMessage()) {
                if ($this->isRepliedPhoto() || $this->isRepliedVideo() || $this->isRepliedDocument()) {
                    $this->setResponse([
                        'file_id' => $this->repliedMessageFileId,
                        'type' => $this->repliedMessageType,
                    ]);
                    $this->sendMessage(self::EMOJI_SUCCESS . ' ' . Yii::t('app', 'now write caption'));
                } else {
                    $this->sendMessage(self::EMOJI_ALERT . ' ' . Yii::t('app', 'caption just for video/image or document'));
                }
            } else {
                $this->sendMessage(self::EMOJI_WARNING . ' ' . Yii::t('app', 'for caption you must be reply a image/video or document message'));
            }
        }
    }

    /**
     * This method checking a message is reply (true) or not (false)
     * @return bool
     */
    public function isRepliedMessage()
    {
        return $this->update->message->reply_to_message !== null;
    }

    /**
     * This method return message mimeType
     * @return string
     */
    public function getRepliedMessageType()
    {
        $replyMessage = $this->update->message->reply_to_message;
        if ($replyMessage->isPhoto()) {
            return self::MESSAGE_TYPE_PHOTO;
        } elseif ($replyMessage->isVideo()) {
            return self::MESSAGE_TYPE_VIDEO;
        } elseif ($replyMessage->isDocument()) {
            return self::MESSAGE_TYPE_DOCUMENT;
        }
        return self::MESSAGE_TYPE_TEXT;
    }

    /**
     * This method checking that replied message is a photo (true) or not (false)
     * @return bool
     */
    public function isRepliedPhoto()
    {
        return $this->repliedMessageType === self::MESSAGE_TYPE_PHOTO;
    }

    /**
     * This method checking that replied message is a video (true) or not (false)
     * @return bool
     */
    public function isRepliedVideo()
    {
        return $this->repliedMessageType === self::MESSAGE_TYPE_VIDEO;
    }

    /**
     * This method checking that replied message is a document (true) or not (false)
     * @return bool
     */
    public function isRepliedDocument()
    {
        return $this->repliedMessageType === self::MESSAGE_TYPE_DOCUMENT;
    }

    /**
     * This method check text length , maximum text length for caption is 200 character
     *
     * @param string $text
     *
     * @return bool
     */
    public function isLongCaption($text)
    {
        return mb_strlen($text) > self::MAX_CAPTION_LENGTH;
    }

    /**
     * This method return replied message file_id
     * @return string
     */
    public function getRepliedMessageFileId()
    {
        $repliedFile = $this->update->message->reply_to_message;
        if ($this->isRepliedPhoto()) {
            $fileId = end($repliedFile->photo)->file_id;
        } elseif ($this->isRepliedVideo()) {
            $fileId = $repliedFile->video->file_id;
        } elseif ($this->isRepliedDocument()) {
            $fileId = $repliedFile->document->file_id;
        } else {
            $fileId = $repliedFile->text;
        }
        return $fileId;
    }

    /**
     * This method set a caption on image/video message
     *
     * @param string $type
     * @param string $caption
     */
    public function sendWithCaption($type, $caption)
    {
        $fileId = $this->fileId;
        $chatId = $this->update->message->chat->id;
        if ($type === self::MESSAGE_TYPE_PHOTO) {
            $this->api->sendPhoto($chatId, $fileId, $caption);
        } elseif ($type === self::MESSAGE_TYPE_VIDEO) {
            $this->api->sendVideo($chatId, $fileId, $caption);
        } elseif ($type === self::MESSAGE_TYPE_DOCUMENT) {
            $this->api->sendDocument($chatId, $fileId, $caption);
        }
    }

    /**
     * Return hidden link for log caption
     * @return bool|string
     */
    public function getLongCaptionLink()
    {
        $fileUrl = $this->fileUrl;
        return $fileUrl === null ? false : Html::a(' ', $fileUrl);
    }

    /**
     * This method return fileUrl from saved file in Media service.
     * @return string
     */
    public function getFileUrl()
    {
        $response = $this->api->getFile($this->fileId);

        if ($response->ok === false) {
            Yii::error(['message' => 'Telegram getFile() Error', 'response' => $response], __METHOD__);
            return null;
        }

        $telegramFile = $response->result;
        $file = $this->saveTelegramFile($telegramFile);

        return $file->url;
    }

    /**
     * @inheritDoc
     */
    public function getDescription()
    {
        return Yii::t('app', 'It\'s a caption command for add caption under image/video or document');
    }
}
