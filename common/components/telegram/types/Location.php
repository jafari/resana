<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\components\telegram\types;

/**
 * Location
 *
 * @property float $longitude
 * @property float $latitude
 *
 * @author Ali Irani <ali@irani.im>
 */
class Location extends BaseType
{
    public $longitude;
    public $latitude;
}
