<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\components\localStorage;

use Yii;
use yii\base\Object;
use yii\helpers\ArrayHelper;

/**
 * Represent a file with its properties as a class.
 *
 * @author Mohammad Davaee <mdavaee@gmail.com>
 */
class File extends Object
{
    /**
     * The full path string of file in local disk.
     * @var
     */
    public $path;

    /**
     * The full path string of file in local disk.
     * @var
     */
    public $fullPath;

    /**
     * A string representing the type of the entry.
     * @var
     */
    public $type;

    /**
     * The last modified time for the file, in a Unix timestamp.
     * @var
     */
    public $timestamp;

    /**
     * The file size in bytes.
     * @var
     */
    public $size;

    /**
     * The base name without path information.
     * @var
     */
    public $basename;

    /**
     * A string containing the file extension, or an empty string if the file has no extension.
     * @var
     */
    public $extension;

    /**
     * Get the mime type of a file.
     * @var
     */
    public $mimetype;

    /**
     * The url of file in Media service.
     * @var
     */
    public $url;

    /**
     * Create a new file instance from the file path. The default implementation works with the ABSOLUTE file path.
     * If the RELATIVE file path provide, corresponding Local Filesystem should be passed.
     * Note that consider dynamic sub file path structure when working with the relative file path.
     *
     * ```php
     * // Usage with the relative file path.
     * $bucket = new Bucket();
     * $file = File::make('relative/file/path/name.ext', $bucket);
     *
     * // Usage with the absolute file path.
     * $file = File::make('/home/user/absolute/file/path/name.ext');
     * ```
     *
     * @param string $fileFullPath the full path of file.
     * @param null $filesystem
     *
     * @return File $this
     */
    public static function make($fileFullPath, $filesystem = null)
    {
        if (is_a($fileFullPath, self::className())) {
            return $fileFullPath;
        }

        // Create a Local Filesystem instance if in $filesystem absence.
        if ($filesystem === null) {
            // Setting the Local Filesystem Adapter root path to `/` makes working with the full path of file possible.
            $adapter = new LocalFilesystemAdapter('/');
            $filesystem = new LocalFilesystem($adapter);
        }

        $self = new self;

        $self->setAttributes($filesystem->getMetadata($fileFullPath));
        
        return $self;
    }

    /**
     * Configures this class with the property values.
     *
     * @param array $properties the property values given in terms of name-value pairs.
     */
    protected function setAttributes(array $properties)
    {
        foreach ($properties as $name => $value) {
            $this->$name = $value;
        }
    }

    /**
     * Upload file to Media service.
     *
     * @return File|null
     */
    public function uploadToNMedia()
    {
        $upload = Yii::createObject([
            'class' => '\stdClass',
            'name' => $this->basename,
            'tempName' => $this->fullPath,
            'type' => $this->mimetype,
        ]);

        $media = Yii::$app->media->save($upload);

        if (ArrayHelper::getValue($media, 'success')) {
            $url = ArrayHelper::getValue($media, 'data.media.0.uriMin');
            $this->setAttributes(compact('url'));
            return $this;
        } elseif (ArrayHelper::getValue($media, 'message')) {
            Yii::error($media['message'], __METHOD__);
        }

        return null;
    }
}
