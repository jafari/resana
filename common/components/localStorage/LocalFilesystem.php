<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\components\localStorage;

use Yii;
use League\Flysystem\Filesystem;

/**
 * Constructor of the Local Filesystem.
 *
 * @author Mohammad Davaee <mdavaee@gmail.com>
 */
class LocalFilesystem extends Filesystem
{
}
