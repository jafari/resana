<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\components\localStorage;

use Yii;
use yii\helpers\ArrayHelper;
use yii\base\InvalidParamException;
use yii\base\InvalidConfigException;

/**
 * This class is exclusively to working with Telegram File Object on local storage based on [[UploadBucket]].
 *
 * @property string $fileId Unique identifier for Telegram file.
 * @property string $filePath The Telegram file path.
 *
 * @author Mohammad Davaee <mdavaee@gmail.com>
 */
class TelegramBucket extends UploadBucket
{
    /**
     * The object represents a Telegram file ready to be downloaded.
     * @var
     */
    public $telegramFileObject;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->telegramFileObject === null) {
            throw new InvalidConfigException('The `telegramFileObject` property must be set.');
        }

        parent::init();
    }

    /**
     * Get unique identifier for Telegram file.
     * @return string
     * @throws InvalidParamException
     */
    public function getFileId()
    {
        if ($fileId = ArrayHelper::getValue($this->telegramFileObject, 'file_id')) {
            return $fileId;
        }

        throw new InvalidParamException('The Telegram file object should have a `file_id` property.');
    }

    /**
     * Get the Telegram file path.
     * @return string
     * @throws InvalidParamException
     */
    public function getFilePath()
    {
        if ($filePath = ArrayHelper::getValue($this->telegramFileObject, 'file_path')) {
            return $filePath;
        }

        throw new InvalidParamException('The Telegram file object should have a `file_path` property.');
    }

    /**
     * Store Telegram file content to a new file.
     *
     * @param mixed $fileContent The content of file which can be achieve from a HTTP request or file_get_contents().
     *
     * @return File|false
     */
    public function save($fileContent)
    {
        $fileName = $this->makeFileName();

        if ($this->put($fileName, $fileContent)) {
            return File::make($fileName, $this);
        }

        return false;
    }

    /**
     * Make custom file basename for storing the Telegram file content to the disk.
     * @return string
     */
    protected function makeFileName()
    {
        return $this->fileId . '.' . pathinfo($this->filePath, PATHINFO_EXTENSION);
    }
}
