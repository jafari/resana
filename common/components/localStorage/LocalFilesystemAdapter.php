<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\components\localStorage;

use Yii;
use SplFileInfo;
use League\Flysystem\Adapter\Local;

/*
 * An adapter can be seen as a plug - it bridges the gap between initially incompatible API’s.
 * The job of the adapter is to translate requests into calls the file system understands and
 * re-format responses to comply with the interface of the generic file system.
 *
 * This class is a custom Local Filesystem Adapter instead of [[League\Flysystem\Adapter\Local]]
 * which comes with the main Flysystem package.
 *
 * @author Mohammad Davaee <mdavaee@gmail.com>
 */

class LocalFilesystemAdapter extends Local
{
    /**
     * This method determine the file info params on File Meta Data request.
     * Any custom file info param can be added.
     * Here, for instant, we customize `path` param and add new param called `extension`.
     *
     * @param SplFileInfo $file
     *
     * @return array
     */
    protected function mapFileInfo(SplFileInfo $file)
    {
        $normalized = parent::mapFileInfo($file);

        $filePath = $this->getFilePath($file);
        $fileMinetype = $this->getMimetype($filePath);

        $normalized['fullPath'] = $this->applyPathPrefix($filePath);

        if ($normalized['type'] === 'file') {
            $normalized['basename'] = $file->getBasename();
            $normalized['extension'] = $file->getExtension();
            $normalized['mimetype'] = reset($fileMinetype);
        }

        return $normalized;
    }
}
