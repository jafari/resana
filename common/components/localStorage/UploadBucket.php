<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\components\localStorage;

use Yii;
use yii\base\Component;

/**
 * This class helps you to work with files and folders on local storage in upload directory.
 * All the [[LocalFilesystem]] based on [[LocalFilesystemAdapter]] methods are available here.
 *
 * Write any custom UploadBucket's methods here like the following:
 *
 * ```php
 * use localStorage/File;
 *
 * public function write($path, $contents, array $config = [])
 * {
 *     if ($result = $this->filesystem->write($path, $contents, $config)) {
 *         return File::make($path, $this);
 *     }
 *
 *     return $result;
 * }
 * ```
 *
 * @method \League\Flysystem\FilesystemInterface addPlugin(\League\Flysystem\PluginInterface $plugin)
 * @method void assertAbsent(string $path)
 * @method void assertPresent(string $path)
 * @method boolean copy(string $path, string $newpath)
 * @method boolean createDir(string $dirname, array $config = null)
 * @method boolean delete(string $path)
 * @method boolean deleteDir(string $dirname)
 * @method \League\Flysystem\Handler get(string $path, \League\Flysystem\Handler $handler = null)
 * @method \League\Flysystem\AdapterInterface getAdapter()
 * @method \League\Flysystem\Config getConfig()
 * @method array|false getMetadata(string $path)
 * @method string|false getMimetype(string $path)
 * @method integer|false getSize(string $path)
 * @method integer|false getTimestamp(string $path)
 * @method string|false getVisibility(string $path)
 * @method array getWithMetadata(string $path, array $metadata)
 * @method boolean has(string $path)
 * @method array listContents(string $directory = '', boolean $recursive = false)
 * @method array listFiles(string $path = '', boolean $recursive = false)
 * @method array listPaths(string $path = '', boolean $recursive = false)
 * @method array listWith(array $keys = [], $directory = '', $recursive = false)
 * @method boolean put(string $path, string $contents, array $config = [])
 * @method boolean putStream(string $path, resource $resource, array $config = [])
 * @method string|false read(string $path)
 * @method string|false readAndDelete(string $path)
 * @method resource|false readStream(string $path)
 * @method boolean rename(string $path, string $newpath)
 * @method boolean setVisibility(string $path, string $visibility)
 * @method boolean update(string $path, string $contents, array $config = [])
 * @method boolean updateStream(string $path, resource $resource, array $config = [])
 * @method boolean write(string $path, string $contents, array $config = [])
 * @method boolean writeStream(string $path, resource $resource, array $config = [])
 *
 * @author Mohammad Davaee <mdavaee@gmail.com>
 */
class UploadBucket extends Component
{
    /**
     * The Storage class.
     * @var mixed
     */
    public $filesystem;

    /**
     * Upload directory root path.
     * @var string
     */
    public $rootPath = '@frontend/uploads';

    /**
     * @inheritdoc
     */
    public function init()
    {
        $root = Yii::getAlias($this->rootPath . '/' . $this->subPath);
        $adapter = new LocalFilesystemAdapter($root);
        $this->filesystem = new LocalFilesystem($adapter);

        parent::init();
    }

    /**
     * Dynamic sub folder structure.
     * @return string
     */
    protected function getSubPath()
    {
        return date('Y/m');
    }

    /**
     * Make possible to call the Storage methods directly.
     *
     * @param string $method
     * @param array $parameters
     *
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        return call_user_func_array([$this->filesystem, $method], $parameters);
    }
}
