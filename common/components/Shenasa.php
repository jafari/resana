<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2014 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\components;

use Yii;
use yii\authclient\OAuth2;

/**
 * Shenasa allows authentication via Shenasa Service.
 *
 * Example application configuration:
 *
 * ~~~
 * 'components' => [
 *     'authClientCollection' => [
 *         'class' => 'yii\authclient\Collection',
 *         'clients' => [
 *             'shenasa' => [
 *                 'class' => 'common\components\Shenasa',
 *                 'clientId' => 'shenasa_client_id',
 *                 'clientSecret' => 'shenasa_client_secret',
 *             ],
 *         ],
 *     ]
 *     ...
 * ]
 * ~~~
 *
 * @see https://shenasaservice.com
 *
 * @author Sam <sam@noghteh.ir>
 * @since 2.0
 */
class Shenasa extends OAuth2
{
    /**
     * @inheritdoc
     */
    public $authUrl = 'https://accounts.shenasaservice.com/oauth2/auth';
    /**
     * @inheritdoc
     */
    public $tokenUrl = 'https://accounts.shenasaservice.com/oauth2/token';
    /**
     * @inheritdoc
     */
    public $apiBaseUrl = 'https://api.shenasaservice.com/oauth2';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if ($this->scope === null) {
            $this->scope = implode(' ', [
                'profile',
                'email',
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    protected function initUserAttributes()
    {
        return $this->api('userinfo', 'GET');
    }

    /**
     * @inheritdoc
     */
    protected function defaultName()
    {
        return 'shenasa';
    }

    /**
     * @inheritdoc
     */
    protected function defaultTitle()
    {
        return Yii::t('app', 'Shenasa');
    }

    /**
     * @inheritdoc
     */
    protected function defaultViewOptions()
    {
        return [
            'popupWidth' => 1024,
            'popupHeight' => 720,
        ];
    }
}
