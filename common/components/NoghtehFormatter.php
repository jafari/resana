<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2014 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\components;

use DateTime;
use DateTimeZone;
use IntlDateFormatter;
use yii\i18n\Formatter;
use yii\helpers\FormatConverter;
use yii\base\InvalidConfigException;

/**
 * @todo: deprecate this release 2.1 version
 * @author Sam <sam@noghteh.ir>
 * @since 2.0
 */
class NoghtehFormatter extends Formatter
{
    /**
     * @inheritdoc
     */
    //public $dateFormat = 'medium';
    /**
     * @inheritdoc
     */
    //public $datetimeFormat = 'medium';
    /**
     * @var string|object the calendar that is used to localize the date.
     * This can be "persian", "islamic", etc.
    */
    public $calendar;

    /**
     * @inheritdoc
     * 
     * @param string|object $calendar the calendar that is used to localize the date.
     */
    public function asDate($value, $format = null, $locale = null, $calendar = null)
    {
        if ($format === null) {
            $format = $this->dateFormat;
        }
        if ($calendar === null) {
            $calendar = $this->calendar;
        }
        return $this->formatDateTimeValue($value, $format, 'date', $locale, $calendar);
    }

    /**
     * @inheritdoc
     * 
     * @param string|object $calendar the calendar that is used to localize the date.
     */
    public function asDatetime($value, $format = null, $locale = null, $calendar = null)
    {
        if ($format === null) {
            $format = $this->datetimeFormat;
        }
        if ($calendar === null) {
            $calendar = $this->calendar;
        }
        return $this->formatDateTimeValue($value, $format, 'datetime', $locale, $calendar);
    }

    /**
     * Show today time just with "Today" word and time else full datetime
     * 
     * @author Ali Irani <ali@irani.im>
     * @param string|object $calendar the calendar that is used to localize the date.
     */
    public function asTodaytime($value, $format = null, $locale = null, $calendar = null)
    {
        $date_diff = time() - $value;
        $daily_time = 24*60*60;
        
        if ($date_diff < $daily_time && date('d', $value) === date('d')) {
            return \Yii::t('app', 'Today') . ' ' . $this->asDatetime($value, 'php:H:i', $locale, $calendar);
        }

        return $this->asDatetime($value, $format, $locale, $calendar);
    }

    /**
     * @var array map of short format names to IntlDateFormatter constant values.
     */
    private $_dateFormats = [
        'short'  => 3, // IntlDateFormatter::SHORT,
        'medium' => 2, // IntlDateFormatter::MEDIUM,
        'long'   => 1, // IntlDateFormatter::LONG,
        'full'   => 0, // IntlDateFormatter::FULL,
    ];

    /**
     * @inheritdoc
     * 
     * @param string|object $calendar the calendar that is used to localize the date.
     */
    private function formatDateTimeValue($value, $format, $type, $locale = null, $calendar = null)
    {
        if ($locale === null) {
            $locale = $this->locale;
        }
        if (is_string($calendar)) {
            $locale .= '@calendar=' . $calendar;
            $calendar = IntlDateFormatter::TRADITIONAL;
        } elseif ($calendar === null) {
            $calendar = $this->calendar;
        } else {
            $calendar = null;
        }

        $timeZone = $this->timeZone;
        // avoid time zone conversion for date-only values
        if ($type === 'date') {
            list($timestamp, $hasTimeInfo) = $this->normalizeDatetimeValue($value, true);
            if (!$hasTimeInfo) {
                $timeZone = $this->defaultTimeZone;
            }
        } else {
            $timestamp = $this->normalizeDatetimeValue($value);
        }
        if ($timestamp === null) {
            return $this->nullDisplay;
        }

        if (extension_loaded('intl')) {
            if (strncmp($format, 'php:', 4) === 0) {
                $format = FormatConverter::convertDatePhpToIcu(substr($format, 4));
            }
            if (isset($this->_dateFormats[$format])) {
                if ($type === 'date') {
                    $formatter = new IntlDateFormatter($locale, $this->_dateFormats[$format], IntlDateFormatter::NONE, $timeZone, $calendar);
                } elseif ($type === 'time') {
                    $formatter = new IntlDateFormatter($locale, IntlDateFormatter::NONE, $this->_dateFormats[$format], $timeZone);
                } else {
                    $formatter = new IntlDateFormatter($locale, $this->_dateFormats[$format], $this->_dateFormats[$format], $timeZone, $calendar);
                }
            } else {
                $formatter = new IntlDateFormatter($locale, IntlDateFormatter::NONE, IntlDateFormatter::NONE, $timeZone, $calendar, $format);
            }
            if ($formatter === null) {
                throw new InvalidConfigException(intl_get_error_message());
            }
            // make IntlDateFormatter work with DateTimeImmutable
            if ($timestamp instanceof \DateTimeImmutable) {
                $timestamp = new DateTime($timestamp->format(DateTime::ISO8601), $timestamp->getTimezone());
            }
            return $formatter->format($timestamp);
        } else {
            if (strncmp($format, 'php:', 4) === 0) {
                $format = substr($format, 4);
            } else {
                $format = FormatConverter::convertDateIcuToPhp($format, $type, $locale);
            }
            if ($timeZone != null) {
                if ($timestamp instanceof \DateTimeImmutable) {
                    $timestamp = $timestamp->setTimezone(new DateTimeZone($timeZone));
                } else {
                    $timestamp->setTimezone(new DateTimeZone($timeZone));
                }
            }
            return $timestamp->format($format);
        }
    }
}
