<?php
/**
 * @link http://www.noghteh.ir/
 * @copyright Copyright (c) 2015 Noghteh
 * @license http://www.noghteh.ir/license/
 */

namespace common\components;

use PhpAmqpLib\Wire\AMQPTable;
use yii\base\Component;
use yii\base\InvalidParamException;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Connection\AMQPStreamConnection;

/**
 * AMQP Component
 *
 * @todo Need to be better and more objective
 * @property AMQPStreamConnection $connection
 * @author Ali Irani <ali@irani.im>
 */
class AMQP extends Component
{
    public $host = 'localhost';
    public $port = 5672;
    public $user;
    public $pass;
    public $vhost = '/';
    
    private $_connection;

    /**
     * @inheritDoc
     */
    public function init()
    {
        foreach (['user', 'pass'] as $property) {
            if ($this->$property === null) {
                throw new InvalidParamException(self::className() . "::$property must be set for this class");
            }
        }
    }


    /**
     * Returns AMQP Connection
     * @return AMQPStreamConnection
     */
    public function getConnection()
    {
        if ($this->_connection === null) {
            return new AMQPStreamConnection($this->host, $this->port, $this->user, $this->pass, $this->vhost);
        }
        return $this->_connection;
    }

    /**
     * Returns channel
     * @param null $channelId
     * @return \PhpAmqpLib\Channel\AMQPChannel
     */
    public function getChannel($channelId = null)
    {
        return $this->getConnection()->channel($channelId);
    }

    /**
     * Close channel and connection
     */
    public function close()
    {
        $this->getChannel()->close();
        $this->getConnection()->close();
    }

    /**
     * Publish message to the queue
     * @param AMQPMessage $message
     * @param string $queue
     * @param string $exchange
     * @param string $exchangeType
     */
    public function publishMessage($message, $queue, $exchange = 'router', $exchangeType = 'direct', $queueArgs = null, $exchangeArgs = null)
    {
        $channel = $this->getChannel();

        $channel->queue_declare($queue, false, true, false, false, false, $queueArgs);

        $channel->exchange_declare($exchange, $exchangeType, false, true, false, false, false, $exchangeArgs);

        $channel->queue_bind($queue, $exchange);

        $channel->basic_publish($message, $exchange);

        $this->close();
    }

    /**
     * Send messages and delayed messages to the contents queue
     * @todo this method wrote just for working and need to be optimize
     * @param $text
     * @param int $delayTime
     */
    public function sendMessage($text, $delayTime = 0)
    {
        $message = new AMQPMessage($text, [
            'content_type' => 'text/plain',
            'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
        ]);

        $queue = 'contents';
        $exchange = 'router';
        $exchangeType = 'direct';
        $queueArgs = null;
        $exchangeArgs = null;

        if ($delayTime > 0) {
            $exchange = 'delayed_router';
            $exchangeType = 'x-delayed-message';
            $exchangeArgs = new AMQPTable(['x-delayed-type' => 'fanout']);
            $queue = 'delayed_contents';
            $queueArgs = new AMQPTable(['x-dead-letter-exchange' => 'delayed']);
            $message->set('application_headers', new AMQPTable(["x-delay" => $delayTime]));
        }

        $this->publishMessage($message, $queue, $exchange, $exchangeType, $queueArgs, $exchangeArgs);
    }
}
